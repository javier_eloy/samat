<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//die($_REQUEST['status'].':'.$_REQUEST['tipo']);

$tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo']:'-1'; 
$status = isset($_REQUEST['status'])? $_REQUEST['status']:'-1';
$fecha_desde =  isset($_REQUEST['fecha_desde'])?$_REQUEST['fecha_desde']: '';
$fecha_hasta =  isset($_REQUEST['fecha_hasta'])?$_REQUEST['fecha_hasta']: '';

function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $tipo;
  var $wcolumnas = array();
  var $fecha_caja;
  var $fecha_hasta;
  
  //Cabecera de página
	function Header()
	{
			parent::Header();
			$this->SetXY(160, 7);
                        $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');			
			$this->Ln(18);
			$this->SetFont('Courier','b',12);
			$this->Cell(180, 5, "RELACION DE ARQUEO DE CAJA",0,1,'C');
            $this->Cell(180, 5, "EN EL PERIODO: ".$this->fecha_caja." al ".$this->fecha_hasta,0,1,'C');
			$this->SetFont('Courier','B',10);			
            $this->Cell(10,5,'Tipo',1,0,'C');
            $this->Cell(20,5,'Estatus',1,0,'C');
            $this->Cell(100,5,'Descripci�n',1,0,'C');
            $this->Cell(30,5,'Debe',1,0,'C');
            $this->Cell(30,5,'Haber',1,1,'C');
	  		$this->Ln(5);
	}

	function Footer()
	{
		$this->SetFont('Courier','I',10);
		//Número de página
		$this->Cell(180,10,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('P','mm');


$q = "select a.* from ingresos.ingresos_enc a
where 1=1 ";

if($status != '-1')
	if($status == 'true') 
		$q .=" AND a.status = 2 ";
	else
		$q .=" AND a.status = 1 ";	 
if($fecha_hasta != '' and $fecha_desde != ''){
	$q .= " AND a.fecha  between '".guardafecha($fecha_desde)."' and '".guardafecha($fecha_hasta)."'";
}else if($fecha_desde != '') 
	$q .= " AND a.fecha = '".guardafecha($fecha_desde)."' ";
if($tipo != '-1') $q .= " AND a.tipo_caja = $tipo ";
 
$q .= " order by a.fecha,a.status";
//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos
$pdf->fecha_caja = $fecha_desde;
$pdf->fecha_hasta = $fecha_hasta;
$pdf->tipo = 0;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);


$subtotal = 0;

$pdf->SetFont('Courier','',8);

$total_debe = 0;
$total_haber = 0;
$total_diferencia = 0;

while(!$rD->EOF){
	$pdf->SetFont('Courier','B',8);
	if($rD->fields['tipo_caja'] == 0)
		$pdf->Cell(10,5,'T',0,0,'C');
	else 
		$pdf->Cell(10,5,'SM',0,0,'C');
	if($rD->fields['status'] == 1)
		$pdf->Cell(20,5,utf8_decode('Abierto'),0,0,'C');
	else	
		$pdf->Cell(20,5,utf8_decode('Cerrado'),0,0,'C');
    $pdf->Cell(100,5,utf8_decode($rD->fields['descripcion']),0,0,'L');
    
    if($rD->fields['monto_debe'] <> $rD->fields['monto_haber']){
    	$pdf->Cell(30,5,muestrafloat(redondeado($rD->fields['monto_debe'])),1,0,'R');
    	$pdf->Cell(30,5,muestrafloat(redondeado($rD->fields['monto_haber'])),1,1,'R');
    }else{
    	$pdf->Cell(30,5,muestrafloat(redondeado($rD->fields['monto_debe'])),0,0,'R');
    	$pdf->Cell(30,5,muestrafloat(redondeado($rD->fields['monto_haber'])),0,1,'R');	
    }
    $total_debe += redondeado($rD->fields['monto_debe']);
    $total_haber += redondeado($rD->fields['monto_haber']);  
  $rD->movenext();
} //Fin loop departamentos
$total_diferencia = $total_debe - $total_haber;
//Total Ultimo documento de la lista
$pdf->SetFont('Courier','B', 10);
$pdf->Cell(130,5,utf8_decode("Totales: "),0,0,'R');
$pdf->Cell(30,5,muestrafloat(redondeado($total_debe)),0,0,'R');
$pdf->Cell(30,5,muestrafloat(redondeado($total_haber)),0,1,'R');

/*
* TOTAL 
*/
$pdf->ln(3);
$pdf->SetFont('Courier','B',12);
$pdf->Cell(160,5,utf8_decode("Total Diferencia "),0,0,'L');
$pdf->Cell(30,5,muestrafloat($total_diferencia),0,1,'R');


$pdf->Output();
?>
