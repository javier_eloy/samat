<?php

include("comun/ini.php");
set_time_limit(0);

$_SESSION['conex'] = $conn;

$oUsuario = new usuarios();

function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}

class PDF extends FPDF
{
	//Cabecera de p�gina
        var $usuario_activo;
        var $fecha_doc;

	function Header()
	{
	  parent::Header();
		//$this->SetLeftMargin(5);
		//$this->SetFont('Courier','',10);
		//$this->Ln(1);
		//$this->Image ("images/logoa.jpg",5,4,66,20);//logo a la izquierda 
		
		//$separaFecha= explode('-',date('Y-m-d'));
		$separaFecha= explode('-',$this->fecha_doc);
		$dia = $separaFecha[2];
		$mes = $separaFecha[1];
		$ano = $separaFecha[0];
    $this->SetFont('Arial','B',12);
		$this->Ln(20);	
		$this->Cell(190,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, 'R','L');
		$this->Ln(10);
	}
	//Pie de p�gina
	function Footer()
	{
		$conn = $_SESSION['conex'];
		$this->SetFont('Arial','',8);
		$this->Ln(10);
		$this->Cell(50,5,"Elaborado por",1,0,'C');
		$this->SetX(75);
		$this->Cell(50,5,"Firma 1",1,0,'C');
		$this->SetX(145);
		$this->Cell(50,5,"Firma 2",1,0,'C');
		$this->Ln(5);
		$this->Cell(50,30,"",1,0,'C');
		$this->SetX(75);
		$this->Cell(50,30,"",1,0,'C');
		$this->SetX(145);
		$this->Cell(50,30,"",1,0,'C');
		$this->Ln(25);
		//$this->Cell(50,5,"EDWIN LEAL",0,0,'C');
                $this->Cell(50,5,$this->usuario_activo,0,0,'C');
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();


$conn = $_SESSION['conex'];

$oChequeExterno = new transferencia_externa;

$oChequeExterno->get($conn, $_GET['id']);

$monto = 0;
$monto =  $oChequeExterno->monto;
$nrocuenta = utf8_decode($oChequeExterno->cedente);
$nrocheque = utf8_decode($oChequeExterno->nrodoc);
$montoLetras = utf8_decode(num2letras($monto));
$oUsuario->get($conn,$oChequeExterno->id_usuario);
$pdf->usuario_activo = "$oUsuario->nombre $oUsuario->apellido";
$pdf->fecha_doc =$oChequeExterno->fecha; 
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,5, 'Sres.: '.utf8_decode(substr($oChequeExterno->cedente,0,strpos($oChequeExterno->cedente,'-'))) ,0, 0, 'L','L');
$pdf->SetFont('Arial','',10);
$pdf->ln(15);
$pdf->SetX(15);
$pdf->MultiCell(180,5,'Sirva la presente para autorizar nos debiten de la cuenta '.$nrocuenta.' la cantidad de '.$montoLetras.' (Bs.'. muestraFloat($monto).')', 0, 'L');
$pdf->ln(4);
$pdf->SetX(15);
$pdf->MultiCell(180,5,'Por el concepto que se define a continuaci'.utf8_decode('ó').'n', 0, 'L');

$pdf->SetXY(15,80);
$pdf->Cell(190,5, 'CONCEPTO',1, 1, 'C','C');
//$pdf->ln(4);
$pdf->SetDrawColor (0,0,0);
$descripcion = dividirStr($oChequeExterno->descripcion, 90);
$id = 1;
$pdf->Cell(190,10, $descripcion[0],'RL', 2, 'L');
while(next($descripcion)){
	$pdf->Cell(190,10, $descripcion[$id],'RL', 2, 'L');
	$id++;
}
$pdf->SetXY(15,85);
$pdf->Cell(190,120, '','RLB', 0, 'L');

$pdf->SetXY(15,210);
$pdf->Cell(180,5,'Para acreditar a nombre de '.$oChequeExterno->beneficiario.'.',0, 1, 'L');
//$pdf->Ln(70);
$pdf->SetXY(15,215);
$separaFecha= explode('-',$oChequeExterno->fecha);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->Cell(190,5,"Nro. de Transferencia: ".$ano.'-'.$mes.'-'.$oChequeExterno->nrodoc,0,1,'R');
$pdf->Output();

?>
