<?
include('adodb/adodb-exceptions.inc.php');
require ("comun/ini.php");
$pagina = $_REQUEST['pagina'];
$tamano_pagina = 20;
if (!$pagina) {
    $inicio = 0;
    $pagina=1;
}
else 
    $inicio = ($pagina - 1) * $tamano_pagina;

// Creando el objeto grupos_proveedores
$oGruposProveedores = new grupos_proveedores;
$today=date("Y-m-d");
$accion = $_REQUEST['accion'];

switch($accion ) {
    case 'Guardar':
        $oGruposProveedores->add($conn,
                $_REQUEST['nombre'],
                $_REQUEST['descripcion'],
                $today,
                $_REQUEST['requisito']);
        break;
    case 'Actualizar':
        $oGruposProveedores->set($conn,
                $_REQUEST['id'],
                $_REQUEST['nombre'],
                $_REQUEST['descripcion'],
                $today,
                $_REQUEST['requisito']);
        break;
    case 'del':
        $oGruposProveedores->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oGruposProveedores->msg;

if ($msg==ERROR_CATCH_VFK)
    $msg = "ERROR: No puede eliminar un grupo que contiene proveedores";

//Seccion paginador

$cGruposProveedores = $oGruposProveedores->buscar($conn, "", $tamano_pagina, $inicio, "nombre");
$total = grupos_proveedores::total_registro_busqueda($conn, "");

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";
?>
<script type="text/javascript">var mygrid,i=0, ipp=0</script>
<br />
<span class="titulo_maestro">Maestro de Grupos de Proveedores</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<fieldset id="buscador">
    <legend>Buscar:</legend>
    <table>
        <tr>
            <td>Nombre:</td>
            <td>
                <input type="text" name="nombre" id="nombre" onkeyup="buscador(this.value, event)" />
                <input type="hidden" name="hidden_nombre" id="hidden_nombre" />
            </td>
        </tr>
    </table>
</fieldset>
<br />
<div id="busqueda"></div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<? 
$validator->create_message("error_nombre", "nombre", "*");
$validator->print_script();
require ("comun/footer.php");?>
<script language="javascript" type="text/javascript">

    var t;
    busca('',1);
    function buscador(nombre, pagina, code)
    {
        if ((code>=48 && code<=57) || (code>=96 && code<=105) || (code>=65 && code<=90) || code==8 || code==13 || code==46)
        {
            clearTimeout(t);
            $('hidden_nombre').value = nombre;
            t = setTimeout("busca('"+nombre+"', "+pagina+")", 800);
        }
    }

    function busca(nombre, pagina)
    {
        var url = 'updater_busca_grupos_proveedores.php';
        var pars = 'nombre=' + nombre + '&pagina=' + pagina + '&ms='+new Date().getTime();
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    function AgregarRP(){
        //alert($('unidad_ejecutora').value);

        if ($('requisitos').value =="0"){

            alert("Primero debe Seleccionar un Requisito.");
            return;
        }else{

            for(j=0;j<i;j++){

                if(mygridrp.getRowIndex(j)!=-1){

                    if (mygridrp.cells(j,'0').getValue() == $('requisitos').value){

                        alert('Este requisito ya ha sido seleccionada, por favor seleccione otra');
                        return false;

                    }
                }
            }

            /*mygridco.getCombo(0).put(JsonData[j]['id_categoria_programatica'],JsonData[j]['categoria_programatica']);
                mygridco.getCombo(1).put(JsonData[j]['id_partida_presupuestaria'],JsonData[j]['partida_presupuestaria']);*/
            mygridrp.addRow(i,$('requisitos').value);
            i++;

        }
    }

    function EliminarRP(){
        mygridrp.deleteRow(mygridrp.getSelectedId());
        i--;

    }

    function Guardar()
    {
        var JsonAux,requisito=new Array;
        mygridrp.clearSelection()
        if(i > 0){
            for(j=0;j<i;j++)
            {
                if((mygridrp.getRowIndex(j)!= -1))
                {
                    requisito[j] = new Array;
                    requisito[j][0]= mygridrp.cells(j,0).getValue();
                }
            }
            JsonAux={"requisito":requisito};
            $("requisito").value=JsonAux.toJSONString();
            validate();
        } else {
            alert('Debe Seleccionar al menos un requisito para este grupo');
        }
    }

    Event.observe('nombre', "keyup", function (evt) {
        buscador($F('nombre'), 1, evt.keyCode);
    });

</script>