<?php
include ("comun/ini.php");
$pagina         = $_REQUEST['pagina'];
$descripcion    = isset($_GET['descrip_cuenta'])?$_GET['descrip_cuenta']:'';
$id             = isset($_GET['codigo_cuenta'])?$_GET['codigo_cuenta']:'';
//echo 'p'.$fecha_hasta."<br>";
if (!$pagina) 
{
    $inicio = 10;
    $pagina=1;
}
else {
    $inicio = ($pagina - 1) * 20;
} 

//die(var_dump($_REQUEST));

$oTiposIngresos = new tipo_ingreso;
$cTiposIngresos = $oTiposIngresos->buscar($conn, $descripcion, $id, 0, $tamano_pagina,'id');
$total          = $oTiposIngresos->total_registro_busqueda($conn, '', '', '');

    if (is_array($cTiposIngresos)) {
        ?>
    <table class="sortable" id="grid" cellpadding="0" cellspacing="1">
        <tr class="cabecera">
            <td width="20%">Codigo</td>
            <td width="53%">Descripci&oacute;n</td>
            <td width="5%">&nbsp;</td>
            <td width="5%">&nbsp;</td>
        </tr>
            <?
            foreach($cTiposIngresos as $tipos_ingresos) {
                ?>
        <tr class="filas">
            <td><?=$tipos_ingresos->id?></td>
            <td><?=$tipos_ingresos->descripcion?></td>
            <td align="center"><a href="?accion=del&id=<?=$tipos_ingresos->id?>" onClick="if (confirm('Si presiona Aceptar será eliminada esta información')){ return true;} else{return false;}"  title="Eliminar Registro"><img src="images/eliminar.gif" border="0" ></a></td>
            <td align="center"><a href="#" onClick="updater(<?=$tipos_ingresos->id?>); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
        </tr>
                <?
            }

            $total_paginas = ceil($total / $tamano_pagina);
            ?>
        <tr class="filas">
            <td colspan="4" align="center">
                    <?
                    for ($j=1; $j<=$total_paginas; $j++) {
                        if ($j==1)
                            echo '<span class="actual">'.$j.'</span>';
                        else
                            echo '<span style="cursor:pointer" onclick="busca($(\'busca_cuenta\').value,$(\'desc_cta\').value, '.$j.');"> - '.$j.'</span>';
                    }
                    ?>
            </td>
        </tr>
        <tr class="filas">
            <td colspan="4" align="center"> Pagina <strong>1</strong> de <strong><?=$total_paginas?></strong></td>
        </tr>
    </table>

<?}?>