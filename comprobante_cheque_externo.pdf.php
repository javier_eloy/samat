<?php

include("comun/ini.php");
include("comprobantes_retencion.php");
include("Constantes.php");


$tipoReporte = $_REQUEST['tipo'];

    $oCheque = new transferencia_externa;
    //Buscqueda con por nro de documento
    $qnrodoc = "SELECT id FROM finanzas.transferencias WHERE nrodoc = '".$_REQUEST['id']."'";
    $onrodoc = $conn->Execute($qnrodoc);
    
    $oCheque->get($conn, $onrodoc->fields['id']);


function dividirStr($str, $max)
{
	$strArray = array();
    do
    {
		if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
      	else
        	$posF = -1;
      
      	if ($posF===false || $posF==-1)
      	{
        	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}
      	else
      	{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }
	while ($posF != -1);
    return ($strArray);
}

class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{
		$this->SetLeftMargin(5);
	}
	//Pie de página
	function Footer()
	{
		$this->SetFont('Arial','',8);
                $this->SetFont('Arial','',8);
		$this->SetXY(10,240);
		$this->Cell(42.5,5,"Elaborado por",1,0,'C');
		$this->SetX(57.5);
		$this->Cell(42.5,5,"FIRMA 1",1,0,'C');
		$this->SetX(110);
		$this->Cell(42.5,5,"FIRMA 2",1,0,'C');
		$this->SetX(162.5);
		$this->Cell(42.5,5,"Recibe conforme",1,0,'C');
		$this->SetX(10);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetX(57.5);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetX(110);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetFont('Arial','',6);

		$this->SetXY(164.5,245);
		$this->Cell(42.5,7,"Nombre _________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(164.5);
		$this->Cell(42.5,7,"Fecha __________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(164.5);
		$this->Cell(42.5,7,"C.I. ".utf8_decode('ó')." R.I.F. ______________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(164.5);
		$this->Cell(42.5,7,"Firma __________________________",0,0,'L');
		$this->Ln(5);
		$this->SetXY(162.5,240);
		$this->Cell(42.5,28,"",1,0,'C');
		
	}
}
//
//class PDF extends PDF_Rotate
//{
//	function RotatedText($x,$y,$txt,$angle)
//	{
//    	//Text rotated around its origin
//    	$this->Rotate($angle,$x,$y);
//    	$this->Text($x,$y,$txt);
//    	$this->Rotate(0);
//	}
//}

$pdf=new PDF('P');
$pdf->AliasNbPages();
$pdf->AddPage('P');
$pdf->SetFont('Arial','',10);
//die('aqui');
$json = new Services_JSON;
$cadena = str_replace("\\","",$oCheque->json);//-->OJO, MUY IMPORTANTE !!!
$vector = $json->decode($cadena);
$x = count($vector);
$docs = '';

for ($i=0;$i<$x;$i++)
{
	$docs.= '\''.$vector[$i]->nroref.'\',';
}
$docs = rtrim($docs,',');

// CHEQUE
$monto =  $oCheque->monto;
//die($monto);
$nrocuenta = utf8_decode($oCheque->cedente);
$nrocheque = utf8_decode($oCheque->nrodoc);
$pdf->SetX(10);
$pdf->Cell(80,5, $nrocuenta ,0, 1, 'L','L');
$pdf->Cell(55,5, $nrocheque,0, 0, 'C','C');
$pdf->SetX(130);
//$pdf->Cell(70,5, '***********'.utf8_decode(muestraFloat(str_replace(',','.',str_replace('.','',$monto)))),0, 0, 'R','R');
$pdf->Cell(70,5, '***********'.muestraFloat($monto),0, 0, 'R','R');

$pdf->ln(5);
$pdf->SetX(10);
$pdf->Cell(195,5, utf8_decode($oCheque->banco),0, 0, 'L','C');
$pdf->ln(15);
$pdf->SetXY(20,19);
$pdf->Cell(185,5, '***'. utf8_decode($oCheque->beneficiario).'***',0, 0, 'L','C');
$pdf->ln(5);

//$montoLetras = num2letras(floatval(str_replace(',','.',str_replace('.','',$monto))));
$montoLetras = num2letras($monto);

$desc_monto = dividirStr($montoLetras, intval(200/$pdf->GetStringWidth('M')));
$pdf->SetXY(25,26);
$pdf->Cell(195,5, strtoupper(utf8_decode($desc_monto[0])),0, 0, 'L','L');
$pdf->SetXY(25,32);
if($desc_monto[1] != '')
  $pdf->Cell(190,5, strtoupper(utf8_decode($desc_monto[1])),0, 0, 'L','L');
$pdf->ln(5);
$separaFecha= explode('-',$oCheque->fecha);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->SetX(10);
$pdf->Cell(195,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, '','L');
$pdf->ln(5);
$pdf->SetX(10);
$pdf->Cell(195,5,'NO ENDOSABLE, CADUCA A LOS 90 DIAS',0,0, '','L');
$pdf->ln(15);
$pdf->SetX(10);
//Programador Ricardo Camejo
//Sellado de Anulado 
//$pdf->RotatedText(100,60,'Hello!',45);


if($tipoReporte!='3')
    $pdf->Cell(195,5, 'OP - '.$docs,0, 0, 'C','L');

$pdf->SetFont('Arial','B',8);
$pdf->ln(12);
$pdf->SetX(10);
$pdf->Cell(195,5, 'CONCEPTO',1, 0, 'C','C');
$pdf->Ln(5);
$pdf->SetDrawColor (255,255,255);
$pdf->SetWidths(array(25,10,15,115,10,10,20));
$pdf->SetAligns(array('C','C','C','L','C','C','C'));

    $pdf->setX(1);
    $pdf->SetFont('Arial','',8);
    
    $pdf->setX(1);
    $pdf->ln(10);
    $pdf->SetAligns(array('C','C','C','C','C','C','C'));
    $pdf->Row(array("","", "", "TRASLADO DE FONDOS AL $oCheque->receptora \n $oCheque->descripcion","" , "", ""),T,11);
    $pdf->SetDrawColor (0,0,0);

    $pdf->SetXY(10, 74);
    $pdf->Cell(195,5,"",1,0,'C');
    $pdf->Ln(5);
    $pdf->SetX(10);
    $pdf->Cell(195,145,"",1,0,'C');
    $pdf->Ln(110);
//Banda de Firma
//banda_firma($pdf);


$docs = str_replace('\'',' ',$docs);
$varAux = explode(',',$docs);

$pdf->Output();

?>
