<? require ("comun/ini.php");
//Seccion paginador
$page_size = 25;
if ($_GET['pg'])
    $start_record=($_GET['pg'] * $page_size) - $page_size;
else
    $start_record=0;

// Creando el objeto motivo
$oResponsable = new responsable;
$accion = $_REQUEST['accion'];

#SECCION DE GUARDAR#
switch($accion ) {
    case 'Guardar':
        $oResponsable->add($conn, $_REQUEST['tra_cod'], guardafecha($_REQUEST['fecha_ini']), guardafecha($_REQUEST['fecha_fin']), $_REQUEST['tipo'], $_REQUEST['status'], $_REQUEST['txtNomEmp']);
        break;
#SECCION DE ACTULIZAR#
    case 'Actualizar':
        $oResponsable->set($conn, $_REQUEST['id'], $_REQUEST['tra_cod'], guardafecha($_REQUEST['fecha_ini']), guardafecha($_REQUEST['fecha_fin']), $_REQUEST['tipo'], $_REQUEST['status'], $_REQUEST['txtNomEmp']);
        break;
#SECCION DE ELIMINAR#
    case 'del':
        $oResponsable->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oResponsable->msg;

$cResponsable=$oResponsable->get_all($conn, $start_record,$page_size);
$pag=new paginator($oResponsable->total,$page_size, self($_SERVER['SCRIPT_NAME']));
$i=$pag->get_total_pages();
require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\" >".$msg."</div><br/>";

?>

<br />
<span class="titulo_maestro">Maestro de Responsables y Custodios de Caja Chica </span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />

<fieldset id="buscador">
    <legend>Buscar:</legend>
    <table>
        <tr>
            <td>Nombre</td>
            <td width="130"><input type="text" name="busca_nombre" id="busca_nombre" onkeypress="buscador()" /></td>
        </tr>
        <tr>
        <table>
            <tr>
                <td>
                    <input style="width:100px"  type="text" name="busca_fecha_desde" id="busca_fecha_desde"
                           onchange="validafecha(this);"/>
                </td>
                <td>
                    <a href="#" id="boton_busca_fecha_desde" onclick="return false;">
                        <img border="0" alt="Seleccionar una fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_desde",
                            button            : "boton_busca_fecha_desde",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>

                <td>
                    <input style="width:100px" type="text" name="busca_fecha_hasta" id="busca_fecha_hasta"
                           onchange="validafecha(this); "/>
                </td>
                <td>
                    <a href="#" id="boton_busca_fecha_hasta" onclick="return false;">
                        <img border="0" alt="Seleccionar una fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_hasta",
                            button            : "boton_busca_fecha_hasta",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>
            </tr>
        </table>
        </tr>
    </table>
</fieldset>
<br />

<div id="busqueda">
    <?
    if(is_array($cResponsable)) {
        ?>
    <table class="sortable" id="grid" cellpadding="0" cellspacing="1">
        <tr class="cabecera">
            <td>Nombre</td>
            <td>Desde</td>
            <td>Hasta</td>
            <td>Tipo</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <?
            foreach($cResponsable as $cb) {
                $tipo = ($cb->tipo==1) ? 'Custodio de Caja' : 'Responsable de Caja'
                        ?>
        <tr class="filas">
            <td><?=$cb->nombre?></td>
            <td><?=muestraFecha($cb->fecha_inicio)?></td>
            <td><?= !empty($cb->fecha_fin) ? muestraFecha($cb->fecha_fin) : '' ?></td>
            <td><?=$tipo?></td>
            <td align="center"><a href="responsable.php?accion=del&id=<?=$cb->id?>" title="Modificar ó Actualizar Registro" ><img src="images/eliminar.gif" width="16" height="10" border="0"></a></td>
            <td align="center"><a href="#" onclick="updater('<?=$cb->id?>'); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
        </tr>
                <?
            }
            ?>
    </table>
        <?
    }
    else {
        echo "No hay registros en la bd";
    }
    ?>
</div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">

    var t;

    function buscador()
    {
        clearTimeout(t);
        t = setTimeout("busca('" + $('busca_nombre').value + "'," + $('busca_fecha_desde').value + ", " + $('busca_fecha_desde').value + ",1)", 800);
    }

    function busca(nombre, desde, hasta, pagina)
    {
        var url = 'updater_busca_cuentas_bancarias.php';
        var pars = 'nombre='+nombre+'&desde='+desde+'&hasta='+hasta+'&pagina='+pagina+'&ms='+new Date().getTime();
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }



    function selDocumento(id, nombre){

        $('txtNomEmp').value = nombre;
        $('tra_cod').value = id;
        Dialog.okCallback();

    }

    var t;

    function busca_popup()
    {
        clearTimeout(t);
        t = setTimeout('traeEmpleadosDesc()', 800);
    }

    function traeEmpleados(){

        var url = 'buscar_empleado.php';
        var pars = 'nombre=&ms='+new Date().getTime();

        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                Dialog.closeInfo();
                Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                        showEffect:Element.show,hideEffect:Element.hide,
                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                    }});

            }
        }
    );
    }

    function traeEmpleadosDesc(){

        var url = 'buscar_empleado.php';
        var pars = 'nombre='+$('search_nombre_emp').value+'&ms='+new Date().getTime();

        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                Dialog.closeInfo();
                Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                        showEffect:Element.show,hideEffect:Element.hide,
                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                    }});

            }
        }
    );
    }
</script>
<?
$validator->create_message("error_nombre", "txtNomEmp", "*");
$validator->create_message("error_tipo", "tipo", "*");
$validator->create_message("error_status", "status", "*");
$validator->create_message("error_fecha_ini", "fecha_ini", "*");
$validator->print_script();
?>
<? require ("comun/footer.php"); ?>
