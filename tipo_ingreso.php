<?
require ("comun/ini.php");
// Creando el objeto Proveedores
$oTiposIngresos = new tipo_ingreso;
$origen = $_POST['tipo_doc'];
$accion = $_REQUEST['accion'];
$tamano_pagina = 20;

switch($accion) {
    case 'Guardar':
        $oTiposIngresos->add($conn, $_REQUEST['descripcion'], $_REQUEST['cta_contable'], $_REQUEST['tMovimiento'],$origen);
        break;
    case 'Actualizar':
        $oTiposIngresos->set($conn,$_REQUEST['id'], $_REQUEST['descripcion'], $_REQUEST['cta_contable'], $_REQUEST['tMovimiento'],$origen);
        break;
    case 'del':
        $oTiposIngresos->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oTiposIngresos->msg;

require ("comun/header.php");
if(!empty($msg))   echo "<div id=\"msj\">".$msg."</div><br/>";

?>
<br />
<span class="titulo_maestro">Tipos de Documentos</span>
<div id="formulario">
    <a href="#" onClick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<br />
<div>
    <fieldset id="buscador">
        <legend>Buscar:</legend>
        <table width="350px">
            <tr>
                <td>Codigo Cuenta</td>
                <td>Descripci&oacute;n</td>
            </tr>

            <tr>
                <td>
                    <?=helpers::combo_ue_cp($conn, 'busca_cuenta','','descripcion','id','busca_cuenta','busca_cuenta','buscador(this.value)',
                            "SELECT A.id, A.codcta||' - '||A.descripcion AS descripcion FROM contabilidad.plan_cuenta A INNER JOIN ingresos.tipos_ingresos B ON (A.id = B.id_cta_contable) ORDER BY descripcion")?>

                </td>
                <td>
                    <input type="text" name="descrip_cuenta" id="descrip_cuenta"  size="30" />
                    <input type="hidden" name="desc_cta" id="desc_cta" />
                </td>
            </tr>
        </table>
    </fieldset>
</div>
<br />
<div style="margin-bottom:10px" id="busqueda">
    
</div>
        <br />
        <br />
        <div style="height:40px;padding-top:10px;">
            <p id="cargando" style="display:none;margin-top:0px;">
                <img alt="Cargando" src="images/loading.gif" /> Cargando...
            </p>
        </div>

        <script type="text/javascript">

            var t;

            function buscador(idcta, descripcion, pagina, keyCode)
            {
                clearTimeout(t);
                if ((keyCode>=65 && keyCode<=90) || (keyCode>=48 && keyCode<=57) || (keyCode>=96 && keyCode<=105) || keyCode==8 || keyCode==46)
                {
                    $('descrip_cuenta').value = descripcion;
                    t = setTimeout("busca('"+idcta+"','"+descripcion+"','"+pagina+"')", 800);
                }else{
                    t = setTimeout("busca('"+idcta+"','','')", 800);
                    $('descrip_cuenta').value = '';
                }

            }

            Event.observe('descrip_cuenta', "keyup", function (evt) {
                buscador('', $F('descrip_cuenta'), 1, evt.keyCode);
            });

            function busca(idcta, descripcion, pagina){
                var url = 'updater_tipos_doc_ing.php';
                var pars = 'codigo_cuenta=' + idcta + '&descrip_cuenta=' + descripcion + '&ms='+new Date().getTime()+ '&pagina='+pagina;
                var updater = new Ajax.Updater('busqueda',
                url,
                {
                    method: 'get',
                    parameters: pars,
                    asynchronous:true,
                    evalScripts:true,
                    onLoading:function(request){Element.show('cargando')},
                    onComplete:function(request){Element.hide('cargando')}
                });
            }


            

            function traeCuentasContables(){

                var tipo = 2;
                var url = 'buscar_cuentas.php';
                var pars = 'id_cuenta='+$('cta_contable').value+'&tipo='+tipo+'&ms='+new Date().getTime();

                var Request = new Ajax.Request(
                url,
                {
                    method: 'get',
                    parameters: pars,
                    onLoading:function(request){},
                    onComplete:function(request){

                        Dialog.closeInfo();
                        Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                                showEffect:Element.show,hideEffect:Element.hide,
                                showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                            }});

                    }
                }
            );
            }

            function traeCuentasContablesDesc(){

                var tipo = 2;
                var url = 'buscar_cuentas.php';
                var pars = 'descripcion='+$('search_descrip').value+'&id_cuenta='+$('cta_contable').value+'&tipo='+tipo+'&ms='+new Date().getTime();
                var Request = new Ajax.Request(
                url,
                {
                    method: 'get',
                    parameters: pars,
                    onLoading:function(request){},
                    onComplete:function(request){

                        Dialog.closeInfo();
                        Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                                showEffect:Element.show,hideEffect:Element.hide,
                                showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                            }});

                    }
                }
            );
            }

            function selDocumento(id, nombre){

                $('txtCuentaContable').value = nombre;
                $('cta_contable').value = id;
                Dialog.okCallback();

            }


            function busca_popup()
            {
                clearTimeout(t);
                t = setTimeout('traeCuentasContablesDesc()', 800);
            }


        </script>

        <?
        $validator->create_message("error_cc", "txtCuentaContable", "*");
        $validator->create_message("error_desc", "descripcion", "*");
//$validator->create_message("error_esc", "escenarios", "*");
        $validator->create_message("error_nominal", "tMovimiento", "*", 2);
        $validator->create_message("error_tipo_doc", "tipo_doc", "*");
        $validator->print_script();
        ?>
        <? require ("comun/footer.php"); ?>