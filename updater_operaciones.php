<?
include ("comun/ini.php");
$id_usuario = $_REQUEST['id_usuario'];
$id_operacion = $_REQUEST['id_operacion'];
$id_modulo = $_REQUEST['id_modulo'];

$operaciones = new operaciones;
$aModulos = $operaciones->getAllMods($conn,'orden');

$conn->StartTrans();
if(!empty($id_usuario)) {
    if(!empty($id_modulo)) {
        //Actualizacion por modulo
        $ModOpe = $operaciones->getAllOperacionModulo($conn, $id_modulo);
        foreach ($ModOpe as $oOperacion) {
            if($operaciones->has_permiso($conn, $id_usuario, $oOperacion->id))
                $operaciones->del_permiso($conn, $id_usuario, $oOperacion->id);
            else
                $operaciones->add_permiso($conn, $id_usuario, $oOperacion->id);
        }
    }else {
        //Se asignas los permisos a nivel de base de datos
        if(!empty($id_operacion)) {
            if($operaciones->has_permiso($conn, $id_usuario, $id_operacion))
                $operaciones->del_permiso($conn, $id_usuario, $id_operacion);
            else
                $operaciones->add_permiso($conn, $id_usuario, $id_operacion);
        }
    }
} //Fin if(!empty($id_usuario))
$conn->CompleteTrans();

$i = 1;
foreach($aModulos as $modulo) {
    ?>

<span class="titulo_maestro" ><?=$modulo->descripcion?></span>
<span style="margin-left:10px"><a id="mt" onclick="marcar('M',<?=$modulo->id?>,<?=$id_usuario?>)">[Marcar Todos]</a></span><span style="margin-left:10px" ><a id="dt" onclick="marcar('D',<?=$modulo->id?>,<?=$id_usuario?>)">[Desmarcar Todos]</a></span>
<ul>
        <?
        $j = 1;
        $aPermisos = $operaciones->get_all($conn, $modulo->id, 'id_padre,orden,id');
        if(is_array($aPermisos)) {
            foreach($aPermisos as $permiso) {
                $tipo = $permiso->tipo == 'C' ? '<img src="images/folders/carpeta.gif" />': '';
                // revisa si el usuario tiene permisos sobre esa operacion
                $checked = ($operaciones->has_permiso($conn, $id_usuario, $permiso->id)) ? "checked=\"checked\"" : "";
                ?>
    <li><input type="checkbox" id="permiso_<?=$i?>_<?=$j?>"  onclick="setPermisos(<?=$id_usuario?>, <?=$permiso->id?>);" <?=$checked?> /><label onclick="new Effect.Highlight(this.parentNode);" for="permiso_<?=$i?>_<?=$j?>"><?= !empty($permiso->nom_padre) ? $permiso->nom_padre." \\ ".$permiso->descripcion : $permiso->descripcion?> <?=$tipo?></label></li>
                <? $j++;
            }
        }else {
            echo "No hay operaciones en este m&oacute;dulo";
        }
    ?>
</ul>
    <? $i++;
} ?>
