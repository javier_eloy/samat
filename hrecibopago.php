<? 
require ("comun/ini.php");
require ("comun/header.php");
?>
<br />
<span class="titulo_maestro">Recibos de Pago</span>

<div id="formulario">
    <table width="700" border="0" >
        <tr id="trNomina" >
            <td width="100" >N&oacute;mina:</td>
            <td ><SELECT name="Nomina" id="Nomina" onChange="RefrescaCombosDependiente();" ></SELECT></td>
        </tr>
        <tr id="trTrabajador">
            <td >Trabajador:</td>
            <td ><SELECT name="Trabajador" id="Trabajador" ></SELECT></td>
        </tr>
        <tr id="trConcepto">
            <td >Concepto:</td>
            <td ><SELECT name="Concepto" id="Concepto" ></SELECT></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><br /><input  type="button"  value="Generar Reporte" onClick="Imprimir()" ></td>
        </tr>        
    </table>
</div>

<div style="height:40px;padding-top:10px;">
        <p id="cargando" style="display:none;margin-top:0px;">
            <img alt="Cargando" src="images/loading.gif" /> Cargando...
        </p>
</div>
<br>

<script language="javascript"  type="text/javascript"> 
    RefrescaCombos();
    
    function RefrescaCombos() {
         ComboNomina();            //--- Datos
         RefrescaCombosDependiente(); // Datos
    }
    
    function RefrescaCombosDependiente() {
        if($('trTrabajador').style.display != 'none') ComboTrabajador($('Nomina').options[$('Nomina').selectedIndex].value);
        if($('trConcepto').style.display != 'none') ComboConcepto($('Nomina').options[$('Nomina').selectedIndex].value);
    }
    
     // Funciones JSON para actualizar combos
    function ComboNomina(){
        var JsonAux;
        $('Nomina').length=1;
	JsonAux={"Forma":1};
        var url = 'CargarCombo.php';
        var pars = 'JsonEnv=' + JsonAux.toJSONString();
        var Request = new Ajax.Request(url,{
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(){Element.show('cargando')},
            onComplete:function(transport){
                var JsonRec = eval( '(' + transport.responseText + ')');
                if(JsonRec){
                    $('Nomina').options[0]= new Option("Seleccione",-1);
                    for(var i=1;i<=JsonRec.length;i++){
                        $('Nomina').options[i]= new Option(JsonRec[i-1]['D'],JsonRec[i-1]['CI']);
                    }
                }
                Element.hide('cargando');
            }
        }
    );       
   }


    function ComboTrabajador(Nomina){
        var JsonAux;
        $('Trabajador').length=1;
        if(Nomina!=-1){
            JsonAux={"Nomina":parseInt(Nomina),"Forma":2};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var request = new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: pars,
                asynchronous:true,
                evalScripts:true,                
                onLoading:function(transport){Element.show('cargando')},
                onComplete:function(transport){
                    var JsonRec = eval( '(' + transport.responseText + ')');
                    if(JsonRec){
                        //$('Trabajador').options[0]= new Option("Todos",-1);
                        $('Trabajador').options[0]= new Option("Seleccione",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Trabajador').options[i]= new Option(Cadena(JsonRec[i-1]['N'])+" "+Cadena(JsonRec[i-1]['A']),JsonRec[i-1]['CI']);
                        }
                    }
                    Element.hide('cargando');
                }
            }
        );
        }
    }
    function ComboConcepto(Nomina){
        var JsonAux;
        $('Concepto').length=1;
        if(Nomina!=-1){
            JsonAux={"Nomina":parseInt(Nomina),"Forma":5};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var Request = new Ajax.Request( url,{
                method: 'post',
                parameters: pars,
                asynchronous:true,
                evalScripts:true,
                onLoading:function(transport){Element.show('cargando')},
                onComplete:function(transport){
                    var JsonRec = eval( '(' + transport.responseText + ')');
                    if(JsonRec){
                        $('Concepto').options[0]= new Option("Todos",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Concepto').options[i]= new Option(JsonRec[i-1]['N'],JsonRec[i-1]['CI']);
                        }
                    }
                    Element.hide('cargando');
                }
            }
        );
        }
    }
    var wxR;
    function Imprimir(){      
       if( $('Nomina').options[$('Nomina').selectedIndex].value==-1 ) { 
            alert("Debe escojer una Nomina");
        }else{
          if (!wxR || wxR.closed) {
                    wxR = window.open("hrecibopago.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
          } else {
                    wxR.focus()
          }             
        }
    }
</script>
<? require ("comun/footer.php"); ?>