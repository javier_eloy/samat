<?
set_time_limit(300);
include("comun/ini.php");
include("Constantes.php");

$fecha_desde = guardafecha($_GET['fecha_desde']);
$fecha_hasta = guardafecha($_GET['fecha_hasta']);
$id_cta_cont = $_GET['id_cta_cont'];

$q = "SELECT movim, descripcion, codcta FROM contabilidad.plan_cuenta WHERE id = '$id_cta_cont'";
$r = $conn->Execute($q);
$movim = (!$r->EOF) ? $r->fields['movim']:"";
$descripcionCta = (!$r->EOF) ? $r->fields['descripcion']:"";
$codCta = (!$r->EOF) ? $r->fields['codcta']:"";
if ($movim=='S' || empty($id_cta_cont))
{
        //die($codCta);
        if(substr($codCta,0,3) == '101'){
            //SE CAMBIO EL IINER  A LEFT PARA QUE SALGA EL SALDO DE LACUENTA
            $sql = "SELECT plan_cuenta.codcta, plan_cuenta.descripcion, plan_cuenta.saldo_inicial, com_enc.*, com_det.debe, com_det.haber, plan_cuenta.naturaleza AS naturaleza, nombre_beneficiario, concepto FROM contabilidad.plan_cuenta ";
            $sql.= "INNER JOIN contabilidad.com_det ON (plan_cuenta.id = com_det.id_cta) ";
            $sql.= "INNER JOIN contabilidad.com_enc ON (com_det.id_com = com_enc.id) ";
            $sql.= "LEFT JOIN finanzas.cheques ON nrodoc = num_doc ";
            $sql.= "WHERE 1=1 ";
            $sql.= (!empty($id_cta_cont) ? " AND com_det.id_cta = $id_cta_cont ":"");
            $sql.= (!empty($fecha_desde) ? " AND com_enc.fecha >= '$fecha_desde' ":"");
            $sql.= (!empty($fecha_hasta) ? " AND com_enc.fecha <= '$fecha_hasta' ":"");
            $sql.= "AND com_enc.status = 'R' ";
            $sql.= "ORDER BY plan_cuenta.codcta::text, com_enc.fecha ";
        }else{
            //SE CAMBIO EL IINER  A LEFT PARA QUE SALGA EL SALDO DE LACUENTA
            $sql =  "SELECT plan_cuenta.codcta, plan_cuenta.descripcion, plan_cuenta.saldo_inicial, com_enc.*, com_det.debe, com_det.haber, plan_cuenta.naturaleza AS naturaleza, 
                    nombre_beneficiario, concepto, orden_pago.descripcion as descrip_odp, orden_pago.id_proveedor, coalesce(proveedores.nombre,'SAMAT INGRESOS') 
                    FROM contabilidad.com_enc 
                    INNER JOIN contabilidad.com_det ON com_det.id_com = com_enc.id 
                    INNER JOIN contabilidad.plan_cuenta ON plan_cuenta.id = com_det.id_cta 
                    LEFT JOIN finanzas.cheques ON com_enc.num_doc = cheques.nrodoc  
                    LEFT outer JOIN finanzas.orden_pago ON com_enc.num_doc = orden_pago.nrodoc 
                    left outer JOIN puser.proveedores ON orden_pago.id_proveedor = proveedores.id 
                    WHERE 1=1 "; 
                    $sql.= (!empty($id_cta_cont) ? " AND com_det.id_cta = $id_cta_cont ":"");
            $sql.= (!empty($fecha_desde) ? " AND com_enc.fecha >= '$fecha_desde' ":"");
            $sql.= (!empty($fecha_hasta) ? " AND com_enc.fecha <= '$fecha_hasta' ":"");
            $sql.= "AND com_enc.status = 'R' ";
            $sql.= "ORDER BY plan_cuenta.codcta::text, com_enc.fecha ";
//            $sql = "SELECT plan_cuenta.codcta, plan_cuenta.descripcion, plan_cuenta.saldo_inicial, com_enc.*, com_det.debe, com_det.haber, plan_cuenta.naturaleza AS naturaleza, nombre_beneficiario, concepto, orden_pago.descripcion, orden_pago.id_proveedor, proveedores.nombre FROM contabilidad.plan_cuenta ";
//            $sql.= "INNER JOIN contabilidad.com_det ON (plan_cuenta.id = com_det.id_cta) ";
//            $sql.= "INNER JOIN contabilidad.com_enc ON (com_det.id_com = com_enc.id) ";
//            $sql.= "LEFT JOIN finanzas.cheques ON nrodoc = num_doc ";
//            $sql.= "LEFT JOIN finanzas.orden_pago ON  com_enc.num_doc = orden_pago.nrodoc ";
//            $sql.= "INNER JOIN puser.proveedores ON orden_pago.id_proveedor = proveedores.id ";
//            $sql.= "WHERE 1=1 ";
//            $sql.= (!empty($id_cta_cont) ? " AND com_det.id_cta = $id_cta_cont ":"");
//            $sql.= (!empty($fecha_desde) ? " AND com_enc.fecha >= '$fecha_desde' ":"");
//            $sql.= (!empty($fecha_hasta) ? " AND com_enc.fecha <= '$fecha_hasta' ":"");
//            //Elimino este filtro para mostrar documentos anulados
//            //$sql.= "AND com_enc.status = 'R' ";
//            $sql.= "ORDER BY plan_cuenta.codcta::text, com_enc.fecha ";
        }
}
else
{
	$q = "SELECT id FROM contabilidad.plan_cuenta WHERE id_acumuladora = $id_cta_cont";
	$r = $conn->Execute($q);
	$array = array();
	while (!$r->EOF)
	{
		$array[] = $r->fields['id'];
		$r->movenext();
	}
	
	$tope = count($array) - 1;
	$ctas = array();
	$acums = array();
	while($tope >= 0)
	{
		$q = "SELECT id FROM contabilidad.plan_cuenta WHERE id_acumuladora = ".$array[$tope];
		$q.= (count($ctas)>0 || count($acums)>0) ?  " AND id NOT IN (".(count($ctas)>0 ? implode(',', $ctas): "").(count($ctas)>0 && count($acums)>0 ? ",":"").(count($acums)>0 ? implode(',', $acums):"").")":""; 
		//echo $q."<br>";
		$r = $conn->Execute($q);

		$copia = array();
		while (!$r->EOF)
		{
			$copia[] = $r->fields['id'];
			$r->movenext();
		}
		
		if (count($copia) > 0)
			$array = array_merge($array, $copia);
		else
		{
			$q = "SELECT movim FROM contabilidad.plan_cuenta WHERE id = ".$array[$tope];
			//echo "Movimiento/acumuladora ".$q."<br>";
			$r = $conn->Execute($q);
			if ($r->fields['movim'] == 'S')
				$ctas[] = array_pop($array);
			else
				$acums[] = array_pop($array);
		}

		$tope = count($array) - 1;
	}

//SE CAMBIO EL IINER  A LEFT PARA QUE SALGA EL SALDO DE LACUENTA
        $sql = "SELECT plan_cuenta.codcta, plan_cuenta.descripcion, plan_cuenta.saldo_inicial, com_enc.*, com_det.debe, com_det.haber, plan_cuenta.naturaleza AS naturaleza, nombre_beneficiario, concepto  FROM contabilidad.plan_cuenta ";
	$sql.= "INNER JOIN contabilidad.com_det ON (plan_cuenta.id = com_det.id_cta) ";
	$sql.= "INNER JOIN contabilidad.com_enc ON (com_det.id_com = com_enc.id) ";
        $sql.= "LEFT JOIN finanzas.cheques ON nrodoc = num_doc ";
	$sql.= "WHERE com_det.id_cta IN (".implode(",", $ctas).") ";
	$sql.= (!empty($fecha_desde) ? " AND com_enc.fecha >= '".$fecha_desde."' ":"");
	$sql.= (!empty($fecha_hasta) ? " AND com_enc.fecha <= '".$fecha_hasta."' ":"");
        //Elimino este filtro para mostrar documentos anulados
	$sql.= "AND com_enc.status = 'R' ";
	$sql.= "ORDER BY plan_cuenta.codcta::text ASC, com_enc.fecha ASC, com_enc.numcom ASC";
	
}
//die($sql);
$r = $conn->Execute($sql);

// Crea un array donde cada posicion es un string de tamaño 'max' caracteres,
// teniendo en cuenta de no cortar una palabra, busca el espacio en blanco  
// mas cerca del tamaño 'max' y ahi corta el string

function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
  }
class PDF extends FPDF
{
	var $fechaDesde;
	var $fechaHasta;
	
	// Definen el ancho de varias celdas que se utilizan en el documento
	var $fechaEmiW  = 20;
	var $descW      = 100;
	var $numComW    = 25;
	var $tipoDocW   = 10;
	var $numDocW    = 20;
	var $codctaW    = 50;
	var $descCtaW   = 160;
	var $descCtaW2  = 95;
	var $saldoW2    = 120;
	var $origenW    = 10;
	var $debeW      = 30;
	var $haberW     = 30;
	var $saldoW     = 30;
	
	
	//Cabecera de página
	function Header()
	{
                        parent::Header();

			$this->SetXY(225, 5);
			$textoFecha = "Fecha: ".date('d/m/Y')."\n";
                        $this->text(225,10,$textoFecha);
			$textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}\n";
                        $this->text(225,15,$textoPag);
			//$this->MultiCell(50,2, $textoDerecha, 0, 'L');
			
			$this->Ln(10);
			$this->SetLeftMargin(10);
			$this->SetFont('Courier','b',12);
			$titulo = "Mayor Analítico\n\n ";
			$titulo.= (empty($this->fechaHasta) ? "Desde ":"").(empty($this->fechaDesde) ? "Hasta ":$this->fechaDesde);
			$titulo.= (empty($this->fechaHasta) ? "":(empty($this->fechaDesde) ? "":" Al ").$this->fechaHasta);
			
			$this->MultiCell(0, 2, utf8_decode($titulo), 0, 'C');
			$this->Ln(8);
			
			$this->SetFont('Courier','B',10);
			$this->SetLineWidth(0.3);
			$this->Cell($this->codctaW, 4, "Cuenta Contable", T);
			$this->Cell($this->descCtaW2, 4, utf8_decode("Descripción"), T);
			//$this->Cell($this->numDocW, 4, '', T,'','C');
			$this->Cell($this->saldoW2, 4, "Saldo Inicial", T, '', 'R');
			$this->Ln();
			$this->Cell($this->fechaEmiW, 4, utf8_decode('Fecha E.'), B);
			$this->Cell($this->numComW, 4, utf8_decode('Nº Comprob.'), B);
			$this->Cell($this->descW, 4, utf8_decode("Descripción del Asiento"), B);
			$this->Cell($this->numDocW, 4, utf8_decode("Documento"), B,'','C');
			$this->Cell($this->origenW, 4, utf8_decode("Tipo"), B,'','C');
			$this->Cell($this->debeW, 4, "Debe", B, '', 'C');
			$this->Cell($this->haberW, 4, "Haber", B, '', 'C');
			$this->Cell($this->saldoW, 4, "Saldo", B);
			/*$this->Cell($this->tipoDocW, 4, 'Tipo Documento', T);
			$this->Cell($this->numDocW, 4, utf8_decode('Nº Documento'), T);*/
			$this->Ln();
			$this->Ln();
	}

	//Pie de página
	function Footer()
	{
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		//$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
}
//Creación del objeto de la clase heredada
$tam_letra = 8;
$pdf=new PDF('L','mm','Letter');
$pdf->fechaDesde = muestrafecha($fecha_desde);
$pdf->fechaHasta = muestrafecha($fecha_hasta);
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Courier','',$tam_letra);
$pdf->SetLeftMargin(10);
$pdf->SetFillColor(240);

if (!$r->EOF)
{
	$totalAcum = 0;
	while(!$r->EOF)
	{
		$pdf->SetFont('Courier', 'B', $tam_letra);
		// Imprime la informacion de la cuenta contable
		$codCtaAct = $r->fields['codcta'];
		$saldoCta = $r->fields['saldo_inicial'];
		$descCta = dividirStr(utf8_decode($r->fields['descripcion']), intval($pdf->descCtaW/$pdf->GetStringWidth('M')));
		
		$pdf->Cell($pdf->codctaW, 4, $r->fields['codcta'] , 0); //
		$pdf->Cell($pdf->descCtaW, 4, $descCta[0], 0);
                $pdf->setx(240);
		$pdf->Cell($pdf->saldoW, 4, muestraFloat($r->fields['saldo_inicial']), 0, '', 'R');
		
		
		while(next($descCta))
		{
			$pdf->Ln();
			$pdf->Cell($pdf->codctaW, 4, '', 0);
			$pdf->Cell($pdf->descCtaW, 4, current($descCta), 0);
			$pdf->Cell($pdf->saldoW, 4, '', 0);
		}
		
		$pdf->Ln(8);
		$pdf->SetFont('Courier', '', $tam_letra);
		$totalDebe = 0;
		$totalHaber = 0;
                $saldo = $r->fields['saldo_inicial'];
                $i = 1;
		// Imprime cada uno de los asientos donde estuvo incluida la cuenta
		while (!$r->EOF && $codCtaAct==$r->fields['codcta'])
		{

                        $concepto= ($r->fields['concepto']!='') ? $r->fields['concepto'] : $r->fields['descripcion'];
                        $nombre_beneficiario = ($r->fields['nombre_beneficiario'] != '') ? $r->fields['nombre_beneficiario'] : $r->fields['nombre'];
			$descCom = dividirStr(utf8_decode($nombre_beneficiario.' : '.$concepto.'=>'.$r->fields['descrip']), intval($pdf->descW/$pdf->GetStringWidth('M')));
			//echo $r->fields['origen'].' !== OP </br>';
			$pdf->Cell($pdf->fechaEmiW, 4, date('d/m/Y', strtotime($r->fields['fecha'])), 0);
			$pdf->Cell($pdf->numComW, 4, $r->fields['numcom'], 0);
			$pdf->Cell($pdf->descW, 4, $descCom[0], 0);
			$numdoc = ((trim($r->fields['origen']) == "OP") or empty($r->fields['num_doc2'])) ? $r->fields['num_doc'] : $r->fields['num_doc2'];
			//die($numdoc);
			$pdf->Cell($pdf->numDocW, 4, $numdoc, 0);
			$pdf->Cell($pdf->origenW, 4, trim($r->fields['origen']), 0,'','C');
			$pdf->Cell($pdf->debeW, 4, muestraFloat($r->fields['debe']), 0, '', 'R');
			$pdf->Cell($pdf->haberW, 4, muestraFloat($r->fields['haber']), 0, '', 'R');
                        //Se realizo este cambio para que las cuentas acreedoras sumen el saldo por el haber
                        if($naturaleza != 'D')
                            $saldo = $saldo - $r->fields['debe'] + $r->fields['haber'];
                        else
                            $saldo = $saldo + $r->fields['debe'] - $r->fields['haber'];
                        $pdf->Cell($pdf->saldoW, 4, muestraFloat($saldo), 0, '', 'R');
			
			while(next($descCom))
			{
				$pdf->Ln();
				$pdf->Cell($pdf->fechaEmiW, 4, '', 0);
				$pdf->Cell($pdf->numComW, 4, '', 0);
				$pdf->Cell($pdf->descW, 4, current($descCom), 0);
				$pdf->Cell($pdf->debeW, 4, '', 0);
				$pdf->Cell($pdf->haberW, 4, '', 0);
			}
			
			$totalDebe += $r->fields['debe'];
			$totalHaber += $r->fields['haber'];
			$naturaleza = $r->fields['naturaleza'];
			$r->movenext();
			$pdf->Ln();
		}
		
		$pdf->SetFont('Courier', 'B', $tam_letra);
		$pdf->Cell($pdf->fechaEmiW, 4, '', T, '', '', 1);
		$pdf->Cell($pdf->numComW, 4, '', T, '', '', 1);
		$pdf->Cell($pdf->descW, 4, '', T, '', '', 1);
		$pdf->Cell($pdf->numDocW, 4, '', T, '', '', 1);
                $pdf->Cell($pdf->debeW+20, 4, '', T, '', 'R', 1);
                $pdf->SetX(185);
        
		$pdf->Cell($pdf->debeW, 4, muestraFloat($totalDebe), T, '', 'R', 1);
                //$pdf->SetX(239);
		$pdf->Cell($pdf->haberW, 4, muestraFloat($totalHaber), T, '', 'R', 1);
			
		//Se realizo este cambio para que las cuentas acreedoras sumen el saldo por el haber
		//die(var_dump($naturaleza));
		if ($naturaleza == 'D'){
			$pdf->Cell($pdf->saldoW, 4, muestraFloat($saldoCta + $totalDebe - $totalHaber), T, '', 'R', 1);
		}else{
			$pdf->Cell($pdf->saldoW, 4, muestraFloat($saldoCta - $totalDebe + $totalHaber), T, '', 'R', 1);
		}
		
		if($anoCurso == 2007){
			$pdf->Ln();
			$pdf->Cell($pdf->descW+$pdf->fechaEmiW+$pdf->numComW+$pdf->numDocW, 4, 'Bs.F.:', T, '', 'R', 1);
			$pdf->Cell($pdf->debeW, 4, muestraFloat($totalDebe/1000), T, '', 'R', 1);
			$pdf->Cell($pdf->haberW, 4, muestraFloat($totalHaber/1000), T, '', 'R', 1);
			if ($naturaleza == 'D'){
				$pdf->Cell($pdf->saldoW, 4, muestraFloat(($saldoCta + $totalDebe - $totalHaber)/1000), T, '', 'R', 1);
			}else{
				$pdf->Cell($pdf->saldoW, 4, muestraFloat(($saldoCta - $totalDebe + $totalHaber)/1000), T, '', 'R', 1);
			}
		}
		$pdf->SetFont('Courier', '', 8);
		$pdf->Ln(8);
		$totalAcum += $saldoCta + $totalDebe - $totalHaber;
	}

	if ($movim=='N')
	{	
		$pdf->Ln();
		$pdf->SetFont('Courier', 'B', $tam_letra);
		$pdf->Cell($pdf->fechaEmiW+$pdf->numComW+$pdf->descW+$pdf->numDocW+$pdf->debeW, 4, 'Total '.utf8_decode($descripcionCta), 0, '', '', 1);
		$pdf->Cell($pdf->saldoW+$pdf->haberW, 4, muestraFloat($totalAcum), 0, '', 'R', 1);
		if($anoCurso == 2007){
			$pdf->Ln();
			$pdf->Cell($pdf->fechaEmiW+$pdf->numComW+$pdf->descW+$pdf->numDocW+$pdf->debeW, 4, 'Bs.F.: ', 0, '', 'R', 1);
			$pdf->Cell($pdf->saldoW+$pdf->haberW, 4, muestraFloat($totalAcum/1000), 0, '', 'R', 1);
		}
		$pdf->Ln(8);
		
		
	}
	
}
//die();
$pdf->Output();
?>
