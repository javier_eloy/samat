<?
require ("comun/ini.php");

// Creando el objeto municipios
$oMunicipios= new municipios;
$accion = $_REQUEST['accion'];
if($_REQUEST['alcaldia'])
    $alcaldia='T';
else 
    $alcaldia='F';
switch ($accion) {
    case 'Guardar':
        $oMunicipios->add($conn, $_REQUEST['descripcion'],$_REQUEST['estado'], $alcaldia);
        break;
    case 'Actualizar' :
        $oMunicipios->set($conn, $_REQUEST['id'], $_REQUEST['descripcion'], $_REQUEST['estado'], $alcaldia);
        break;
    case 'del':
        $oMunicipios->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oMunicipios->msg;

$cMunicipios=$oMunicipios->buscar($conn, '','', $num,$inicio);
$total_M = municipios::total_registro_busqueda($conn,'','');
$total = $total_M;

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";
?>

<br />
<span class="titulo_maestro">Maestro de Municipios</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />

<fieldset id="buscador">
    <legend>Buscar:</legend>
    <table>
        <tr>
            <td>Descripci&oacute;n:</td>
            <td>
                <input type="text" name="busca_desc" id="busca_desc" />
                <input type="hidden" name="hid_desc" id="hid_desc" />
            </td>
        </tr>
        <tr>
            <td>
				Estado:
            </td>
            <td>
                <? echo helpers::superCombo($conn,'SELECT id AS id, descripcion AS descripcion FROM puser.estado','','search_estado','search_estado','width:200px');?>
            </td>
        </tr>
    </table>
</fieldset>
<br />
<div id="busqueda"> </div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<script language="javascript" type="text/javascript">
    var t;
    buscador($F('busca_desc'), '1', 46);
    function buscador(descripcion, pagina, keyCode)
    {
        if ((keyCode>=65 && keyCode<=90) || (keyCode>=48 && keyCode<=57) || (keyCode>=96 && keyCode<=105) || keyCode==8 || keyCode==46)
        {
            clearTimeout(t);
            $('hid_desc').value = descripcion;
            var estado = $('search_estado').value;
            t = setTimeout("busca('"+descripcion+"','"+estado+"','"+pagina+"')", 800);
        }
    }

    function busca(descripcion, estado, pagina)
    {
        var url = 'updater_busca_municipios.php';
        var pars = 'descripcion=' + descripcion + '&estado=' + estado + '&ms='+new Date().getTime()+ '&pagina='+pagina;
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    Event.observe('busca_desc', "keyup", function (evt)
    {	buscador($F('busca_desc'), '1', evt.keyCode);	});

    Event.observe('search_estado', "change", function ()
    {	buscador($F('busca_desc'),'1',66 );	});
</script>

<?
$validator->create_message("error_estado", "estado", "* ");
$validator->create_message("error_descripcion", "descripcion", "*");
$validator->print_script();
require ("comun/footer.php");
?>