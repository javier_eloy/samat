<? require ("comun/ini.php");

$ocontrol_chequera = new control_chequera;
$accion = $_REQUEST['accion'];

#SECCION DE GUARDAR#
switch($accion ) {
    case 'Guardar' :
        if (!empty($_REQUEST['nro_chequera']))
            $ocontrol_chequera->add($conn, $_REQUEST['nro_chequera'], $_REQUEST['nro_cuenta'], $_REQUEST['fecha'], $_REQUEST['cheque_desde'], $_REQUEST['cheque_hasta'],
                    $_REQUEST['ultimo_cheque'], $_REQUEST['activa']);
        break;
#SECCION DE ACTULIZAR#
    case 'Actualizar' :
        if(!empty($_REQUEST['nro_chequera']))
            $ocontrol_chequera->set($conn, $_REQUEST['id'], $_REQUEST['nro_chequera'], $_REQUEST['nro_cuenta'], $_REQUEST['fecha'], $_REQUEST['cheque_desde'], $_REQUEST['cheque_hasta'],
                    $_REQUEST['ultimo_cheque']);
        break;
#SECCION DE ELIMINAR#
    case 'del':
        $ocontrol_chequera->del($conn, $_REQUEST['id']);
        break;
}
$msg = $ocontrol_chequera->msg;

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";

?>
<br />
<span class="titulo_maestro">Maestro de Control de Chequeras </span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<div id="busqueda"></div>

<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<script>
    busca(1);
   
    function validadornro(desde,hasta){

        if (desde>=hasta){
            alert("El Numero de Cheque de inicial es mayor que el final");
            return false;
        }else{
            return true;
        }
    }

    //FUNCION QUE TRAE LAS CUENTAS BANCARIAS AL MOMENTO DE SELECCIONAR UN BANCOS//
    function traeCuentasBancarias(id_banco, div, id_cuenta){
        var url = 'updater_selects.php';
        var pars = 'combo=cuentas_bancarias&id_banco=' + id_banco + '&id_cuenta=' + id_cuenta +'&style=width:150px&ms='+new Date().getTime();
        var updater = new Ajax.Updater(div,
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando_cuentas')},
            onComplete:function(request){Element.hide('cargando_cuentas')}
        });
    }

    function traeUltimoCheque(id_chequera,id_cuenta){
        var aux_nan = 0;
    }
    function activarChequera(id_chequera,id_cuenta){
        var url = 'json.php';
        var pars = 'op=activar_chequera&id_cuenta=' + id_cuenta + '&id_chequera=' + id_chequera;
        var myAjax = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onComplete: function(peticion){
                var jsonData = eval('(' + peticion.responseText + ')');
                if (jsonData == undefined) { return }
                alert('Proceso Realizado con Exito')
            }
        });
    }

 function busca(pagina)
    {
        var url  = 'updater_busca_control_chequera.php';
        var pars = 'pagina='+pagina;
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }
</script>
<?
$validator->create_message("error_nro_chequera", "nro_chequera", "*");
$validator->create_message("error_nro_cuenta", "nro_cuenta", "*");
$validator->create_message("error_fecha", "fecha", "*");
$validator->create_message("error_cheque_desde", "cheque_desde", "*");
$validator->create_message("error_cheque_hasta", "cheque_hasta", "*");
$validator->create_message("error_ultimo_cheque", "ultimo_cheque", "*");
$validator->print_script();
require ("comun/footer.php"); ?>
