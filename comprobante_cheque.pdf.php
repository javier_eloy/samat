<?php

include("comun/ini.php");
include("comprobantes_retencion.php");
include("Constantes.php");
$tipoReporte = $_REQUEST['tipo'];



if($tipoReporte=='1')
{
    $oCheque = new cheque;
    $oCheque->get($conn, $_REQUEST['id']);
    $oOrdenPago = new orden_pago();
}
elseif($tipoReporte=='2')
{
    $oCheque = new cheque_anteriores;
    $oCheque->get($conn, $_REQUEST['id']);
}elseif($tipoReporte=='3'){
    $oCheque = new transferencia();
    //Buscqueda con por nro de documento
    $qnrodoc = "SELECT id FROM finanzas.transferencias WHERE nrodoc = '".$_REQUEST['id']."'";
    $onrodoc = $conn->Execute($qnrodoc);
    $oCheque->get($conn, $onrodoc->fields['id']);
}


function dividirStr($str, $max)
{
	$strArray = array();
    do
    {
		if (strlen($str) > $max)
        	$posF = strrpos(substr($str, 0, $max), ' ' );
      	else
        	$posF = -1;
      
      	if ($posF===false || $posF==-1)
      	{
        	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}
      	else
      	{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }
	while ($posF != -1);
    return ($strArray);
}

class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{
		$this->SetLeftMargin(5);
	}
	//Pie de página
	function Footer()
	{
		$this->SetFont('Arial','',8);
		$this->SetFont('Arial','',8);
		$this->SetXY(10,240);
		$this->Cell(42.5,5,"Elaborado por",1,0,'C');
		$this->SetX(57.5);
		$this->Cell(42.5,5,"FIRMA 1",1,0,'C');
		$this->SetX(110);
		$this->Cell(42.5,5,"FIRMA 2",1,0,'C');
		$this->SetX(162.5);
		$this->Cell(42.5,5,"Recibe conforme",1,0,'C');
		$this->SetX(10);
		$this->Cell(42.5,27,"",1,0,'C');
		$this->SetX(57.5);
		$this->Cell(42.5,27,"",1,0,'C');
		$this->SetX(110);
		$this->Cell(42.5,27,"",1,0,'C');
		$this->SetFont('Arial','',6);
				
		$this->SetXY(164.5,245);
		$this->Cell(42.5,7,"Nombre _________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(164.5);
		$this->Cell(42.5,7,"Fecha __________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(164.5);
		$this->Cell(42.5,7,"C.I. ".utf8_decode('ó')." R.I.F. ______________________",0,0,'L');
		$this->Ln(5);
		$this->SetX(164.5);
		$this->Cell(42.5,6,"Firma __________________________",0,0,'L');
		$this->Ln(5);
		$this->SetXY(162.5,240);
		$this->Cell(42.5,27,"",1,0,'C');
		
	}
}

//$pdf=new PDF('P','mm','Legal');
$pdf=new PDF('P');
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');
$pdf->SetFont('Arial','',10);
$json = new Services_JSON;
$cadena = str_replace("\\","",$oCheque->json);//-->OJO, MUY IMPORTANTE !!!
$vector = $json->decode($cadena);
$x = count($vector);
$docs = '';

for ($i=0;$i<$x;$i++)
{
	$docs.= '\''.$vector[$i]->nroref.'\',';
}
$docs = rtrim($docs,',');

// CHEQUE
if($tipoReporte=='3'){
    $monto =  $oCheque->monto;
    $nrocuenta = utf8_decode($oCheque->cedente);
    $nrocheque = utf8_decode($oCheque->nrodoc);
}else {
    $monto = $oCheque->montoTotalCheque($conn, $oCheque->nrodoc);
    $nrocuenta = utf8_decode($oCheque->nro_cuenta);
    $nrocheque = utf8_decode($oCheque->nro_cheque);
}
$pdf->SetX(10);
$pdf->Cell(80,5, $nrocuenta ,0, 0, 'L','L');
$pdf->Cell(55,5, $nrocheque,0, 0, 'C','C');
$pdf->SetX(130);
if($tipoReporte!='3')
    $pdf->Cell(70,5, '***********'.utf8_decode(muestraFloat($monto)),0, 0, 'R','R');
else
    $pdf->Cell(70,5, '***********'.$monto,0, 0, 'R','R');
$pdf->ln(5);
$pdf->SetX(10);
$pdf->Cell(195,5, utf8_decode($oCheque->banco),0, 0, 'L','C');
$pdf->ln(15);
$pdf->SetXY(20,19);
if($tipoReporte!='3')
    $pdf->Cell(185,5, '***'. utf8_decode($oCheque->nomBenef).'***',0, 0, 'L','C');
else
    $pdf->Cell(185,5, '*** SAMAT ***',0, 0, 'L','C');
$pdf->ln(5);

if($tipoReporte!='3'){
    $montoLetras = num2letras($monto,false,true);
	}else{
    //$montoLetras = num2letras(str_replace(',',',',str_replace('.','',$monto)));
    $montoLetras = num2letras(guardafloat($monto),false,true);
	}

//$desc_monto = $montoLetras;
$desc_monto = dividirStr($montoLetras, 400);
$pdf->SetXY(25,26);
$pdf->Cell(195,5, strtoupper(utf8_decode($desc_monto[0])),0, 0, 'L','L');
$pdf->SetXY(25,32);
if($desc_monto[1] != '')
  $pdf->Cell(190,5, strtoupper(utf8_decode($desc_monto[1])),0, 0, 'L','L');
$pdf->ln(5);
$separaFecha= explode('-',$oCheque->fecha);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->SetX(10);
$pdf->Cell(195,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, '','L');
$pdf->ln(5);
$pdf->SetX(10);
$pdf->Cell(195,5,'NO ENDOSABLE, CADUCA A LOS 90 DIAS',0,0, '','L');
$pdf->ln(15);
$pdf->SetX(10);
if($tipoReporte!='3')
    $pdf->Cell(195,5, 'OP - '.$docs,0, 0, 'C','L');

$pdf->SetFont('Arial','',8);
$pdf->ln(12);
$pdf->SetX(10);
$pdf->Cell(195,5, 'CONCEPTO',1, 0, 'C','C');
$pdf->Ln(5);
$pdf->SetDrawColor (255,255,255);
$pdf->SetWidths(array(25,20,25,75,20,20,20));
$pdf->SetAligns(array('C','C','C','L','C','C','C'));
//-- Para Cheques o distinto de transferencias ------------------------------------------------
if($tipoReporte!='3'){
    
    $pdf->Row(array("Documento", "Soporte", "Fecha", "Descripci".utf8_decode('ó')."n", "Monto", "Retenciones", "Monto"),11);
    $pdf->Ln(5);
    $pdf->SetAligns(array('C','C','C','L','R','R','R'));
	
    
                $orden_c="SELECT
                       B.nrodoc,
                       A.fecha,
                       B.fecha as fecha_ord_pago,
                       B.descripcion,
                       B.id_tipo_solicitud_si,
                       A.monto,
                       B.monto_si as monto_si,
                       B.nroref,
                       A.nrofactura,
                       B.nrodoc_anticipo,
                       A.iva_retenido,
                       COALESCE(A.id_proveedor,0) As id_proveedor
                    FROM
                       finanzas.orden_pago AS B
                       LEFT Join finanzas.facturas AS A ON B.nrodoc = A.nrodoc
                    WHERE
                       B.nrodoc in (".rtrim($docs).")";
        
        
              /*  $orden_c="SELECT
                       nrodoc,
                       fecha,
                       descripcion,
                       monto_si as monto,
                       nrodoc_anticipo,
                       0 as iva_retenido
                    FROM
                       finanzas.orden_pago
                    WHERE
                       nrodoc in (".rtrim($docs).")"; */
        
	
    //die($orden_c);
    //var_dump($orden_c);
    //echo $orden_c;
    $r_orden_c = $conn->Execute($orden_c);
    //die(print_r($r_orden_c));
    $Total=0;
    while(!$r_orden_c->EOF)
    {
        //Salto de Pagina, se crea una nueva pagina para casos donde el detalle supere la capacidad de una pagina carta
        if($pdf->GetY() >=230 and $pdf->GetY() <240 and $tipoReporte!='3'){
            $pdf->setX(1);
            $pdf->SetFont('Arial','B',8);
            $pdf->SetAligns(array('C','C','C','R','R','R','R'));
            $pdf->Row(array("","", "", "","" , "",  ""),T,11);
            $pdf->SetDrawColor (0,0,0);
            $pdf->SetXY(10, 74);
            $pdf->Cell(195,5,"",1,0,'C');
            $pdf->Ln(5);
            $pdf->SetX(10);
            $pdf->Cell(195,160,"",1,0,'C');
            $pdf->SetXY(180, 236);
            $pdf->Cell(25,3,"CONTINUA...",1,0,'C');
            $pdf->Ln(110);
            //banda_firma($pdf);
            $pdf->AddPage('P','Letter');
            $pdf->SetFont('Arial','',8);
            $pdf->Ln(25);
            $pdf->SetDrawColor (255,255,255);

        }
        //echo $r_orden_c->fields['id_tipo_solicitud_si'];
        $r_orden_c_monto = ($r_orden_c->fields['monto'] != null) ? $r_orden_c->fields['monto'] : 0;
        if(is_null($r_orden_c->fields['iva_retenido']))
            $r_orden_c_iva_retenido = 0;
        else
            $r_orden_c_iva_retenido = $r_orden_c->fields['iva_retenido'];
        //ECHO 'AQUI';
        if($r_orden_c->fields['id_tipo_solicitud_si']==0){
            $nroref=$r_orden_c->fields['nroref'];
            $q="select nroref from finanzas.solicitud_pago where nrodoc='$nroref'";
            $res = $conn->Execute($q);
            $referencia=$res->fields['nroref'];
            

            //DIE('vENENO');
            //$pdf->Row(array($r_orden_c->fields['nrofactura'], $oOrdenPago->nrorefcomp, muestrafecha($r_orden_c->fields['fecha']), $r_orden_c->fields['descripcion'],  muestraFloat($r_orden_c->fields['monto']), "", ""),11);
            $pdf->Row(array($r_orden_c->fields['nrofactura'], $referencia, muestrafecha($r_orden_c->fields['fecha']), utf8_decode($r_orden_c->fields['descripcion']),  muestraFloat($r_orden_c_monto), "", ""),11);

            //$pdf->Row(array("","","", "I.V.A.", "", muestraFloat($r_orden_c->fields['iva_retenido']), ""),11);
            //die($r_orden_c_iva_retenido);
            if($r_orden_c_iva_retenido <> 0)
                $pdf->Row(array("","","", "I.V.A.", "", muestraFloat($r_orden_c_iva_retenido), ""),11);

            //$Total+=$r_orden_c->fields['monto']-$r_orden_c->fields['iva_retenido'];
            $Total+=$r_orden_c_monto-$r_orden_c_iva_retenido;

            $strfactura=$r_orden_c->fields['nrofactura'];
            if($r_orden_c->fields['id_proveedor'] > 0 ) $strfactura = $r_orden_c->fields['id_proveedor']."-".$strfactura;
            
            $q_retencion=" SELECT B.mntret,
                                       A.descri,
                                       B.codret,
                                       A.es_iva
                               FROM  finanzas.retenciones_adiciones AS A
                               INNER JOIN finanzas.relacion_retenciones_orden AS B ON B.codret = A.id
                               WHERE B.nrofactura =  '".$strfactura."' AND
                                     B.nrodoc in (".trim($docs).")";
            //die($q_retencion);
            $r_retencion=$conn->Execute($q_retencion);
            //die(var_dump($r_retencion));

            //Impresiones de Retenciones
            while(!$r_retencion->EOF)
            {
                    switch($r_retencion->fields['es_iva']){
                            case '1':
                                $nomRet = 'IVA';
                                break;
                            case '2':
                                //$nomRet = 'ISLR';
                                $nomRet = $r_retencion->fields['descri'];
                                break;
                            case '3': $nomRet = 'Impuesto Municipal';
                                $nomRet = $r_retencion->fields['descri'];
                                break;
                            case '4': $nomRet = 'Impuesto Nacional';
                                $nomRet = $r_retencion->fields['descri'];
                                break;
                    }
                    $pdf->Row(array("","", "", $nomRet, "", muestraFloat($r_retencion->fields['mntret']), ""),11);
                    $Total-=$r_retencion->fields['mntret'];
                    $r_retencion->movenext();
                    //die('entro');
            }
      }else{

          //echo('else');
            //$pdf->Row(array('', '', muestrafecha($oCheque->fecha), $oCheque->concepto,  muestraFloat($monto), "", ""),11);
            //$Total += $monto;
            //Cambios por Ricardo Camejo 20-01-2010
            $pdf->Row(array("-",$r_orden_c->fields['nrodoc_anticipo'],muestrafecha($r_orden_c->fields['fecha_ord_pago']),utf8_decode($r_orden_c->fields['descripcion']),muestraFloat($r_orden_c->fields['monto_si']),"",""),11);

            //$pdf->Row(array("","","", "I.V.A.", "", muestraFloat($r_orden_c->fields['iva_retenido']), ""),11);
            if($r_orden_c_iva_retenido > 0)
               $pdf->Row(array("","","", "I.V.A.", "", muestraFloat($r_orden_c_iva_retenido), ""),11);

            //$Total+=$r_orden_c->fields['monto']-$r_orden_c->fields['iva_retenido'];
            $Total+=$r_orden_c->fields['monto_si']-$r_orden_c_iva_retenido;

            $q_retencion="	SELECT
                                                    B.mntret,
                                                    A.descri,
                                                    B.codret,
                                                    A.es_iva
                                            FROM
                                                            finanzas.retenciones_adiciones AS A
                                                    INNER Join finanzas.relacion_retenciones_orden AS B ON B.codret = A.id
                                            WHERE
                                                    B.nrofactura =  '".$r_orden_c->fields['nrofactura']."' AND
                                                    B.nrodoc in (".trim($docs).")";
            //die($q_retencion);
            $r_retencion=$conn->Execute($q_retencion);
            //die(var_dump($r_retencion));
            while(!$r_retencion->EOF)
            {
                    switch($r_retencion->fields['es_iva']){
                            case '1':
            $nomRet = 'IVA';
                                    break;
                            case '2':
            //$nomRet = 'ISLR';
            $nomRet = $r_retencion->fields['descri'];
                                    break;
                            case '3': $nomRet = 'Impuesto Municipal';
                              $nomRet = utf8_decode($r_retencion->fields['descri']);
                                    break;
                            case '4': $nomRet = 'Impuesto Nacional';
                              $nomRet = utf8_decode($r_retencion->fields['descri']);
                                    break;
                    }
                    $pdf->Row(array("","", "", $nomRet, "", muestraFloat($r_retencion->fields['mntret']), ""),11);
                    $Total-=$r_retencion->fields['mntret'];
                    $r_retencion->movenext();
                    //die('entro');
            }


    } //Fin if
            $r_orden_c->movenext();

    }
    //Busco si hay Anticipos aplicados
    //Datos Orden de pago
    $qr="SELECT A.nroref,b.monto_anticipo FROM finanzas.relacion_cheque AS A INNER JOIN finanzas.orden_pago AS B ON A.nroref=B.nrodoc WHERE A.nrodoc='$oCheque->nrodoc'";
    $rr= $conn->execute($qr);

    if($rr->fields['monto_anticipo'] > 0 ){
            $pdf->Row(array("","", "", utf8_decode("Amortización Anticipo"), "", muestraFloat($rr->fields['monto_anticipo']), ""),11);
          $Total-= $rr->fields['monto_anticipo'];
    }
    //$pdf->Line();
    $pdf->setX(1);
    $pdf->SetFont('Arial','B',8);
    
    $pdf->SetAligns(array('C','C','C','R','R','R','R'));
    $pdf->Row(array("","", "", "Total a Pagar","" , "",  muestraFloat($Total)),T,11);
    $pdf->SetDrawColor (0,0,0);
}else {
    $pdf->setX(1);
    $pdf->ln(10);
    $pdf->SetFont('Arial','B',8);
    $pdf->SetAligns(array('C','C','C','C','C','C','C'));
    $pdf->Row(array("","", "", "TRASLADO DE FONDOS AL $oCheque->receptora","" , "", ""),T,11);
    $pdf->SetDrawColor (0,0,0);
}//Fin tiporeporte

//Consulto pagina actual para pie
if($pdf->PageNo() >1){
    $pdf->SetXY(10, 25);
    $pdf->SetFont('Arial','',8);
    $pdf->SetAligns(array('C','C','C','L','C','C','C'));
    $pdf->SetWidths(array(20,20,20,80,20,20,15));
    $pdf->Row(array("Documento", "Soporte", "Fecha", "Descripci".utf8_decode('ó')."n", "Monto", "Retenciones", "Monto"),10);
    $pdf->SetX(10);
    $pdf->Cell(195,160,"",1,0,'C');
    $pdf->Ln(110);
}else{
    $pdf->SetXY(10, 74);
    $pdf->Cell(195,5,"",1,0,'C');
    $pdf->Ln(5);
    $pdf->SetX(10);
    $pdf->Cell(195,160,"",1,0,'C');
    $pdf->Ln(110);
}
//Banda de Firma
//banda_firma($pdf);

function banda_firma($pdf){

		$pdf->SetFont('Arial','',8);
		$pdf->SetXY(10,240);
		$pdf->Cell(42.5,5,"Elaborado por",1,0,'C');
		$pdf->SetX(57.5);
		$pdf->Cell(42.5,5,"FIRMA 1",1,0,'C');
		$pdf->SetX(110);
		$pdf->Cell(42.5,5,"FIRMA 2",1,0,'C');
		$pdf->SetX(162.5);
		$pdf->Cell(42.5,5,"Recibe conforme",1,0,'C');
		$pdf->SetX(10);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetX(57.5);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetX(110);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetFont('Arial','',6);
				
		$pdf->SetXY(164.5,245);
		$pdf->Cell(42.5,7,"Nombre _________________________",0,0,'L');
		$pdf->Ln(6);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"Fecha __________________________",0,0,'L');
		$pdf->Ln(6);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"C.I. ".utf8_decode('ó')." R.I.F. ______________________",0,0,'L');
		$pdf->Ln(5);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"Firma __________________________",0,0,'L');
		$pdf->Ln(5);
		$pdf->SetXY(162.5,240);
		$pdf->Cell(42.5,28,"",1,0,'C');
		
} //Fin banda_firma

$docs = str_replace('\'',' ',$docs);
$varAux = explode(',',$docs);
foreach($varAux as $aux){
	//echo $aux.'<br \>';
	retencion_iva($conn,trim($aux),$escEnEje,$pdf);
        retencion_iva_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_islr($conn,trim($aux),$escEnEje,$pdf);
	retencion_islr_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_municipal($conn,trim($aux),$escEnEje,$pdf);
	retencion_municipal_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_nacional($conn,trim($aux),$escEnEje,$pdf);
	retencion_nacional_tercero($conn,trim($aux),$escEnEje,$pdf);
}
//echo('loop');

$pdf->Output();

?>
