<?
include ("comun/ini.php");
$pagina = $_REQUEST['pagina'];
$tamano_pagina = 20;

if (!$pagina) {
    $inicio = 10;
    $pagina=1;
}
else {
    $inicio = ($pagina - 1) * $tamano_pagina;
}
$statusB=(empty($_REQUEST['EstatusB']) ? 0 : $_REQUEST['EstatusB']);
$odepartamento= new departamento();
$cdepartamento=$odepartamento->get_all($conn, $_SESSION['EmpresaL'],$inicio,$tamano_pagina,'A.dep_ord',$statusB);
$total=$odepartamento->totalFilas;

 if(is_array($cdepartamento)) {
 ?>
<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
     <tr class="cabecera">
        <td>Orden</td>
        <td>Nombre</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <?
         foreach($cdepartamento as $departamento) {
            ?>
    <tr class="filas">
        <td><?=$departamento->dep_ord?></td>
        <td align="center"><?=$departamento->dep_nom?></td>
        <td align="center"><a href="?accion=del&int_cod=<?=$departamento->int_cod?>" onclick="if (confirm('Si presiona Aceptar ser� eliminada esta informaci�n')){ return true;} else{return false;}"  title="Eliminar Registro"><img src="images/eliminar.gif" border="0" ></a></td>
        <td align="center"><a href="#" onclick="updater('<?=$departamento->int_cod?>'); return false;" title="Modificar � Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
    </tr>
        <?
        }

        $total_paginas = ceil($total / $tamano_pagina);
    ?>
    <tr class="pietabla">
        <td colspan="7" align="center">
                <?
                for ($j=1;$j<=$total_paginas;$j++) {
                    if ($j==1) {
                        if ($j==$pagina)
                            echo "<span>".$j."</span>";
                        else 
                            echo '<span style="cursor:pointer" onclick="busca('.$j.',$F(\'EstatusB\'));">'.$j.'</span>';
                    }
                    else {
                        if ($j==$pagina)
                            echo "<span>- ".$j."</span>";
                        else
                            echo '<span style="cursor:pointer" onclick="busca('.$j.',$F(\'EstatusB\'));">- '.$j.'</span>';
                    }
                }
                ?>
        </td>
    </tr>
    <tr class="pietabla">
        <td colspan="7" align="center"> Pagina <strong><?=$_REQUEST['pagina']?></strong> de <strong><?=$total_paginas?></strong></td>
    </tr>
</table>
    <?
}
else {
    echo "No hay registros en la bd";
}
?>