var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
function LZ(x) {return(x<0||x>9?"":"0")+x}

function pad(number, length) {

    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;

}

function MM_jumpMenu(page,targ,selObj,restore){ //Se7h V.1
    var url=page+selObj.options[selObj.selectedIndex].value;
    //alert (url);
    eval(location=url);
}
function popup(theURL,winName,features) {//se7h
    window.open(theURL,winName,features);
}
/*SI SELECCIONO UN REQ DENTRO DE REQUISITOS_PROVEEDOR.PHP, ACTUALISO EL SELF Y MUSTRO*/
/*de momento no la estoy utilizando: se7h: 01/03/2006
function MM_jumpMenu_reqs(page,id,targ,selObj,restore){ //Se7h V.1
	var url=page+selObj.options[selObj.selectedIndex].value;
	eval(location=url);
}
*/
function deleteLastRow(tblName){
    var tbl = document.getElementById(tblName);
    if (tbl.rows.length > 1) tbl.deleteRow(tbl.rows.length - 1);
}

function onlynumbers(evento){ //valida q la entrada sea solo numerica, pero permita la 'coma'
    if (!evento) var evento = window.event;
    if (evento.keyCode) code = evento.keyCode;
    else if (evento.which) code = evento.which;
    if ((code < 48) || (code > 57)) return false;
    if (code == 44) return true;
}

//se7h: esta podria ser pero de momento uso la de arriba

function noAlpha(obj){
    reg = /[^0-9.]/g;
    obj.value =  obj.value.replace(reg,"");
}
	
function onlyNumbersCI(evento){ //valida q la entrada sea solo numerica
    if (!evento) var evento = window.event;
    if (evento.keyCode) code = evento.keyCode;
    else if (evento.which) code = evento.which;
    //	 alert(code)
    // si la tecla presionada es
    if (code == 8 || // backspace
        code == 9 || // tab
        code == 35 || // flecha izq
        code == 36 || // flecha izq
        code == 37 || // flecha izq
        code == 39 || // flecha der
        code == 46 || // suprimir
        code == 13) // enter
        return true;
	
    if ((code >= 48 && code <= 57) || (code >= 96 && code <= 105))
        return true
    else
        return false;
}

function formatoNumero(fld,e) {
    var milSep='.';
    var decSep=',';
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
	
    var whichCode = (e.which) ? e.which : e.keyCode;

    // 8 y 46
    if (whichCode == 13) return true;  // Enter
    if (whichCode >= 96 && whichCode <= 105)
        whichCode -= 48;
    key = String.fromCharCode(whichCode);  // Get key value from key code
	
    if (strCheck.indexOf(key) == -1){
        if(whichCode == 8 || whichCode == 9 || whichCode == 46){
            return true;
        }else{
            return false;  // Not a valid key
        }
    }
	
    len = fld.value.length;
	
    for(i = 0; i < len; i++)
        if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) fld.value = '';
    if (len == 1) fld.value = '0'+ decSep + '0' + aux;
    if (len == 2) fld.value = '0'+ decSep + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += milSep;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        fld.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
            fld.value += aux2.charAt(i);
        fld.value += decSep + aux.substr(len - 2, len);
    }
    return false;
}

// convierte una cifra de formato USA a formato VEN
// similar a la funcion php muestrafloat en $appRoot/lib/functiones.php
function muestraFloat(nStr, dec){
    nStr += '';
    dec = (arguments.length > 1) ? dec : 2;
    x = nStr.split('.');
    x1 = x[0];
    x2 = ',' + ((x.length > 1) ? x[1].substr(0, dec) : '00');
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

// recibe un numero con formato VEN, y la convierte en un formato 
// utilizable para ejecutar operaciones matematicas
function usaFloat(nStr){
    return parseFloat(replace_caracter(replace_caracter(nStr, '.', ''), ',', '.'));
}

function replace_caracter(inputString, fromString, toString) {
    var temp = inputString;
    if (fromString == "") {
        return inputString;
    }
    if (toString.indexOf(fromString) == -1) {
        while (temp.indexOf(fromString) != -1) {
            var toTheLeft = temp.substring(0, temp.indexOf(fromString));
            var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
            temp = toTheLeft + toString + toTheRight;
        }
    } else {
        var midStrings = new Array("~", "`", "_", "^", "#");
        var midStringLen = 1;
        var midString = "";

        while (midString == "") {
            for (var i=0; i < midStrings.length; i++) {
                var tempMidString = "";
                for (var j=0; j < midStringLen; j++) {
                    tempMidString += midStrings[i];
                }
                if (fromString.indexOf(tempMidString) == -1) {
                    midString = tempMidString;
                    i = midStrings.length + 1;
                }
            }
        }
	  
        while (temp.indexOf(fromString) != -1) {
            var toTheLeft = temp.substring(0, temp.indexOf(fromString));
            var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
            temp = toTheLeft + midString + toTheRight;
        }

        while (temp.indexOf(midString) != -1) {
            var toTheLeft = temp.substring(0, temp.indexOf(midString));
            var toTheRight = temp.substring(temp.indexOf(midString)+midString.length, temp.length);
            temp = toTheLeft + toString + toTheRight;
        }
    }
    return temp;
}

function popup(url, ancho, alto, left){
    left = left == undefined ? "300" : left;
    window.open(url,
        "popup1",
        "width="+ancho+", height="+alto+", scrollbars=yes, menubar=no, location=no, resizable=no,left = "+left+",top = 300");
}

// recibe como parametro una cadena que finaliza en un numero y devuelve ese numero
function getFila(nombre){
    var aNombre = nombre.split("_");
    return aNombre[aNombre.length - 1];
}
// CEPV.SN 16-08-2006   
function Cadena(Vector){
    var Palabra;
    Vector=Vector.split(' ');
    if(Vector[1]){
        Palabra=Vector[1];
        Palabra=Palabra.substring(0,1)+".";
        return Vector[0]+" "+Palabra;
    }
    return Vector[0];
}
//CEPV.EN
function mostrarFecha(cadena){
    var Fecha= new String(cadena)	// Crea un string
	
    // Cadena A�o
    var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))
    // Cadena Mes
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
    // Cadena D�a
    var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))
	
	
    var fecha_nueva = Dia + '/' + Mes + '/' + Ano ;
	
    return fecha_nueva;
}
// CEPV EN
function digitosMin(el, tam){
    elem = $(el);
    if(elem.value.length != tam){
        alert("Este campo debe contener al menos " + tam + " digitos");
        elem.value = "";
        elem.focus();
    }
}

// Ismael Depablos 16/11/06
function validarEmail(valor) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
        return (true);
    } else {
        alert("La direcci�n de email es incorrecta.");
        return (false);
    }
}
 
function redondeo(rnum, rlength) { // Parametros: rnum numero a redondear, rlength numero de decimales
    //alert(rnum + ' ' + rlength);
    var numero = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
    //alert(numero);
    return numero;
}

function datedifference(strdate1,strdate2)
{
    var bldatediff = false

    //Start date split to UK date format and add 31 days for maximum datediff
    startd= strdate1.split("/");
    starttime = new Date(startd[2],startd[1]-1,startd[0]);

    //End date split to UK date format
    endd= strdate2.split("/");
    endtime = new Date(endd[2],endd[1]-1,endd[0]);

    if(endtime >= starttime)
    {
        bldatediff = true
    }

    return bldatediff
}
function setDateDays(strdate,days)
{

    //Start date split to UK date format 
    sdate= strdate.split("/");
    ddate= new Date(sdate[2],sdate[1]-1,sdate[0]);

    ddate.setDate(ddate.getDate()+days);
    return ddate;

}

function formatDate(date,format) {
    format=format+"";
    var result="";
    var i_format=0;
    var c="";
    var token="";
    var y=date.getYear()+"";
    var M=date.getMonth()+1;
    var d=date.getDate();
    var E=date.getDay();
    var H=date.getHours();
    var m=date.getMinutes();
    var s=date.getSeconds();
    var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
    // Convert real date parts into formatted versions
    var value=new Object();
    if (y.length < 4) {
        y=""+(y-0+1900);
    }
    value["y"]=""+y;
    value["yyyy"]=y;
    value["yy"]=y.substring(2,4);
    value["M"]=M;
    value["MM"]=LZ(M);
    value["MMM"]=MONTH_NAMES[M-1];
    value["NNN"]=MONTH_NAMES[M+11];
    value["d"]=d;
    value["dd"]=LZ(d);
    value["E"]=DAY_NAMES[E+7];
    value["EE"]=DAY_NAMES[E];
    value["H"]=H;
    value["HH"]=LZ(H);
    if (H==0){
        value["h"]=12;
    }
    else if (H>12){
        value["h"]=H-12;
    }
    else {
        value["h"]=H;
    }
    value["hh"]=LZ(value["h"]);
    if (H>11){
        value["K"]=H-12;
    } else {
        value["K"]=H;
    }
    value["k"]=H+1;
    value["KK"]=LZ(value["K"]);
    value["kk"]=LZ(value["k"]);
    if (H > 11) {
        value["a"]="PM";
    }
    else {
        value["a"]="AM";
    }
    value["m"]=m;
    value["mm"]=LZ(m);
    value["s"]=s;
    value["ss"]=LZ(s);
    while (i_format < format.length) {
        c=format.charAt(i_format);
        token="";
        while ((format.charAt(i_format)==c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (value[token] != null) {
            result=result + value[token];
        }
        else {
            result=result + token;
        }
    }
    return result;
}
   
   
  
    
//Mas en: http://javascript.espaciolatino.com/
//Objeto oNumero
//function oNumero(numero)
//{
//    //Propiedades
//    this.valor = numero || 0
//    this.dec = -1;
//    //Métodos
//    this.formato = numFormat;
//    this.ponValor = ponValor;
//    //Definición de los métodos
//    function ponValor(cad)
//    {
//        if (cad =='-' || cad=='+') return
//        if (cad.length ==0) return
//        if (cad.indexOf('.') >=0)
//            this.valor = parseFloat(cad);
//        else
//            this.valor = parseInt(cad);
//    }
//    function numFormat(dec, miles)
//    {
//        var num = this.valor, signo=3, expr;
//        var cad = ""+this.valor;
//        var ceros = "", pos, pdec, i;
//        for (i=0; i < dec; i++)
//        ceros += '0';
//        pos = cad.indexOf('.')
//        if (pos < 0)
//            //cad = cad+"."+ceros;
//            cad = cad+","+ceros;    
//        else
//            {
//            pdec = cad.length - pos -1;
//            if (pdec <= dec)
//                {
//                for (i=0; i< (dec-pdec); i++)
//                    cad += '0';
//                }
//            else
//                {
//                num = num*Math.pow(10, dec);
//                num = Math.round(num);
//                num = num/Math.pow(10, dec);
//                cad = new String(num);
//                }
//            }
//        pos = cad.indexOf('.')
//        if (pos < 0) pos = cad.lentgh
//        if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
//               signo = 4;
//        if (miles && pos > signo)
//            do{
//                expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
//                cad.match(expr)
//                //cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
//                cad=cad.replace(expr, RegExp.$1+'.'+RegExp.$2)
//                }
//        //while (cad.indexOf(',') > signo)
//        while (cad.indexOf('.') > signo)    
//            //if (dec<0) cad = cad.replace(/\./,'')
//            if (dec<0) cad = cad.replace(/\,/,'')
//                return cad;
//    }
//}//Fin del objeto oNumero:    
    
