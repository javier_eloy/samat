<?php
/* 
 * Programador: Ricardo Camejo
 * Fecha: 22/03/2010
 */
include ("comun/ini.php");

        //Actualizacion de cuentas contables de acumulados de retenciones tanto en nomina como en finanzas
        //este proceso se realiza con store procedure en la base datos
        $query = 'Select * From finanzas.carga_ret_aportes()';
        $rquery = $conn->Execute($query);


$opcion = $_REQUEST['opcion'];
$especifico = $_REQUEST['especifico'];
$desde = guardafecha($_REQUEST['desde']);
$hasta = guardafecha($_REQUEST['hasta']);
//die("$opcion:$especifico:$desde:$hasta");
//$cDocRef = traFondosTerceros::getAporRetXpagar($conn, $descripcion,$desde,$hasta);

if($opcion==1)
 $qvalor = "select * from finanzas.validar_retenciones('$desde','$hasta',$especifico)";
else
 $qvalor = "select * from rrhh.calcula_pago_aportes('$desde','$hasta',$especifico,0::smallint)";
//die($qvalor);
$rqvalor = $conn->Execute($qvalor);
if(!$rqvalor->EOF)
    if($opcion==1)
        echo number_format($rqvalor->fields['validar_retenciones'],2,',','.');
    else
        echo number_format($rqvalor->fields['calcula_pago_aportes'],2,',','.');
?>