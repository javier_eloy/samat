<?php
/*
* Programador: Ricardo Camejo
* Fecha: 28/08/2009
* Variables globales para reportes
*/


define("PAIS","Rep".utf8_decode('ú')."blica Bolivariana de Venezuela");
define("ORGANISMO_NOMBRE","Servicio Aut".utf8_decode('ó')."nomo Municipal de Administraci".utf8_decode('ó')."n Tributaria");
define("ORGANISMO_SIGLAS","S.A.M.A.T");
define("ENTE","Alcald".utf8_decode('í')."a del Municipio Maracaibo");
define("UBICACION","Maracaibo, Edo. Zulia");
define("DEPARTAMENTO","Gerencia de Administraci".utf8_decode('ó')."n y Finanzas");
define("DIRECCIONFISCAL", "AV. 3F ENTRE CALLE 81 Y 81A EDIF. MAELGA 81-35 PISO PB LOCAL SAMAT SECTOR VALLE FRIO");
define("RIF","G-20002908-0");
define("FAOVCONTRATO","10001613");
define("TELFENTE","(0261)793-4855,793-5869,793-5508");

/* Personal */

define("INTENDENTE","Econ. Maryana Machado");
define("GTEADMINISTRACION","Econ. Rosangel Butr".utf8_decode('ó')."n");
define("CONTROLINTERNO","Econ. Elio Carroz");
define("JEFEPRESUPUESTO","Econ. Elio Carroz");
define("CONTABILIDAD","Lcda. Milagros Rinc".utf8_decode('ó')."n");
define("JEFERRHH", "Abog. Adriana Hernandez");
define("CARGOJEFERRHH", "Jefe de RRHH y N".utf8_decode('ó')."mina"); //'Coordinador de Recursos Humanos'
define("JEFEADMINISTRACION","Lcda. Dohilme Rodriguez");
define("JEFETESORO","T.S.U. Edwin Leal");

?>