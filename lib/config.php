<?
session_cache_limiter('private'); // CEPV.070906 permite abrir los reportes pdf en el IE... 
session_start();
/*
 * VARIABLES DE CONFIGURACION:
 * appRoot: 		Ubicacion fisica de la aplicacion
 * webRoot: 	Ubicacion HTTP de la aplicacion
 * DBuser:			Nombre de usuario de la base de datos
 * DBpass:			Password del usuario de la base de datos
 * DBname:			Nombre de la base de datos
 * DBserver:		Nombre del servidor de bases de datos
 */

// este archivo es necesario ya que contiene las funciones appRoot() y webRoot()
require("lib/ini.func.php");
$appRoot = appRoot();
$webRoot = webRoot();


// Parametros necesarios para conectarse a la BD
$DBuser = "puser";
$DBpass = "1234";
$DBserver ="localhost:5432";
$DBname = "scg3_2012";


// Establecemos el encoding por defecto del sistema, debe ser el mismo de la BD
header("Content-Type: text/html; charset=UTF-8");
header("Cache-Control: no-cache, must-revalidate"); //no guardar en CACHE 
header("Pragma: no-cache");
// hago la conex a la BD
include($appRoot . "/lib/conn.php");

// Incluyo las librerias
include($appRoot . "/lib/core.lib.php");

//Variable para marcar cual es id del proveedor, en el cual esta instalada la aplicacion.
//Esto no es la mejor forma de establecer este campo, pero hay que hacer un estudio mas fondo de sus implicaciones
$idProveedorEnte = 777;

// Politicas de comportamiento
//Divididas por modulo
//Finanzas: F
//Contabilidad: C
//Nomina: N
//PResupuesto: P
//Compras: O
//General: G
//El nombre de la variable debe estar compuesto por Primera letra segun el modulo, seguido de un consecutivo de 4 digitos.
define("F0001","false");  //Numero de Orden de Pago automatico.
define("F0002","false");  //Validar disponibilidad presupuestaria en Orden de Servicio/trabajo
define("F0003","false");  //Permite afectar una pp multiples veces en el mismo documento de ayuda
define("F0004","false");  //Validar disponibilidad presupuestaria en ayuda
define("F0005","false");  //Permite afectar una pp multiples veces en el mismo documento de ayuda
define("F0006","false");  //Validar disponibilidad presupuestaria en Caja Chica


//Politicas de presupuesto
define("P0001","false");  //Utilizar anio actual como anio en las transacciones, y no el anio segun el presupuesto en ejecucion.
?>
