<?
class tipos_solicitud_sin_imp {

    // Propiedades

    var $id;
    var $descripcion;
    var $cc;
    var $anio;

    var $total;

    function get($conn, $id) {
        $q = "SELECT * FROM finanzas.tipos_solicitud_sin_imp ";
        $q.= "WHERE id='$id'";
        //die($q);
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $r->fields['id'];
            $this->descripcion = $r->fields['descripcion'];
            $this->cc = $r->fields['cuenta_contable'];
            $this->anio = $r->fields['anio'];
            return true;
        }else
            return false;
    }

    function getAll($conn, $orden="id") {
        $q = "SELECT * FROM finanzas.tipos_solicitud_sin_imp ";
        $q.= "ORDER BY $orden ";
        $r = $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new tipos_solicitud_sin_imp;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $descripcion, $cc, $anio) {
        $q = "INSERT INTO finanzas.tipos_solicitud_sin_imp ";
        $q.= "(descripcion, cuenta_contable, anio) ";
        $q.= "VALUES ";
        $q.= "('$descripcion', '$cc', '$anio' ) ";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans()) {
                throw new Exception(__METHOD__,$conn->ErrorNo());
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();

        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;

    }

    function set($conn, $id, $descripcion, $cc, $anio) {
        $q = "UPDATE finanzas.tipos_solicitud_sin_imp SET descripcion='$descripcion', ";
        $q.= "cuenta_contable='$cc', anio = '$anio' ";
        $q.= "WHERE id='$id' ";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans()) {
                throw new Exception(__METHOD__,$conn->ErrorNo());
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();

        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $id) {
        $q = "DELETE FROM finanzas.tipos_solicitud_sin_imp WHERE id='$id'";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans()) {
                throw new Exception(__METHOD__,$conn->ErrorNo());
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
            
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }
}
?>
