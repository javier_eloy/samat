<?
class responsable {

# PROPIEDADES #
    var $msg ="";
    var $hasError = false;
    var $id;
    var $tra_cod;
    var $fecha_inicio;
    var $fecha_fin;
    var $tipo;
    var $nombre;
    var $status;

    var $total;

# METODOS #

    function get($conn, $id) {
        $q = "SELECT A.* FROM puser.responsable A ";
        $q.= "INNER JOIN rrhh.trabajador B ON (A.tra_cod = B.tra_cod) ";
        $q.= "WHERE A.id='$id'";
        //die($q);
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $r->fields['id'];
            $this->tra_cod = $r->fields['tra_cod'];
            $this->fecha_inicio = $r->fields['fecha_desde'];
            $this->fecha_fin = $r->fields['fecha_hasta'];
            $this->tipo = $r->fields['tipo'];
            $this->status = $r->fields['status'];
            $this->nombre = $r->fields['nombre'];

            return true;
        }else
            return false;
    }

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM puser.responsable ";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new responsable;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $tra_cod, $fecha_ini, $fecha_fin, $tipo, $status, $nombre) {

        $flag=true;
        if($status==1) {
            $hasError = !$this->valida_status($conn,$tipo);
            //die(var_dump($resp));
            if ($hasError == false) {
                $q = "INSERT INTO puser.responsable ";
                $q.= "(tra_cod,fecha_desde, ";
                $q.= !empty($fecha_fin) ?   "fecha_hasta, " : "";
                $q.= "tipo, status, nombre) ";
                $q.= " VALUES ";
                $q.= "($tra_cod, '$fecha_ini', ";
                $q.= !empty($fecha_fin) ? "'$fecha_fin', " : "";
                $q.= "'$tipo', '$status', '$nombre') ";
                //die($q);

                $hasError=false;
                $conn->StartTrans();
                try {
                    $r = $conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());
                }catch(ADODB_Exception $e) {
                    $this->msg = setDBError($e,$conn->ErrorNo());
                }catch (Exception $e) {
                    $this->msg = ERROR_ADD." Error #:".$e->getCode();
                    if(!$conn->HasFailedTrans())$conn->FailTrans();
                }
                $hasError = $conn->HasFailedTrans();
                $conn->CompleteTrans();
            }else{
                $this->msg= "Existe Reponsable o Custodio de Caja activo";
            }

        }
        if (!$hasError) $this->msg= REG_ADD_OK;
        return !$hasError;
    }

    function set($conn, $id, $tra_cod, $fecha_ini, $fecha_fin, $tipo, $status, $nombre) {
        $q  = "UPDATE puser.responsable SET ";
        $q .= "tra_cod = $tra_cod, ";
        $q .= "fecha_desde = '$fecha_ini', ";
        $q .= !empty($fecha_fin) ? "fecha_hasta = '$fecha_fin', " : "";
        $q .= "status = '$status', ";
        $q .= "tipo = '$tipo', ";
        $q .= "nombre = '$nombre' ";
        $q .= "WHERE id=$id";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_SET_OK;
        return !$hasError;
    }

    function del($conn, $id) {
        $q = "DELETE FROM puser.responsable WHERE id='$id'";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_DEL_OK;
        return !$hasError;
    }


    function buscar($conn, $nombre='', $desde='',$hasta='', $from=0, $max=0) {
        $q = "SELECT id FROM puser.responsable WHERE 1=1 ";
        if ($nombre != '')
            $q.= " AND nombre ILIKE '%$nombre%' ";
        if ($desde != '')
            $q.= " AND fecha_desde >= '$desde' ";
        if ($hasta != '')
            $q.= " AND fecha_hasta <= '$hasta' ";
        die($q);
//		return $q;
        $rs = ($max!=0) ? $conn->SelectLimit($q, $max, $from):$conn->Execute($q);
        $coleccion = array();
        while (!$rs->EOF) {
            $obj = new responsable;
            $obj->get($conn, $rs->fields['id']);
            $coleccion[] = $obj;
            $rs->movenext();
        }

        return $coleccion;
    }

    function total_registro_busqueda($conn, $nro_cuenta, $banco) {
        $q = "SELECT id FROM puser.responsable WHERE 1=1 ";
        if ($nombre != '')
            $q.= " AND nombre ILIKE '%$nombre%' ";
        if ($desde != '')
            $q.= " AND fecha_desde >= '$desde' ";
        if ($hasta != '')
            $q.= " AND fecha_hasta <= '$hasta' ";

        $rs = $conn->Execute($q);
        return $rs->RecordCount();
    }

    function valida_status($conn,$tipo) {
        $q = "SELECT id FROM puser.responsable ";
        $q.= "WHERE tipo = '$tipo' AND status = '1' ";
        //die($q);
        $r = $conn->Execute($q);
        $count = $r->RecordCount();
        //die(var_dump($r->RecordCount()));
        if($count>0)
            return false;
        else
            return true;
    }

    function getNombre($conn, $tipo, $fecha) {
        $q = "SELECT nombre FROM puser.responsable WHERE 1 = 1 ";
        $q.= "AND tipo = '$tipo' ";
        $q.= !empty($fecha) ? "AND fecha_desde >= '$fecha' AND fecha_hasta < '$fecha' " : "";
        $q.= !empty($fecha) ? "AND status = '2' " : "AND status = '1' ";
        //die($q);
        $r = $conn->Execute($q);
        $this->nombre = $r->fields['nombre'];
        return true;
    }


}

?>