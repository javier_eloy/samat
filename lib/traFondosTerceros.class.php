<?
class traFondosTerceros {

    // Propiedades
    var $msg="";
    var $hasError=false;
    var $nrodoc;
    var $num_com;
    var $descripcion;
    var $fecha;
    var $id;
    var $status;
    var $fechaAnul;
    var $motivoAnul;
    var $detTransferencia;
    var $total;
    var $msj;
    var $beneficiario;
    var $tipo;
    var $tipo_pago;


    function get($conn, $id) {
        $q = "SELECT A.* FROM finanzas.tra_fondos_terceros AS A ";
        $q.= "WHERE id='$id' ";
        $q.= "ORDER BY id";
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id           = $r->fields['id'];
            $this->fecha        = $r->fields['fecha'];
            $this->descripcion  = $r->fields['descripcion'];
            $this->status       = $r->fields['status'];
            $this->nrodoc       = $r->fields['nrodoc'];
            $this->num_com      = $r->fields['num_com'];
            $this->fechaAnul    = $r->fields['fecha_anulado'];
            $this->motivoAnul   = $r->fields['motivo_anulado'];
            $this->beneficiario = $r->fields['beneficiario'];
            $this->tipo         = $r->fields['tipo'];
            $this->tipo_pago    = $r->fields['tipo_pago'];
            $this->getDetalle($conn, $id);
            //die('entro');
            return true;
        }else
            return false;
    } //Fin Get

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM finanzas.tra_fondos_terceros ";
        $q.= "ORDER BY $orden ";
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new traFondosTerceros;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $fecha, $status, $servicio, $beneficiario, $tipo, $tipo_pago,
             $prmjson_det, $txt_numero_cheque,$txt_numero_transferencia, $beneficiario, $txt_observacion, $txt_fecha,$escEnEje) {

        $hasError=false;
        $conn->StartTrans();

        $oComprobante = new comprobante($conn);
        try {

            #Busca el ultimo correlativo ingresado en la tabla de comprobantes contables
            $q      = "SELECT numcom FROM contabilidad.com_enc WHERE id = (SELECT MAX(id) as max FROM contabilidad.com_enc)";
            $rs     = $conn->Execute($q);
            $NumCom = sprintf("%05d", substr($rs->fields['numcom'],8)+1);
            $fech   = split('/',$txt_fecha);
            $numcom = $fech[2].'-'.$fech[1].'-'.$NumCom;
            if($tipo_pago == '0') {
                $nro_doc = $txt_numero_cheque;
                $descripcion = $beneficiario;
                $qcuenta_tercero = 'SELECT b.id FROM contabilidad.fondos_terceros a inner join finanzas.cuentas_bancarias b on a.id_cta = b.id_plan_cuenta';
                $r_qcuenta_tercero = $conn->Execute($qcuenta_tercero);
                $occ = new control_chequera;
                if (!$occ->set_ultimo_cheque_emitido($conn,$r_qcuenta_tercero->fields['id'],$nro_doc))
                        throw new Exception(__METHOD__,$conn->ErrorNo());
            }else {
                $nro_doc = $txt_numero_transferencia;
                $descripcion = $txt_observacion;
            }

            //Se agrega el registro en la tabla de pagos de terceros
            $q = "INSERT INTO finanzas.tra_fondos_terceros ";
            $q.= "(nrodoc, num_com, fecha, status, descripcion, beneficiario, tipo, tipo_pago) ";
            $q.= " VALUES ";
            $q.= "('$nro_doc', '$NumCom', '$fecha', $status, '$descripcion', '$beneficiario', $tipo, $tipo_pago) ";
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

            if( $this->add_detalle($conn, $servicio)==false)
                throw new Exception(__METHOD__,$conn->ErrorNo());

            #Aqui generamos el asiento contable
            $desc   = 'Asiento de Transferencia de Fondos a Terceros';
            //if($tipo == 0) {
                $JsonRec = new Services_JSON();
                $JsonRec = $JsonRec->decode(str_replace("\\","",$prmjson_det));
                if(is_array($JsonRec)) {
                    $ti = 0;
                    foreach($JsonRec as $rec) {
                        if($rec->haber == 0) {
                            $fechas         = substr($rec->descrip,-27);
                            $fechadesde     = substr($fechas,0,10);
                            $fechahasta     = substr($fechas,17);
                            $rec->debe      = $this->get_val_concepto($conn,$fechadesde,$fechahasta,$rec->id_cta,$ti);
                            if(!empty($rec->id_cta)){
                            	if($tipo == 0 ){
                            		$sql_enterado   = "UPDATE rrhh.hist_nom_tra_conc SET estatus_enterado = 2 WHERE estatus_enterado = 1 AND conc_cod = ".$rec->id_cta;
                            	}else {
					$sql_enterado   = "UPDATE finanzas.relacion_retenciones_orden SET  estatus_enterado=2 WHERE estatus_enterado=1";
                            	}
                            	$rsql_enterado  = $conn->Execute($sql_enterado);
                            	if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());
                            }
                            $rec->id_cta    = $this->get_cuenta_concepto($conn,$rec->id_cta,$ti++);
							
                        }
                        if($ti == 2) $ti = 0;
                    }
                    //var_dump($JsonRec);
                    $JsonDef = new Services_JSON();
                    $json_det = $JsonDef->encode($JsonRec);
                    if($oComprobante->create($escEnEje,$fech[2],$fech[1],$numcom,$desc,$fecha,'TFT','R',$numcom,$prmjson_det,'1')===false)
                        throw new Exception(__METHOD__,$conn->ErrorNo());
                }
                

            //}else {
            //    if($oComprobante->create($escEnEje,$fech[2],$fech[1],$numcom,$desc,$fecha,'TFT','R',$numcom,$prmjson_det,'1')===false)
            //        throw new Exception(__METHOD__,$conn->ErrorNo());
            //}
            //$oComprobante->create($escEnEje,$fech[2],$fech[1],$numcom,$desc,guardafecha($_REQUEST['fecha']),'TFT','R',$_REQUEST['nrodoc'],$_REQUEST['json_det'],'1');

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            die($e);
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;

    } //fin add

    function add_detalle($conn, $json_det) {
        $hasError=false;
        $conn->StartTrans();
        try {
            //Busqueda del ultimo id de la tabla maestra
            $q = "SELECT MAX(id) AS id FROM finanzas.tra_fondos_terceros ";
            $r = $conn->Execute($q);
            $id = $r->fields['id'];
            //Leo JSON
            $JsonRec = new Services_JSON();
            $JsonRec = $JsonRec->decode(str_replace("\\","",$json_det));
            if(is_array($JsonRec->transfer)) {
                //die(var_dump($JsonRec->transfer));
                //$iri = 0;
                foreach ($JsonRec->transfer as $oTFT) {
                    $sql = "INSERT INTO finanzas.relacion_tra_fondos_terceros (id_tra, id_cta, descrip,monto) ";
                    $sql.= "VALUES ";
                    $sql.= "($id, $oTFT[2],'$oTFT[0]', ".guardaFloat($oTFT[1]).")"; //.guardaFloat($oTFT[2]).") ";
                    //echo($iri++.'br');
                    //die($sql);
                    $r = $conn->Execute($sql);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());
                    //Actualizacion de retenciones de iva
                    if($oTFT[2] == 4559)
                        $qret = "UPDATE finanzas.facturas SET estatus_enterado=2,id_pago=$id WHERE estatus_enterado=1";
                    else
                        $qret = "UPDATE finanzas.relacion_retenciones_orden SET estatus_enterado=2,id_pago=$id WHERE estatus_enterado=1";
                    $rqret = $conn->Execute($qret);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());
                }
                //die('listo');
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
            //die('primer error');
            //die('primer error');
        }catch (Exception $e) {
            $msg = ERROR." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
            //die('segundo error');
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError)
            $msg= OK;
        if($this)
            $this->msg=$msg;
        //die('Error'.$hasError);
        return !$hasError;
    }

    function add_detalle_retenciones($conn, $json_det) {
        $q = "SELECT MAX(id) AS id FROM finanzas.tra_fondos_terceros ";
        $r = $conn->Execute($q);
        $id = $r->fields['id'];
        $JsonRec = new Services_JSON();
        $JsonRec=$JsonRec->decode(str_replace("\\","",$json_det));
        if(is_array($JsonRec->transfer)) {
            foreach ($JsonRec->transfer as $oTFT) {

                $sql = "INSERT INTO finanzas.relacion_tra_fondos_terceros (id_tra, id_cta, monto) ";
                $sql.= "VALUES ";
                $sql.= "($id, '$oTFT[0]', ".guardaFloat($oTFT[1]).") ";
                $r = $conn->Execute($sql);
            }
            if($r)
                return true;
            else
                return false;
        }

    }

    function getDetalle ($conn,$id) {

        $q = "SELECT id_cta, monto, descrip FROM finanzas.relacion_tra_fondos_terceros ";
        $q.= "WHERE id_tra = $id ";
        $r = $conn->Execute($q);
        //die($q);
        while (!$r->EOF) {
            $tft                = new traFondosTerceros;
            $tft->id_cta        = $r->fields['id_cta'];
            $tft->monto         = $r->fields['monto'];
            $tft->descripcion   = $r->fields['descrip'];
            $coleccion[]        = $tft;
            $r->movenext();
        }
        //die(var_dump($tft));
        $this->detTransferencia = new Services_JSON();
        $this->detTransferencia = is_array($coleccion) ? $this->detTransferencia->encode($coleccion) : false;
        return $coleccion;
    }


    function buscar($conn,$nrodoc, $fecha, $max=0, $from=0, $orden="id") {
        //die("aqui ".$grupo_prov);
        try {
            if(empty($nrodoc) and empty($fecha))
                return false;
            $q = "SELECT * FROM finanzas.tra_fondos_terceros ";
            $q.= "WHERE 1=1 ";
            $q.= !empty($nrodoc) ? "AND nrodoc = '$nrodoc'  ":"";
            $q.= !empty($fecha) ? "AND fecha >= '".guardafecha($fecha)."'  ":"";
            $q.= "ORDER BY $orden ";
            //die($q);
            $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
            $collection=array();
            while(!$r->EOF) {
                $ue = new traFondosTerceros;
                $ue->get($conn, $r->fields['id']);
                $coleccion[] = $ue;
                $r->movenext();
            }
            return $coleccion;
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    } //Fin Buscar

    function total_registro_busqueda($conn,$nrodoc, $fecha, $orden="id") {
        if(empty($nrodoc) and empty($fecha))
            return false;
        $q = "SELECT * FROM finanzas.tra_fondos_terceros ";
        $q.= "WHERE 1=1 ";
        $q.= !empty($nrodoc) ? "AND nrodoc = '$nrodoc'  ":"";
        $q.= !empty($fecha) ? "AND fecha = '".guardafecha($fecha)."'  ":"";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        $total = $r->RecordCount();

        return $total;
    } // Fin total_registro_busqueda

    function anular($conn, $id, $fechaAnul, $status, $motivoAnul,
                    $txt_fecha, $nroDoc, $prmjson_det, $escEnEje) {

        $hasError=false;
        $conn->StartTrans();

        $oComprobante = new comprobante($conn);
        try {

            $q = "UPDATE finanzas.tra_fondos_terceros SET status = 2, fecha_anulado = '$fechaAnul', motivo_anulacion = '$motivoAnul' ";
            $q.= "WHERE id = '$id' ";
            //die($q);
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

            #Busca el ultimo correlativo ingresado en la tabla de comprobantes contables
            $q      = "SELECT numcom FROM contabilidad.com_enc WHERE id = (SELECT MAX(id) as max FROM contabilidad.com_enc)";
            $rs     = $conn->Execute($q);
            $NumCom = sprintf("%05d", substr($rs->fields['numcom'],8)+1);
            $fech   = split('/',$txt_fecha);
            $numcom = $fech[2].'-'.$fech[1].'-'.$NumCom;

            #Reverso de marcado de retenciones o facturas
            $qtiporet = "select id_cta from finanzas.relacion_tra_fondos_terceros where id_tra = $id and id_cta = 4559";
            $rc = $conn->Execute($qtiporet);
            if(!$rc->EOF)
                $qret = "UPDATE finanzas.facturas SET estatus_enterado=0,id_pago=null WHERE estatus_enterado=2 and id_pago = $id";
            else
                $qret = "UPDATE finanzas.relacion_retenciones_orden SET estatus_enterado=0,id_pago=null WHERE estatus_enterado=2 and id_pago = $id";

            $r = $r and $conn->Execute($qret);
            if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

            #Aqui generamos el asiento contable
            $desc   = 'Asiento de Transferencia de Fondos a Terceros Anulada';
            if($oComprobante->create($escEnEje,$fech[2],$fech[1],$numcom,$desc,guardafecha($txt_fecha),'TFT','A',$nroDoc,$prmjson_det,'1')==false)
                throw new Exception(__METHOD__,$conn->ErrorNo());
        }
        catch (ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            die($e);
            $msg = ERROR." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;

    } //Fin anular

    function getOrdenes($conn, $nrodoc) {
        $q= "SELECT DISTINCT(op.nrodoc), op.descripcion ";
        $q.= "FROM finanzas.orden_pago op ";
        $q.= "LEFT JOIN finanzas.relacion_cheque rc ON (rc.nroref = op.nrodoc) ";
        $q.= "LEFT JOIN finanzas.relacion_otros_pagos rop ON (rop.nroref = op.nrodoc) ";
        $q.= "WHERE op.status = 2 AND transferido = '0' AND op.montoret > 0 ";
        $q.= !empty($nrodoc) ? "AND op.nrodoc = '$nrodoc' " : "";
        //die($q);
        //echo $q.'<br>';
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $tra = new traFondosTerceros;
            $tra->nrodoc = $r->fields['nrodoc'];
            $tra->descripcion = $r->fields['descripcion'];
            $coleccion[] = $tra;
            $r->movenext();
        }
        return $coleccion;
    }

    function getAsientos ($conn, $nrodoc, $ctaTra) {
        $q = "SELECT A.id_cta, A.haber as monto, C.descripcion AS descripcion, (SELECT cta_contable FROM puser.proveedores WHERE cta_contable = id_cta) AS cta_proveedor FROM ";
        $q.= "contabilidad.com_det A ";
        $q.= "INNER JOIN contabilidad.com_enc B ON (A.id_com = B.id) ";
        $q.= "INNER JOIN contabilidad.plan_cuenta C ON (A.id_cta = C.id) ";
        $q.= "WHERE B.num_doc = '$nrodoc' AND A.haber > 0 AND A.id_cta <> $ctaTra ";
        //die($q);
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $oTra = new traFondosTerceros;
            $oTra->idCta = $r->fields['id_cta'];
            $oTra->monto = $r->fields['monto'];
            $oTra->ctaProveedor = $r->fields['cta_proveedor'];
            $oTra->descripcion = $r->fields['descripcion'];
            $coleccion[] = $oTra;
            $r->movenext();
        }
        return $coleccion;
    }

    function setOrdenPago($conn,$nrodoc,$status) {
        $q = "UPDATE finanzas.orden_pago SET transferido = '$status' WHERE nrodoc = '$nrodoc'";
        $r = $conn->Execute($q);
    }

    function getAporRetXpagar($conn, $descripcion, $desde='', $hasta='') {
        $q = "SELECT DISTINCT A.id, A.codcta, A.descripcion, COALESCE(SUM(D.debe),0) AS debe, COALESCE(SUM(D.haber),0) AS haber
                      FROM contabilidad.plan_cuenta A ";
        $q.= "INNER JOIN finanzas.ret_apor_x_cancelar B ON (A.id = B.id_cta) ";
        $q.= "LEFT JOIN contabilidad.com_det D ON (A.id = D.id_cta) ";
        $q.= "INNER JOIN contabilidad.com_enc E ON (d.id_com=e.id and e.id_escenario = 1111) WHERE 1=1 ";
        $q.= !empty($descripcion) ? "AND A.descripcion ILIKE '%$descripcion%' " : "";
        $q.= !empty($desde) ? "AND E.fecha >='".guardafecha($desde)."' AND E.fecha <='".guardafecha($hasta)."' ":"";
        $q.= "GROUP BY 1,2,3 ";
        //die($q);
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $tra = new traFondosTerceros;
            $tra->idCta         = $r->fields['id'];
            $tra->codCta        = $r->fields['codcta'];
            $tra->descripcion   = $r->fields['descripcion'];
            //echo 'hab: '.$r->fields['haber'].' debe: '.$r->fields['debe'];
            if(substr($r->fields['codcta'],0,1) == '1' or substr($r->fields['codcta'],0,1) == '4'
                    or substr($r->fields['codcta'],0,1) == '5' or substr($r->fields['codcta'],0,1) == '6')
                $saldo              = ($r->fields['debe'] - $r->fields['haber']);
            else
                $saldo              = ($r->fields['haber'] - $r->fields['debe']);
            //die($saldo);
            $tra->saldo         = $saldo;
            $coleccion[]        = $tra;
            $r->movenext();
        }
        return $coleccion;
    }

    //Obtengo el valor para el concepto de pago segun id_cta
    function get_val_concepto($conn,$fechadesde,$fechahasta,$id_cta,$tipo) {
        $qvalor = "select * from rrhh.calcula_pago_aportes('".guardafecha($fechadesde)."','".guardafecha($fechahasta)."',$id_cta,".($tipo+1)."::smallint)";
        $rqvalor = $conn->Execute($qvalor);
        if(!$rqvalor->EOF)
            return $rqvalor->fields['calcula_pago_aportes'];
        else
            return -1;
    }

	//Obtiene la cuenta contable a afectar segun el id_concepto
    function get_cuenta_concepto($conn,$id_concepto,$tipo) {
        $qvalor = "select coalesce(cuenta_contable,0) as cuenta_contable,coalesce(cuenta_aporte,0) as cuenta_aporte from rrhh.concepto where int_cod = $id_concepto";
        $rqvalor = $conn->Execute($qvalor);
        if(!$rqvalor->EOF)
            if($tipo==0)
                return $rqvalor->fields['cuenta_contable'];
            else
                return $rqvalor->fields['cuenta_aporte'];
        else
            return -1;
    }
}
?>
