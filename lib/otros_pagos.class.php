<?
class otros_pagos {

    #PROPIEDADES#

    var $id;
    var $id_banco;
    var $banco;
    var $id_nro_cuenta;
    var $nro_cuenta;
    var $nro_otros_pagos;
    var $id_proveedor;
    var $proveedor;
    var $rif_proveedor;
    var $fecha;
    var $status;
    var $json;
    var $observacion;
    var $nrodoc;
    var $descripcion;
    var $nro_control;
    var $cerrado;
    var $beneficiario;
    var $cuentaDestino;

    function get($conn, $id) {

        $q = "SELECT * FROM finanzas.otros_pagos WHERE id ='$id' AND id_escenario = '".$_SESSION['escEnEje']."'";
        $r = $conn->execute($q);

        if (!$r->EOF) {

            $this->id               = $r->fields['id'];
            $this->id_banco         = $r->fields['id_banco'];
            $ban                    = new banco;
            $ban->get($conn, $r->fields['id_banco']);
            $this->banco            = $ban->descripcion;
            $cue                    = new cuentas_bancarias;
            $this->id_nro_cuenta    = $r->fields['nro_cuenta'];
            $cue->get($conn, $r->fields['nro_cuenta']);
            $this->nro_cuenta       = $cue->nro_cuenta;
            $this->fecha            = $r->fields['fecha'];
            $cue                    = new proveedores;
            $this->id_proveedor     = $r->fields['id_proveedor'];
            $cue->get($conn, $r->fields['id_proveedor']);
            $this->proveedor        = $cue->nombre;
            $this->rif_proveedor    = $cue->rif;
            $this->nro_otros_pagos  = $r->fields['nro_otros_pagos'];
            $this->status           = $r->fields['status'];
            $this->nrodoc           = $r->fields['nrodoc'];
            $this->observacion      = $r->fields['observacion'];
            $this->descripcion      = $r->fields['descripcion'];
            $this->nro_control      = $r->fields['nro_control'];
            $this->beneficiario     = $r->fields['beneficiario'];
            $this->cuentaDestino    = $r->fields['cuenta_destino'];
            $this->json             = $this->getRelaciones($conn, $r->fields['nrodoc']);
            $this->cerrado          = $r->fields['cerrado'];

            return true;

        }else {

            return false;

        }

    }

    function get_all($conn,$orden="id") {

        $q = "SELECT * FROM finanzas.otros_pagos WHERE id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= "ORDER BY $orden ";
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $ue = new otros_pagos;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn,$id_banco,$nro_cuenta,$nro_otros_pagos,$id_proveedor,$fecha,$status,$aOrdenes, $id_usuario, $eseEnEje,$anoCurso,$observacion,$total_cheque,$descripcion,$nro_control, $beneficiario, $cuentaDestino ) {
        //VALIDO QUE LA CUENTA TENGA DISPONIBILIDAD
        $oCuentasBancarias =  new cuentas_bancarias;
        $oCuentasBancarias->get($conn,$nro_cuenta);
        $saldo = $oCuentasBancarias->saldo_inicial + $oCuentasBancarias->creditos - $oCuentasBancarias->debitos;
        //Politica para activar la verificacion de saldo en la cuenta en true no cheque saldo
        $sindisponibilidad = true;
        if($saldo>=guardafloat($total_cheque) or $sindisponibilidad) {
            $nrodoc = otros_pagos::getNroDoc($conn);
            //die($nrodoc);
            $this->hasError=false;
            $conn->StartTrans();
            try {
                $q = "INSERT INTO finanzas.otros_pagos (id_banco,nro_cuenta,nro_otros_pagos,id_proveedor,fecha,status,nrodoc, id_escenario,observacion,descripcion,nro_control, beneficiario, cuenta_destino ) ";
                $q.= "VALUES ($id_banco,'$nro_cuenta','$nro_otros_pagos',$id_proveedor,'$fecha','$status','$nrodoc', '$eseEnEje','$observacion','$descripcion','$nro_control', '$beneficiario', '$cuentaDestino' ) ";
                //die($q);

                $r = $conn->Execute($q);
                if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                $this->addrelacion($conn, $nrodoc,$aOrdenes,$id_usuario,$anoCurso,$observacion,$fecha);
                $v=$this->checkAsiento($conn,$aOrdenes,$eseEnEje);
                if($v){ // relacion asientos banco proveedor
                  $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 8::int2, $eseEnEje::int8)";
                  $conn->Execute($q);
                }
                else{
                  $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 1::int2, $eseEnEje::int8)";
		  $conn->Execute($q);
                }
                if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

            }catch(ADODB_Exception $e) {
                $this->msg = setDBError($e,$conn->ErrorNo());
            }catch (Exception $e) {
                $this->msg = ERROR_ADD." Error #:".$e->getCode();
                if(!$conn->HasFailedTrans())$conn->FailTrans();
            }
            $this->hasError = $conn->HasFailedTrans();
            $conn->CompleteTrans();
            if (!$this->hasError) $this->msg= REG_ADD_OK;
        }else {
            $this->msg = 'No hay disponibilidad en la cuenta bancaria';
        }
        return !$this->hasError;
    }

     function checkRetenciones($conn,$nrodoc){
            $q="SELECT COALESCE(SUM(mntret)::float,0) AS monto";
            $q.=" FROM finanzas.relacion_retenciones_orden";
            $q.=" WHERE nrodoc IN ";
            $q.=" (SELECT DISTINCT nroref";
            $q.="  FROM finanzas.relacion_otros_pagos";
            $q.="  WHERE nrodoc = '$nrodoc')";
            $rM = $conn->Execute($q);
	    if($rM->fields[0]>0) //mayor que cero es que si tiene retenciones
                return true;
        else
                return false;
        }
        
        function checkAsiento($conn,$Ordenes,$eseEnEje){
        $sw=0;
	$JsonRec = new Services_JSON();
	$JsonRec = $JsonRec->decode(str_replace("\\","",$Ordenes));
	if(is_array($JsonRec->ordenes)){
            foreach($JsonRec->ordenes as $orden){
            //echo $orden[0]."<br>";
            $q="select";
            $q.=" t1.status, t2.nrodoc ";
            $q.=" from finanzas.cheques t1, finanzas.relacion_otros_pagos t2";
            $q.=" where t1.nrodoc=t2.nrodoc";
            $q.=" and t1.status =1";
            $q.=" and t1.id_escenario ='$eseEnEje'";
            $q.=" and t2.nroref='$orden[0]'";
            $q.=" order by t2.nrodoc asc";
            //echo $q."<br>";
            $rM = $conn->Execute($q);
            while(!$rM->EOF){
                $sw=0;
		// hallar numero de cheque que se elimino (nrodoc)
                $nroCheque=$rM->fields['nrodoc'];
                $q2=" select id";
                $q2.=" from contabilidad.com_enc";
                $q2.=" where";
                $q2.=" id_escenario='$eseEnEje'";
                $q2.=" and num_doc='$nroCheque'";
                $q2.=" and status='A'";
                //echo $q2."<br>";;
                $rM2 = $conn->Execute($q2);
                if($rM2->fields['id']){
                    $id=$rM2->fields['id'];
                    $q3="select";
                    $q3.=" sum(debe)";
                    $q3.=" from contabilidad.com_det";
                    $q3.=" where";
                    $q3.=" id_com=$id";
                    $q3.=" and id_cta in (select distinct(id_cta) from finanzas.retenciones_adiciones)";
                    //$q3.=" and id_cta in (select distinct(id_cta) from finanzas.retenciones_adiciones)";

                    //echo $q3."<br>";;
                    $rM3 = $conn->Execute($q3);
                    if($rM3->fields[0]>0)
                        //return true; // si tiene asientos de retenciones eliminadas
                        $sw=1;
                }
                $rM->movenext();
            }
            //return false;
          }
        }
        if($sw==1) return true;
        else return false;
        }

    function anular($conn, $nrodoc, $escEnEje,$status,$observacion,$id_usuario,$anoCurso,$descripcion,$nro_control) {
        $this->hasError=false;
        $conn->StartTrans();
        try {
          if($status==1 || $status==2) {
                #CAMBIO EL ESTATUS DEL CHEQUE POR ANULADO#
                $q = "UPDATE finanzas.otros_pagos SET  ";
                $q.= "status = 1, observacion='$observacion', descripcion='$descripcion', nro_control='$nro_control' ";
                $q.= "WHERE nrodoc='$nrodoc' ";
                $r =$conn->Execute($q);
                if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                if($status==1) { // status 1 conserva retenciones
                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 1::int2, $escEnEje::int8)";
                    $r=$conn->Execute($q);
                    if ($conn->HasFailedTrans()) throw new ADODB_Exception($conn->ErrorMsg(),$conn->ErrorNo());
                }
               if($status==2) { // status 2 no conserva retenciones --- Esto no esta funcionando - comentado por javier hernandez

                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 8::int2, $escEnEje::int8)";
                    $r=$conn->Execute($q);
                    if ($conn->HasFailedTrans()) throw new ADODB_Exception($conn->ErrorMsg(),$conn->ErrorNo());
               }
                $q = "SELECT nroref,monto FROM finanzas.relacion_otros_pagos WHERE nrodoc='$nrodoc'";
                $r = $conn->Execute($q);

                while(!$r->EOF) {
                    $nroOrden=$r->fields['nroref'];
                    $monto=$r->fields['monto'];
                    $q = "UPDATE finanzas.orden_pago SET montopagado=montopagado-$monto WHERE nrodoc='$nroOrden'";
                    $conn->Execute($q);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                    $q = "SELECT montodoc,montoret,id_unidad_ejecutora,id_proveedor,nroref FROM finanzas.orden_pago WHERE nrodoc='$nroOrden' ";
                    $rM = $conn->Execute($q);
                    if($rM->fields['nroref']) {
                        $idUniEje=$rM->fields['id_unidad_ejecutora'];
                        $idProveedor=$rM->fields['id_proveedor'];
                        $porc=$monto*100/($rM->fields['montodoc']-$rM->fields['montoret']);
                        $fecha=date("Y-m-d");

                        $q = "INSERT INTO movimientos_presupuestarios ";
                        $q.= "(id_usuario, id_unidad_ejecutora, ano, descripcion, nrodoc, tipdoc, tipref,nroref, status_movimiento,  ";//nroref,
                        $q.= "fechadoc, fecharef, status, id_proveedor) ";
                        $q.= "VALUES ";
                        $q.= "($id_usuario, '$idUniEje', '$anoCurso', '$observaciones', '$nrodoc', '005', '','$nrodoc-ANULADO',2, ";//'$nroref',
                        $q.= " '$fecha', '$fecha', '3', $idProveedor) ";
                        //die($q);
                        $conn->Execute($q);
                        if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                        $q = "SELECT * FROM finanzas.relacion_orden_pago WHERE id_orden_pago='$nroOrden' ";
                        $rP = $conn->Execute($q);
                        while(!$rP->EOF) {
                            $MontoImputar=($rP->fields['monto']*$porc/100);
                            if(movimientos_presupuestarios::add_relacion_nomina($conn,
                                    $rP->fields['id_parcat'],
                                    $rP->fields['id_categoria_programatica'],
                                    $rP->fields['id_partida_presupuestaria'],
                                    $nrodoc,
                                    (-1*$MontoImputar))==false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());
                            
                            if(relacion_pp_cp::set_desde_pagado_cheque_anulado($conn, $rP->fields['id_parcat'], $MontoImputar)==false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());

                            $rP->movenext();
                        }
                    }
                    $r->movenext();
                  }
            }else {
                #CAMBIO EL ESTATUS DEL CHEQUE POR ANULADO#
                $q = "UPDATE finanzas.otros_pagos SET  ";
                $q.= "observacion='$observacion',descripcion='$descripcion', nro_control='$nro_control' ";
                $q.= "WHERE nrodoc='$nrodoc' and id_escenario = '$escEnEje'";
                //die($q);
                $r =$conn->Execute($q);
                if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

            }
        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $this->msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();

        }
        $this->hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$this->hasError) $this->msg= REG_SET_OK;
        return !$this->hasError;

    }
    function addrelacion($conn, $nrodoc,$Ordenes,$id_usuario,$anoCurso,$observacion,$fecha) {
        $JsonRec = new Services_JSON();
        $JsonRec = $JsonRec->decode(str_replace("\\","",$Ordenes));
        if(is_array($JsonRec->ordenes)) {
            $this->hasError=false;
            $conn->StartTrans();
            try {
                foreach($JsonRec->ordenes as $orden) {
                    //INSERTO LA RELACION DETALLE DEL CHEQUE
                    $q = "INSERT INTO finanzas.relacion_otros_pagos (nrodoc, nroref, monto, ano) VALUES ('$nrodoc','$orden[0]', $orden[1],$anoCurso)";
                    $r= $conn->Execute($q);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                    //ACTAULIZO EL MONTO PAGADO DE LA ORDEN DE PAGO
                    $q = "UPDATE finanzas.orden_pago SET montopagado=montopagado+$orden[1] WHERE nrodoc='$orden[0]'";
                    $r = $conn->execute($q);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                    //REALIZO LA IMPUTACION PRESUPESTARIA
                    $q = "SELECT montodoc,montoret,id_unidad_ejecutora,id_proveedor,nroref FROM finanzas.orden_pago WHERE nrodoc='$orden[0]' ";
                    //die($q);
                    $rM = $conn->Execute($q);
                    if($rM->fields['nroref']) {
                        $idUniEje=$rM->fields['id_unidad_ejecutora'];
                        $idProveedor=$rM->fields['id_proveedor'];
                        $porc=$orden[1]*100/($rM->fields['montodoc']-$rM->fields['montoret']);

                        //$fecha=date("Y-m-d");

                        $q = "INSERT INTO movimientos_presupuestarios ";
                        $q.= "(id_usuario, id_unidad_ejecutora, ano, descripcion, nrodoc, tipdoc, tipref,nroref, ";//nroref,
                        $q.= "fechadoc, fecharef, status, id_proveedor) ";
                        $q.= "VALUES ";
                        $q.= "($id_usuario, '$idUniEje', '$anoCurso', '$observacion', '$nrodoc', '017', '','$orden[0]', ";//'$nroref',
                        $q.= " '$fecha', '$fecha', '3', $idProveedor) ";
                        $r = $conn->Execute($q);
                        if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                        $q = "SELECT * FROM finanzas.relacion_orden_pago WHERE id_orden_pago='$orden[0]' ";
                        $r = $conn->Execute($q);
                        while(!$r->EOF) {
                            $MontoImputar=$r->fields['monto']*$porc/100;
                            if (movimientos_presupuestarios::add_relacion_nomina($conn,
                                    $r->fields['id_parcat'],
                                    $r->fields['id_categoria_programatica'],
                                    $r->fields['id_partida_presupuestaria'],
                                    $nrodoc,
                                    $MontoImputar) == false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());

                            if (relacion_pp_cp::set_desde_pagado_cheque($conn, $r->fields['id_parcat'], $MontoImputar)==false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());

                            $r->movenext();
                        }
                    }
                }
            }catch(ADODB_Exception $e) {
                $this->msg = setDBError($e,$conn->ErrorNo());
            }
            catch (Exception $e) {
                $this->msg = ERROR." Error #:".$e->getCode();
                if(!$conn->HasFailedTrans())$conn->FailTrans();

            }
            $this->hasError = $conn->HasFailedTrans();
            $conn->CompleteTrans();
            if (!$this->hasError) $this->msg= OK;
        }
        return !$this->hasError;

    }
    function montoTotal($conn, $nrodoc) {

        $q = "SELECT Sum(monto) as total FROM finanzas.relacion_otros_pagos WHERE nrodoc =  '$nrodoc' AND ano  = '".$_SESSION['anoCurso']."'";
        $r = $conn->execute($q);

        if($r)
            return $r->fields['total'];
        else
            return false;
    }
    function buscar($conn, $id_proveedor, $id_banco, $fecha_desde, $fecha_hasta, $nrodoc, $nrocontrol, $cuenta, $orden="id", $from, $max) {

        if(empty($id_proveedor) && empty($id_banco) && empty($fecha_desde) && empty($fecha_hasta)
                && empty($nrodoc) && empty($nrocontrol) && empty($cuenta))

            return false;

        $q = 	"SELECT * from finanzas.otros_pagos  ";

        $q.= 	"WHERE  id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= 	!empty($nrodoc) ? "AND otros_pagos.nrodoc='$nrodoc' ": "";
        $q.= 	!empty($nrocontrol) ? "AND otros_pagos.nro_control='$nrocontrol' ": "";
        $q.= 	!empty($fecha_desde) ? "AND otros_pagos.fecha >='".guardafecha($fecha_desde)."' ": "";
        $q.= 	!empty($fecha_hasta) ? "AND otros_pagos.fecha <='".guardafecha($fecha_hasta)."' ": "";
        $q.= 	!empty($id_proveedor) ? "AND otros_pagos.id_proveedor = '$id_proveedor'  ":"";
        $q.= 	!empty($id_banco) ? "AND otros_pagos.id_banco = '$id_banco'  ":"";
        $q.= 	!empty($cuenta) ? "AND otros_pagos.nro_cuenta = '$cuenta'  ":"";
        $q.= 	"ORDER BY otros_pagos.$orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from):$conn->Execute($q);
        if(!r || $r->EOF)
            return false;
        $collection=array();
        while(!$r->EOF) {
            $ue = new otros_pagos;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        return $coleccion;
    }

    function totalRegsBusqueda($conn, $id_proveedor, $id_banco, $fecha_desde, $fecha_hasta, $nrodoc,$cuenta) {
        if(empty($id_proveedor) && empty($id_banco) && empty($fecha_desde) && empty($fecha_hasta) && empty($nrodoc) && empty($nrocontrol)&& empty($cuenta))
            return 0;

        $q = 	"SELECT * from finanzas.otros_pagos  ";
        $q.= 	"WHERE  id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= 	!empty($nrodoc) ? "AND otros_pagos.nrodoc='$nrodoc' ": "";
        $q.= 	!empty($nrocontrol) ? "AND otros_pagos.nro_control='$nrocontrol' ": "";
        $q.= 	!empty($fecha_desde) ? "AND otros_pagos.fecha >='".guardafecha($fecha_desde)."' ": "";
        $q.= 	!empty($fecha_hasta) ? "AND otros_pagos.fecha <='".guardafecha($fecha_hasta)."' ": "";
        $q.= 	!empty($id_proveedor) ? "AND otros_pagos.id_proveedor = '$id_proveedor'  ":"";
        $q.= 	!empty($id_banco) ? "AND otros_pagos.id_banco = '$id_banco'  ":"";
        $q.= 	!empty($cuenta) ? "AND otros_pagos.nro_cuenta = '$cuenta'  ":"";
        //die($q);
        $r = $conn->Execute($q);

        return $r->RecordCount();
    }

    function getRelaciones($conn, $nrodoc) {

        $q="SELECT A.nroref,A.monto,(B.montodoc-B.montoret-B.montopagado) AS montopagar,B.montopagado AS montopagado FROM finanzas.relacion_otros_pagos AS A INNER JOIN finanzas.orden_pago AS B ON A.nroref=B.nrodoc WHERE A.ano = '".$_SESSION['anoCurso']."' AND A.nrodoc='$nrodoc'";
        $r= $conn->execute($q);

        while(!$r->EOF) {

            $ch = new cheque;
            $ch->nroref = $r->fields['nroref'];
            $ch->montopagar = $r->fields['montopagar'];
            $ch->montopagado = $r->fields['montopagado'];
            $ch->monto 	= $r->fields['monto'];
            $coleccion[] = $ch;
            $r->movenext();

        }

        $json = new Services_JSON();
        return $json->encode($coleccion);

    }
    function getNroDoc($conn) {
        $q = "SELECT max(nrodoc) AS nrodoc FROM finanzas.otros_pagos WHERE id_escenario = '".$_SESSION['escEnEje']."'";
        $r = $conn->execute($q);
        //die($r->fields['nrodoc']);
        return "017-".str_pad(substr($r->fields['nrodoc'], 4, 4) + 1, 4, 0, STR_PAD_LEFT);
    }

}

?>