<?
class tipo_ingreso {
    #PROPIEDADES#
    var $msg = "";
    var $hasError=false;
    var $id;
    var $idcta;
    var $descripcion;
    var $tMovimiento;
    var $descCuenta;
    var $origen;
    #FUNCIONES#
    function get($conn, $id) {
        $id = trim($id);
        if (empty($id)) return false;
        $q = "SELECT A.*, B.descripcion AS desc_cuenta FROM ingresos.tipos_ingresos A ";
        $q.= "INNER JOIN contabilidad.plan_cuenta B ON (A.id_cta_contable = B.id)";
        $q.= " WHERE A.id = $id ";
        $r = $conn->execute($q);
        //die($q);
        //die(var_dump($r->EOF));
        if (!$r->EOF) {
            $this->id          = $r->fields['id'];
            $this->idcta       = $r->fields['id_cta_contable'];
            $this->descripcion = $r->fields['descripcion'];
            $this->tMovimiento = $r->fields['operacion'];
            $this->descCuenta  = $r->fields['desc_cuenta'];
            $this->origen      = $r->fields['origen'];
            return true;
        }
        else {
            return false;
        }
    }

    function get_all($conn,$orden="id",$opcion='') {
        $q = "SELECT * FROM ingresos.tipos_ingresos ";
        $q.= !empty($opcion) ? "WHERE operacion = $opcion " : "";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $ue = new tipo_ingreso;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        //$this->total = $r->RecordCount();
        return $coleccion;
    }

    function get_all_con_ambos($conn,$orden="id",$opcion='') {
        $q = "SELECT * FROM ingresos.tipos_ingresos ";
        $q.= !empty($opcion) ? "WHERE operacion = $opcion OR operacion = 3 " : "";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $ue = new tipo_ingreso;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        //$this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $descripcion, $idcta, $tMovimiento,$origen) {
        $q = "INSERT INTO ingresos.tipos_ingresos ";
        $q.= "(descripcion, id_cta_contable, operacion,origen) ";
        $q.= "VALUES ";
        $q.= "('$descripcion', $idcta, '$tMovimiento','$origen') ";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function set($conn, $id, $descripcion, $idcta, $tMovimiento,$origen) {
        $id = trim($id);
        if (empty($id)) return false;
        $q = "UPDATE ingresos.tipos_ingresos SET descripcion='$descripcion', id_cta_contable=$idcta, operacion = '$tMovimiento', origen='$origen'";
        $q.= "WHERE id = $id";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $id) {
        $id = trim($id);
        if (empty($id)) return false;
        $q = "DELETE FROM ingresos.tipos_ingresos  WHERE id = $id";

        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function buscar($conn, $descripcion='', $idcta='', $from=0, $max=0, $orden="id") {
        /*if(empty($codcta) && empty($descripcion) && empty($ano))return false;*/
        $q = "SELECT * FROM ingresos.tipos_ingresos ";
        $q.= "WHERE  1=1 ";
        $q.= !empty($descripcion) ? "AND descripcion ILIKE '%$descripcion%'  ":"";
        $q.= !empty($idcta) ? "AND id_cta_contable = $idcta  ":"";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        
        //$collection=array();
        while(!$r->EOF) {
            $ue = new tipo_ingreso;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        //var_dump($coleccion);
        return $coleccion;
    }

    function total_registro_busqueda($conn, $descripcion='', $idcta='', $from=0, $max=0, $orden="id") {
        $q = "SELECT * FROM ingresos.tipos_ingresos ";
        $q.= "WHERE  1=1 ";
        $q.= !empty($descripcion) ? "AND descripcion ILIKE '%$descripcion%'  ":"";
        $q.= !empty($idcta) ? "AND id_cta_contable = $idcta  ":"";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = $conn->Execute($q);
        $total = $r->RecordCount();
        return $total;
    }

    function get_all_opciones($conn) {
        $q = "SELECT * FROM ingresos.opciones ";
        //die($q);
        $rs = $conn->Execute($q);
        //$result = array();
        while(!$rs->EOF) {
            $gr = new tipo_ingreso;
            $gr->id = $rs->fields['id'];
            $gr->descripcion = $rs->fields['descripcion'];
            $result[] = $gr;
            $rs->movenext();
        }
        return $result;
    }

    //Funcion para Manejar las Modificaciones a la Base de Datos, Manejando Excepciones y Errores
    private function submit_bd($cadena, $conn) {
        try {
            $conn->StartTrans();
            $r = $conn->Execute($cadena);
            $conn->CompleteTrans();
        }
        catch (ADODB_Exception $e) {
            return false; //$conn->ErrorMsg();
        }
        return true;
    }
}


?>
