<?
class organismos {

    // Propiedades
    var $msg="";
    var $hasError = false;
    var $id;
    var $id_escenario;
    var $descripcion;
    var $escenario;

    var $total;

    function get($conn, $id) {
        $q = "SELECT * FROM organismos WHERE id='$id'";
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $id;
            $this->id_escenario = $r->fields['id_escenario'];
            $oEscenario = new escenarios;
            $oEscenario->get($conn, $r->fields['id_escenario']);
            $this->escenario = $oEscenario;
            $this->descripcion = $r->fields['descripcion'];
            return true;
        }else
            return false;
    }

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM organismos ";
        $q.= "ORDER BY $orden ";
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new organismos;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $id, $id_escenario, $descripcion) {
        $q = "INSERT INTO organismos ";
        $q.= "(id, id_escenario, descripcion) ";
        $q.= "VALUES ('$id', '$id_escenario', '$descripcion') ";

        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_ADD_OK;
        return !$hasError;
    }

    function set($conn, $id_nuevo, $id, $id_escenario, $descripcion) {
        $q = "UPDATE organismos SET id = '$id_nuevo', id_escenario='$id_escenario', ";
        $q.= "descripcion = '$descripcion' ";
        $q.= "WHERE id='$id'";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_SET_OK;
        return !$hasError;
    }

    function del($conn, $id) {
        $q = "DELETE FROM organismos WHERE id='$id'";

        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_DEL_OK;
        return !$hasError;
    }

    function buscar($conn, $id, $id_escenario, $descripcion, $orden="id_escenario, id") {
        if(empty($id) && empty($id_escenario) && empty($descripcion))
            return false;
        $q = "SELECT * FROM organismos ";
        $q.= "WHERE  1=1 ";
        $q.= !empty($id) ? "AND id = '$id'  ":"";
        $q.= !empty($id_base) ? "AND id_escenario = '$id_escenario'  ":"";
        $q.= !empty($descripcion) ? "AND descripcion ILIKE '%$descripcion%'  ":"";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new organismos;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        return $coleccion;
    }
}
?>