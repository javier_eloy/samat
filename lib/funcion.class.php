<?
class funcion {

    // Propiedades
    var $msg="";
    var $hasError=false;
    var $int_cod;
    var $fun_cod;
    var $fun_nom;
    var $fun_hp;
    var $fun_estatus;
    var $fun_ord;

    var $total;

    function get($conn, $int_cod) {
        try {
            $q = "SELECT * FROM rrhh.funciones WHERE int_cod=$int_cod";
            //die($q);
            $r = $conn->Execute($q);
            if(!$r->EOF) {
                $this->int_cod = $r->fields['int_cod'];
                $this->fun_cod = $r->fields['fun_cod'];
                $this->fun_nom = $r->fields['fun_nom'];
                $this->fun_hp = muestrafloat($r->fields['fun_hp']);
                $this->fun_ord = $r->fields['fun_ord'];
                $this->fun_estatus = $r->fields['fun_estatus'];
            }
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }
    function get_all($conn, $orden="int_cod", $Tipo, $Valor, $TipoE, $max=10, $from=1) {
        try {
            $TipoE = empty($TipoE) ? 0 : $TipoE;
            if(empty($Valor)) {
                $q = "SELECT * FROM rrhh.funciones WHERE fun_estatus=$TipoE ";
                $q.= "ORDER BY $orden ";
            }
            elseif($Tipo==0) {
                $q = "SELECT * FROM rrhh.funciones ";
                $q.= "where fun_ord ILIKE '%$Valor%' AND fun_estatus=$TipoE ";
                $q.= "ORDER BY $orden ";
            }
            elseif($Tipo==1) {
                $q = "SELECT * FROM rrhh.funciones ";
                $q.= "where fun_nom ILIKE '%$Valor%' AND fun_estatus=$TipoE ";
                $q.= "ORDER BY $orden ";
            }
            //die($q);
            $r = $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
            $collection=array();
            while(!$r->EOF) {
                $ue = new funcion;
                $ue->get($conn, $r->fields['int_cod']);
                $coleccion[] = $ue;
                $r->MoveNext();
            }
            $this->total = $r->RecordCount();
            return $coleccion;
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }

    function add($conn, $fun_cod, $fun_nom,$fun_hp,$fun_ord,$fun_estatus) {
        $hasError=false;
        $conn->StartTrans();
        try {
            $q = "INSERT INTO rrhh.funciones ";
            if(is_numeric($fun_ord)) {
                $q.= "(fun_cod, fun_nom, fun_hp, fun_ord,fun_estatus) ";
                $q.= "VALUES ";
                $q.= "('$fun_cod','$fun_nom',$fun_hp,$fun_ord,$fun_estatus) ";
            }
            else {
                $q.= "(fun_cod, fun_nom, fun_hp, fun_estatus) ";
                $q.= "VALUES ";
                $q.= "('$fun_cod','$fun_nom',$fun_hp,$fun_estatus) ";
            }
            //die($q);
            $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

            $q = "SELECT MAX(int_cod) AS int_cod ";
            $q.= "FROM rrhh.funciones ";
            //die($q);
            $r = $conn->Execute($q);
            $ue = new funcion;
            if($ue->reorder($conn, $fun_ord,$r->fields['int_cod'])==false)
                throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function set($conn, $int_cod, $fun_cod, $fun_nom,$fun_hp,$fun_ord,$fun_estatus) {
        $hasError=false;
        $conn->StartTrans();
        try {
            $q = "UPDATE rrhh.funciones SET fun_cod='$fun_cod',fun_nom='$fun_nom',fun_hp=$fun_hp,fun_ord=$fun_ord,fun_estatus=$fun_estatus ";
            $q.= "WHERE int_cod=$int_cod";
            //die($q);
            $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

            $ue = new funcion;
            if($ue->reorder($conn, $fun_ord, $int_cod)==false)
                throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $int_cod) {

        $q = "DELETE FROM rrhh.funciones WHERE int_cod='$int_cod'";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function reorder($conn, $fun_ord,$int_cod) {
        $hasError=false;

        try {
            $q = "SELECT int_cod,fun_ord FROM rrhh.funciones WHERE fun_ord=$fun_ord AND int_cod!=$int_cod";
            $r = $conn->Execute($q);
            if($r->EOF) {
                return true;
            }else {
                $nuevo = $r->fields['fun_ord'] + 1;
                $ue = new funcion;
                if($ue->reorder($conn, $nuevo,$int_cod)==false)
                    throw new Exception(__METHOD__,$conn->ErrorNo());

                while(!$r->EOF) {
                    $int_cod = $r->fields['int_cod'];
                    $q = "UPDATE rrhh.funciones SET fun_ord=$nuevo";
                    $q.= "WHERE int_cod=$int_cod";
                    $conn->Execute($q);
                    if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                    $r->movenext();
                }
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
            $hasError=true;
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            $hasError=true;
        }
        return !$hasError;
    }

    function getListadoKeys() {
        try {
            //PARA LISTADO
            $listado[0]['F'] = 'fun_cod';
            $listado[0]['F2'] = 'fun_cod';
            $listado[0]['D'] = 'Codigo';

            $listado[1]['F'] = 'fun_nom';
            $listado[1]['F2'] = 'fun_nom';
            $listado[1]['D'] = 'Nombre';

            $listado[2]['F'] = 'fun_hp';
            $listado[2]['F2'] = 'fun_hp';
            $listado[2]['D'] = 'Honorario Profesional';

            $listado[3]['F'] = 'fun_ord';
            $listado[3]['F2'] = 'fun_ord';
            $listado[3]['D'] = 'Orden';

            return $listado;
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }

    function total_registro_busqueda($conn,$Tipo, $Valor, $TipoE, $orden="fun_cod") {
        $TipoE = empty($TipoE) ? 0 : $TipoE;
        if(empty($Valor)) {
            $q = "SELECT * FROM rrhh.funciones WHERE fun_estatus=$TipoE ";
        }
        elseif($Tipo==0) {
            $q = "SELECT * FROM rrhh.funciones ";
            $q.= "where fun_ord ILIKE '%$Valor%' AND fun_estatus=$TipoE ";
            $q.= "ORDER BY $orden ";
        }
        elseif($Tipo==1) {
            $q = "SELECT * FROM rrhh.funciones ";
            $q.= "where fun_nom ILIKE '%$Valor%' AND fun_estatus=$TipoE ";
            $q.= "ORDER BY $orden ";
        }

        $r = $conn->Execute($q);
        $total = $r->RecordCount();
        return $total;
    }
}
?>
