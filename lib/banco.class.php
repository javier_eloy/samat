<?
class banco {

    // Propiedades

    var $id;
    var $descripcion;
    var $codigo;
    var $nombre_corto;
    var $status;
    var $msg = "";
    var $hasError=false;

    function get($conn, $id) {
        $q = "SELECT * FROM public.banco ";
        $q.= "WHERE id=$id";
        //echo($q);
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $r->fields['id'];
            $this->descripcion = $r->fields['descripcion'];
            $this->codigo = $r->fields['codigo'];
            $this->nombre_corto = $r->fields['nombre_corto'];
            $this->status = $r->fields['status'];
            return true;
        }else
            return false;
    }

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM public.banco ";
        $q.= "ORDER BY $orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new banco;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $descripcion, $codigo, $nombre_corto, $status) {
        $q = "INSERT INTO public.banco ";
        $q.= "(descripcion, codigo, nombre_corto, status) ";
        $q.= "VALUES ";
        $q.= "('$descripcion', '$codigo', '$nombre_corto', $status) ";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function set($conn, $id, $descripcion, $codigo, $nombre_corto, $status) {
        $q = "UPDATE public.banco SET descripcion='$descripcion', codigo='$codigo', nombre_corto='$nombre_corto', status=$status ";
        $q.= "WHERE id=$id";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $id) {
        $q = "DELETE FROM public.banco WHERE id='$id'";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());
            
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }
}
?>
