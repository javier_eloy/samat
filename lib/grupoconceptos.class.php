<?
class grupoconceptos {

    // Propiedades
    var $int_cod;
    var $gconc_cod;
    var $gconc_nom;

    var $total;
    var $msg="";
    var $hasError=false;

    function get($conn, $int_cod) {
        try {
            $q = "SELECT * FROM rrhh.grupoconceptos WHERE int_cod=$int_cod";
            //die($q);
            $r = $conn->Execute($q);
            if(!$r->EOF) {
                $this->int_cod = $r->fields['int_cod'];
                $this->gconc_cod = $r->fields['gconc_cod'];
                $this->gconc_nom = $r->fields['gconc_nom'];
            }
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }

    function get_all($conn, $orden="int_cod") {
        try {
            $q = "SELECT * FROM rrhh.grupoconceptos ";
            $q.= "ORDER BY $orden ";
            $r = $conn->Execute($q);
            $collection=array();
            while(!$r->EOF) {
                $ue = new grupoconceptos;
                $ue->get($conn, $r->fields['int_cod']);
                $coleccion[] = $ue;
                $r->movenext();
            }
            $this->total = $r->RecordCount();
            return $coleccion;
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }

    function add($conn, $gconc_cod, $gconc_nom) {
        $q = "INSERT INTO rrhh.grupoconceptos ";
        $q.= "(gconc_cod, gconc_nom) ";
        $q.= "VALUES ";
        $q.= "('$gconc_cod', '$gconc_nom') ";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $this->msg = ERROR_ADDs." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();

        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_ADD_OK;
        return !$hasError;
    }

    function set($conn, $int_cod, $gconc_cod, $gconc_nom) {
        $q = "UPDATE rrhh.grupoconceptos SET gconc_cod='$gconc_cod',gconc_nom='$gconc_nom' ";
        $q.= "WHERE int_cod=$int_cod";
        //die($q);
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_SET_OK;
        return !$hasError;
    }

    function del($conn, $int_cod) {
        $q = "DELETE FROM rrhh.grupoconceptos WHERE int_cod='$int_cod'";
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$conn->ErrorNo());
        } catch (Exception $e) {
            $this->msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_DEL_OK;
        return !$hasError;
    }
    function getListadoKeys() {
        try {
            //PARA LISTADO
            $listado[0]['C'] = 'gconc_cod';
            $listado[0]['C2'] = 'gconc_cod';
            $listado[0]['D'] = 'Codigo';

            $listado[1]['C'] = 'gconc_nom';
            $listado[1]['C2'] = 'gconc_nom';
            $listado[1]['D'] = 'Nombre';

            return $listado;
        }
        catch( ADODB_Exception $e ) {
            if($e->getCode()==-1)
                return ERROR_CATCH_VFK;
            elseif($e->getCode()==-5)
                return ERROR_CATCH_VUK;
            else
                return ERROR_CATCH_GENERICO;
        }
    }
}
?>
