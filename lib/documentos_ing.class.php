<?php
class documentos_ing {
    public $id; //id autonumerico
    var $msg = "";
    var $hasError=false;
    public $nrodoc;
    public $id_tipo;
    public $fecha;
    public $id_cta_contable;
    public $monto;
    public $detalle;
    public $fecha_caja;
    public $fecha_documento;
    public $descripcion_tipo;
    public $tipo_caja;
    public $cerrado;
    public $operacion; //Se utiliza para almacenar si el documento es NC, ND o ambos

    private $conn;          //Conexion a base de datos
    private $prep_ins;      //Sentencia Prepare para el Insert
    private $prep_ins_det;
    private $prep_upd;      //Sentencia Prepare para el Update

    function __construct($conn) {
        //echo('aqui');
        $this->conn = $conn;
        $this->prep_ins = $this->conn->Prepare("INSERT INTO ingresos.doc_ing_enc (id_tipo, id_cta_contable, fecha, monto, nrodoc, fecha_caja, cerrado, tipo_caja) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        $this->prep_upd=$this->conn->Prepare("UPDATE ingresos.doc_ing_enc SET nrodoc = ?, id_tipo = ? , id_cta_contable = ?, monto = ?, fecha_caja = ?, tipo_caja = ? WHERE id = ?");
    }

    function __destruct() {
    }

    public function get($id) {
        if (empty($id)) return false;
        $sql = "SELECT * FROM ingresos.doc_ing_enc WHERE id = $id";
        //echo($sql);
        $rs = $this->conn->Execute($sql);
        $res = false;
        if(!$rs->EOF) {
            $this->id               = $id;
            $this->nrodoc           = $rs->fields["nrodoc"];
            $this->id_tipo          = $rs->fields["id_tipo"];
            $this->fecha            = $rs->fields["fecha"];
            $this->id_cta_contable  = $rs->fields["id_cta_contable"];
            $this->monto            = $rs->fields["monto"];
            $this->fecha_caja       = $rs->fields["fecha_caja"];
            $this->tipo_caja        = $rs->fields["tipo_caja"];
            $this->cerrado          = $rs->fields["cerrado"];

            if (!empty($id)) {
                $this->get_det($id);
            }
            $res = true;
        }
        //$rs->Close();
        return $res;
    }

    function get_det($id) {
        if (empty($id)) return false;
        $q  = "SELECT * FROM ingresos.doc_ing_det WHERE id_documento = $id ";
        $r  = $this->conn->Execute($q);
        $ia = 0;

        while($ia <= $r->RecordCount()) {
            $ue                     = new documentos_ing($this->conn);
            $ue->nrodoc             = $r->fields['num_doc'];
            $ue->id_tipo            = $r->fields['tipo'];
            $ue->monto              = $r->fields['monto'];
            $ue->id_banco           = $r->fields['id_banco'];
            //$ue->fecha_documento    = str_replace('-','/',$r->fields['fecha_documento']);
            //Cambio efectuado el 22/05/2011 
            $ue->fecha_documento    = $r->fields['fecha_documento'];
            
            $coleccion[]            = $ue;
            $r->movenext();
            $ia++;
        }
        $this->detalle = new Services_JSON();
        $this->detalle = is_array($coleccion) ? $this->detalle->encode($coleccion) : false;
        return $coleccion;
    }

    public function create($nrodoc, $tipo, $fecha, $monto, $fecha_caja, $json_det, $tipo_caja) {
        if (empty($tipo)) return false;
        $res = false;
        $sql = "SELECT id_cta_contable FROM ingresos.tipos_ingresos WHERE id = $tipo";
        //die($sql);
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $row = $this->conn->Execute($sql);
            $auxCta = $row->fields['id_cta_contable'];
            if(empty($auxCta))
                throw new ADODB_Exception("[ERROR: No existe cuenta contable] ",$this->conn->ErrorNo());

            $sql_ins = "INSERT INTO ingresos.doc_ing_enc (id_tipo, id_cta_contable, fecha, monto, nrodoc, fecha_caja, cerrado, tipo_caja)
                    VALUES($tipo, $auxCta, '$fecha', ".guardafloat($monto).",$nrodoc,'$fecha_caja', false,$tipo_caja)";
            //die($sql_ins);
            $res = $this->conn->Execute($sql_ins);
            if ($this->conn->HasFailedTrans())  
                    throw new Exception(__METHOD__,$this->conn->ErrorNo());

            if($json_det != "" ) {
                $sql = "SELECT max(id) as max FROM ingresos.doc_ing_enc";
                $rs = $this->conn->Execute($sql);
                if(!$rs->EOF) {
                    $id_com = $rs->fields["max"];
                    if ($id_com != "")
                        if( $this->set_det($id_com, $json_det)==false)
                            throw new Exception(__METHOD__,$this->conn->ErrorNo());
                    //Recalculo de total del documento de ingreso por BD
                    $qu = "UPDATE ingresos.doc_ing_enc SET monto =".$this->sumaDet($id_com)." WHERE id = $id_com";     
                    $rs = $this->conn->Execute($qu);
                    if( $this->conn->HasFailedTrans())
                            throw new Exception(__METHOD__,$this->conn->ErrorNo());
                }
            }

        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_ADD." Error #A:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_ADD_OK;
        return !$hasError;
    }

    public function set($id, $nrodoc, $tipo, $monto, $fecha_caja, $json_det,$tipo_caja) {
        if (empty($tipo))
            return false;
        //$res = false;
        $sql = "SELECT id_cta_contable FROM ingresos.tipos_ingresos WHERE id = $tipo";
        //die($sql);
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $row = $this->conn->Execute($sql);
            $auxCta = $row->fields['id_cta_contable'];
            if(empty($auxCta))
                throw new ADODB_Exception("[ERROR: No existe cuenta contable] ",$this->conn->ErrorNo());
            //$array = array($nrodoc, $tipo, $auxCta, guardafloat($monto), $fecha_caja, $tipo_caja,$id);
            //$sql_mod = "UPDATE ingresos.doc_ing_enc SET nrodoc = '$nrodoc', id_tipo = $tipo , id_cta_contable = $auxCta,
            //             monto =".guardafloat($monto).", fecha_caja = '$fecha_caja', tipo_caja = $tipo_caja WHERE id = $id";
            $sql_mod = "UPDATE ingresos.doc_ing_enc SET nrodoc = '$nrodoc', id_tipo = $tipo , id_cta_contable = $auxCta,
                         monto =0, fecha_caja = '$fecha_caja', tipo_caja = $tipo_caja WHERE id = $id";
            //die(var_dump($array));
            //$res = $this->conn->Execute($this->prep_upd, $array);
            //die($sql_mod);
            $res = $this->conn->Execute($sql_mod);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

            if ($json_det!="") {
                //die('dentro');
                if($this->set_det($id, $json_det)==false)
                    throw new ADODB_Exception("ERROR: Problemas con detalle de documento: ",$this->conn->ErrorNo());//Exception(__METHOD__,$this->conn->ErrorNo());
                $sql_act_mon = "UPDATE ingresos.doc_ing_enc SET monto =".$this->sumaDet($id)." WHERE id = $id";
                $rsql_act_mon = $this->conn->Execute($sql_act_mon);
                if ($this->conn->HasFailedTrans())  
                   throw new Exception(__METHOD__,$this->conn->ErrorNo());
            }
        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_SET." Error #S:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_SET_OK;
        return !$hasError;
    }

    public function delete($id) {
        if (empty($id))return false;

        $hasError=false;
        $this->conn->StartTrans();
        try {
            //die('Aqui'.$id);
            $qval = "SELECT cerrado FROM ingresos.doc_ing_enc WHERE id = $id";
            $rqval = $this->conn->Execute($qval);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

            if(!$rqval->EOF) {
                //die('aqui');
                if($rqval->fields['cerrado'] == 't')
                    throw new ADODB_Exception("[ERROR:Estado cerrado = 't'] ",$this->conn->ErrorNo());
                else {
                    $sql = "DELETE FROM ingresos.doc_ing_det WHERE id_documento = $id;
                              DELETE FROM ingresos.doc_ing_enc WHERE id = $id;";
                    $res = $this->conn->Execute($sql);
                    if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());
                }
            }
        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR_DEL." Error #D:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $this->msg= REG_DEL_OK;
        return !$hasError;
    }


    public function set_det($id_doc, $json_det) {
        if (empty($id_doc)) return false;
        $prep_det_ins = $this->conn->Prepare("INSERT INTO ingresos.doc_ing_det (id_documento, tipo, monto, id_banco, num_doc, fecha_documento)
                                              VALUES (?, 1, ?, 1, ?, ?);");		
        $sql = "DELETE FROM ingresos.doc_ing_det WHERE id_documento = $id_doc";
        $hasError=false;
        $this->conn->StartTrans();
        
        try {
            $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());
            
            $JsonRec = new Services_JSON();
            $JsonRec = $JsonRec->decode(str_replace("\\","",$json_det));
            //Se modifica esta funcion para buscar el id si se pasa el codigo de la cuenta o si se pasa ya el id de la cuenta
            ///$res = false;  --- por Javier Hernandez, se agrego transaccion y se elimina estas lineas
            //$res_final = true;
            if(is_array($JsonRec)) {
                //die(var_dump($JsonRec));
                foreach($JsonRec as $det) {
                    //echo(var_dump($det));
                    if($det->nrodoc != '' and $det->monto != '' and $det->fecha !='') {
                        $array = array($id_doc, guardaFloat($det->monto),  $det->nrodoc, guardafecha(str_replace('/','-',$det->fecha)));
                        //echo(var_dump($array).'<b>');
                        $res = $this->conn->Execute($prep_det_ins,$array);
                        if ($this->conn->HasFailedTrans())  
                                throw new Exception(__METHOD__,$this->conn->ErrorNo());
                    }
                }
            }
        }catch(ADODB_Exception $e) {
            $this->msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $this->msg = ERROR." Error #SD:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) 
            $this->msg= OK;
        return !$hasError;
    }

    public function get_all($tipo="", $fecha_desde="", $fecha_hasta="", $from=0, $max=0, $orden="fecha") {
        if (empty($tipo) && empty($fecha_desde) && empty($fecha_hasta))
            return false;

        $sql = "SELECT id, fecha FROM ingresos.doc_ing_enc ";
        $sql.= "WHERE 1=1 ";
        $sql.= !empty($tipo) ? "AND id_tipo = '$tipo' " : "";
        $sql.= !empty($fecha_desde) ? "AND fecha >= '".guardafecha($fecha_desde)."' " : "";
        $sql.= !empty($fecha_hasta) ? "AND fecha <= '".guardafecha($fecha_hasta)."' " : "";
        $sql.= !empty($nrodoc) ? "AND nrodoc = '$nrodoc' " : "";
        $sql.= " ORDER BY $orden";
        //die($sql);
        $rs = ($max!=0) ? $this->conn->SelectLimit($sql, $max, $from) : $this->conn->Execute($sql);
        $grs = array();
        while(!$rs->EOF) {
            $gr = new documentos_ing($this->conn);
            $gr->get($rs->fields['id']);
            $grs[] = $gr;
            $rs->moveNext();
        }

        $this->lista = $grs;
        $this->lista_count = count($grs);
        return $grs;
    }

    function total_registro_busqueda($tipo="", $fecha_desde="", $fecha_hasta="") {
        if (empty($tipo) && empty($fecha_desde) && empty($fecha_hasta))
            return false;

        $sql = "SELECT id, fecha FROM ingresos.doc_ing_enc ";
        $sql.= "WHERE 1=1 ";
        $sql.= !empty($tipo) ? "AND id_tipo = '$tipo' " : "";
        $sql.= !empty($fecha_desde) ? "AND fecha >= '".guardafecha($fecha_desde)."' " : "";
        $sql.= !empty($fecha_hasta) ? "AND fecha <= '".guardafecha($fecha_hasta)."' " : "";
        $sql.= !empty($nrodoc) ? "AND nrodoc = '$nrodoc' " : "";
        //die($sql);
        $r = $this->conn->Execute($sql);
        return $r->RecordCount();
    }


    function get_ingresos_fecha_caja($fecha_caja,$tipo) {
        if (empty($fecha_caja)) return false;
        //$q = "SELECT * FROM ingresos.doc_ing_enc WHERE fecha_caja = '$fecha_caja' AND tipo_caja = $tipo";
        $q = "select a.id_cta_contable, sum(a.monto) as monto,a.tipo_caja, a.fecha,a.id_tipo,a.nrodoc,max(b.operacion) as operacion
              from ingresos.doc_ing_enc a
              inner join ingresos.tipos_ingresos b on a.id_tipo = b.id 
              where a.fecha ='$fecha_caja' and a.tipo_caja = $tipo and a.cerrado = false
              group by a.id_cta_contable,a.tipo_caja,a.fecha,a.id_tipo,a.nrodoc";
        //die($q);
        $r = $this->conn->Execute($q);
        if(!$r->EOF){
            while(!$r->EOF) {
                $ue             = new documentos_ing($this->conn);
                $ue->nrodoc     = $r->fields['nrodoc'];
                $ue->id_tipo    = $r->fields['id_tipo'];
                $ue->fecha      = $r->fields['fecha'];
                $ue->monto      = $r->fields['monto'];
                $ue->operacion  = $r->fields['operacion'];
                $ue->tipo_caja  = $r->fields['tipo_caja'];
                $coleccion[]    = $ue;
                $r->MoveNext();
            }
        }else{
            $coleccion = 0;
        }     
        $this->detalle = new Services_JSON();
        $this->detalle= is_array($coleccion) ? $this->detalle->encode($coleccion) : false;
        return $coleccion;
    }

    //Busqueda de documento de ingreso en la base de datos
    function busca_doc_ingresos($doc,$tipo_ing) {
        if (empty($doc) or empty($tipo_ing)) return false;
        //$q = "SELECT * FROM ingresos.doc_ing_det WHERE num_doc = '$doc' ";
        $q = "select a.num_doc,b.fecha,c.descripcion from ingresos.doc_ing_det a
              inner join ingresos.doc_ing_enc b on a.id_documento = b.id
              inner join ingresos.tipos_ingresos c on b.id_tipo =  c.id
              where substring(a.num_doc from 1 for 1) != 'T' and a.num_doc = '$doc' and c.id = $tipo_ing
              group by a.id_documento,a.num_doc,b.fecha,c.descripcion 
             ";
        //die($q);
        $r = $this->conn->Execute($q);
        if($r->EOF) {
            return false;
        }else {
            return 'Fecha: '.$r->fields['fecha'].' #: '.$r->fields['num_doc'].' : '.$r->fields['descripcion'];
        }
    }
    
    // Funcion que actualiza el numero de conciliacion a los documentos de ingresos
    // Javier Hernandez
    // 07/06/2010
    function setDatosConc_fromEdoCta($id_conc,$edoCta) {
        $q= "UPDATE ONLY ingresos.doc_ing_det SET  id_conciliacion=$id_conc
             FROM ingresos.doc_ing_enc dienc,ingresos.tipos_ingresos ting, contabilidad.relacion_estado_cuenta redo, contabilidad.estado_cuenta edo, finanzas.cuentas_bancarias cta
            WHERE ingresos.doc_ing_det.id_documento = dienc.id
              AND ting.id = dienc.id_tipo
              AND ingresos.doc_ing_det.num_doc= redo.num_documento
              AND ting.origen = redo.tipo_documento
              AND edo.id = redo.id_estado_cuenta
              AND cta.id = edo.id_cuenta
              AND redo.id_estado_cuenta= $edoCta AND dienc.id_cta_contable= cta.id_plan_cuenta";

        $hasError=false;
        $this->conn->StartTrans();
        try {
            $r = $this->conn->Execute($q);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR." Error #:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;

    }

    function sumaDet($id_doc){
        $q = "select sum(monto) as total from ingresos.doc_ing_det where id_documento = $id_doc";
        $rq = $this->conn->Execute($q);
        if(!$rq->EOF)
           return $rq->fields['total'];
        else
           return 0; 
    }
}
?>