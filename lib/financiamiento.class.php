<?
class financiamiento {

    // Propiedades
    var $msg = "";
    var $hasError=false;
    var $id;
    var $descripcion;
    var $genera_retenciones;
    var $genera_pagos_parciales;
    var $total;

    function get($conn, $id) {
        $id = trim($id);
        if (empty($id)) return false;
        $q = "SELECT * FROM financiamiento ";
        $q.= "WHERE id='$id'";
        //die($q);
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $r->fields['id'];
            $this->descripcion = $r->fields['descripcion'];
            $this->genera_retenciones = $r->fields['genera_retenciones'];
            $this->genera_pagos_parciales = $r->fields['genera_pagos_parciales'];
            return true;
        }else
            return false;
    }

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM financiamiento ";
        $q.= "ORDER BY $orden ";

        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new financiamiento;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn, $descripcion) {    //, $genera_retenciones, $genera_pagos_parciales){
        $genera_retenciones = ($genera_retenciones)? "true" : "false";
        $genera_pagos_parciales = ($genera_pagos_parciales)? "true" : "false";
        $q = "INSERT INTO financiamiento ";
        $q.= "(descripcion, genera_retenciones, genera_pagos_parciales) ";
        $q.= "VALUES ";
        $q.= "('$descripcion', $genera_retenciones, $genera_pagos_parciales ) ";
        //die($q);
        //1
        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function set($conn, $id, $descripcion) {    //, $genera_retenciones, $genera_pagos_parciales){
        $genera_retenciones = ($genera_retenciones)? "true" : "false";
        $genera_pagos_parciales = ($genera_pagos_parciales)? "true" : "false";
        $q = "UPDATE financiamiento SET descripcion='$descripcion', ";
        $q.= "genera_retenciones = $genera_retenciones, genera_pagos_parciales = $genera_pagos_parciales ";
        $q.= "WHERE id='$id' ";
        //die($q);

        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $id) {
        $q = "DELETE FROM financiamiento WHERE id='$id'";

        $hasError=false;
        $conn->StartTrans();
        try {
            $r = $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

}
?>
