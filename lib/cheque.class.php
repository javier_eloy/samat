<?
class cheque {

    #PROPIEDADES#

    var $id;
    var $id_banco;
    var $banco;
    var $id_nro_cuenta;
    var $nro_cuenta;
    var $nro_cheque;
    var $id_proveedor;
    var $proveedor;
    var $rif_proveedor;
    var $fecha;
    var $status;
    var $json;
    var $observacion;
    var $nrodoc;
    var $nomBenef;
    var $concepto;
    var $cerrado;

    function get($conn, $id) {

        $q = "SELECT * FROM finanzas.cheques WHERE id ='$id'";
        $r = $conn->execute($q);

        if (!$r->EOF) {

            $this->id           = $r->fields['id'];
            $this->id_banco	= $r->fields['id_banco'];
            $ban                = new banco;
            $ban->get($conn, $r->fields['id_banco']);
            $this->banco        = $ban->descripcion;
            $cue                = new cuentas_bancarias;
            $this->id_nro_cuenta= $r->fields['nro_cuenta'];
            $cue->get($conn, $r->fields['nro_cuenta']);
            $this->nro_cuenta	= $cue->nro_cuenta;
            $this->fecha        = $r->fields['fecha'];
            $cue                = new proveedores;
            $this->id_proveedor = $r->fields['id_proveedor'];
            $cue->get($conn, $r->fields['id_proveedor']);
            $this->proveedor    = $cue->nombre;
            $this->rif_proveedor= $cue->rif;
            $this->nro_cheque   = $r->fields['nro_cheque'];
            $this->status       = $r->fields['status'];
            $this->nrodoc       = $r->fields['nrodoc'];
            $this->observacion  = $r->fields['observacion'];
            $this->json         = $this->getRelaciones($conn, $r->fields['nrodoc']);
            $this->nomBenef     = $r->fields['nombre_beneficiario'];
            $this->concepto     = $r->fields['concepto'];
            $this->cerrado      = $r->fields['cerrado'];
            return true;

        }else {

            return false;

        }

    }

    function get_all($conn,$orden="id") {

        $q = "SELECT * FROM finanzas.cheque WHERE id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= "ORDER BY $orden ";
        $r = $conn->Execute($q);
        while(!$r->EOF) {
            $ue = new cheque;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn,$id_banco,$nro_cuenta,$nro_cheque,$id_proveedor,$fecha,$status,$aOrdenes, $id_usuario, $eseEnEje,$anoCurso,$observacion,$total_cheque,$nomBenef,$concepto ) {
        //VALIDO QUE LA CUENTA TENGA DISPONIBILIDAD
        $oCuentasBancarias =  new cuentas_bancarias;
        $oCuentasBancarias->get($conn,$nro_cuenta);
        $saldo = $oCuentasBancarias->saldo_inicial + $oCuentasBancarias->creditos - $oCuentasBancarias->debitos;
        //die('ch: '.$total_cheque.' saldo: '.$saldo);
        //Codigo Original de Valencia. Descubierto el 7-01-10 con la linea siguiente no se valida saldo en la cuenta
        $saldo = 9999999999;
        $hasError=false;
        $conn->StartTrans();
        try {

            if($saldo>=guardafloat($total_cheque)) {
                $nrodoc = cheque::getNroDoc($conn);
                $q = "INSERT INTO finanzas.cheques (id_banco,nro_cuenta,nro_cheque,id_proveedor,fecha,status,nrodoc, id_escenario,observacion,nombre_beneficiario,concepto ) ";
                $q.= "VALUES ($id_banco,'$nro_cuenta','$nro_cheque',$id_proveedor,'$fecha','$status','$nrodoc', '$eseEnEje','$observacion','$nomBenef','$concepto' ) ";
                $r = $conn->Execute($q);
                //die($q);
                if ($conn->HasFailedTrans()) {
                    $msgdetalle = 'No se puede grabar cheque';
                    throw new Exception(__METHOD__,$conn->ErrorNo());
                }            
                //$r=true; // eliminar, parametro directo de prueba
                //if($r) {
                //Actulizacion de Partidas presupuestarias

                if($this->addrelacion($conn, $nrodoc,$aOrdenes,$id_usuario,$anoCurso,$observacion,$fecha)==false)
                    throw new ADODB_Exception('Error al Actualizar Presupuesto',$conn->ErrorNo());
                    //throw new Exception(__METHOD__,$conn->ErrorNo());

                //Creo Comprobante contable
                if($this->checkAsiento($conn,$aOrdenes,$eseEnEje)==false)  // asiento
                    //Asiento de cheque con retenciones
                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 9::int2, $eseEnEje::int8)";
                else
                    //Asiento contable sin retenciones
                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 0::int2, $eseEnEje::int8)";

                $conn->Execute($q);
                if ($conn->HasFailedTrans())  
                        throw new ADODB_Exception($conn->ErrorMsg().'No se puede generar asiento contable',$conn->ErrorNo());
                //}
                //Actualizo chequera
                $q = "UPDATE finanzas.control_chequera SET ultimo_cheque=$nro_cheque WHERE nro_cuenta=$nro_cuenta";
                $r = $conn->Execute($q); 
                if ($conn->HasFailedTrans()){  
                    $msgdetalle = 'No se puede actualizar chequera.';
                    throw new Exception(__METHOD__,$conn->ErrorNo());
                }        

            }else {
                $msgdetalle = 'No hay disponibilidad en la cuenta bancaria';
                throw new Exception(__METHOD__,$conn->ErrorNo());

            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            if(!empty($msgdetalle)) 
                $msg=$msgdetalle;
            else
                $msg = ERROR_SET." Error #:".$e->getCode();

            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function checkRetenciones($conn,$nrodoc) {
        $q="SELECT COALESCE(SUM(mntret)::float,0) AS monto";
        $q.=" FROM finanzas.relacion_retenciones_orden";
        $q.=" WHERE nrodoc IN ";
        $q.=" (SELECT DISTINCT nroref";
        $q.="  FROM finanzas.relacion_cheque";
        $q.="  WHERE nrodoc = '$nrodoc')";
        $rM = $conn->Execute($q);
        if($rM->fields[0]>0) //mayor que cero es que si tiene retenciones
            return true;
        else
            return false;
    }

    //Valida si la orden tiene en su asiento contable anulado retenciones
    //Devuelve false en caso de conseguir y true de lo contrario
    function checkAsiento($conn,$Ordenes,$eseEnEje) {
        $JsonRec = new Services_JSON();
        $JsonRec = $JsonRec->decode(str_replace("\\","",$Ordenes));
        $resultado = False;
        if(is_array($JsonRec->ordenes)) {
            foreach($JsonRec->ordenes as $orden) {
                //echo $orden[0]."<br>";
                $q="select";
                $q.=" nrodoc";
                $q.=" from finanzas.relacion_cheque";
                $q.=" where";
                $q.=" nroref = '$orden[0]'";
                $q.=" order by nrodoc asc";
                //echo $q."-";
                $rM = $conn->Execute($q);
                while(!$rM->EOF) {
                    // hallar numero de cheque que se elimino (nrodoc)
                    $nroCheque=$rM->fields['nrodoc'];
                    $q2=" select id";
                    $q2.=" from contabilidad.com_enc";
                    $q2.=" where";
                    $q2.=" num_doc='$nroCheque'";
                    $q2.=" and status='A'";
                    //echo $q2;
                    $rM2 = $conn->Execute($q2);
                    if($rM2->fields['id']) {
                        $id=$rM2->fields['id'];
                        $q3="select";
                        $q3.=" sum(debe)";
                        $q3.=" from contabilidad.com_det";
                        $q3.=" where";
                        $q3.=" id_com=$id";
                        $q3.=" and id_cta in (select distinct(id_cta) from finanzas.retenciones_adiciones)";
                        //echo $q3;
                        $rM3 = $conn->Execute($q3);
                        if($rM3->fields[0]==0)
                            $resultado = true; // si tiene asientos de retenciones eliminadas
                    }
                    $rM->movenext();
                }
                //$resultado = false;
            }
        }
        return $resultado;
    }

    function anular($conn, $nrodoc, $escEnEje,$status,$observacion,$id_usuario,$anoCurso,$nomBenef,$concepto,$fecha,$valida_ret) {
        $hasError=false;
        $conn->StartTrans();
        //die($status);
        try {
            if($status==1 || $status==2) {
                if($status==1) { // status 1 conserva retenciones
                    #CAMBIO EL ESTATUS DEL CHEQUE POR ANULADO#
                    $q = "UPDATE finanzas.cheques SET  ";
                    $q.= "status = 1, observacion='$observacion' ";
                    $q.= "WHERE nrodoc='$nrodoc' AND id_escenario = '".$_SESSION['escEnEje']."'";
                    //die($q);
                    $r =$conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 0::int2, $escEnEje::int8)";
                    //echo $q;
                    $conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new ADODB_Exception($conn->ErrorMsg(),$conn->ErrorNo());
                }
                if($status==2) { // status 2 no conserva retenciones
                    #CAMBIO EL ESTATUS DEL CHEQUE POR ANULADO#
                    $q = "UPDATE finanzas.cheques SET  ";
                    $q.= "status = 1, observacion='$observacion' ";
                    $q.= "WHERE nrodoc='$nrodoc' AND id_escenario = '".$_SESSION['escEnEje']."'";
                    //die($q);
                    $r =$conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

                    $q="SELECT public.asiento_cheque ('$nrodoc'::varchar, 9::int2, $escEnEje::int8)";
                    $conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new ADODB_Exception($conn->ErrorMsg(),$conn->ErrorNo());                    
                }
                    //Ajuste de deuda por pagar
                    $q = "SELECT nroref,monto FROM finanzas.relacion_cheque WHERE nrodoc='$nrodoc' AND ano = '".$_SESSION['anoCurso']."'";
                    $r = $conn->Execute($q);

                    while(!$r->EOF) {
                        $nroOrden   = $r->fields['nroref'];
                        $monto      = $r->fields['monto'];
                        //Modificacion de consulta para evitar montos negativos en el monto pagado.
                        $q = "UPDATE finanzas.orden_pago SET montopagado=montopagado-$monto WHERE nrodoc='$nroOrden' and montopagado-$monto >=0";
                        $conn->Execute($q);
                        if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

                        $q = "SELECT montodoc,montoret,id_unidad_ejecutora,id_proveedor,nroref FROM finanzas.orden_pago WHERE nrodoc='$nroOrden' ";
                        $rM = $conn->Execute($q);
                        if($rM->fields['nroref']) {
                            $idUniEje=$rM->fields['id_unidad_ejecutora'];
                            $idProveedor=$rM->fields['id_proveedor'];
                            $porc=$monto*100/($rM->fields['montodoc']-$rM->fields['montoret']);
                            $fecha=date("Y-m-d");

                            $q = "INSERT INTO movimientos_presupuestarios ";
                            $q.= "(id_usuario, id_unidad_ejecutora, ano, descripcion, nrodoc, tipdoc, tipref,nroref, status_movimiento,  ";//nroref,
                            $q.= "fechadoc, fecharef, status, id_proveedor) ";
                            $q.= "VALUES ";
                            $q.= "($id_usuario, '$idUniEje', '$anoCurso', '$observaciones', '$nrodoc', '005', '','$nrodoc-ANULADO',2, ";//'$nroref',
                            $q.= " '$fecha', '$fecha', '3', $idProveedor) ";
                            //die($q);
                            $conn->Execute($q);
                            if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                            $q = "SELECT * FROM finanzas.relacion_orden_pago WHERE id_orden_pago='$nroOrden' ";
                            $rP = $conn->Execute($q);
                            while(!$rP->EOF) {
                                $MontoImputar=($rP->fields['monto']*$porc/100);
                                if(movimientos_presupuestarios::add_relacion_nomina($conn,
                                        $rP->fields['id_parcat'],
                                        $rP->fields['id_categoria_programatica'],
                                        $rP->fields['id_partida_presupuestaria'],
                                        $nrodoc,
                                        (-1*$MontoImputar))==false)
                                    throw new Exception(__METHOD__,$conn->ErrorNo());

                                if(relacion_pp_cp::set_desde_pagado_cheque_anulado($conn, $rP->fields['id_parcat'], $MontoImputar)==false)
                                    throw new Exception(__METHOD__,$conn->ErrorNo());

                                $rP->movenext();
                            }
                        }
                        $r->movenext();
                    }
                //}
            }else {
                #Actualizado datos del cheque, este no se anula#
                $q = "UPDATE finanzas.cheques SET  ";
                $q.= "observacion='$observacion', nombre_beneficiario = '$nomBenef', concepto = '$concepto', fecha = '$fecha'  ";
                $q.= "WHERE nrodoc='$nrodoc' AND id_escenario = '".$_SESSION['escEnEje']."' ";
                //die($q);
                $r =$conn->Execute($q);
                if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function addrelacion($conn, $nrodoc,$Ordenes,$id_usuario,$anoCurso,$observacion,$fecha) {
        $JsonRec = new Services_JSON();
        $JsonRec = $JsonRec->decode(str_replace("\\","",$Ordenes));

        $hasError=false;
        $conn->StartTrans();
        try {
            if(is_array($JsonRec->ordenes)) {
                foreach($JsonRec->ordenes as $orden) {
                    //INSERTO LA RELACION DETALLE DEL CHEQUE
                    $q = "INSERT INTO finanzas.relacion_cheque (nrodoc, nroref, monto, ano) VALUES ('$nrodoc','$orden[0]', $orden[1], $anoCurso)";
                    //echo $q."<br>";
                    $r= $conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

                    //ACTAULIZO EL MONTO PAGADO DE LA ORDEN DE PAGO
                    //montopagado=montopagado+$orden[1] esta suma se realiza para permitir pagar una orden fraccionadamente
                    $q = "UPDATE finanzas.orden_pago SET montopagado=montopagado+$orden[1] WHERE nrodoc='$orden[0]' and id_escenario = '".$_SESSION['escEnEje']."'";
                    //echo $q."<br>";
                    $r = $conn->Execute($q);
                    if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());
                    //REALIZO LA IMPUTACION PRESUPESTARIA
                    $q = "SELECT montodoc,montoret,id_unidad_ejecutora,id_proveedor,nroref FROM finanzas.orden_pago WHERE nrodoc='$orden[0]' and id_escenario = '".$_SESSION['escEnEje']."'";
                    //echo $q."<br>";
                    //die($q);
                    $rM = $conn->Execute($q);
                    if($rM->fields['nroref']) {
                        $idUniEje=$rM->fields['id_unidad_ejecutora'];
                        $idProveedor=$rM->fields['id_proveedor'];
                        $porc=$orden[1]*100/($rM->fields['montodoc']-$rM->fields['montoret']);

                        //$fecha=date("Y-m-d");

                        $q = "INSERT INTO movimientos_presupuestarios ";
                        $q.= "(id_usuario, id_unidad_ejecutora, ano, descripcion, nrodoc, tipdoc, tipref,nroref, ";//nroref,
                        $q.= "fechadoc, fecharef, status, id_proveedor) ";
                        $q.= "VALUES ";
                        $q.= "($id_usuario, '$idUniEje', '$anoCurso', '$observacion', '$nrodoc', '005', '','$orden[0]', ";//'$nroref',
                        $q.= " '$fecha', '$fecha', '3', $idProveedor) ";
                        $r = $conn->Execute($q);
                        if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

                        //echo $q."<br>";
                        $q = "SELECT * FROM finanzas.relacion_orden_pago WHERE id_orden_pago='$orden[0]' ";
                        $r = $conn->Execute($q);
                        while(!$r->EOF) {
                            $MontoImputar=$r->fields['monto']*$porc/100;
                            if(movimientos_presupuestarios::add_relacion_nomina($conn,
                                    $r->fields['id_parcat'],
                                    $r->fields['id_categoria_programatica'],
                                    $r->fields['id_partida_presupuestaria'],
                                    $nrodoc,
                                    $MontoImputar)==false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());

                            if(relacion_pp_cp::set_desde_pagado_cheque($conn, $r->fields['id_parcat'], $MontoImputar)==false)
                                throw new Exception(__METHOD__,$conn->ErrorNo());
                            $r->movenext();
                        }
                    }
                }
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }
    /*
	function actualizarMovimientos($conn, $nroref, $nrodoc, $id_usuario){
	
		$q="SELECT relacion_solicitud_pago.id_solicitud_pago,
			finanzas.relacion_orden_pago.nrodoc,
			relacion_solicitud_pago.id_categoria_programatica,
			relacion_solicitud_pago.id_partida_presupuestaria,
			relacion_solicitud_pago.monto,
			relacion_solicitud_pago.id_parcat
			FROM
			finanzas.relacion_orden_pago
			Inner Join puser.relacion_solicitud_pago ON 

			finanzas.relacion_orden_pago.nroref = relacion_solicitud_pago.id_solicitud_pago
			WHERE finanzas.relacion_orden_pago.nrodoc =  '$nroref'";
		//die($q);
		
		##ISMAEL DEPABLOS 28/12/2006
		##ESPERAR A REVISAR EL PROCESO DEL PAGO DE CHEQUES PARA SEGUIR MODIFICANDO ESTE MODULO
		
		$r = $conn->execute($q);	

			$ob = new movimientos_presupuestarios;
			$ob->getNroRef($conn,$r->fields['id_solicitud_pago'], 2);
			
			#REGISTRO EL CHEQUE EN MOVIMIENTOS PRESUPUESTARIOS#	
			$q2 = "INSERT INTO movimientos_presupuestarios ";
			$q2.= "(id_usuario, id_unidad_ejecutora, ano, descripcion, nrodoc, tipdoc, nroref,"; 
			$q2.= "fechadoc, fecharef, status, id_proveedor) ";
			$q2.= "VALUES ";
			$q2.= "('$id_usuario', '".$ob->id_unidad_ejecutora."', '".date('Y')."', '".$ob->descripcion."', '".$nrodoc."', '005', '$nroref', "; 
			$q2.= " '".date('Y-m-d')."', '".date('Y-m-d')."', '3', '".$ob->id_proveedor."') ";
			//echo $q2."<br>";
			//die($q2);
			$r2 = $conn->Execute($q2) or die($q2);
	}
	function addRelacionesMov($conn, $nrodoc, $monto_total, $monto_pagado, $nroref, $escEnEje){
		
		$q="SELECT relacion_solicitud_pago.id_solicitud_pago,
			finanzas.relacion_orden_pago.nrodoc,
			relacion_solicitud_pago.id_categoria_programatica,
			relacion_solicitud_pago.id_partida_presupuestaria,
			relacion_solicitud_pago.monto,
			relacion_solicitud_pago.id_parcat
			FROM
			finanzas.relacion_orden_pago
			Inner Join puser.relacion_solicitud_pago ON 
			finanzas.relacion_orden_pago.nroref = relacion_solicitud_pago.id_solicitud_pago
			WHERE finanzas.relacion_orden_pago.nrodoc =  '$nrodoc'";
		//die($q);
		$r = $conn->execute($q);	
		$ob = new movimientos_presupuestarios;
		if($aRelaciones = $ob->get_relaciones($conn,$r->fields['id_solicitud_pago'], $escEnEje)){
			foreach($aRelaciones as $relacion){
			
				$total_partidas = ($monto_pagado / $monto_total) * $relacion->monto;
				//die(muestraFloat($total_partidas));
				$aIdParCat[] 				= $relacion->idParCat;
				$aCategoriaProgramatica[] 	= $relacion->id_categoria_programatica;
				$aPartidaPresupuestaria[] 	= $relacion->id_partida_presupuestaria;
				$aMonto[] 					= muestraFloat($total_partidas);
			
			}
			
			movimientos_presupuestarios::add_relacion($conn,
						$aIdParCat,
						$aCategoriaProgramatica,
						$aPartidaPresupuestaria,
						$nroref,
						$aMonto);
		return true;
		}else{
			return false;
		}
	} 	*/
    function montoTotalCheque($conn, $nrodoc) {

        $q = "SELECT Sum(monto) as total FROM finanzas.relacion_cheque WHERE nrodoc =  '$nrodoc' AND ano = '".$_SESSION['anoCurso']."'";
        $r = $conn->execute($q);

        if($r)
            return $r->fields['total'];
        else
            return false;
    }
    function buscar($conn, $id_proveedor, $id_banco, $fecha_desde, $fecha_hasta, $nrodoc, $nrocheque, $cuenta, $orden="id", $from, $max) {
        if(empty($id_proveedor) && empty($id_banco) && empty($fecha_desde) && empty($fecha_hasta) && empty($nrodoc) && empty($nrocheque) && empty($cuenta))
            return false;

        $q = 	"SELECT * from finanzas.cheques  ";
        $q.= 	"WHERE  id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= 	!empty($nrodoc) ? "AND cheques.nrodoc='$nrodoc' ": "";
        $q.= 	!empty($nrocheque) ? "AND cheques.nro_cheque='$nrocheque' ": "";
        $q.= 	!empty($fecha_desde) ? "AND cheques.fecha >='".guardafecha($fecha_desde)."' ": "";
        $q.= 	!empty($fecha_hasta) ? "AND cheques.fecha <='".guardafecha($fecha_hasta)."' ": "";
        $q.= 	!empty($id_proveedor) ? "AND cheques.id_proveedor = '$id_proveedor'  ":"";
        $q.= 	!empty($id_banco) ? "AND cheques.id_banco = '$id_banco'  ":"";
        $q.= 	!empty($cuenta) ? "AND cheques.nro_cuenta = '$cuenta'  ":"";
        $q.= 	"ORDER BY cheques.$orden ";
        //die($q);
        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from):$conn->Execute($q);
        if(!r || $r->EOF)
            return false;

        $collection=array();
        while(!$r->EOF) {
            $ue = new cheque;
            $ue->get($conn, $r->fields['id']);
            $coleccion[] = $ue;
            $r->movenext();
        }
        return $coleccion;
    }

    function totalRegsBusqueda($conn, $id_proveedor, $id_banco, $fecha_desde, $fecha_hasta, $nrodoc,$cuenta) {
        if(empty($id_proveedor) && empty($id_banco) && empty($fecha_desde) && empty($fecha_hasta) && empty($nrodoc) && empty($cuenta))
            return 0;

        $q = 	"SELECT * from finanzas.cheques  ";
        $q.= 	"WHERE  id_escenario = '".$_SESSION['escEnEje']."' ";
        $q.= 	!empty($nrodoc) ? "AND cheques.nrodoc='$nrodoc' ": "";
        $q.= 	!empty($nrocheque) ? "AND cheques.nro_cheque='$nrocheque' ": "";
        $q.= 	!empty($fecha_desde) ? "AND cheques.fecha >='".guardafecha($fecha_desde)."' ": "";
        $q.= 	!empty($fecha_hasta) ? "AND cheques.fecha <='".guardafecha($fecha_hasta)."' ": "";
        $q.= 	!empty($id_proveedor) ? "AND cheques.id_proveedor = '$id_proveedor'  ":"";
        $q.= 	!empty($id_banco) ? "AND cheques.id_banco = '$id_banco'  ":"";
        $q.= 	!empty($cuenta) ? "AND cheques.nro_cuenta = '$cuenta'  ":"";
        //die($q);
        $r = $conn->Execute($q);

        return $r->RecordCount();
    }

    //Se obtienen los datos del cheque y la orden de pago a nivel de montos, luego de estar guardados.
    function getRelaciones($conn, $nrodoc) {

        $q="SELECT A.nroref,A.monto::numeric,(B.montodoc-B.montoret-B.montopagado)::numeric AS montopagar,B.montopagado::numeric AS montopagado, B.id_tipo_solicitud_si AS tipo_si FROM finanzas.relacion_cheque AS A INNER JOIN finanzas.orden_pago AS B ON A.nroref=B.nrodoc WHERE A.nrodoc='$nrodoc'";
        //die($q);
        $r= $conn->execute($q);

        while(!$r->EOF) {

            $ch = new cheque;
            $ch->nroref         = $r->fields['nroref'];     //Nro de orden de pago
            $ch->montopagar     = $r->fields['montopagar']; //Saldo
            $ch->montopagado    = $r->fields['montopagado'];//Pagado
            $ch->monto          = $r->fields['monto'];      //Monto del cheque
            $ch->tipo_si        = $r->fields['tipo_si'];    //Sin imputacion presupuestaria o no
            $coleccion[]        = $ch;
            $r->movenext();

        }

        $json = new Services_JSON();
        return $json->encode($coleccion);

    }
    function getNroDoc($conn) {
        $q = "SELECT max(nrodoc) AS nrodoc FROM finanzas.cheques  WHERE id_escenario ='".$_SESSION['escEnEje']."'";
        $r = $conn->execute($q);
        $q2 = "SELECT MAX(nrodoc) AS nrodoc FROM finanzas.cheques_anteriores WHERE id_escenario ='".$_SESSION['escEnEje']."'";
        $r2 = $conn->Execute($q2);
        if($r->fields['nrodoc']>=$r2->fields['nrodoc']) {
            $resp = $r->fields['nrodoc'];
        } else {
            $resp = $r->fields['nrodoc'];
        }
        return "005-".str_pad(substr($r->fields['nrodoc'], 4, 4) + 1, 4, 0, STR_PAD_LEFT)."-".$_SESSION['anoCurso'];
    }

    function addLote($conn, $id_banco, $nro_cuenta,$nro_cheque_desde, $nro_cheque_hasta, $id_proveedor,$fecha, $eseEnEje,$observacion) {
        for($i=$nro_cheque_desde;$i<=$nro_cheque_hasta;$i++) {
            $nrodoc = cheque::getNroDoc($conn);
            $q = "INSERT INTO finanzas.cheques (id_banco,nro_cuenta,nro_cheque,id_proveedor,fecha,status,nrodoc, id_escenario,observacion,nombre_beneficiario,concepto ) ";
            $q.= "VALUES ($id_banco,'$nro_cuenta','$i','".$GLOBALS['idProveedorEnte']."','$fecha','1','$nrodoc', '".$_SESSION['escEnEje']."','$observacion','','') ";
            //die($q);
            $r = $conn->execute($q);
        }
        $this->msg = $r ?  REG_ADD_OK : ERROR_ADD;
    }


    function revisaLote($conn, $id_banco, $nro_cuenta,$nro_cheque_desde, $nro_cheque_hasta) {
        $coleccion = array();
        for($i=$nro_cheque_desde;$i<=$nro_cheque_hasta;$i++) {
            $q = "SELECT nro_cheque  FROM finanzas.cheques WHERE id_banco=$id_banco AND nro_cuenta=$nro_cuenta AND nro_cheque = $i ";
            $r = $conn->execute($q);
            if (!$r->EOF) {
                $coleccion[]=$r->fields['nro_cheque'];
            }
        }
        //die($q);
        return $coleccion;
    }
}
?>