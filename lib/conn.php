<?
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING); # reporta todos los errores
ini_set("display_errors", 0); # desactiva la muestra en pantalla
define('ADODB_ERROR_LOG_TYPE',3); # El tipo 3 es para log en archivos
define('ADODB_ERROR_LOG_DEST',$appRoot.'/errores/SCG3_errores.log'); # destino del log
define('ADODB_LANG','es'); #idioma de los errores

//Implementacion de manejo de errores mediante archivos Logs.
include_once('adodb/adodb-errorhandler.inc.php');
require_once('adodb/adodb-exceptions.inc.php');//OJO:declarar siempre la libreria de Manejo de Errores ANTES que la libreria de Excepciones
require_once('adodb/adodb.inc.php');
register_shutdown_function('checkerror_shutdown'); /// Registra la funcion que verifica los errores fatales

$conn = &ADONewConnection('postgres');
$conn->Connect($DBserver, $DBuser, $DBpass, $DBname); 

function setDBError($error,$code) {
    $msgerror=$error->getMessage();

    $i=strpos($msgerror,"ERROR:");
    if($i !== false) $i+=6;

    $j=stripos($msgerror,"] ");
    if($j===false) $j=stripos($msgerror,"CONTEXT:");
    if($j===false) $j=stripos($msgerror,"DETAIL:");

    if($j > $i) $msgerror=substr($msgerror,$i,$j-$i);

    switch($code) {
        case -1:
            return ERROR_CATCH_VFK." DETALLE: ".$msgerror;
        case -5:
            return ERROR_CATCH_VUK;
        default:
            return ERROR_CATCH_GENERICO. " DETALLE: ".$msgerror;
    }
}

function checkerror_shutdown() {
    $isError = false;
    if ($error = error_get_last()) {
        switch($error['type']) {
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
                $isError = true;
                break;
        }
    }
    if ($isError) {
        error_log("Error Fatal: ".$error['message']." Archivo: ".$error['file']." Linea:".$error['line']."\n", ADODB_ERROR_LOG_TYPE, ADODB_ERROR_LOG_DEST);
        header("location:ErrorFatal.php");
    }
}
?>
