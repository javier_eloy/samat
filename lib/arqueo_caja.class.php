<?
class arqueo_caja {
    var $msg = "";
    var $hasError=false;

    public $id; //id autonumerico

    public $fecha;
    public $detalle;
    public $status;
    public $monto_debe;
    public $monto_haber;
    public $tipo_caja;

    private $conn; //Conexion a base de datos
    private $prep_ins; //Sentencia Prepare para el Insert
    private $prep_ins_det;
    private $prep_upd; //Sentencia Prepare para el Update

    function __construct($conn) {
        $this->conn = $conn;
        $this->prep_ins = $this->conn->Prepare("INSERT INTO ingresos.ingresos_enc (fecha,descripcion,status,monto_debe,monto_haber,tipo_caja) VALUES(?, ?, ?, ?, ?,?);");
        $this->prep_upd=$this->conn->Prepare("UPDATE ingresos.ingresos_enc SET fecha = ?,descripcion = ?,status= ?,monto_debe = ?,monto_haber = ?, tipo_caja = ? WHERE id = ?;");
    }

    function __destruct() {
    }

    public function get($id) {

        $id = str_replace('/','-',$id);
        $sql = "SELECT * FROM ingresos.ingresos_enc WHERE id = $id";

        $rs  = $this->conn->Execute($sql);
        $res = false;
        if(!$rs->EOF) {
            $this->id           = $id;
            $this->fecha        = $rs->fields["fecha"];
            $this->descripcion  = $rs->fields["descripcion"];
            $this->status       = $rs->fields["status"];
            $this->monto_debe   = $rs->fields["monto_debe"];
            $this->monto_haber  = $rs->fields["monto_haber"];
            $this->tipo_caja    = $rs->fields["tipo_caja"];

            if (!empty($id)) {
                $this->get_det($id);
            }
            $res = true;
        }
        //$rs->Close();
        return $res;
    }

    function get_det($id,$fecha='1900-01-01',$tipo=0) {

        //Obtengo todos los documentos desde la fuente misma.
        $q2 = "select ti.id,ti.descripcion,di.nrodoc, di.fecha_caja,di.monto, ti.operacion,
               case when ti.operacion=1 then 'Debe' when ti.operacion=2 then 'Haber' end as nom_operacion
               from ingresos.doc_ing_enc as di inner join ingresos.tipos_ingresos as ti on di.id_tipo = ti.id
               where di.fecha = '$fecha' and di.tipo_caja=$tipo order by ti.operacion";

        //Ingreso los documentos de ingresos de la fecha
        $r2 = $this->conn->Execute($q2);
        while(!$r2->EOF) {
            $ue = new documentos_ing($this->conn);
            $ue->nrodoc         = $r2->fields['nrodoc'];
            $ue->id_tipo_pago	= $r2->fields['id'];
            $ue->monto          = $r2->fields['monto'];
            $ue->fecha          = $r2->fields['fecha_caja'];
            $ue->origen         = $r2->fields['operacion'];
            $ue->tipo_caja      = $r2->fields['tipo_caja'];

            $coleccion[] = $ue;
            $r2->movenext();
        }
        
        $q = "SELECT * FROM ingresos.ingresos_det WHERE id_caja = $id AND nrodoc = 'AC'";

        $r = $this->conn->Execute($q);
        while(!$r->EOF) {
            $ue = new documentos_ing($this->conn);
            $ue->nrodoc         = $r->fields['nrodoc'];
            $ue->id_tipo_pago	= $r->fields['id_tipo_pago'];
            $ue->monto          = $r->fields['monto'];
            $ue->fecha          = $r->fields['fecha'];
            $ue->origen         = $r->fields['origen'];
            $ue->tipo_caja      = $tipo;

            $coleccion[] = $ue;
            $r->movenext();
        }

        $this->detalle = new Services_JSON();
        $this->detalle= is_array($coleccion) ? $this->detalle->encode($coleccion) : false;
        return $coleccion;
    }

    public function create($fecha,$descripcion,$status,$monto_debe,$monto_haber,$json_det,$tipo_caja) {
        $hasError=false;
        $this->conn->StartTrans();

        try {
            $array = array($fecha,$descripcion,$status,guardafloat($monto_debe),guardafloat($monto_haber),$tipo_caja);
            //die(var_dump($array));
            $this->conn->Execute($this->prep_ins,$array);
            if ($this->conn->HasFailedTrans())throw new Exception(__METHOD__,$this->conn->ErrorNo());

            if($json_det != "" ) {
                $sql = "SELECT max(id) as max FROM ingresos.ingresos_enc";
                $rs = $this->conn->Execute($sql);
                //var_dump($rs->fields);
                if(!$rs->EOF) {
                    $id_com = $rs->fields["max"];
                    if ($id_com != "")
                        if($this->set_det($id_com, $json_det,$fecha,$tipo_caja)==false)
                            throw new Exception(__METHOD__,$this->conn->ErrorNo());

                }
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_ADD." Error #C:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }


    public function set($id,$fecha,$descripcion,$status,$monto_debe,$monto_haber,$json_det,$tipo_caja) {
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $array = array($fecha,$descripcion,$status,guardafloat($monto_debe),guardafloat($monto_haber), $tipo_caja,$id);
            //die(var_dump($array));
            
            $this->conn->Execute($this->prep_upd,$array);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

            if ($json_det!="" )
                if ($this->set_det($id, $json_det,$fecha,$tipo_caja)==false)
                    throw new Exception(__METHOD__,$this->conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #S:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    public function delete($id) {
        $sql = "DELETE FROM ingresos.ingresos_enc WHERE id = $id";
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #D:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= REG_DEL_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }


    public function set_det($id_doc, $json_det,$fecha_caja,$tipo_caja) {
        //die('set_det');
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $prep_det_ins = $this->conn->Prepare("INSERT INTO ingresos.ingresos_det (id_tipo_pago, nrodoc, fecha, monto, origen, id_caja)
                                                    VALUES (?, ?, ?, ?, ?, ?)");
            $sql = "DELETE FROM ingresos.ingresos_det WHERE id_caja=$id_doc"; // AND nrodoc != 'AC'";
            $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

            $JsonRec = new Services_JSON();
            $JsonRec = $JsonRec->decode(str_replace("\\","",$json_det));
            //Se modifica esta funcion para buscar el id si se pasa el codigo de la cuenta o si se pasa ya el id de la cuenta
            //$sql = "UPDATE ingresos.doc_ing_enc SET cerrado = False WHERE fecha_caja = '$fecha_caja' AND tipo_caja= $tipo_caja";
            //die($sql);
            //$this->conn->Execute($sql);
            //if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

            //die(var_dump($JsonRec));

            if(is_array($JsonRec)) {
                foreach($JsonRec as $det) {
                    //Este numero de documento nro_doc esta malo no esta relacionando al documento de ingreso sino al detalle.
                    //y la funcion funciona con el id del doc. ingreso.
                    //if($this->set_doc_cerrados($fecha_caja,$det->nro_doc,$tipo_caja)==false)
                    //if($this->set_doc_cerrados($fecha_caja,$id_doc,$tipo_caja)==false)        
                    //    throw new Exception(__METHOD__,$this->conn->ErrorNo());
                    $tipo       = is_numeric($det->tipo)?$det->tipo:-1;
                    //echo $det->origen.'<br>';
                    //echo $tipo;
                    if($tipo>-1) {
                        
                        $nrodoc     = empty($det->nro_doc)?'S/N':$det->nro_doc;
                        $fechadoc   = empty($det->fecha)?$fecha_caja:guardafecha($det->fecha);
                        $array      = array($det->tipo, $nrodoc, $fechadoc, guardaFloat($det->monto),  $det->origen, $id_doc);
                        //echo(var_dump($array)."<br>");
                        $this->conn->Execute($prep_det_ins,$array);
                        if ($this->conn->HasFailedTrans())  
                           throw new Exception(__METHOD__,$this->conn->ErrorNo());
                    }
                }
                //die('aqui');
            }
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_SET." Error #SD:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function set_doc_cerrados($fecha_caja, $nrodoc,$tipo_caja) {
        $sql = "UPDATE ingresos.doc_ing_enc SET cerrado = True WHERE ";
        //Elimino el filtrado por nrodoc ya que no lo tengo al momento de grabar los registros
        //Ademas de esta forma estoy tomando en cuenta todos los documentos de ingresos porcesados para la fecha y tipo de caja
        //$sql.= "fecha_caja = '".$fecha_caja."' AND id = $nrodoc AND tipo_caja = $tipo_caja";
        $sql.= "fecha = '".$fecha_caja."' AND tipo_caja = $tipo_caja";
        //echo($sql);
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $r = $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR." Error #SDC:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    public function get_all($fecha_desde="", $fecha_hasta="", $from=0, $max=0, $orden="fecha") {
        if (empty($fecha_desde) && empty($fecha_hasta))
            return false;

        $sql = "SELECT id, fecha FROM ingresos.ingresos_enc ";
        $sql.= "WHERE 1=1 ";
        $sql.= !empty($fecha_desde) ? "AND fecha >= '".guardafecha($fecha_desde)."' " : "";
        $sql.= !empty($fecha_hasta) ? "AND fecha <= '".guardafecha($fecha_hasta)."' " : "";
        $sql.= " ORDER BY $orden";
        //die($sql);
        $rs = ($max!=0) ? $this->conn->SelectLimit($sql, $max, $from) : $this->conn->Execute($sql);
        $grs = array();
        while(!$rs->EOF) {
            $gr = new arqueo_caja($this->conn);
            $gr->get($rs->fields['id']);
            $grs[] = $gr;
            $rs->movenext();
        }

        $this->lista        = $grs;
        $this->lista_count  = count($grs);
        return $grs;
    }

    function total_registro_busqueda($fecha_desde="", $fecha_hasta="") {
        if (empty($fecha_desde) && empty($fecha_hasta))
            return false;

        $sql = "SELECT id, fecha FROM ingresos.ingresos_enc ";
        $sql.= "WHERE 1=1 ";
        $sql.= !empty($fecha_desde) ? "AND fecha >= '".guardafecha($fecha_desde)."' " : "";
        $sql.= !empty($fecha_hasta) ? "AND fecha <= '".guardafecha($fecha_hasta)."' " : "";
        //die($sql);
        $r = $this->conn->Execute($sql);
        return $r->RecordCount();
    }

    function cerrar_caja($id,$fecha,$tipo_caja) {
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $sql = "SELECT ingresos.asiento_cierre_caja($id::bigint, 1::smallint)";
            $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new ADODB_Exception($this->conn->ErrorMsg(),$this->conn->ErrorNo());

            $sql2 = "UPDATE ingresos.ingresos_enc SET status = 2 WHERE id = ".$id;
            $this->conn->Execute($sql2);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());
            
            //Este numero de documento nro_doc esta malo no esta relacionando al documento de ingreso sino al detalle.
            //y la funcion funciona con el id del doc. ingreso.
            //if($this->set_doc_cerrados($fecha_caja,$det->nro_doc,$tipo_caja)==false)
            if($this->set_doc_cerrados($fecha,$id,$tipo_caja)==false)        
                throw new Exception(__METHOD__,$this->conn->ErrorNo());
            
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR." Error #:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function reversar_cierre_caja($id) {
        $hasError=false;
        $this->conn->StartTrans();
        try {
            $sql = "SELECT ingresos.asiento_cierre_caja($id::bigint, 2::smallint)";
            $this->conn->Execute($sql);
            if ($this->conn->HasFailedTrans())  throw new ADODB_Exception($this->conn->ErrorMsg(),$this->conn->ErrorNo());
            
            $sql2 = "UPDATE ingresos.ingresos_enc SET status = 1 WHERE id = ".$id;
            $this->conn->Execute($sql2);
            if ($this->conn->HasFailedTrans())  throw new Exception(__METHOD__,$this->conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$this->conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR." Error #:".$e->getCode();
            if(!$this->conn->HasFailedTrans())$this->conn->FailTrans();
        }
        $hasError = $this->conn->HasFailedTrans();
        $this->conn->CompleteTrans();
        if (!$hasError) $msg= OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }
    
    //Verifica si existe para la fecha y tipo de caja arqueo de caja cerrado.
    //Esta validacion se aplica para impedir la carga de documentos de ingreso para dias cerrados
    //Retorna: false si hay cajas cerradas para los parametros dados
    //y true de no haber caja cerrada.
    function validaEstatus($fecha,$tipo){
        if($fecha != '' and $tipo != ''){
            $q = "select * from ingresos.ingresos_enc where fecha = '$fecha' and tipo_caja = $tipo and status = 2";
            //die($q);
            $rq = $this->conn->Execute($q);
            if(!$rq->EOF)
                return 'false';
            else
                return 'true';
        }else{
            return 'false';
        }
    }
}
?>