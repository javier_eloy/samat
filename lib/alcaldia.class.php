<?
class alcaldia {

    // Propiedades
    var $msg = "";
    var $hasError=false;
    var $id;
    var $descripcion;
    var $razon;
    var $domicilio;
    var $fecha_creacion;
    var $ciudad;
    var $estado;
    var $telefono;
    var $fax;
    var $web_site;
    var $cpostal;
    var $alcalde;
    var $personal;
    var $concejales;
    var $id_proveedor;
    var $proveedor;

    var $total;

    function get($conn, $id) {
        $q = "SELECT A.*, B.nombre AS nombre_proveedor FROM puser.alcaldia AS A ";
        $q.= "INNER JOIN puser.proveedores AS B ON (A.id_proveedor = B.id) ";
        $q.= "WHERE A.id='$id'";
        //die($q);
        $r = $conn->Execute($q);
        if(!$r->EOF) {
            $this->id = $r->fields['id'];
            $this->descripcion = $r->fields['descripcion'];
            $this->razon = $r->fields['razon'];
            $this->domicilio = $r->fields['domicilio'];
            $this->fecha_creacion = muestrafecha($r->fields['fecha_creacion']);
            $this->ciudad = $r->fields['ciudad'];
            $this->estado = $r->fields['estado'];
            $this->telefono = $r->fields['telefono'];
            $this->fax = $r->fields['fax'];
            $this->web_site = $r->fields['web_site'];
            $this->cpostal = $r->fields['cpostal'];
            $this->alcalde = $r->fields['alcalde'];
            $this->personal = $r->fields['personal'];
            $this->concejales = $r->fields['concejales'];
            $this->id_proveedor = $r->fields['id_proveedor'];
            $this->proveedor = $r->fields['nombre_proveedor'];
            return true;
        }else
            return false;
    }

    function get_all($conn, $from=0, $max=0,$orden="id") {
        $q = "SELECT * FROM puser.alcaldia ";
        $q.= "ORDER BY $orden ";

        $r = ($max!=0) ? $conn->SelectLimit($q, $max, $from) : $conn->Execute($q);
        $collection=array();
        while(!$r->EOF) {
            $ue = new alcaldia;
            $ue->get($conn, $r->fields[0]);
            $coleccion[] = $ue;
            $r->movenext();
        }
        $this->total = $r->RecordCount();
        return $coleccion;
    }

    function add($conn,
            $id,
            $descripcion,
            $razon,
            $domicilio,
            $fecha_creacion,
            $ciudad,
            $estado,
            $telefono,
            $fax = '',
            $web_site = '',
            $cpostal,
            $alcalde,
            $personal,
            $concejales,
            $id_proveedor) {

        $hasError=false;
        $conn->StartTrans();
        try {
            $q = "INSERT INTO alcaldia ";
            $q.= "(id, descripcion, razon, domicilio, fecha_creacion, ciudad, estado, fax, telefono, web_site, ";
            $q.= "cpostal, alcalde, personal, concejales,id_proveedor) ";
            $q.= "VALUES ('$id', '$descripcion', '$razon', '$domicilio', '".guardafecha($fecha_creacion)."', '$ciudad', '$estado', ";
            $q.= "'$telefono', '$fax', '$web_site', '$cpostal', '$alcalde', '$personal', '$concejales',$id_proveedor) ";
            $conn->Execute($q);
            if ($conn->HasFailedTrans()) throw new Exception(__METHOD__,$conn->ErrorNo());

        }
        catch (ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $msg = ERROR_ADD." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_ADD_OK;
        if($this) $this->msg=$msg;
        return !$hasError;
    }

    function set($conn, $id_nuevo,
            $id,
            $descripcion,
            $razon,
            $domicilio,
            $fecha_creacion,
            $ciudad,
            $estado,
            $telefono,
            $fax = '',
            $web_site = '',
            $cpostal,
            $alcalde,
            $personal,
            $concejales,
            $id_proveedor) {

        $hasError=false;
        $conn->StartTrans();
        try {
            $q = "UPDATE alcaldia SET id = '$id_nuevo', descripcion='$descripcion', razon = '$razon', domicilio = '$domicilio', ";
            $q.= "fecha_creacion = '".guardafecha($fecha_creacion)."', ciudad = '$ciudad', estado = '$estado', telefono = '$telefono', ";
            $q.= "fax = '$fax', web_site = '$web_site', cpostal = '$cpostal', alcalde = '$alcalde', personal = '$personal', ";
            $q.= "concejales = '$concejales', id_proveedor = $id_proveedor ";
            $q.= "WHERE id='$id' ";
            $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());

        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }
        catch (Exception $e) {
            $msg = ERROR_SET." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg= REG_SET_OK;
        if ($this) $this->msg=$msg;
        return !$hasError;
    }

    function del($conn, $id) {
        $hasError=false;
        $conn->StartTrans();
        try {
            $q = "DELETE FROM alcaldia WHERE id='$id'";
            $conn->Execute($q);
            if ($conn->HasFailedTrans())  throw new Exception(__METHOD__,$conn->ErrorNo());
        }catch(ADODB_Exception $e) {
            $msg = setDBError($e,$conn->ErrorNo());
        }catch (Exception $e) {
            $msg = ERROR_DEL." Error #:".$e->getCode();
            if(!$conn->HasFailedTrans())$conn->FailTrans();
        }
        $hasError = $conn->HasFailedTrans();
        $conn->CompleteTrans();
        if (!$hasError) $msg=REG_DEL_OK;
        if ($this) $this->msg= $msg;
        return !$hasError;
    }
}
?>
