<?php

include("comun/ini.php");
include("comprobantes_retencion.php");
include("Constantes.php");
$tipoReporte = $_REQUEST['tipo'];



$oCheque = new traFondosTerceros;
//Buscqueda con por nro de documento
//$qnrodoc = "SELECT id FROM finanzas.tra_fondos_terceros WHERE nrodoc = '".$_REQUEST['id']."'";
//$onrodoc = $conn->Execute($qnrodoc);
//$oCheque->get($conn, $onrodoc->fields['id']);
$oCheque->get($conn, $_REQUEST['id']);


function dividirStr($str, $max)
{
	$strArray = array();
    do
    {
		if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
      	else
        	$posF = -1;
      
      	if ($posF===false || $posF==-1)
      	{
        	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}
      	else
      	{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }
	while ($posF != -1);
    return ($strArray);
}

class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{
		$this->SetLeftMargin(5);
	}
	//Pie de página
	function Footer()
	{
		$this->SetFont('Arial','',8);
		$this->SetFont('Arial','',8);
		$this->SetXY(15,240);
		$this->Cell(42.5,5,"Elaborado por",1,0,'C');
		$this->SetX(62.5);
		$this->Cell(42.5,5,"FIRMA 1",1,0,'C');
		$this->SetX(115);
		$this->Cell(42.5,5,"FIRMA 2",1,0,'C');
		$this->SetX(167.5);
		$this->Cell(42.5,5,"Recibe conforme",1,0,'C');
		$this->SetX(15);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetX(62.5);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetX(115);
		$this->Cell(42.5,28,"",1,0,'C');
		$this->SetFont('Arial','',6);
				
		$this->SetXY(169.5,245);
		$this->Cell(47.5,7,"Nombre _________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(169.5);
		$this->Cell(47.5,7,"Fecha __________________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(169.5);
		$this->Cell(47.5,7,"C.I. ".utf8_decode('ó')." R.I.F. ______________________",0,0,'L');
		$this->Ln(6);
		$this->SetX(169.5);
		$this->Cell(47.5,7,"Firma __________________________",0,0,'L');
		$this->Ln(5);
		$this->SetXY(167.5,240);
		$this->Cell(42.5,28,"",1,0,'C');
		
	}
}

$pdf=new PDF('P');
$pdf->AliasNbPages();
$pdf->AddPage('P','Letter');
$pdf->SetFont('Arial','',10);
$json = new Services_JSON;
$cadena = str_replace("\\","",$oCheque->json);//-->OJO, MUY IMPORTANTE !!!
$vector = $json->decode($cadena);
$x = count($vector);
$docs = '';

for ($i=0;$i<$x;$i++)
{
	$docs.= '\''.$vector[$i]->nroref.'\',';
}
$docs = rtrim($docs,',');

// CHEQUE
$monto = 0;
$descripcion_detallada = '';
if($oCheque->detTransferencia!=false){
  
  $detTransferencia = new Services_JSON();
  $dDetTrans = $detTransferencia->decode($oCheque->detTransferencia);
  
  foreach($dDetTrans  as $detTrans){
    //echo($detTrans->monto."\n"); 
    //$pdf->cell(100,10,$detTrans,0,1,'','R');
    $descripcion_detallada = $descripcion_detallada.$detTrans->descripcion.', '; 
    $monto += $detTrans->monto;
  }
}else{
  $monto=-1;
}
//die(':ajaja');
//Consulta para identificar cuenta bancaria a utilizar
$qDatosCuenta = "SELECT A.id, A.id_cta, B.descripcion
                FROM contabilidad.fondos_terceros A
                INNER JOIN contabilidad.plan_cuenta B ON A.id_cta = B.id";
$rqDatosCuenta = $conn->Execute($qDatosCuenta);
if(!$rqDatosCuenta->EOF)
    $nrocuenta = utf8_decode($rqDatosCuenta->fields['descripcion']);
 //Numero de cheque
$nrocheque = utf8_decode($oCheque->nrodoc);

$pdf->SetX(10);
$pdf->Cell(80,5, $nrocuenta ,0, 0, 'L','L');
$pdf->Cell(55,5, $nrocheque,0, 0, 'C','C');
$pdf->SetX(130);
$pdf->Cell(70,5, '***********'.number_format($monto,2,',','.'),0, 0, 'R','R');
$pdf->ln(5);
$pdf->SetX(10);
$pdf->Cell(195,5, utf8_decode($oCheque->banco),0, 0, 'L','C');
$pdf->ln(15);
$pdf->SetXY(20,19);
$pdf->Cell(185,5, '***'. utf8_decode($oCheque->beneficiario).'***',0, 0, 'L','C');
$pdf->ln(5);

$montoLetras = num2letras($monto,false,true);

$desc_monto = dividirStr($montoLetras, intval(200/$pdf->GetStringWidth('M')));
$pdf->SetXY(25,26);
$pdf->Cell(195,5, strtoupper(utf8_decode($desc_monto[0])),0, 0, 'L','L');
$pdf->SetXY(25,32);
if($desc_monto[1] != '')
  $pdf->Cell(190,5, strtoupper(utf8_decode($desc_monto[1])),0, 0, 'L','L');
$pdf->ln(5);
$separaFecha= explode('-',$oCheque->fecha);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->SetX(15);
$pdf->Cell(195,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, '','L');
$pdf->ln(5);
$pdf->SetX(15);
$pdf->Cell(195,5,'NO ENDOSABLE, CADUCA A LOS 90 DIAS',0,0, '','L');
$pdf->ln(15);
$pdf->SetX(15);

$pdf->SetFont('Arial','B',8);
$pdf->ln(12);
$pdf->SetX(15);
$pdf->Cell(195,5, 'CONCEPTO',1, 0, 'C','C');
$pdf->Ln(10);
$pdf->SetDrawColor(255,255,255);
$pdf->SetWidths(array(25,20,25,75,20,20,20));
$pdf->SetAligns(array('C','C','C','L','C','C','C'));
$pdf->SetDrawColor (0,0,0);
$pdf->SetX(15);
$pdf->Cell(400, 5, "$oCheque->descripcion".': '.$descripcion_detallada);
$pdf->SetFont('Arial','',8);
//Consulto pagina actual para pie
$pdf->SetXY(15, 74);
$pdf->Cell(195,5,"",1,0,'C');
$pdf->Ln(5);
$pdf->SetX(15);
$pdf->Cell(195,160,"",1,0,'C');
$pdf->Ln(110);
//Banda de Firma
//banda_firma($pdf);

function banda_firma($pdf){

		$pdf->SetFont('Arial','',8);
		$pdf->SetXY(10,240);
		$pdf->Cell(42.5,5,"Elaborado por",1,0,'C');
		$pdf->SetX(57.5);
		$pdf->Cell(42.5,5,"FIRMA 1",1,0,'C');
		$pdf->SetX(110);
		$pdf->Cell(42.5,5,"FIRMA 2",1,0,'C');
		$pdf->SetX(162.5);
		$pdf->Cell(42.5,5,"Recibe conforme",1,0,'C');
		$pdf->SetX(10);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetX(57.5);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetX(110);
		$pdf->Cell(42.5,28,"",1,0,'C');
		$pdf->SetFont('Arial','',6);
				
		$pdf->SetXY(164.5,245);
		$pdf->Cell(42.5,7,"Nombre _________________________",0,0,'L');
		$pdf->Ln(6);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"Fecha __________________________",0,0,'L');
		$pdf->Ln(6);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"C.I. ".utf8_decode('ó')." R.I.F. ______________________",0,0,'L');
		$pdf->Ln(6);
		$pdf->SetX(164.5);
		$pdf->Cell(42.5,7,"Firma __________________________",0,0,'L');
		$pdf->Ln(5);
		$pdf->SetXY(162.5,240);
		$pdf->Cell(42.5,28,"",1,0,'C');
} //Fin banda_firma

//$docs = str_replace('\'',' ',$docs);
//$varAux = explode(',',$docs);

//echo('loop');

$pdf->Output();

?>
