<?
include("comun/ini.php");
include("comprobantes_retencion.php");
include("Constantes.php");
$oOtrosPagos = new otros_pagos;
$oOtrosPagos->get($conn, $_REQUEST['id']);
//Datos de monto y retenciones
$json = new Services_JSON;
$cadena = str_replace("\\","",$oOtrosPagos->json);//-->OJO, MUY IMPORTANTE !!!
$vector = $json->decode($cadena);
$x = count($vector);
$docs = '';
$monto_pagar = 0;
for ($i=0;$i<$x;$i++)
{
	$docs.= '\''.$vector[$i]->nroref.'\',';
        $monto_pagar += $vector[$i]->monto;
}

class PDF extends FPDF
{
    var $fecha;
//Cabecera de página
	function Header()
	{
            parent::Header();
            /*$this->SetLeftMargin(5);
            $this->SetFont('Courier','',10);
            $this->Ln(1);
            $this->Image ("images/logoa.jpg",5,4,66,20);//logo a la izquierda*/

            $separaFecha= explode('-',$this->fecha);
            $dia = $separaFecha[2];
            $mes = $separaFecha[1];
            $ano = $separaFecha[0];
            if($this->PageNo() == 1){
                $this->Ln(20);
                $this->Cell(190,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, 'R','L');
                $this->Ln(10);
            }else{
               $this->setXY(15,40);
            }
	}

	
} 
$pdf=new PDF();
$pdf->fecha = $oOtrosPagos->fecha;
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Arial','',10);

$monto= 0;
$monto =  $monto_pagar;
$nrocuenta = utf8_decode($oOtrosPagos->nro_cuenta);
$montoLetras = utf8_decode(num2letras($monto,false,true));
//$pdf->Cell(190,5, 'Sres.: '.utf8_decode(substr($oOtrosPagos->cedente,0,strpos($oOtrosPagos->cedente,'-'))) ,0, 0, 'L','L');
$pdf->Cell(190,5, 'Sres.: '.utf8_decode($oOtrosPagos->banco) ,0, 0, 'L','L');
$pdf->ln(15);
$pdf->SetX(15);
$pdf->MultiCell(180,5,'Sirva la presente para autorizar nos debiten de la cuenta '.$nrocuenta.' la cantidad de '.$montoLetras.' (Bs.'. muestraFloat($monto).')', 0, 'L');
$pdf->ln(5);
$pdf->SetX(15);
$pdf->MultiCell(180,5,'Por el concepto que se define a continuaci'.utf8_decode('ó').'n', 0, 'L');

$pdf->ln(10);
$pdf->Cell(190,5, 'CONCEPTO',1, 0, 'C','C');
$pdf->ln(5);
$pdf->SetDrawColor (0,0,0);
$pdf->Cell(190,10, $oOtrosPagos->descripcion,'LR', 1, 'L');
$pdf->Cell(190,90, '','LRB', 0, 'L');
$pdf->ln(55);
$pdf->SetX(15);
$pdf->Cell(180,100,'Para acreditar a nombre de '.$oOtrosPagos->beneficiario.'.',0, 0, 'L');
$pdf->Ln(55);
$separaFecha= explode('-',$oOtrosPagos->fecha);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->Cell(190,5,"NUESTRO CONTROL		".$ano.'-'.$mes.'-'.$oOtrosPagos->nrodoc,0,0,'R');


$pdf->SetFont('Arial','',8);
$pdf->Ln(10);
$pdf->Cell(50,5,"Elaborado por",1,0,'C');
$pdf->SetX(75);
$pdf->Cell(50,5,"Firma 1",1,0,'C');
$pdf->SetX(145);
$pdf->Cell(50,5,"Firma 2",1,0,'C');
$pdf->Ln(5);
$pdf->Cell(50,30,"",1,0,'C');
$pdf->SetX(75);
$pdf->Cell(50,30,"",1,0,'C');
$pdf->SetX(145);
$pdf->Cell(50,30,"",1,0,'C');
$pdf->Ln(25);
//$pdf->Cell(50,5,"EDWIN LEAL",0,0,'C');


//Generacion de comprobantes de retenciones asociadas


$docs = rtrim($docs,',');


$docs = str_replace('\'',' ',$docs);
$varAux = explode(',',$docs);
foreach($varAux as $aux){
	//echo $aux.'<br \>';
	retencion_iva($conn,trim($aux),$escEnEje,$pdf);
        retencion_iva_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_islr($conn,trim($aux),$escEnEje,$pdf);
	retencion_islr_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_municipal($conn,trim($aux),$escEnEje,$pdf);
	retencion_municipal_tercero($conn,trim($aux),$escEnEje,$pdf);
	retencion_nacional($conn,trim($aux),$escEnEje,$pdf);
	retencion_nacional_tercero($conn,trim($aux),$escEnEje,$pdf);
}


$pdf->Output();

?>
