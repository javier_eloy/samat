<?
include("comun/ini.php");
//die(var_dump($_REQUEST));

$fecha_desde    = $_REQUEST['fecha_desde'];
$fecha_hasta    = $_REQUEST['fecha_hasta'];
$id_proveedor   = $_REQUEST['id_proveedor'];
$id_ue          = $_REQUEST['id_ue'];
$tipoProv       = $_REQUEST['tipoprov'];
$tipoBCom       = $_REQUEST['tipoCom'];
$id_status      = $_REQUEST['status'];
	
	
function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
  }

class PDF extends FPDF
{
  var $benDW,$ndoDW,$fecDW,$monDW,$tdoDW,$comDW,$fdcDW,$staDW;
  
	function Header()
	{
	     parent::header();
			//$this->SetLeftMargin(18);
			//$this->SetFont('Courier','',10);
			//$this->Ln(1);
			//$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda 
			//$this->SetXY(42, 15); 
			//$textoCabecera = "INTENDENCIA MUNICIPAL\n";
			//$this->MultiCell(50,2, $textoCabecera, 0, 'L');

			//$ovehiculo = unserialize($_SESSION['pdf']);
			//$tipo = $ovehiculo->id_tipo_documento;

			$this->SetXY(200, 10);
			$textoFecha = "Fecha: ".date('d/m/Y')."\n\n";
			$this->Text(280, 10, $textoFecha);
			//$textoDerecha.= "Nro.:".$nro_recibo."\n";
			//$textoDerecha.= "Fecha Generac.:".muestrafecha($fecha)."\n\n";
			//$textoDerecha.= "Fecha Aprob.:".muestrafecha($oOrden->fecha_aprobacion)."\n";
			$textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}\n\n";
			$this->Text(280, 15, $textoPag);
			//$this->MultiCell(100,2, $textoDerecha, 0, 'L');
			
			$this->Ln();

			$this->SetFont('Courier','b',12);
			//if($tipo == '002')
			//	$tipoOrden = "Orden de Servicio";
			//elseif($tipo == '009')
				$tipoOrden = "SOLICITUDES DE PAGO SIN ORDEN DE PAGO (CxP)";
			
			$this->Text(150, 20, $tipoOrden);
			//$this->Line(18, 41, 393, 41);
			$this->Ln(20);
			//$this->Text(160, 40, '#');
			//$this->Text(175, 40, $id);
			$this->Cell(320,8,'SOLICITUDES DE PAGO SIN ORDEN DE PAGO RELACIONADO',1,1,'C');
      $this->Cell($this->benDW,5,'Beneficiario',1,0,'C');
      $this->Cell($this->ndoDW,5,'# Documento',1,0,'C');
      $this->Cell($this->fecDW,5,'Fecha SP',1,0,'C');
      $this->Cell($this->monDW,5,'Monto',1,0,'C');
      $this->Cell($this->tdoDW,5,'TD',1,0,'C');
      $this->Cell($this->comDW,5,'Compromiso',1,0,'C');
      $this->Cell($this->fdcDW,5,'Fecha DC',1,0,'C');
      $this->Cell($this->staDW,5,'Status',1,0,'C');
      $this->Ln();
	}

	function Footer()
	{	

		//$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//N�mero de p�gina
		//$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
}
//Creaci�n del objeto de la clase heredada
$pdf=new PDF('l','mm','legal');
$pdf->AliasNbPages();

$benDW = 90;
$ndoDW = 40;
$fecDW = 25;
$monDW = 40;
$tdoDW = 10;
$comDW = 40;
$fdcDW = 35;
$staDW = 40;

$pdf->benDW = $benDW;
$pdf->ndoDW = $ndoDW;
$pdf->fecDW = $fecDW;
$pdf->monDW = $monDW;
$pdf->tdoDW = $tdoDW;
$pdf->comDW = $comDW;
$pdf->fdcDW = $fdcDW;
$pdf->staDW = $staDW;

$pdf->AddPage();
$pdf->SetFont('Courier','B',14);
$pdf->SetLeftMargin(15);

$q  = "SELECT A.nrodoc,A.nroref,A.fecha,A.status,A.Descripcion,A.pago,A.id_unidad_ejecutora,B.id,B.nombre,B.rif,C.abreviacion,D.fechadoc FROM finanzas.solicitud_pago A ";
$q .= "INNER JOIN puser.proveedores B ON B.id = A.id_proveedor ";
$q .= "INNER JOIN puser.tipos_documentos C on substring(A.nroref from 1 for 3) = C.id ";
$q .= "inner join puser.movimientos_presupuestarios D on D.nrodoc = A.nroref ";
$q .= "WHERE 1=1 ";
$q .= !empty($id_proveedor) ? "AND A.id_proveedor = $id_proveedor " : "";
$q .= !empty($tipoProv) ? "AND B.provee_contrat = '$tipoProv' " : "";
$q .= !empty($id_ue) ? "AND A.id_unidad_ejecutora = '$id_ue' " : "";
$q .= !empty($fecha_desde) ? "AND A.fecha >= '".guardafecha($fecha_desde)."' " : "";
$q .= !empty($fecha_hasta) ? "AND A.fecha <= '".guardafecha($fecha_hasta)."' " : "";
$q .= !empty($tipoBCom) ? "AND COALESCE(C.id,'0') = '$tipoBCom' " : "";
$q .= "AND A.id_escenario = '1111' ";
$q .= "ORDER BY A.nrodoc,A.nroref,A.fecha";
//die($q);
$r = $conn->Execute($q);
$pdf->SetFont('Courier','B',10);

$pdf->SetFont('Courier','',10);
$pro_act = 0;
$pro_act_des = '';
while(!$r->EOF){
        //Sub total por porveedor
        if($pro_act == 0){
            $pro_act = $r->fields['id'];
            $pro_act_des = $r->fields['nombre'];
        }
        if($pro_act != $r->fields['id']){
          $pdf->Line(175, $pdf->GetY(), 210, $pdf->GetY());
          $pdf->SetXY(129,$pdf->GetY()+2);
          $pdf->Cell(80,5,'Total '.$pro_act_des.' BsF. : '.muestraFloat($sub_total_pro),0,1,'R');
          $pdf->Ln(2);
          $sub_total_pro = $r->fields['pago'];
          $pro_act_des = $r->fields['nombre'];
          $pro_act = $r->fields['id'];
        }else{
          $sub_total_pro = $sub_total_pro + $r->fields['pago'];
        }
        $desc_proveedor = dividirStr($r->fields['nombre'], intval($benDW/$pdf->GetStringWidth('M')));
	$pdf->Cell($benDW,5,utf8_decode($desc_proveedor[0]),0,0,'L');
	$pdf->Cell($ndoDW,5,$r->fields['nrodoc'],0,0,'C');
	$pdf->Cell($fecDW,5,muestraFecha($r->fields['fecha']),0,0,'C');
	$pdf->Cell($monDW,5,muestraFloat($r->fields['pago']),0,0,'R');
	$pdf->Cell($tdoDW,5,$r->fields['abreviacion'],0,0,'C');
	$nrodoccom = (!empty($r->fields['nroref'])) ? $r->fields['nroref'] : "OP";
		$pdf->Cell(40,5,$nrodoccom,0,0,'C');
	$fechacom = (!empty($r->fields['fechadoc'])) ? muestraFecha($r->fields['fechadoc']) : "SIN/IMP";
		$pdf->Cell($fdcDW,5,$fechacom,0,0,'C');
	switch($r->fields['status']){
	 	case "1":
			$status = "Registrada";
		break;
		case "2":
			$status = "Aprobada";
		break;
		case "3":
			$status = "Anulada";
		break;
	}

	if($r->fields['status'] != 3)
		$total_sp += $r->fields['pago'];
	
	$pdf->Cell($staDW,5,$status,0,0,'C');
        //Segundas lineas de descripciones
	$hay_pv = next($desc_proveedor);
  		for ($i=1; $hay_pv!==false; $i++)
  		{
                        $pdf->Ln();
			//$pdf->Cell($benDW,4, $desc_unidad[$i],0, 0,'L');
                        $pdf->Cell($benDW,4, $desc_proveedor[$i],0, '','L');
                        $pdf->Cell($ndoDW,4, '',0, '','L');
                        $pdf->Cell($fecDW,4, '',0, '','L');
			                  $pdf->Cell($monDW,4, '',0, '','L');
			$pdf->Cell($tdoDW,4,'',0,0,'C');
			$pdf->Cell($comDW,4, '',0, '','L');
			$pdf->Cell($fdcDW,4, '',0, '','L');
			$pdf->Cell($staDW,4, '',0, '','C');
                        $hay_pv = next($desc_proveedor);
  		}
	$r->movenext();
	$pdf->Ln();
	
}

if($sub_total_pro >= 0){
          $pdf->Line(190, $pdf->GetY(), 200, $pdf->GetY());
          $pdf->SetXY(129,$pdf->GetY()+2);
          $pdf->Cell(100,5,'Total '.$pro_act_des.' BsF. : '.muestraFloat($sub_total_pro),0,1,'R');
          $pdf->Ln(2);
          $sub_total_pro = 0;
          $pro_act_des = '';
          $pro_act = 0;
}

$pdf->Output();
?>