<?
include("comun/ini.php");
header ("content-type: text/xml");
$id = $_GET['id'];
$rif = $_REQUEST['rif'];
$oProveedor = new proveedores;

if ($id!=""){
	$oProveedor->get($conn, $id);
}elseif($rif!=""){
	$oProveedor->proveedores_rif($conn, $rif);
	}
//Busco si el nombre el proveedore contiene el caracter especial & para ser sustituido por YYYY el cual con JS lo convierto en &
//Problema con cadena de paramentros get al utilizar ajax

$pos_caracter = strpos($oProveedor->nombre,'&');
if($pos_caracter != false){
    //$nompro = str_replace('&','YYYY',$oProveedor->nombre);
    $nompro = htmlentities($oProveedor->nombre, ENT_QUOTES);
}else{
    $nompro = $oProveedor->nombre;
}
        //die(print_r($oProveedor));
?>

<xmldoc>
	<proveedor>
		<id_proveedor><?=$oProveedor->id?></id_proveedor>
                <nombre><?=$nompro?></nombre>
		<direccion><?=urldecode(urlencode($oProveedor->direccion))?></direccion>
		<ciudad><?=$oProveedor->estado?></ciudad>
		<telefono><?=$oProveedor->telefono?></telefono>
	</proveedor>
</xmldoc>