<?
require ("comun/ini.php");

// Creando el objeto requisitos
$oRequisitos = new requisitos;
$accion = $_REQUEST['accion'];
$vencido=$_REQUEST['vencido'];
$today=date("Y-m-d");
if($vencido!='TRUE') {
    $vencido='FALSE';
}

switch($accion) {
    case 'Guardar':
        $oRequisitos->add($conn,
                $_REQUEST['nombre'],
                $_REQUEST['descripcion'],
                $today,
                $vencido);
        break;
    case 'Actualizar':
        $oRequisitos->set($conn, $_REQUEST['id'],
                $_REQUEST['nombre'],
                $_REQUEST['descripcion'],
                $vencido, $today);
        break;
    case 'del':
        $oRequisitos->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oRequisitos->msg;

//Seccion paginador
//$cRequisitos=$oRequisitos->get_all($conn, $start_record,$page_size);

$cRequisitos = $oRequisitos->buscar($conn, "", $tamano_pagina, $inicio, "nombre");
$total = requisitos::total_registro_busqueda($conn, "");

require ("comun/header.php");
if(!empty($msg))   echo "<div id=\"msj\">".$msg."</div><br/>";

?>
<br />
<span class="titulo_maestro">Maestro de requisitos</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<fieldset id="buscador">
    <legend>Buscar:</legend>
    <table>
        <td>Nombre:</td>
        <td>
            <input type="text" name="nombre" id="nombre" />
            <input type="hidden" name="hidden_nombre" id="hidden_nombre" />
        </td>
    </table>
</fieldset>
<br />
<div id="busqueda"></div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">
    var t;

    buscador($F('nombre'), 1, 46);
    
    function buscador(nombre, pagina, code)
    {
        if ((code>=48 && code<=57) || (code>=96 && code<=105) || (code>=65 && code<=90) || code==8 || code==13 || code==46)
        {
            clearTimeout(t);
            $('hidden_nombre').value = nombre;
            t = setTimeout("busca('"+nombre+"', "+pagina+")", 800);
        }
    }

    function busca(nombre, pagina)
    {
        var url = 'updater_busca_requisitos.php';
        var pars = 'nombre=' + nombre + '&pagina=' + pagina + '&ms='+new Date().getTime();
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    Event.observe('nombre', "keyup", function (evt) {
        buscador($F('nombre'), 1, evt.keyCode);
    });
</script>
<? 
//$validator->create_message("error_cod", "id_nuevo", "*");
$validator->create_message("error_nombre", "nombre", "*");
$validator->print_script();
require ("comun/footer.php");?>
