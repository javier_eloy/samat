<?
include ("comun/ini.php");
$pagina = $_REQUEST['pagina'];
$tamano_pagina = 20;

if (!$pagina) {
    $inicio = 10;
    $pagina=1;
}
else {
    $inicio = ($pagina - 1) * $tamano_pagina;
}
$pcontrol_chequera= new control_chequera();
$ccontrol_chequera=$pcontrol_chequera->get_all($conn, $inicio,$tamano_pagina,'nro_cuenta');
$total= $pcontrol_chequera->totalFilas;

if(is_array($ccontrol_chequera)) { 
 ?>
<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
    <tr class="cabecera">
        <td>Nro. de Chequera</td>
        <td>Cuenta Bancaria</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <?
        $i = 0;
        foreach($ccontrol_chequera as $cc) {
            ?>
    <tr class="filas">
        <td><?=$cc->nro_chequera?><?=$cc->activa==1?'*':''?></td>
        <td><?=$cc->nro_cuenta->banco->descripcion?> - <?=$cc->nro_cuenta->tipo_cuenta->descripcion?> Nro: <?=$cc->nro_cuenta->nro_cuenta?></td>
        <td align="center">
            <a href="control_chequera.php?accion=del&id=<?=$cc->id?>" title="Modificar o Actualizar Registro" ><img src="images/eliminar.gif" width="16" height="10" border="0"></a></td>
        <td align="center">
            <a href="#" onclick="updater(<?=$cc->id?>); return false;" title="Modificar o Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
    </tr>
        <?
        }
       
        $total_paginas = ceil($total / $tamano_pagina);
    ?>
    <tr class="pietabla">
        <td colspan="7" align="center">
                <?
                for ($j=1;$j<=$total_paginas;$j++) {
                    if ($j==1) {
                        if ($j==$pagina)
                            echo "<span>".$j."</span>";
                        else
                            echo '<span style="cursor:pointer" onclick="busca('.$j.');">'.$j.'</span>';
                    }
                    else {
                        if ($j==$pagina)
                            echo "<span>- ".$j."</span>";
                        else
                            echo '<span style="cursor:pointer" onclick="busca('.$j.');">- '.$j.'</span>';
                    }
                }
                ?>
        </td>
    </tr>
    <tr class="pietabla">
        <td colspan="7" align="center"> Pagina <strong><?=$_REQUEST['pagina']?></strong> de <strong><?=$total_paginas?></strong></td>
    </tr>
</table>
    <?
}
else {
    echo "No hay registros en la bd";
}
?>