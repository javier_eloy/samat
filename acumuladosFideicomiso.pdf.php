<?
include("comun/ini.php");
$_SESSION['conex'] = $conn;
class PDF extends FPDF
{
//Cabecera de página
	function Header()
	{

                        
//			$this->SetLeftMargin(15);
//                      $this->SetFont('Courier','',6);
//			$this->Ln(1);
//			$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda
//                      $this->SetXY(42, 7);

                        parent::Header();
			$this->SetXY(230, 7);
			$this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');

			$this->Ln(20);
			$this->SetFont('Courier','b',12);
			$this->Cell(0, 0, "Reporte de Fideicomiso ",0,0,'C');
			$this->Ln(6);
			$this->SetFont('Courier','B',8);
			$this->Cell(260, 0, "Periodo: ".$_GET['Periodo'],0,0,'R');
			$this->Line(15, 37, 275, 37);
			$this->SetFont('Courier','B',10);
			$this->Ln(5);
			$this->Cell(15,0,"C.I.",0, 0,'C');
			$this->Cell(60,0,"Trabajador",0, 0,'C');
			$this->Cell(30,0,"Fecha Ingreso",0, 0,'C');
			$this->Cell(25,0,"Sueldo",0, 0,'C');
			$this->Cell(12,0,"Dias",0, 0,'C');
			$this->Cell(65,0,"Concepto",0, 0,'C');
			$this->Cell(20,0,"B. F/A",0, 0,'C');
			$this->Cell(20,0,"B. Vac",0, 0,'C');
			$this->Cell(15,0,"P/S",0, 0,'C');
			$this->Line(15, 42, 275, 42);
			$this->Ln(5);
			
	}

	function Footer()
	{
		
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(180,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetLeftMargin(15);
$pdf->SetFont('Courier','',8);
//$q = "SELECT DISTINCT A.tra_cod,C.tra_sueldo,C.tra_fec_ing,C.tra_ced, C.tra_nom,C.tra_ape FROM (rrhh.acum_tra_conc AS A INNER JOIN rrhh.acumulado AS B ON A.acum_cod=B.int_cod) INNER JOIN rrhh.trabajador AS C ON A.tra_cod=C.int_cod WHERE B.periodo='".$_GET['Periodo']."' ORDER BY tra_ced";
$qfin_periodo = "select max(nom_fec_fin) as fecha from rrhh.historial_nom where substring(nom_fec_fin from 6 for 2) = '".substr($_GET['Periodo'],0,2)."'";
//die($qfin_periodo);
$rqfin_periodo = $conn->Execute($qfin_periodo);
$fecha_fin_periodo = $rqfin_periodo->fields['fecha'];
//$q = "SELECT DISTINCT A.tra_cod,C.tra_sueldo,C.tra_fec_ing,C.tra_ced, C.tra_nom,C.tra_ape,rrhh.obtener_edad(C.tra_fec_ing,'$fecha_fin_periodo') as edad
//FROM (rrhh.acum_tra_conc AS A INNER JOIN rrhh.acumulado AS B ON A.acum_cod=B.int_cod)
//INNER JOIN rrhh.trabajador AS C ON A.tra_cod=C.int_cod
//WHERE B.periodo='".$_GET['Periodo']."'
//ORDER BY tra_ced";
$q = "select a.cont_cod,d.int_cod as tra_cod,c.tra_sueldo,d.tra_fec_ing,d.tra_ced,d.tra_ape,d.tra_nom,rrhh.obtener_edad(d.tra_fec_ing,'$fecha_fin_periodo') as edad
from rrhh.acumulado as a
inner join rrhh.acum_tra_conc as b on a.int_cod = b.acum_cod
inner join rrhh.acum_tra_sueldo as c on a.int_cod = c.acum_cod and b.tra_cod = c.tra_cod
inner join rrhh.trabajador as d on c.tra_cod = d.int_cod
where a.periodo = '".$_GET['Periodo']."' order by d.tra_ced";
//die($q);
$rT = $conn->Execute($q);
$TotalNomina=0;
$dias_ps_defecto = 5;
while(!$rT->EOF){
	$q = "SELECT C.conc_nom, A.conc_val::numeric(20,2),A.conc_desc 
            FROM (rrhh.acum_tra_conc AS A
            INNER JOIN rrhh.acumulado AS B ON A.acum_cod=B.int_cod)
            INNER JOIN rrhh.concepto AS C ON A.conc_cod=C.int_cod
            WHERE B.periodo='".$_GET['Periodo']."' AND A.conc_val<>0 AND A.tra_cod=".$rT->fields['tra_cod']."
            ORDER BY A.conc_cod";
        //die($q);
	$rC = $conn->Execute($q);
	$TotalTrabajador=0;
        //Ricardo Camejo
        //Esto no debe hacerse asi, se debe cambiar el comportamiento del programa para que exista una
        //tabla intermedia donde por tipo de contrato se defina la cantidad de dias para prestaciones.
        if($rT->fields['cont_cod'] == 15)
           $dias_ps_defecto = 10;
        else
           $dias_ps_defecto = 5;
	if(!$rC->EOF){
		$pdf->Cell(15,0,utf8_decode($rT->fields['tra_ced']),0, 0,'L');	
		$pdf->Cell(60,0,utf8_decode($rT->fields['tra_nom'])." ".utf8_decode($rT->fields['tra_ape']),0, 0,'L');	
		$pdf->Cell(30,0,muestrafecha($rT->fields['tra_fec_ing']),0, 0,'L');	
		$pdf->Cell(25,0,muestrafloat($rT->fields['tra_sueldo']),0, 0,'L');
                if(substr($_GET['Periodo'],0,2) == substr($rT->fields['tra_fec_ing'],5,2))
                    $pdf->Cell(12,0,muestrafloat(($rT->fields['edad']*2)-2+$dias_ps_defecto),0, 0,'L');
		else
                    $pdf->Cell(12,0,$dias_ps_defecto,0, 0,'L');

		while(!$rC->EOF) {
			if($TotalTrabajador!=0) {
				$pdf->Cell(70,0, '',0, 0,'L');	
			}
			$pdf->Cell(65,0, empty($rC->fields['conc_desc']) ? $rC->fields['conc_nom'] : $rC->fields['conc_desc'],0, 0,'L');
			
			$qbono = "select conc_val from rrhh.hist_nom_tra_conc a 
			inner join rrhh.historial_nom b on a.hnom_cod = b.int_cod and substring(nom_fec_ini from 1 for 4) = '".substr($_GET['Periodo'],3,4)."' and substring(nom_fec_ini from 6 for 2) = '".substr($_GET['Periodo'],0,2)."'
			where tra_cod = ".$rT->fields['tra_cod']." and conc_cod in (159,160)";
			$rqbono = $conn->Execute($qbono);
			//Bono de fin de a�o 
			$pdf->Cell(20,0,  muestrafloat($rqbono->fields['conc_val']),0, 0,'R');
			//bONO VACACIONAL
			$qbono = "select conc_val from rrhh.hist_nom_tra_conc a 
			inner join rrhh.historial_nom b on a.hnom_cod = b.int_cod and substring(nom_fec_ini from 1 for 4) = '".substr($_GET['Periodo'],3,4)."' and substring(nom_fec_ini from 6 for 2) = '".substr($_GET['Periodo'],0,2)."'
			where tra_cod = ".$rT->fields['tra_cod']." and conc_cod IN (73,169)";
			$rqbono = $conn->Execute($qbono);
			$pdf->Cell(20,0,  muestrafloat($rqbono->fields['conc_val']),0, 0,'R');

			//die($qbono);
			
			//Total Fideicomiso
			$pdf->Cell(15,0,  muestrafloat($rC->fields['conc_val']),0, 0,'R');	
			$TotalTrabajador+=calculafloat($rC->fields['conc_val'],2);
			$pdf->Ln(5);
			$rC->movenext();
		}
		$TotalNomina+=calculafloat($TotalTrabajador,2);
		//$pdf->Cell(230,3, '' ,0, 0,'R');	
		//$pdf->Cell(30,3, "Sub-Total: ".muestrafloat($TotalTrabajador) ,'T', 0,'R');	
		//$pdf->Ln(4);
		//$pdf->Cell(270,0, '' ,'B', 0,'L');	
		$pdf->Ln(2);
	}
	$rT->movenext();
} 
$pdf->Cell(170,0, "Total: ".muestrafloat($TotalNomina) ,0, 0,'R');	
$pdf->Output();
?>
