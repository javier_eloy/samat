<? 
include("comun/ini.php");
include("Constantes.php");
$oOrden = new orden_servicio_trabajo();
$mPresupuestario = new movimientos_presupuestarios;
$oOrden->get($conn, $_GET['id'], $escEnEje);

if(empty($oOrden->nrodoc))
	header ("location: orden_servicio_trabajo.php");
$_SESSION['pdf'] = serialize($oOrden);

//Obtenemos nombre completo de usuario
$oUsuarios = new usuarios();
$cUsuarios = $oUsuarios->get($conn,$oOrden->id_usuario);
//die($oOrden->id_usuario.' de Nombre: '.$oUsuarios->nombre.$oUsuarios->apellido.$oUsuarios->login);
// Crea un array donde cada posicion es un string de tamaño 'max' caracteres,
// teniendo en cuenta de no cortar una palabra, busca el espacio en blanco  
// mas cerca del tamaño 'max' y ahi corta el string

function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
  }
  
  
class PDF extends FPDF
{
//Cabecera de página
	function Header()
	{
                        parent::Header();
//			$this->SetLeftMargin(15);
//			$this->SetFont('Courier','',6);
//			$this->Ln(1);
//			$this->Image ("images/logoa.jpg",15,4,55,20);//logo a la izquierda
//			$this->SetXY(15, 22);
//			$textoCabecera = DEPARTAMENTO."\n";
			//$this->MultiCell(50,2, $textoCabecera, 0, 'L');

			$oCompra = unserialize($_SESSION['pdf']);

			$this->SetXY(150, 12); 
			$this->SetFont('Courier','B',12);
                        $this->Text(130, 10, "ORDEN DE SERVICIO/TRABAJO");
			//$textoDerecha = "ORDEN DE SERVICIO\n";
                        $this->Text(130, 15, "Nro.:".$oCompra->nrodoc);
			//$textoDerecha.= $oCompra->nrodoc."\n\n";
			
			$textoFecha = "Fecha: ".muestrafecha($oCompra->fecha);
                        $this->Text(130, 20, $textoFecha);
			$textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}";
                        $this->Text(130, 25, $textoPag);
			//$this->MultiCell(60,4, $textoDerecha, 0, 'L');
			$this->Line(15, 40, 190, 40);
                        $this->SetXY(15, 45);
			//$this->Ln(4);
			
	}

	//Pie de página
	function Footer()
	{
		
		//$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		//$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
}
//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);
$pdf->SetLeftMargin(15);


//DATOS DEL PROVEEDOR
$oProveedor = new proveedores;
$oProveedor->get($conn, $oOrden->id_proveedor);
$pdf->Cell(90,4,utf8_decode('Señores:'),0,'','L');
$pdf->Cell(90,4,utf8_decode('Descripción:'),0,'','L');
$pdf->Ln();

$pdf->SetFont('Arial','',8);
$observacion = dividirStr($oOrden->observaciones, intval(100/$pdf->GetStringWidth('M')));
$nomb_proveedor = dividirStr($oProveedor->nombre, intval(120/$pdf->GetStringWidth('M')));
$pdf->Cell(15,4,'Proveedor:',0,'','L');
$pdf->Cell(80,4,utf8_decode($nomb_proveedor[0]),0,'','L');
$indice = 0;
$pdf->Cell(80,4,utf8_decode($observacion[$indice]),0,'','L');

$linea = 0;
$hay_ue = next($nomb_proveedor);
for ($i=1; $hay_ue!==false; $i++)
{
        $linea += 3;
        $indice += 1;
        $pdf->Ln();
        $pdf->Cell(15,4,'',0,'','L');
        $pdf->Cell(80,4, $nomb_proveedor[$i], 0, '', 'L');
        $pdf->Cell(80,4, $observacion[$indice], 0, '', 'L');
        $hay_ue = next($nomb_proveedor);
}
$pdf->Ln();
$pdf->Cell(15,4,'RIF:',0,'','L');
$pdf->Cell(80,4,$oProveedor->rif,0,'','L');
$indice += 1;
$pdf->Cell(80,4, $observacion[$indice], 0, '', 'L');
$pdf->Ln();
$dir_proveedor = dividirStr($oProveedor->direccion, intval(200/$pdf->GetStringWidth('M')));
$pdf->Cell(15,4,'Direcci'.utf8_decode('ó').'n:',0,'','L');
$pdf->Cell(80,4,utf8_decode($dir_proveedor[0]),0, '','L');
$indice += 1;
$pdf->Cell(80,4, $observacion[$indice], 0, '', 'L');
//$linea = 0;
$hay_ue = next($dir_proveedor);
for ($i=1; $hay_ue!==false; $i++)
{
        $linea+= 3;
        $pdf->Ln();
        $indice += 1;
        $pdf->Cell(15,4, '', 0, '', 'L');
        $pdf->Cell(80,4, $dir_proveedor[$i], 0, '', 'L');
        $pdf->Cell(80,4, $observacion[$indice], 0, '', 'L');
        $hay_ue = next($dir_proveedor);
}
$pdf->Ln(6-$linea+1);
$pdf->Cell(120,4,utf8_decode('Sirvase de despachar/Realizar por órden y cuenta nuestra lo que a continuación detallamos'),0,'','L');
$pdf->Ln(6);
$pdf->Line(15,69,195,69);
$pdf->Line(15,$pdf->GetY()-1,15,210);
$pdf->Line(195,$pdf->GetY()-1,195,210);

//Detalle de los servicios a contratar
$pdf->setXY(16,70);
$pdf->SetAligns(array('C','C','C','C'));
$pdf->SetWidths(array(65,35,40,39));
$pdf->SetFont('Courier','B',8);
$pdf->RowNL(array(utf8_decode('Descripción'),'Precio',' IVA',' Total'));
$pdf->Line(15,74,195,74);


//Lineas divisorias de titulos
$pdf->Line(85,69,85,74);
$pdf->Line(115,69,115,74);
$pdf->Line(155,69,155,74);

$JsonPro = new Services_JSON();
//die(print_r($oOrden->relacionProductos));
$JsonPro = $JsonPro->decode(str_replace("\\","",$oOrden->relacionProductos));

$pdf->SetFont('Courier','B',7);
$pdf->SetAligns(array('L','R','R','R'));
$pdf->SetXY(15,76);
foreach($JsonPro as $producto){
	$sumCosto += $producto[1];
	$sumIva += $producto[2];
	$totalFila = $producto[1]+ $producto[2];
	$precioTotal += $totalFila;
	$pdf->RowNL(array(utf8_decode($producto[0]),muestraFloat($producto[1]),muestraFloat($producto[2]),muestraFloat($totalFila)));
}

$JsonPresupuesto = $mPresupuestario->getImputacionReportes($conn,$oOrden->nrodoc,$escEnEje);

$pdf->Line(15,210,195,210);

$pdf->Line(170,210,170,222);
$pdf->Line(195,210,195,222);
$pdf->Line(170,214,195,214);
$pdf->Line(170,218,195,218);
$pdf->Line(170,222,195,222);

$pdf->Text(150,213,'Subtotal');
$pdf->Text(150,217,'Impuestos');
$pdf->Text(150,221,'Total');


//$pdf->Text(172,213,muestraFloat($sumCosto),0,'','R');
$pdf->setxy(178,212);
$pdf->Cell(17,1,muestraFloat($sumCosto),0,'','R');
$pdf->setxy(178,216);
$pdf->Cell(17,1,muestraFloat($sumIva),0,'','R');
$pdf->setxy(178,220);
$pdf->Cell(17,1,muestraFloat($precioTotal),0,'','R');

$pdf->Line(15,212,75,212);
$pdf->Line(15,212,15,232);
$pdf->Line(75,212,75,232);
$pdf->Line(15,232,75,232);
$pdf->SetFont('Arial','B',8);
$pdf->Text(15,215,'Elaborado Por:');
$pdf->Line(85,216,135,216);
$pdf->SetFont('Arial','',8);

//$pdf->Text(15,219,utf8_decode($usuario->nombre.' '.$usuario->apellido));
$oCompra = unserialize($_SESSION['pdf']);
//$pdf->Text(15,219,utf8_decode($oUsuarios->nombre.' '.$oUsuarios->apellido));
$pdf->SetFont('Arial','B',8);
$pdf->Text(15,231,utf8_decode(CONTROLINTERNO));
$pdf->SetFont('Arial','',8);
$pdf->Line(85,212,135,212);
$pdf->Line(85,212,85,232);
$pdf->Line(135,212,135,232);
$pdf->Line(85,232,135,232);
$pdf->SetFont('Arial','B',8);
$pdf->Text(85,215,'Aprobado Por:');
$pdf->Text(87,231,INTENDENTE);
$pdf->Line(15,216,75,216);
$pdf->SetFont('Arial','',8);
$pdf->Line(15,237,100,237);
$pdf->Line(15,237,15,270);
$pdf->Line(100,237,100,270);
$pdf->Line(15,270,100,270);
$pdf->SetFont('Arial','B',8);
$pdf->Text(15,240,'Presupuesto');
$pdf->Line(15,242,100,242);
$pdf->SetFont('Arial','',8);

$pdf->SetXY(15,243);
$pdf->SetFont('Arial','',8);
foreach($JsonPresupuesto as $partida){
	$pdf->Cell(50,3,masc_presupuesto($partida->id_categoria,$partida->id_partida),0, '','L');
	$pdf->Cell(30,3, muestraFloat($partida->monto),0, '','R');
	$pdf->Ln();
}

$pdf->Line(110,237,195,237);
$pdf->Line(110,237,110,270);
$pdf->Line(195,237,195,270);
$pdf->Line(110,270,195,270);
$pdf->SetFont('Arial','B',8);
$pdf->Text(130,240,'Recibido por el Proveedor:');
$pdf->Line(110,241,195,241);
$pdf->SetFont('Arial','',8);
$pdf->Text(112,246,'Nombre y Apellido: ___________________________________');
$pdf->Text(112,252,'N. C'.utf8_decode('é').'dula: ___________________________________________');
$pdf->Text(112,258,'Firma: _______________________________________________');
$pdf->Text(112,264,'Fecha: ______________________________________________');


$pdf->Output();

?>