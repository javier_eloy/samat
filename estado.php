<?
require ("comun/ini.php");

// Creando el objeto Estados
$oEstados = new estado;
$accion = $_REQUEST['accion'];
switch ($accion ) {
    case 'Guardar':
        $oEstados->add($conn, $_REQUEST['descripcion']);
        break;
    case 'Actualizar':
        $oEstados->set($conn, $_REQUEST['id'], $_REQUEST['descripcion']);
        break;
    case 'del':
        $oEstados->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oEstados->msg;


$cEstados=$oEstados->buscar($conn, $num,$inicio);
$total_E = estado::total_registro_busqueda($conn);
$total = $total_E;
//echo "aqui ".$total;

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";

?>
<br />
<span class="titulo_maestro">Maestro de Estados</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />

<fieldset id="buscador">
    <legend>Buscar:</legend>
    <table>
        <td>Descripci&oacute;n:</td>
        <td>
            <input type="text" name="busca_desc" id="busca_desc" />
            <input type="hidden" name="hid_desc" id="hid_desc" />
        </td>
    </table>
</fieldset>
<br />

<div id="busqueda"></div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">

    var t;
    buscador($F('busca_desc'), '1', 46);

    function buscador(descripcion, pagina, keyCode)
    {
        if ((keyCode>=65 && keyCode<=90) || (keyCode>=48 && keyCode<=57) || (keyCode>=96 && keyCode<=105) || keyCode==8 || keyCode==46)
        {
            clearTimeout(t);
            $('hid_desc').value = descripcion;
            t = setTimeout("busca('"+descripcion+"','"+pagina+"')", 600);
        }
    }

    function busca(descripcion, pagina)
    {
        var url = 'updater_busca_estado.php';
        var pars = 'descripcion=' + descripcion + '&ms='+new Date().getTime()+ '&pagina='+pagina;
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    Event.observe('busca_desc', "keyup", function (evt)
    {	buscador($F('busca_desc'), '1', evt.keyCode);	});
</script>

<?
$validator->create_message("error_desc", "descripcion", "*");
$validator->print_script();
require ("comun/footer.php");
?>
