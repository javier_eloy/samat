<?

require ("comun/ini.php");
//die('no');
$OtraFondosTerceros = new traFondosTerceros;
$accion = $_REQUEST['accion'];
$tipo = $_REQUEST['rdb_tipo']; //0 PARA NOMINA, 1 PARA PROVEEDORES

if($accion == 'Guardar') {
    $OtraFondosTerceros->add($conn,guardaFecha($_REQUEST['fecha']), 1, $_REQUEST['transferencia'], $_REQUEST['beneficiario'],
            $_REQUEST['rdb_tipo'],$_REQUEST['rdb_formaPago'],$_REQUEST['json_det'], $_REQUEST['txt_numeroCheque'],
            $_REQUEST['txt_numero_transferencia'], $_REQUEST['beneficiario'], $_REQUEST['txt_observacion'], $_REQUEST['fecha'],$escEnEje);

}elseif($accion=='Anular') {
    $OtraFondosTerceros->anular($conn, $_REQUEST['id'], date('Y-m-d'), 2, $_REQUEST['mAnulado'],
                                $_REQUEST['fecha'], $_REQUEST['nrodoc'], $_REQUEST['json_det'],$escEnEje);
}	

//$msg = empty($ctaFondos) ? 'No existe cuenta asociada a la transferencia de fondos, Por favor realize el enlace' : $OtraFondosTerceros->msg; <--- no se consigue la variable a evaluar.
$msg=$OtraFondosTerceros->msg;


#ESTE EL LA CABECERA DE LA PAGINA#
require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div>";
?>
<br />
<span class="titulo_maestro">Pagos Retenciones y Aportes Patronales</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<div id="divbuscador">
    <fieldset id="buscador">
        <legend>Buscar:</legend>
        <table>
            <tr>
                <td>N&ordm; de Documento</td>
            </tr>
            <tr>
                <td><input style="width:100px" type="text" name="busca_nrodoc" id="busca_nrodoc" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="width:125px">Desde</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <input style="width:100px"  type="text" name="busca_fecha" id="busca_fecha"
                                       onchange="validafecha(this);"/>
                            </td>
                            <td>
                                <a href="#" id="boton_busca_fecha" onClick="return false;">
                                    <img border="0" alt="Seleccionar una fecha" src="images/calendarA.png" width="20" height="20" />
                                </a>
                                <script type="text/javascript">
                                    new Zapatec.Calendar.setup({
                                        firstDay          : 1,
                                        weekNumbers       : true,
                                        showOthers        : false,
                                        showsTime         : false,
                                        timeFormat        : "24",
                                        step              : 2,
                                        range             : [1900.01, 2999.12],
                                        electric          : false,
                                        singleClick       : true,
                                        inputField        : "busca_fecha",
                                        button            : "boton_busca_fecha",
                                        ifFormat          : "%d/%m/%Y",
                                        daFormat          : "%Y/%m/%d",
                                        align             : "Br"
                                    });
                                </script>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </fieldset>
</div>
<br />
<div style="margin-bottom:10px" id="busqueda"></div>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">




    function traeUltimoCheque(){
        var url = 'json.php';
        var pars = 'op=ultimo_cheque_terceros&ms='+new Date().getTime();
        var myAjax = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onComplete: function(peticion){
                var jsonData = eval('(' + peticion.responseText + ')');
                if (jsonData == undefined) {
                    alert('No hay chequera activa para la cuenta de tercero');
                    return; }
                if (jsonData != -1) {
                    //alert('aqui');
                    $('txt_numeroCheque').value    = jsonData.ultimo_cheque;
                    $('txt_numeroCheque').readonly = true;
                    //$('txt_numeroCheque').disabled = true;
                }else{
                    alert(" Chequera Agotada, Por Favor active otra chequera para la cuenta");
                    $('txt_numeroCheque').value = '';
                }
            }
        });
    }




    /* Metodos utilizados en el buscador */
    function busca(fecha, nrodoc, pagina){
        var url = 'updater_busca_traterceros.php';
        var pars = '&nrodoc=' + nrodoc + '&fecha=' + fecha+ '&pagina=' + pagina;
        //alert(pars);
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }



    Event.observe('busca_nrodoc', "keyup", function () {
        busca($F('busca_fecha'),
        $F('busca_nrodoc'), 1);
    });


    function validafecha(fecha){
        var upper = 31;
        if(/^(\d{2})\/(\d{2})\/(\d{4})$/.test(fecha.value)) { // dd/mm/yyyy
            if(RegExp.$2 == '02') upper = 29;
            if((RegExp.$1 <= upper) && (RegExp.$2 <= 12)) {
                busca($F('busca_fecha'),
                $F('busca_nrodoc'), 1);
            } else {
                alert("Fecha incorrecta");
                fecha.value = "";
            }
        }else if(fecha.value != '') {
            alert("Fecha incorrecta");
            fecha.value = "";
        }
    }

    var t;
    function busca_popup(descripcion) {

        clearTimeout(t);
        t = setTimeout("busca_solicitud()", 1500);
    };





    function CargarGridPP(id){

        mygridpp.clearSelection();
        mygridpp.clearAll();
        //var idpc_iva = <?= $idpc_iva?>;

        var url = 'json.php';
        var pars = 'op=transferencia&id='+ id;
        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                var totMonto = 0;
                var JsonData = eval( '(' + request.responseText + ')');
                //alert(JsonData);
                if(JsonData){
                    for(var j=0;j<JsonData.length;j++){

                        if((JsonData[j]['ctaProveedor'] == null) && (JsonData[j]['idCta'] != 673)) {
                            var monto= parseFloat(JsonData[j]['monto']);
                            totMonto += monto;
                            monto = monto.toFixed(2);

                            mygridpp.getCombo(0).put(JsonData[j]['idCta'],JsonData[j]['descripcion']);
                            mygridpp.addRow(JsonData[j]['idCta'],JsonData[j]['idCta']+";"+muestraFloat(monto));
                            //totMonto += monto;
                            //alert(totMonto);
                        }
                    }
                    $('monto_causar').value = muestraFloat(totMonto);
                }
            }
        }
    );

    }



    function GuardarPP(idCtaTra,status,accion)
    {

        var JsonAux = new Array;
        var JsonAuxServ = new Array;
        var transfer = new Array;
        var lon = mygridpp.getRowsNum();
        var fecha = '';
        mygridpp.clearSelection()
        if (lon == 0)
            return false;


        if(status==1){
            //Evaluo si es pago de Aportes/Deduccion de Nomina o Retenciones
            if($('rdb_tipo').value == 0){
                //alert('hola');
                for(j=0; j<lon; j++)
                {
                    rowId = mygridpp.getRowId(j);
                    transfer[j] = new Array;
                    transfer[j][0]= mygridpp.cells(mygridpp.getRowId(j),0).getValue();
                    transfer[j][1]= mygridpp.cells(mygridpp.getRowId(j),1).getValue();
                    transfer[j][2]= rowId;
                    JsonAux.push({id_cta:rowId,descrip:mygridpp.cells(mygridpp.getRowId(j),0).getValue(), debe:0, haber:0, id_esc:<?=$escEnEje?>});
                    JsonAux.push({id_cta:rowId,descrip:mygridpp.cells(mygridpp.getRowId(j),0).getValue(), debe:0,haber:0, id_esc:<?=$escEnEje?>});
                }
                //Contrapartida
                JsonAux.push({id_cta:idCtaTra, descrip:'Banco', debe:0, haber:usaFloat($('monto_pagar').value), id_esc:<?=$escEnEje?>});
            }

            else{
                for(j=0; j<lon; j++)
                {
                    rowId = mygridpp.getRowId(j);
                    transfer[j] = new Array;
                    transfer[j][0]= mygridpp.cells(mygridpp.getRowId(j),0).getValue();
                    transfer[j][1]= mygridpp.cells(mygridpp.getRowId(j),1).getValue();
                    transfer[j][2]= rowId;
                    //alert('aqui:'+rowId);
                    JsonAux.push({id_cta:rowId,descrip:mygridpp.cells(mygridpp.getRowId(j),1).getValue(), debe:usaFloat(mygridpp.cells(rowId,1).getValue()), haber:0, id_esc:<?=$escEnEje?>});
                }
                //Contracuenta contable
                JsonAux.push({id_cta:idCtaTra, descrip:'Banco', debe:0, haber:usaFloat($('monto_pagar').value), id_esc:<?=$escEnEje?>});

            }

        }else if(status==2){
            if($('mAnulado').value==''){
                alert('Debe especificar el motivo de la anulacion');
                return false;
            }
            for(j=0; j<lon; j++)
            {
                rowId = mygridpp.getRowId(j);
                JsonAux.push({id_cta:mygridpp.cells(rowId,0).getValue(), descrip:'', debe:0, haber:usaFloat(mygridpp.cells(rowId,1).getValue()), id_esc:<?=$escEnEje?>});
            }

            JsonAux.push({id_cta:idCtaTra, descrip:'', debe:usaFloat($('monto_pagar').value), haber:0, id_esc:<?=$escEnEje?>});
        }
        JsonAuxServ={"transfer":transfer};
        //JSON para Transaccion
        $("transferencia").value=JsonAuxServ.toJSONString();
        //JSON para contabilidad
        $('json_det').value = JsonAux.toJSONString();
        //alert($('json_det').value);
        $('accion').value = accion;
        //alert('aqui');
        document.form1.submit();
    }

    function mostrar_ventana(){

        //var tipo = "()";

        //if($('fechadesde').value == '' &&  $('fechahasta').value == '')
        //    alert('sin nada')
        //else
        //alert($('fechadesde').value+':'+$('fechahasta').value);

        var url = 'buscar_ordenesXtransferir.php';
        var pars = 'ms='+new Date().getTime()+'&desde='+$('fechadesde').value+'&hasta='+$('fechahasta').value;
        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                Dialog.closeInfo();
                Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                        showEffect:Element.show,hideEffect:Element.hide,
                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                    }});

            }
        }
    );
    }

    function busca_popup()
    {
        clearTimeout(t);
        t = setTimeout("buscaRetAportes()", 800);
    }

    function buscaRetAportes()
    {
        //var tipo = "('P','B')";
        //el parametro status se pasa vacio para que no filtre solo los activos, en caso de que se quiera esto se tiene que pasar status=A
        var url = 'buscar_ordenesXtransferir.php';
        var pars = 'desc='+$('search_descrip').value+'&opcion=2&ms='+new Date().getTime();

        var updater = new Ajax.Updater('divcuentas',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    function selDocumento(id, descripcion, saldo){

        $('descCta').value = descripcion;
        $('hIdCta').value = id;
        $('hSaldo').value = saldo;
        $('saldo').value = muestraFloat(saldo);
        Dialog.okCallback();

    }

    //Agrega linea al grid de totalizacion de pago
    function agregarCta(){

        if($('txt_descripcion').value == ''){
            alert ("Debe ingresar una descripción.");
            return;
        }else if($('saldo').value=="" || parseFloat($('saldo').value)<=0){
            alert("Monto a transferir debe ser mayor a cero.");
            return;
            //}else if(parseFloat($('hSaldo').value) < usaFloat($('saldo').value)){
            //       alert("El saldo disponible en la cuenta es menor al requerido");
            //       $('saldo').value='0,00';
            //       return;
        }else{
            for(j=0;j<mygridpp.getRowsNum();j++){
                if (mygridpp.getRowId(j)!=undefined){
                    if (mygridpp.cells(mygridpp.getRowId(j),'0').getValue() == $('hIdCta').value){
                        alert('Esta cuenta ya ha sido seleccionada, por favor seleccione otra cuenta');
                        return false;
                    }
                }
            }
            descrip_largo = $('txt_descripcion').value+' desde '+$('fechadesde').value+' hasta '+$('fechahasta').value;
            mygridpp.addRow($('hIdCta').value,descrip_largo+";"+$('saldo').value+";0;0");
            sumaTotal();
            $('saldo').value='0,00';
            $('txt_descripcion').value = '';
        }
    } //Fin agregacta

    function sumaTotal(){
        var totalCuenta = 0;
        for(j=0;j<mygridpp.getRowsNum();j++){
            if(mygridpp.getRowId(j)!= undefined){
                totalCuenta += usaFloat(mygridpp.cells(mygridpp.getRowId(j),1).getValue());
            }
        }
        $('monto_pagar').value = (isNaN(totalCuenta))? '0,00' : muestraFloat(totalCuenta);

    }

    function eliminaCta(){
        mygridpp.deleteRow(mygridpp.getSelectedId());
        sumaTotal();
    }

    function ver_ctas(){
        Effect.toggle('ctasDiv', 'blind');
    }

    function calcular(){
        var url = 'calcular_apo_ret.php';
        var pars = 'desc='+$('search_descrip').value+'&opcion=2&ms='+new Date().getTime();

        var updater = new Ajax.Updater('divcuentas',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    function calculo(){
        var url = 'calcular_apo_ret.php';
        var radioValue = Form.getInputs('form1','radio','rdb_tipo').find(function(radio) { return radio.checked; }).value;
        //var descrip = '';

        if(radioValue == 0){
            if($('slt_aportes').value != 0) {
                var pars = 'opcion='+radioValue+'&especifico='+$('slt_aportes').value+'&desde='+$('fechadesde').value+'&hasta='+$('fechahasta').value+'&ms='+new Date().getTime();
                $('hIdCta').value = $('slt_aportes').value;
                //descrip = $('slt_aportes').selecteditem;
            }else{
                alert('Debe seleccionar un tipo de aporte de nómina');    
                return false;
            }
        }
        if(radioValue == 1){
            if($('slt_retencion').value != 0) {
                var pars = 'opcion='+radioValue+'&especifico='+$('slt_retencion').value+'&desde='+$('fechadesde').value+'&hasta='+$('fechahasta').value+'&ms='+new Date().getTime();
                //descrip = $('slt_retencion').selecteditem;
                $('hIdCta').value = $('slt_retencion').value;
            }else{
                alert('Debe seleccionar un tipo de retención');
                return false;
            }
        }
        var updater = new Ajax.Updater('calculando',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous: true,
            evalScripts:true,
            onLoading: function(request){Element.Show('calculando')},
            onComplete:function(request){Element.hide('calculando');
                $('saldo').value = request.responseText;
                //$('txt_descripcion').value = descrip.text;
            }
        });

    }


    //Accion para actulizar estados de inputs en forma de pago
    function set_fp(control){
        if(control.value == '0'){
            $('beneficiario').disabled = false;
            $('txt_numeroCheque').disabled = false;
            $('txt_numero_transferencia').disabled = true;
            $('txt_observacion').disabled = true;
        }else{
            $('beneficiario').disabled = true;
            $('txt_numeroCheque').disabled = true;
            $('txt_numero_transferencia').disabled = false;
            $('txt_observacion').disabled = false;
        }
    }

    //Accion para actualizar los estados de los listbox en tipo de pago
    function set_tp(control){

        if(control.value == '0'){
            $('slt_aportes').disabled = false;
            $('slt_retencion').disabled = true;
        }else{
            $('slt_aportes').disabled = true;
            $('slt_retencion').disabled = false;
        }
    }

    //Script para control de calendarios
    //Ricardo Camejo
    <!--  to hide script contents from old browsers
    var startDate;
    var endDate;
    var ONEDAY = 3600 * 24;

    function resetDates() {
        startDate = endDate = null;
    }
    /*
     * Given two dates (in seconds) find out if date1 is bigger, date2 is bigger or
     * they're the same, taking only the dates, not the time into account.
     * In other words, different times on the same date returns equal.
     * returns -1 for date1 bigger, 1 for date2 is bigger 0 for equal
     */

    function compareDatesOnly(date1, date2) {
        var year1   = date1.getYear();
        var year2   = date2.getYear();
        var month1  = date1.getMonth();
        var month2  = date2.getMonth();
        var day1    = date1.getDate();
        var day2    = date2.getDate();

        if (year1 > year2) {
            return -1;
        }
        if (year2 > year1) {
            return 1;
        }

        //years are equal
        if (month1 > month2) {
            return -1;
        }
        if (month2 > month1) {
            return 1;
        }

        //years and months are equal
        if (day1 > day2) {
            return -1;
        }
        if (day2 > day1) {
            return 1;
        }

        //days are equal
        return 0;


        /* Can't do this because of timezone issues
                        var days1 = Math.floor(date1.getTime()/Date.DAY);
                        var days2 = Math.floor(date2.getTime()/Date.DAY);
                        return (days1 - days2);
         */
    }

    function filterDates1(cal) {
        startDate = cal.date;
        /* If they haven't chosen an
                        end date before we'll set it to the same date as the start date This
                        way if the user scrolls in the start date 5 months forward, they don't
                        need to do it again for the end date.
         */

        if (endDate == null) {
            Zapatec.Calendar.setup({
                inputField     :    "fechahasta",
                button         :    "boton_fechahasta",  // What will trigger the popup of the calendar
                ifFormat       :    "%d/%m/%Y",
                daFormat          : "%Y/%m/%d",
                timeFormat     :    "24",
                date           :     startDate,
                electric       :     false,
                showsTime      :     false,          //no time
                disableFunc    :    dateInRange2, //the function to call
                onUpdate       :    filterDates2
            });
        }
    }

    function filterDates2(cal) {
        endDate = cal.date;
    }

    /*
     * Both functions disable and hilight dates.
     */

    /*
     * Can't choose days after the
     * end date if it is choosen, hilights start and end dates with one style and dates between them with another
     */
    function dateInRange1(date) {

        if (endDate != null) {

            // Disable dates after end date
            var compareEnd = compareDatesOnly(date, endDate);
            if  (compareEnd < 0) {
                return (true);
            }

            // Hilight end date with "edges" style
            if  (compareEnd == 0) {
                {return "edges";}
            }


            // Hilight inner dates with "between" style
            if (startDate != null){
                var compareStart = compareDatesOnly(date, startDate);
                if  (compareStart < 0) {
                    return "between";
                }
            }
        }

        //disable days prior to today
        //Obtengo el año en curso, lo malo de esta tácnica que toma la fecha del cliente
        var ano_actual = new Date();
        var today = new Date(ano_actual.getFullYear(),0,1);
        //alert(today);
        var compareToday = compareDatesOnly(date, today);
        if (compareToday > 0) {
            return(true);
        }


        //all other days are enabled
        return false;
        //alert(ret + " " + today + ":" + date + ":" + compareToday + ":" + days1 + ":" + days2);
        return(ret);
    }

    /*
     * Can't choose days before the
     * start date if it is choosen, hilights start and end dates with one style and dates between them with another
     */

    function dateInRange2(date) {

        if (startDate != null) {
            //alert(startDate+':'+endDate+'->'+date);
            // Disable dates before start date
            var compareDays = compareDatesOnly(startDate, date);
            if  (compareDays < 0) {
                return (true);
            }

            // Hilight end date with "edges" style
            if  (compareDays == 0) {
                {return "edges";}
            }

            // Hilight inner dates with "between" style
            if ((endDate != null) && (date > startDate) && (date < endDate)) {
                return "between";
            }
        }

        var ano_actual = new Date();
        var now = new Date(ano_actual.getFullYear(),0,1);
        if (compareDatesOnly(now, date) < 0) {
            return (true);
        }

        //all other days are enabled
        return false;
    }
    // end hiding contents from old browsers  -->



</script>
<? require("comun/footer.php");?>
