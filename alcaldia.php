<?
require ("comun/ini.php");
// Creando el objeto alcaldia
$oAlcaldia = new alcaldia;
$del = $_REQUEST['del'];
$accion = $_REQUEST['accion'];
switch(true) {
    case ($accion=='Guardar'):
        $oAlcaldia->add($conn, $_REQUEST['id_nuevo'],
                $_REQUEST['descripcion'],
                $_REQUEST['razon'],
                $_REQUEST['domicilio'],
                $_REQUEST['fecha_creacion'],
                $_REQUEST['ciudad'],
                $_REQUEST['estado'],
                $_REQUEST['telefono'],
                $_REQUEST['fax'],
                $_REQUEST['web_site'],
                $_REQUEST['cpostal'],
                $_REQUEST['alcalde'],
                $_REQUEST['personal'],
                $_REQUEST['concejales'],
                $_REQUEST['id_proveedor']);
        break;

    case($accion == 'Actualizar') :
        $oAlcaldia->set($conn, $_REQUEST['id_nuevo'],
                $_REQUEST['id'],
                $_REQUEST['descripcion'],
                $_REQUEST['razon'],
                $_REQUEST['domicilio'],
                $_REQUEST['fecha_creacion'],
                $_REQUEST['ciudad'],
                $_REQUEST['estado'],
                $_REQUEST['telefono'],
                $_REQUEST['fax'],
                $_REQUEST['web_site'],
                $_REQUEST['cpostal'],
                $_REQUEST['alcalde'],
                $_REQUEST['personal'],
                $_REQUEST['concejales'],
                $_REQUEST['id_proveedor']);
        break;
    case($del == 's') :
        $oAlcaldia->del($conn, $_REQUEST['id']);
        break;
}
$msg = $oAlcaldia->msg;


//Seccion paginador
$page_size = 25;
if ($_GET['pg'])
    $start_record=($_GET['pg'] * $page_size) - $page_size;
else
    $start_record=0;

$cAlcaldia=$oAlcaldia->get_all($conn, $start_record,$page_size);
$pag=new paginator($oAlcaldia->total,$page_size, self($_SERVER['PHP_SELF']));
$i=$pag->get_total_pages();

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div>";
?>
<br />
<span class="titulo_maestro">Maestro de Alcald&iacute;a</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<? if(is_array($cAlcaldia)) { ?>
<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
    <tr class="cabecera">
        <td>C&oacute;digo</td>
        <td>Nombre</td>
        <td>Fecha Creaci&oacute;n</td>
        <td>Ciudad</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <?
        $i = 0;
        foreach($cAlcaldia as $alcaldia) {
            ?>
    <tr class="filas">
        <td><?=$alcaldia->id?></td>
        <td><?=$alcaldia->descripcion?></td>
        <td><?=$alcaldia->fecha_creacion?></td>
        <td><?=$alcaldia->ciudad?></td>
        <td><a href="?del=s&id=<?=$alcaldia->id?>" onclick="if (confirm('Si presiona Aceptar será eliminada esta información')){ return true;} else{return false;}"  title="Eliminar Registro"><img src="images/eliminar.gif" border="0" ></a></td>
        <td align="center">
            <a href="#" onclick="updater('<?=$alcaldia->id?>'); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
    </tr>
            <? $i++;
        }
        ?>
</table>
    <? }else {
    echo "No hay registros en la bd";
} ?>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<table width="762" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><span class="paginator"><? $pag->print_page_counter()?></span></td>
        <td align="right"><span class="paginator"><? $pag->print_paginator("pulldown")?> </span></td>
    </tr>
</table>
<script type="text/javascript">
    function mostrar_ventana(){

        //var tipo = "()";
        var url = 'buscar_proveedores.php';
        var pars = 'status=&tipo=&ms='+new Date().getTime();
        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                Dialog.closeInfo();
                Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                        showEffect:Element.show,hideEffect:Element.hide,
                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                    }});

            }
        }
    );
    }

    var t=0;
    function busca_popup()
    {
        clearTimeout(t);
        t = setTimeout("buscaProveedor()", 800);
    }


    function buscaProveedor()
    {
        //var tipo = "('P','B')";
        //el parametro status se pasa vacio para que no filtre solo los activos, en caso de que se quiera esto se tiene que pasar status=A
        var url = 'buscar_proveedores.php';
        var pars = 'tipo='+$('tipo_prov').value+'&status=&rif='+$('search_rif_prov').value+ '&nombre='+ $('search_nombre_prov').value+'&opcion=2&ms'+new Date().getTime();

        var updater = new Ajax.Updater('divProveedores',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }

    function selDocumento(id, nombre){

        //$('nombrepro').value = nombre;
        $('id_proveedor').value = id;
        Dialog.okCallback();

    }

    function traeProveedorDesdeXML(id_proveedor){
        //$('id_proveedor').value = id_proveedor;

        var url = 'xmlTraeProveedor.php';
        var pars = 'id=' + id_proveedor;
        var myAjax = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onComplete: traeProveedor
        });

        function traeProveedor(originalRequest){
            var xmlDoc = originalRequest.responseXML;
            var x = xmlDoc.getElementsByTagName('proveedor');

            for(j=0;j<x[0].childNodes.length;j++){
                if (x[0].childNodes[j].nodeType != 1) continue;
                var nombre = x[0].childNodes[j].nodeName
                //alert(nombre);
                $(nombre).value = x[0].childNodes[j].firstChild.nodeValue;
            }
            //alert(nombre);
        }
    }
</script>
<?
$validator->create_message("error_cod", "id_nuevo", "*");
$validator->create_message("error_nom", "descripcion", "*");
$validator->create_message("error_raz", "razon", "*");
$validator->create_message("error_dom", "domicilio", "*");
$validator->create_message("error_fechac", "fecha_creacion", "*");
$validator->create_message("error_cdad", "ciudad", "*");
$validator->create_message("error_edo", "estado", "*");
$validator->create_message("error_tlf", "telefono", "*");
$validator->create_message("error_postal", "cpostal", "*");
$validator->create_message("error_alcalde", "alcalde", "*");
$validator->create_message("error_personal", "personal", "*");
//$validator->create_message("error_concejales", "concejales", "*");
$validator->print_script();
require ("comun/footer.php");
?>