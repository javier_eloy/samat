<?
include ("comun/ini.php");
$pagina = $_REQUEST['pagina'];
$fecha_desde = $_GET['fecha_desde'];
$fecha_hasta = $_GET['fecha_hasta'];
//echo 'p'.$fecha_hasta."<br>";
if (!$pagina) 
{
    $inicio = 10;
    $pagina=1;
}
else {
    $inicio = ($pagina - 1) * 20;
} 

//die(var_dump($_REQUEST));
$oArqueoCaja = new arqueo_caja($conn);
$cArqueoCaja = $oArqueoCaja->get_all($fecha_desde, $fecha_hasta, $inicio, 20);
//var_dump($cComprobante);
$total = $oArqueoCaja->total_registro_busqueda($fecha_desde, $fecha_hasta);

if(is_array($cArqueoCaja) && count($cArqueoCaja) > 0)
{
?>
	<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
		<tr class="cabecera"> 
		<td width="13%">Tipo de Caja</td>
                <td width="50%">Descripci&oacute;n</td>
		<td width="12%">Fecha</td>
                <td width="12%">Estatus</td>
		<td width="8%">Reporte</td>
		<td width="5%">&nbsp;</td>
	</tr>
	<? 
	foreach($cArqueoCaja as $com) 
	{ 
	?> 
	<tr class="filas"> 
		<td><?=($com->tipo_caja==0)?'Tributo':'Servicio Municipal'?></td>
        <td><?=$com->descripcion?></td>
		<td align="center"><?= muestraFecha($com->fecha)?></td>
                <td align="center"><?=($com->status==1)?'Abierta':'Cerrada';?></td>
		<td align="center">
                    <? if($com->status == 2){
                    ?>
                    <a target="_blank" href="reporte_arqueo_caja.pdf.php?id=<?=$com->id?>" target="_blank" title="Emitir Reporte"><img src="images/reporte.jpg" border="0" ></a>
                    <?}?>    
		</td>
		<td align="center">
			<a href="#" onClick="updater('<?=$com->id?>&id_escenario=<?=$com->id_escenario?>'); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a>
		</td>
	</tr>
	<?
	}

	$total_paginas = ceil($total / 20);
?>
	<tr class="pietabla">
		<td colspan="7" align="center">
		<? 
		for ($j=1;$j<=$total_paginas;$j++)
		{
			if ($j==1)
			{ 
				if ($j==$pagina)
					echo "<span class=\"actual\">".$j."</span>";
				else
					echo "<span style=\"cursor:pointer\" onclick=\"busca($('busca_fecha_desde').value, $('busca_fecha_hasta').value , '".$j."');\">".$j."</span>";
			}
			else 
			{
				if ($j==$pagina)
					echo "- <span class=\"actual\">".$j."</span>";
				else
					echo "<span style=\"cursor:pointer\" onclick=\"busca($('busca_fecha_desde').value, $('busca_fecha_hasta').value, '".$j."');\">- ".$j."</span>";
			}
		}
		?>
		</td>
	</tr>
	<tr class="pietabla">
		<td colspan="7" align="center"> Pagina <strong><?=$pagina?></strong> de <strong><?=$total_paginas?></strong></td>
	</tr>
</table>
<? 
}
else 
{
	echo "No hay registros en la bd";
} 
?>
