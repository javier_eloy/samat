<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//die($_REQUEST['status'].':'.$_REQUEST['tipo']);

$id = isset($_REQUEST['id']) ? $_REQUEST['id']:'-1'; 
//$status = isset($_REQUEST['status'])? $_REQUEST['status']:'-1';
//$fecha_desde =  isset($_REQUEST['fecha_desde'])?$_REQUEST['fecha_desde']: '';


function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $tipo;
  var $wcolumnas = array();
  var $fecha_caja;
  //Cabecera de página
	function Header()
	{
			parent::Header();
			$this->SetXY(230, 7);
                        $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');			
			$this->Ln(18);
			$this->SetFont('Courier','b',12);
			$this->Cell(260, 5, "ARQUEO DE CAJA",0,1,'C');
			$this->SetFont('Courier','B',10);			
	}

	function Footer()
	{
		$this->SetFont('Courier','I',10);
                $this->ln();
		$this->Cell(260,5,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,1,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('L','mm');
$pdf->SetAutoPageBreak(true ,1);
$pdf->wcolumnas[1] = 20;
$pdf->wcolumnas[2] = 25;
$pdf->wcolumnas[3] = 25;

$total_general = 0;

$qe = "SELECT * FROM ingresos.ingresos_enc WHERE ";
$qe .= ($id != -1) ? "id = $id " :  "1=1"; //else $q.=" AND a.status = 0 ";
//die($q);
$rqe = $conn->Execute($qe);
//Loop de Departamentos
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);

$tipo_des_actual = 'Tributos';
$pdf->SetFont('Courier','',8);
//Encabezado
$pdf->SetFont('Courier','B',8);
$pdf->cell(50,5,"Tipo: ",0,0,'L');
$pdf->SetFont('Courier','',8);
$pdf->cell(50,5,$rqe->fields['descripcion'],0,1,'L');
$pdf->SetFont('Courier','B',8);
$pdf->cell(50,5,"Fecha de Caja: ",0,0,'L');
$pdf->SetFont('Courier','',8);
$pdf->cell(50,5,muestrafecha($rqe->fields['fecha']),0,1,'L');
$pdf->SetFont('Courier','B',8);
$pdf->cell(50,5,"Tipo: ",0,0,'L');
$pdf->SetFont('Courier','',8);
$pdf->cell(50,5,($rqe->fields['tipo_caja'] == 0) ? "Tributo" : "Servicios Municipales",0,1,'L');
$pdf->SetFont('Courier','B',8);
$pdf->cell(80,5,utf8_decode("Descripción"),1,0,'C');
$pdf->cell(120,5,"Cuenta Contable",1,0,'C');
//$pdf->cell(20,5,"Monto",1,0,'C');
$pdf->cell(30,5,"Debe",1,0,'C');
$pdf->cell(30,5,"Haber",1,1,'C');
$pdf->SetFont('Courier','',8);

$qd = "select b.monto,b.origen,case when b.origen = 1 then 'Debe' else 'Haber' end as accion,c.descripcion,d.codcta||'-'||d.descripcion as nombre
from ingresos.ingresos_enc a
inner join ingresos.ingresos_det b on a.id = b.id_caja
inner join ingresos.tipos_ingresos c on b.id_tipo_pago = c.id
inner join contabilidad.plan_cuenta d on c.id_cta_contable = d.id
where a.id = $id order by accion";
//die($qd);
$rqd = $conn->Execute($qd);
$totalDebe = 0;
$totalHaber = 0;
while(!$rqd->EOF){
    $pdf->cell(80,5,utf8_decode(substr($rqd->fields['descripcion'],0,50)),0,0,'L');
    $pdf->cell(120,5,substr($rqd->fields['nombre'],0,70),0,0,'L');
    
    //$pdf->cell(20,5,$rqd->fields['accion'],0,1,'C');
    if($rqd->fields['origen'] == 1){
       $pdf->cell(30,5,muestrafloat($rqd->fields['monto']),0,0,'R');     
       $pdf->cell(30,5,"",0,1,'R');     
       $totalDebe += $rqd->fields['monto'];
    }else{
       $pdf->cell(30,5,"",0,0,'R');      
       $pdf->cell(30,5,muestrafloat($rqd->fields['monto']),0,1,'R');     
       $totalHaber += $rqd->fields['monto'];  
    }   
    $rqd->movenext();
}

//Totales
$pdf->setX(175);
$pdf->SetFont('Courier','B',8);
$pdf->cell(40,5,"Totales: ",0,0,'L');
$pdf->SetFont('Courier','',8);
$pdf->cell(30,5,muestrafloat($totalDebe),0,0,'R');
$pdf->cell(30,5,muestrafloat($totalHaber),0,1,'R');
//firma
$pdf->ln(4);
$pdf->SetX(120);
$pdf->Cell(50, 30, "",1,1,'C');
$pdf->SetX(120);
$pdf->Cell(50, 5, "Realizado Por",1,1,'C');


//Seccion sin uso
$detallado = false;
if($detallado){
    while(!$rqd->EOF){
      $encabezado = 0;  
      if($id_actual == '') {
        $id_actual = $rD->fields['id_doc'];
        $encabezado = 1;
      }
      elseif($id_actual != $rD->fields['id_doc']){
             //Total Documento actual
             $pdf->SetFont('Courier','B',8);
             $pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso Bs.: "),0,0,'L');
             $pdf->Cell(25,5,muestrafloat(redondeado($total_doc_actual)),0,1,'R');
             if($rD->fields['tipo_caja'] == 1 and $pdf->tipo == 0){
                $pdf->tipo = 1;
                $subtotal = redondeado($total_documentos);
                $pdf->SetFont('Courier','B',8);
                $pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso ".$tipo_des_actual." Bs.: "),0,0,'L');
                $pdf->Cell(25,5,muestrafloat(redondeado($subtotal)),0,1,'R');
                $tipo_des_actual = 'Servicios Municipales';
                $pdf->addpage();
             }
             $id_actual = $rD->fields['id_doc'];
             $total_doc_actual = 0;
             $encabezado = 1;
             }

      if($encabezado == 1){
         $pdf->SetFont('Courier','B',8); 
         $pdf->Cell(130,5,$rD->fields['des_tipo'].' : '.$rD->fields['descripcion'],0,0,'L'); 
         $pdf->Cell(50,5,muestrafecha($rD->fields['fecha']),0,1,'R'); 
         $pdf->SetFont('Courier','',8);
      }     
      //if($rD->fields['status'] == 1) $pdf->SetFont('Courier','U',8); else	$pdf->SetFont('Courier','',8);
      $pdf->SetFont('Courier','',6);
      $pdf->Cell($pdf->wcolumnas[1],2,muestrafecha($rD->fields['fecha_documento']),0,0,'L');
      $pdf->Cell($pdf->wcolumnas[2],2,$rD->fields['num_doc'],0,0,'L');
      $pdf->Cell($pdf->wcolumnas[3],2,muestrafloat($rD->fields['monto']),0,1,'R');
      $pdf->SetFont('Courier','B',8);


      $total_doc_actual += redondeado($rD->fields['monto']);
      $total_documentos += redondeado($rD->fields['monto']);
      $rD->movenext();
    } //Fin loop departamentos
}


$pdf->Output();
?>
