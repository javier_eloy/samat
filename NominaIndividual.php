<? require ("comun/ini.php"); require ("comun/header.php"); ?>
<br />
<span class="titulo_maestro">C&aacute;lculo de N&oacute;mina Individual</span>
<div id="formulario">
	<table width="100%" border="0">
		<tr>
			<td width="100" >Contrato:</td>
			<td width="400"><?=helpers::combonomina($conn, '', '','','','Contrato','int_cod','cont_nom','Contrato','','SELECT * FROM rrhh.contrato WHERE emp_cod='.$_SESSION['EmpresaL'].' ORDER BY int_cod','ComboTrabajador(this.options[this.selectedIndex].value);ComboNomina(this.options[this.selectedIndex].value)','true');?></td>
		</tr>
		<tr>
			<td width="100">Trabajador:</td>
			<td width="400"><SELECT name="Trabajador" id="Trabajador"  ></SELECT></td>
		</tr>
                <tr>
			<td width="100">N&oacute;mina:</td>
			<td width="400"><SELECT name="nomina" id="nomina" onChange="ComboConceptos(this.options[this.selectedIndex].value)" ></SELECT></td>
		</tr>
                <tr>
			<td colspan="2" ><br /></td>
		<tr>
                <tr>
			<td width="100">Conceptos:</td>
			<td width="400"><SELECT name="concepto" id="concepto" ></SELECT>
                            <input  type="button"  value="Agregar" onClick="Agregar()" >
                            <input  type="button"  value="Eliminar" onClick="Eliminar_grid()" >
                        </td>
                        
		</tr>
		
			<td colspan="2"><div id="gridbox" width="100%" height="350" class="gridbox"></div></td>
		</tr>
		<tr>
			<td colspan="2" ><br /></td>
		</tr>
		<tr>
			<td >Total Asignaciones:</td>
			<td ><input readonly name="TotalA" id="TotalA"  value="0.00"></td>
		</tr>
		<tr>
			<td >Total Deducciones:</td>
			<td ><input readonly name="TotalD" id="TotalD"  value="0.00"></td>
		</tr>
		<tr>
			<td >Total a Pagar:</td>
			<td ><input readonly name="Total" id="Total" value="0.00"></td>
		</tr>
		<tr>
			<td colspan="2" ><br /></td>
		</tr>
		<tr>
			<td align="left" ><input  type="button"  value="Eliminar Relacion" onClick="Eliminar()" ></td>
			<td align="right"><input  type="button"  value="Recibo de Pago" onClick="Imprimir()" ></td>
		</tr>
	</table>
</div>
<script> 
var mygrid;
var i=0;
var iEli=0;

buildGrid();
ComboTrabajador($('Contrato').options[$('Contrato').selectedIndex].value);

function ComboTrabajador(Contrato){
var JsonAux;
	$('Trabajador').length=0;
	mygrid.clearSelection();
	mygrid.clearAll();
	if(Contrato!=-1){
		JsonAux={"Contrato":parseInt(Contrato),"Forma":0};
		var url = 'CargarCombo.php';
		var pars = 'JsonEnv=' + JsonAux.toJSONString();
		var Request = new Ajax.Request(
			url,
			{
				method: 'post',
				parameters: pars,
		//		asynchronous:true, 
				onComplete:function(request){
					var JsonRec = eval( '(' + request.responseText + ')');
					if(JsonRec){
						for(var i=0;i<JsonRec.length;i++){
							$('Trabajador').options[i]= new Option(Cadena(JsonRec[i]['N'])+" "+Cadena(JsonRec[i]['A']),JsonRec[i]['CI']);
						}
						//CargarGrid();
					}
				}
			}
		); 
	}
}

function ComboNomina(Contrato){
var JsonAux;
	$('nomina').length=0;
	mygrid.clearSelection();
	mygrid.clearAll();
	if(Contrato!=-1){
		JsonAux={"Contrato":parseInt(Contrato),"Forma":7};
		var url = 'CargarCombo.php';
		var pars = 'JsonEnv=' + JsonAux.toJSONString();
		var Request = new Ajax.Request(
			url,
			{
				method: 'post',
				parameters: pars,
		//		asynchronous:true, 
				onComplete:function(request){
					var JsonRec = eval( '(' + request.responseText + ')');
					if(JsonRec){
						for(var i=0;i<JsonRec.length;i++){
							$('nomina').options[i]= new Option(Cadena(JsonRec[i]['D']),JsonRec[i]['ID']);
						}
                                                
						//CargarGrid();
					}
				}
			}
		); 
	}
}

function ComboConceptos(Nomina){
var JsonAux;
	$('concepto').length=0;
	mygrid.clearSelection();
	mygrid.clearAll();
	if(Nomina!=-1){
		JsonAux={"Nomina":parseInt(Nomina),"Forma":5};
		var url = 'CargarCombo.php';
		var pars = 'JsonEnv=' + JsonAux.toJSONString();
		var Request = new Ajax.Request(
			url,
			{
				method: 'post',
				parameters: pars,
		//		asynchronous:true, 
				onComplete:function(request){
					var JsonRec = eval( '(' + request.responseText + ')');
					if(JsonRec){
						for(var i=0;i<JsonRec.length;i++){
							$('concepto').options[i]= new Option(Cadena(JsonRec[i]['N']),JsonRec[i]['CI']);
						}
                                                
						//CargarGrid();
					}
				}
			}
		); 
	}
}


function buildGrid(){
	//set grid parameters
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.selMultiRows = true;
        mygrid.setImagePath("dhtmlx/dhtmlxgrid/codebase/imgs/");
        mygrid.setSkin("dhx_skyblue");
        mygrid.setHeader("C&oacute;digo,Nombre,Asignaci&oacute;n,Deducci&oacute;n",null,["text-align:center;","text-align:center;","text-align:center","text-align:center"]);
	mygrid.setInitWidths("100,400,150,150")
	mygrid.setColAlign("center,left,right,right")
	mygrid.setColTypes("ro,ro,ro,ro");
	mygrid.setColSorting("int,str,int,int")
	mygrid.setColumnColor("white,white,white,green")
	mygrid.rowsBufferOutSize = 0;
	mygrid.setMultiLine(false);
	mygrid.selmultirows="true";
	mygrid.delim=";";
	mygrid.init();
}

function Agregar(){
var JsonAux;    
    if($('concepto').selectedIndex!=-1 ){
            JsonAux={"concepto":$('concepto').options[$('concepto').selectedIndex].value};
            var url = 'updater_variables_nomina.php';
            //alert(JsonAux.toJSONString());
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var Request = new Ajax.Request(
                    url,
                    {
                            method: 'post',
                            parameters: pars,
                	    asynchronous:true, 
                            onComplete:function(request){
                                Dialog.closeInfo();
                                Dialog.alert(request.responseText, {
                                    windowParameters: {width:600, height:400,
                                        showEffect:Element.show,hideEffect:Element.hide,
                                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }
                                    }
                                })
                            }
                    }
                    ); 
    } 
   //if(parseFloat($('valor').value) > 0){
        if(i==0){
            mygrid.addRow(i,$('concepto').value+";"+"Concepto"+";");
            var row = mygrid.doesRowExist(i);
            if(row){
                mygrid.selectCell(mygrid.getRowIndex(i),0,false,true,true);
                i++;
            }
        }else{
               mygrid.addRow(i-iEli,";;");
               mygrid.selectCell(mygrid.getRowIndex(i-iEli),0,false,true,true);
               i++;
        }
   //} 
}

function selDocumento(id){
    $('nroref').value = id;
    Dialog.okCallback();
}


function Eliminar_grid(){
    
}

function CargarGrid(){
var JsonAux;
	mygrid.clearSelection();
	mygrid.clearAll();
	if($('Trabajador').selectedIndex!=-1){
		JsonAux={"Contrato":parseInt($('Contrato').options[$('Contrato').selectedIndex].value),"Trabajador":parseInt($('Trabajador').options[$('Trabajador').selectedIndex].value),"Forma":2,"Accion":0};
		var url = 'OperarGrid.php';
		var pars = 'JsonEnv=' + JsonAux.toJSONString();
		var Request = new Ajax.Request(
			url,
			{
				method: 'post',
				parameters: pars,
		//		asynchronous:true, 
				onComplete:function(request){
				var JsonRec = eval( '(' + request.responseText + ')');
					if(JsonRec){
						for(var i=0;i<JsonRec.length-1;i++){
							mygrid.addRow(i,JsonRec[i]['CU']+";"+JsonRec[i]['N']+";"+JsonRec[i]['V'],i);
						} 
						$('TotalA').value=JsonRec[JsonRec.length-1]['CU'];
						$('TotalD').value=JsonRec[JsonRec.length-1]['N'];
						$('Total').value=JsonRec[JsonRec.length-1]['V'];
						
					}
				}
			}
		); 
	}
}

function Eliminar(){
var JsonAux,res;
	if($('Trabajador').selectedIndex==-1){
		alert("Debe escojer un Trabajador");
	}else{
		res=confirm("Se eliminaran los valores de los conceptos para el trabajador. �Esta Seguro que desea continuar?");
		if(res){
			JsonAux={"Contrato":parseInt($('Contrato').options[$('Contrato').selectedIndex].value),"Trabajador":parseInt($('Trabajador').options[$('Trabajador').selectedIndex].value),"Forma":2,"Accion":1};
			var url = 'OperarGrid.php';
			var pars = 'JsonEnv=' + JsonAux.toJSONString();
			var Request = new Ajax.Request(
				url,
				{
					method: 'post',
					parameters: pars,
			//		asynchronous:true, 
					onComplete:function(request){
						CargarGrid();
						alert(request.responseText);
					}
				}
			);  
		}
	}
} 

var wx;

function Imprimir(){
var JsonAux;
	if($('Contrato').options[$('Contrato').selectedIndex].value==-1){
		alert("Debe escojer un Contrato");
	}else{
		if (!wx || wx.closed) { 
			wx = window.open("precibopago.pdf.php?id="+$('Contrato').options[$('Contrato').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc=-1","winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
			wx.focus()
		} else { 
			wx.focus()
		} 
	}
}
</script>
<? require ("comun/footer.php"); ?>

