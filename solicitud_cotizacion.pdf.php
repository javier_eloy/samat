<?
include("comun/ini.php");
include("Constantes.php");
$id_requisicion = $_REQUEST['id_requisicion'];
if(empty($id_requisicion))
	header ("location: revision_requisicion.php");
$_SESSION['pdf'] = serialize($oCajaChica);
class PDF extends FPDF
{
       var $fecha_registro;
//Cabecera de p�gina
	function Header()
	{
                        parent::Header();
//			$this->SetLeftMargin(15);
//			$this->SetFont('Courier','',8);
//			$this->Ln(1);
//			$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda
//			$this->SetXY(42, 5);
//			$textoCabecera = PAIS."\n\n";
//			$textoCabecera.= UBICACION."\n\n";
//			$textoCabecera.= ENTE."\n";
//			$this->MultiCell(100,2, $textoCabecera, 0, 'L');

			$oReviRequi = unserialize($_SESSION['pdf']);

			$this->SetXY(150, 20); 
			//$textoDerecha = "Fecha: ".date('d/m/Y')."\n";
			$textoFecha = "Fecha: ".muestrafecha($this->fecha_registro)."\n";
                        $this->Text(150, 10, $textoFecha);
			/*$textoDerecha.= "Fecha Aprob.:".muestrafecha($oCajaChica->fecha_aprobacion)."\n";*/
			$textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}\n";
                        $this->Text(150, 15, $textoPag);
			//$this->MultiCell(50,2, $textoDerecha, 0, 'L');
			
			/*$this->SetFont('Courier','b',12);
			$this->Text(80, 40, "Caja Chica");
			$this->Text(153, 40, "Nro.:".$oCajaChica->nrodoc."\n");
			$this->Line(15, 41, 190, 41);
			$this->Ln(16);*/
			
	}

	//Pie de p�gina
	function Footer()
	{
		
		//$this->SetY(-15);
		//Arial italic 8
		//$this->SetFont('Arial','I',8);
		//N�mero de p�gina
		//$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
                $this->SetFont('Courier','B',8);
                $this->Ln();
                $this->Ln();
                $this->Ln();
                $this->Ln();
                $this->Ln(5);
                $this->Cell(175,20,'____________________',0,'','C');
                $this->Ln();
                $this->Cell(175,2, JEFEPRESUPUESTO,0, '','C');
                $this->Ln();
                $this->Cell(175,2, 'Analista de Presupuesto',0, '','C');
	}
}
//Creaci�n del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();

$pdf->SetFont('Courier','B',8);
$pdf->SetLeftMargin(15);


//OBTIENE LA CANTIDAD DE EMPRESAS SELECCIONADAS PARA COTIZAR
$q="SELECT DISTINCT puser.proveedores.nombre, puser.proveedores.rif, puser.proveedores.direccion, puser.proveedores.contacto, puser.proveedores.telefono, puser.proveedores_requisicion.fecha_registro ";
$q.="FROM puser.proveedores_requisicion ";
$q.="Inner Join puser.proveedores ON puser.proveedores_requisicion.id_proveedor = puser.proveedores.id ";
$q.="WHERE puser.proveedores_requisicion.id_requisicion =  '$id_requisicion'";
//die($q);
$provee= $conn->Execute($q);
$numero = $provee->RecordCount();

$pdf->fecha_registro = $provee->fields['fecha_registro'];

//OBTIENE LOS PRODUCTOS DE LA REQUISICION
$q = "SELECT DISTINCT puser.proveedores_requisicion.id_requisicion, puser.productos.descripcion,puser.relacion_gbl_requisicion.cantidad, productos.unidad_medida ";
$q.= "FROM puser.proveedores_requisicion ";
$q.= "Inner Join puser.relacion_gbl_requisicion ON puser.proveedores_requisicion.id_requisicion = puser.relacion_gbl_requisicion.id_gbl_requisicion ";
$q.= "Inner Join puser.productos ON puser.relacion_gbl_requisicion.id_producto = puser.productos.id ";
$q.= "WHERE puser.proveedores_requisicion.id_requisicion =  '$id_requisicion'";

$cArticulo= $conn->Execute($q);
$cantidad= $cArticulo->RecordCount();


for($i=0;$i<$numero;$i++){
$pdf->AddPage();
$pdf->Ln(15);
$pdf->Ln(3);
$pdf->Cell(175,0.2, '',1, '','C');

$pdf->Ln();
$pdf->SetFont('Courier','B',12);
$pdf->Cell(175,4, 'SOLICITUD DE COTIZACI'.utf8_decode('Ó').'N',0, '','C');

$pdf->Ln();
$pdf->Cell(175,0.2, '',1, '','C');
//$pdf->SetFillColor(232 , 232, 232);
$pdf->Ln(2);
$pdf->SetFont('Courier','B',8);
$pdf->Cell(25,4, 'Sr(a).:',0, '','L' );
$pdf->SetFont('Courier','',8);
$pdf->Cell(100,4,utf8_decode($provee->fields['nombre']),0, '','L');

$pdf->Ln();
$pdf->SetFont('Courier','B',8);
$pdf->Cell(25,4, 'RIF:',0, '','L');
$pdf->SetFont('Courier','',8);
$pdf->Cell(100,4, utf8_decode($provee->fields['rif']),0, '','L');

$pdf->Ln();
$pdf->SetFont('Courier','B',8);
$pdf->Cell(25,4, 'Direcci'.utf8_decode('ó').'n:',0, '','L');
$pdf->SetFont('Courier','',8);
$pdf->MultiCell(100,4, utf8_decode($provee->fields['direccion']),0, '','L');

$pdf->Ln();
$pdf->SetFont('Courier','B',8);
$pdf->Cell(7,4, 'Tlf:',0, '','L' );
$pdf->SetFont('Courier','',8);
$pdf->Cell(25,4, utf8_decode($provee->fields['telefono']),0, '','L' );
$pdf->Ln();
$pdf->Ln(3);
$pdf->Cell(175,0.2, '',1, '','C');

$pdf->Ln();
$pdf->SetFont('Courier','B',12);
$pdf->Cell(175,4, 'ARTICULOS INCLUIDOS EN LA COTIZACI'.utf8_decode('Ó').'N',0, '','C');

$pdf->Ln();
$pdf->Cell(175,0.2, '',1, '','C');

$pdf->Ln(2);
$pdf->SetFont('Courier','B',8);
$pdf->Cell(110,4,'',0,'','L');
$pdf->Cell(30,4,'UNIDAD',0,'','C');
$pdf->Ln();
$pdf->Cell(110,4, 'DESCRIPCI'.utf8_decode('Ó').'N',0,'','L');
$pdf->Cell(30,4,'MEDIDA',0,'','C');
$pdf->Cell(35,4, 'CANTIDAD',0, '','R');
$pdf->Ln();
$pdf->Cell(175,0.2, '',1, '','C');

$pdf->Ln();

$pdf->SetFont('Courier','',8);
for ($j=0;$j<$cantidad;$j++){
	
	$pdf->Ln();
	$pdf->Cell(110,4, utf8_decode($cArticulo->fields['descripcion']),0, '','L');
	$pdf->Cell(30,4, utf8_decode($cArticulo->fields['unidad_medida']),0, '','C');
	$pdf->Cell(35,4, number_format($cArticulo->fields['cantidad'],2,',','.'),0, '','R');
	$cArticulo->movenext();
}
	$cArticulo->MoveFirst();

$pdf->Ln();
$pdf->Ln();
$pdf->Cell(175,0.2, '',1, '','C');


$provee->movenext();
//$pdf->AddPage();
}

$pdf->Output();
?>
