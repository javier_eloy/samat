<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title><?=$page->title?></title>
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
        <meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Pragma-directive" content="no-cache">
	<meta http-equiv="Cache-Directive" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<style type="text/css" media="screen">@import url("css/estilos.css");</style>
	<link rel="stylesheet" type="text/css" href="css/default.css">
	<link rel="stylesheet" type="text/css" href="css/alert.css">
        <link rel="stylesheet" type="text/css" href="css/alphacube.css">
        <link rel="stylesheet" type="text/css" href="css/default.css">
        <link rel="stylesheet" type="text/css" href="css/lighting.css">
	
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js" type="text/javascript"></script>
	<script src="js/window.js" type="text/javascript"></script>
	<script src="js/application.js" type="text/javascript"></script>
	<!-- Loading Calendar JavaScript files -->
	<script src="js/calendar/src/utils.js" type="text/javascript"></script>
	<script src="js/calendar/src/calendar.js" type="text/javascript"></script>
	<script src="js/calendar/src/calendar-setup.js" type="text/javascript"></script>
	<!-- Loading language definition file -->
	<script src="js/calendar/lang/calendar-sp.js" type="text/javascript"></script>
	<script src="js/script_st.js" type="text/javascript"></script>
	<!--280606.CEPV.SN Librerias que necesito -->
	<script src="js/json.js" type="text/javascript"></script>

        <?php
            $form = basename($_SERVER['SCRIPT_FILENAME']);
            if($form != 'documentos_ing.php' and $form != 'arqueo_caja.php' and $form != 'NominaIndividual.php'){
        ?>
        <link rel="STYLESHEET" type="text/css" href="js/tabs/css/dhtmlXTabBar.css">
	<link rel="STYLESHEET" type="text/css" href="js/grid/css/calendar.css">
        <link rel="STYLESHEET" type="text/css" href="js/grid/css/dhtmlxgrid.css">
	<script src="js/grid/js/dhtmlXCommon.js" type="text/javascript"></script>
	<script src="js/grid/js/dhtmlXGrid.js" type="text/javascript"></script>	
	<script src="js/grid/js/dhtmlXGridCell.js" type="text/javascript"></script>
	<script src="js/grid/js/dhtmlXGrid_srnd.js" type="text/javascript"></script>
	<script src="js/grid/js/dhtmlXGrid_excell_calendar.js" type="text/javascript"></script>
	<script src="js/tabs/js/dhtmlXCommon.js" type="text/javascript"></script>
	<script src="js/tabs/js/dhtmlXTabbar.js" type="text/javascript"></script> 
	<script src="js/tabs/js/dhtmlXTabBar_start.js" type="text/javascript"></script>
        
        <?} else {?>
        
        
        <link rel="STYLESHEET" type="text/css" href="dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css">
        <link rel="stylesheet" type="text/css" href="dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css">
        <link rel="STYLESHEET" type="text/css" href="dhtmlx/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
        <script>_css_prefix="dhtmlx/dhtmlxGrid/codebase/"; _js_prefix="dhtmlx/dhtmlxGrid/codebase/"; </script>
        <script src="dhtmlx/dhtmlxGrid/codebase/dhtmlXCommon.js" type="text/javascript"></script>
	<script src="dhtmlx/dhtmlxGrid/codebase/dhtmlXGrid.js" type="text/javascript"></script>
	<script src="dhtmlx/dhtmlxGrid/codebase/dhtmlXGridCell.js" type="text/javascript"></script>
	<script src="dhtmlx/dhtmlxGrid/codebase/ext/dhtmlXGrid_srnd.js" type="text/javascript"></script>
        <script  src="dhtmlx/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>  
	<script src="dhtmlx/dhtmlxGrid/codebase/excells/dhtmlXGrid_excell_calendar.js" type="text/javascript"></script>
        <link rel="STYLESHEET" type="text/css" href="dhtmlx/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_yahoolike.css">
	<script src="dhtmlx/dhtmlxTabbar/dhtmlXCommon.js" type="text/javascript"></script>
	<script src="dhtmlx/dhtmlxTabbar/dhtmlXTabbar.js" type="text/javascript"></script>
	<script src="dhtmlx/dhtmlxTabbar/dhtmlXTabBar_start.js" type="text/javascript"></script>
        <?}?>
	
	<!--280606.CEPV.EN Libreria que necesito -->
</head>
<body >
<div id="controlMenu"> <img id="controlMenuImg" src="images/minimize_gray.gif" onclick="javascript:top.toggle();return false;"  title="Oculta/Muestra el Menu" /></div>
<div id="contenedor" >       
    <div id="cabecera"> </div>
        <div id="cuerpo">
	
