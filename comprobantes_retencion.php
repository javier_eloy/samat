<?
include("Constantes.php");



function encabezado ($titulo, $pdf, $fecDoc,$mostrar_fecha=true) {
		$pdf->SetLeftMargin(15);
                $pdf->SetRightMargin(10);
		$pdf->SetFont('Courier','b',12);
		$pdf->Ln(1);
		$pdf->Image ("images/logo-unico.jpg",15,4,55.6,25.3);//logo a la izquierda
		$pdf->Ln(12);
		//$pdf->SetXY(55, 7); 
		/*$textoCabecera = PAIS."\n\n";
		$textoCabecera.= ENTE."\n\n";
		$textoCabecera.= UBICACION."\n\n";
		$textoCabecera.= DEPARTAMENTO."\n\n";
		$pdf->MultiCell(150,2, $textoCabecera, 0, 'L');*/
		$pdf->SetXY(210, 10);
		//$pdf->Cell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');
		if($mostrar_fecha)
		  $pdf->Cell(50,2, "Fecha: ".$fecDoc, 0, 'L');
		$pdf->Ln(20);
		//$pdf->SetFont('Courier','',14);
		$pdf->Cell(0, 0, $titulo.$detalleTitulo,0,0,'C');
		$pdf->Ln(5);
}

function piepagina($pdf) {
		$pdf->SetFont('Courier','',12);
		$pdf->SetY(165);
                $pdf->SetX(20);
		$pdf->Cell(70,5,"Tesorero",1,0,'C');
		$pdf->SetX(195);
		$pdf->Cell(70,5,"Contribuyente",1,0,'C');
		$pdf->Ln(5);
                $pdf->SetX(20);
		$pdf->Cell(70,22,"",1,0,'C');
		$pdf->SetX(195);
		$pdf->Cell(70,22,"",1,0,'C');
}



function retencion_iva($conn,$id,$escEnEje,$pdf){
	
	$q = "SELECT f.*,ra.porcret FROM finanzas.facturas f ";
	$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.id_retencion = ra.id) ";
	$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '1' AND COALESCE(f.id_proveedor,0) <= 0 ";
	//die($q);
	$r = $conn->Execute($q); //or die($q);
	
	if($r->RecordCount()<1 )
		return;
	elseif($r->fields['iva_retenido'] == 0){
            return;
        }else{

		$pdf->AddPage('L');
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
		encabezado('COMPROBANTE DE RETENCION DE IVA',$pdf,muestrafecha($oOrdenPago->fecha),false);
		$pdf->Ln(2);
		$pdf->SetFont('Courier','',8);
		$pdf->Cell(80,6,'',0,'','C');
		$pdf->Cell(60,6,'Nro. DE COMPROBANTE','LRT','','C');
		$pdf->Cell(55,6,'',0,'','C');
		$pdf->Cell(60,6,'FECHA','LRT','','C');
		$pdf->Ln();
		$pdf->Cell(80,6,'',0,'','C');
		$sql = "SELECT nrocorret FROM finanzas.facturas A ";
		$sql.= "INNER JOIN finanzas.retenciones_adiciones B ON (A.id_retencion = B.id) ";
		$sql.= "WHERE A.nrodoc = '$id' AND B.es_iva = '1'";
		//die($sql);
		$row = $conn->Execute($sql);
		$pdf->Cell(60,6,$row->fields['nrocorret'],'LRBT','','C');
		$pdf->Cell(55,6,'',0,'','C');
		$pdf->Cell(60,6,muestrafecha($oOrdenPago->fecha),'LRBT','','C');
		$pdf->Ln();
		
		/*$pdf->Cell(40,6,'',0,'','C');
		$pdf->Cell(90,6,'COMPROBANTE DE RETENCION DE I.V.A.','LRBT','','C');*/
		$pdf->Ln();
		
		$pdf->Cell(100,6, 'NOMBRE O RAZ'.utf8_decode('Ó').'N SOCIAL DEL AGENTE DE RETENCI'.utf8_decode('Ó').'N:','LRTB', '','C' );
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,'R.I.F. DEL CONTRIBUYENTE','LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(40,6,'PERIODO FISCAL','LRBT','','C');
		$pdf->Ln();
		
		$auxfec = explode('-',$oOrdenPago->fecha);
		$ano = $auxfec[0];
		$mes = $auxfec[1];
		$pdf->Cell(100,6, ORGANISMO_NOMBRE,'LRTB', '','C' );
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,RIF,'LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(40,6,'A'.utf8_decode('Ñ').'O '.$ano.' / MES '.$mes,'LRBT','','C');
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->Cell(150,6, 'DIRECCI'.utf8_decode('Ó').'N FISCAL DEL AGENTE DE RETENCI'.utf8_decode('Ó').'N','LRTB', '','C' );
		$pdf->Ln();
		$pdf->Cell(150,6, DIRECCIONFISCAL,'LRTB', '','C' );
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->Cell(100,6,'NOMBRE O RAZ'.utf8_decode('Ó').'N SOCIAL DEL SUJETO RETENIDO','LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,'R.I.F. DEL SUJETO RETENIDO','LRBT','','C');
		$pdf->Ln();
		
		$pdf->Cell(100,6, $oOrdenPago->proveedor,'LRB', '','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6, $oOrdenPago->rif_proveedor,'LRB', '','C');
		
		$pdf->Ln();
		$pdf->Cell(188,6,'',0,'','C');
		$pdf->Cell(67,6,'COMPRAS INTERNAS O IMPORTACIONES','LRT','','C');
		
		$pdf->Ln(6);
		$pdf->Cell(255,0.1, '',1, '','C');
		
		$pdf->Ln();
	
				
		$pdf->SetFont('Courier','B',6);
		$cfact = 1;
		$pdf->Cell(5,5,'','LRT','','C');
		$pdf->Cell(15,5,'FECHA DE','LRT','','C');
		$pdf->Cell(18,5,'N'.utf8_decode('Ú').'MERO DE','LRT','','C');
		$pdf->Cell(20,5,'N'.utf8_decode('Ú').'MERO DE','LRT','','C');
		$pdf->Cell(15,5,'N'.utf8_decode('Ú').'MERO NOTA','LRT','','C');
		$pdf->Cell(15,5,'N'.utf8_decode('Ú').'MERO NOTA','LRT','','C');
		$pdf->Cell(19,5,'TIPO DE','LRT','','C');
		$pdf->Cell(21,5,utf8_decode('Nº FACTURA'),'LRT','','C');
		$pdf->Cell(25,5,'TOTAL COMPRAS','LRT','','C');
		$pdf->Cell(25,5,'COMPRAS SIN DERECHO','LRT','','C');
		$pdf->Cell(25,5,'BASE','LRT','','C');
		$pdf->Cell(12,5,'%','LRT','','C');
		$pdf->Cell(15,5,'IMPUESTO','LRT','','C');
		$pdf->Cell(10,5,'%','LRT','','C');
		$pdf->Cell(15,5,'IVA','LRT','','C');
		$pdf->Ln();
		$pdf->Cell(5,5,'','LRB','','C');
		$pdf->Cell(15,5,'FACTURA','LRB','','C');
		$pdf->Cell(18,5,'FACTURA','LRB','','C');
		$pdf->Cell(20,5,'CONTROL','LRB','','C');
		$pdf->Cell(15,5,'DE D'.utf8_decode('É').'BITO','LRB','','C');
		$pdf->Cell(15,5,'DE CR'.utf8_decode('É').'DITO','LRB','','C');
		$pdf->Cell(19,5,'TRANSACCI'.utf8_decode('Ó').'N','LRB','','C');
		$pdf->Cell(21,5,utf8_decode('AFECTADA'),'LRB','','C');
		$pdf->Cell(25,5,'INCLUYENO EL IVA','LRB','','C');
		$pdf->Cell(25,5,'A CR'.utf8_decode('É').'DITO IVA','LRB','','C');
		$pdf->Cell(25,5,'IMPONIBLE','LRB','','C');
		$pdf->Cell(12,5,'ALICUOTA','LRB','','C');
		$pdf->Cell(15,5,'IVA','LRB','','C');
		$pdf->Cell(10,5,'RETENIDO','LRT','','C');
		$pdf->Cell(15,5,'RETENIDO','LRB','','C');
		$pdf->Ln();
		
		while(!$r->EOF){
			$pdf->SetFont('Courier','',6);
			$pdf->Cell(5,6,$cfact,1,'LRB','C');
			$pdf->Cell(15,6,muestraFecha($r->fields['fecha']),1,'LRB','C');
			$pdf->Cell(18,6,$r->fields['nrofactura'],1,'LRB','C');
			$pdf->Cell(20,6,$r->fields['nrocontrol'],1,'LRB','C');
			$pdf->Cell(15,6,'',1,'LRB','C');
			$pdf->Cell(15,6,'',1,'LRB','C');
			$pdf->Cell(19,6,'01-REG',1,'LRB','C');
			$pdf->Cell(21,6,'','LRB','','C');
                        //Total Compras Incluyendo IVA
			$pdf->Cell(25,6,muestraFloat($r->fields['monto']),1,'LRB','C');
                        //Compras sin derecho a Credito IVA
			$pdf->Cell(25,6,muestraFloat($r->fields['monto_excento']),1,'LRB','C');
                        //Base Imponible
			$pdf->Cell(25,6,muestraFloat($r->fields['base_imponible']),1,'LRB','C');
			$pdf->Cell(12,6,muestraFloat($r->fields['iva']),1,'LRB','C');
			$pdf->Cell(15,6,muestraFloat($r->fields['monto_iva']),1,'LRB','C');
			$pdf->Cell(10,6,muestraFloat($r->fields['porcret']),1,'LRB','C');
			$pdf->Cell(15,6,muestraFloat($r->fields['iva_retenido']),1,'LRB','C');
			$total_fact+= $r->fields['monto'];
			$total_bi+= $r->fields['base_imponible'];
			$total_iva+= $r->fields['monto_iva'];
			$total_ret+= $r->fields['iva_retenido'];
			$cfact++;
			$pdf->Ln();
			$r->movenext();
		}
		
		$pdf->Ln();
		$pdf->Cell(122,4, '',0,'','C');
		$pdf->Cell(25,4, muestraFloat($total_fact),0,'','R');
		$pdf->Cell(20,4, '',0,'','C');
		$pdf->Cell(30,4, muestraFloat($total_bi),0,'','R');
		$pdf->Cell(18,4, '',0,'','C');
		$pdf->Cell(15,4, muestraFloat($total_iva),0,'','R');
		$pdf->Cell(25,4, muestraFloat($total_ret),0,'','R');
		
		piepagina($pdf);
	}

}

function retencion_iva_tercero($conn,$id,$escEnEje,$pdf){

	$q = "SELECT DISTINCT f.id_proveedor FROM finanzas.facturas f ";
	$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.id_retencion = ra.id) ";
        $q.= "INNER JOIN puser.proveedores p ON (p.id = f.id_proveedor) ";
	$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '1' AND f.iva_retenido > 0 ";
  //die($q);
	$r = $conn->Execute($q); //or die($q);

	if($r->RecordCount()<1 ) {
		return;
        }else{
             while (!$r->EOF) {
		$pdf->AddPage('L');
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
                $oProveedor = new proveedores();
                $oProveedor->get($conn,$r->fields['id_proveedor']);
                
		encabezado('COMPROBANTE DE RETENCION DE IVA A TERCEROS',$pdf,muestrafecha($oOrdenPago->fecha),false);
		$pdf->Ln(2);
		$pdf->SetFont('Courier','',8);
		$pdf->Cell(80,6,'',0,'','C');
		$pdf->Cell(60,6,'Nro. DE COMPROBANTE','LRT','','C');
		$pdf->Cell(55,6,'',0,'','C');
		$pdf->Cell(60,6,'FECHA','LRT','','C');
		$pdf->Ln();
		$pdf->Cell(80,6,'',0,'','C');
		$sql = "SELECT nrocorret FROM finanzas.facturas A ";
		$sql.= "INNER JOIN finanzas.retenciones_adiciones B ON (A.id_retencion = B.id) ";
		$sql.= "WHERE A.nrodoc = '$id' AND B.es_iva = '1'";
		//die($sql);
		$row = $conn->Execute($sql);
		$pdf->Cell(60,6,$row->fields['nrocorret'],'LRBT','','C');
		$pdf->Cell(55,6,'',0,'','C');
		$pdf->Cell(60,6,muestrafecha($oOrdenPago->fecha),'LRBT','','C');
		$pdf->Ln();

		/*$pdf->Cell(40,6,'',0,'','C');
		$pdf->Cell(90,6,'COMPROBANTE DE RETENCION DE I.V.A.','LRBT','','C');*/
		$pdf->Ln();

		$pdf->Cell(100,6, 'NOMBRE O RAZ'.utf8_decode('Ó').'N SOCIAL DEL AGENTE DE RETENCI'.utf8_decode('Ó').'N:','LRTB', '','C' );
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,'R.I.F. DEL CONTRIBUYENTE','LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(40,6,'PERIODO FISCAL','LRBT','','C');
		$pdf->Ln();

		$auxfec = explode('-',$oOrdenPago->fecha);
		$ano = $auxfec[0];
		$mes = $auxfec[1];
		$pdf->Cell(100,6, ORGANISMO_NOMBRE,'LRTB', '','C' );
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,RIF,'LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(40,6,'A'.utf8_decode('Ñ').'O '.$ano.' / MES '.$mes,'LRBT','','C');
		$pdf->Ln();
		$pdf->Ln();

		$pdf->Cell(150,6, 'DIRECCI'.utf8_decode('Ó').'N FISCAL DEL AGENTE DE RETENCI'.utf8_decode('Ó').'N','LRTB', '','C' );
		$pdf->Ln();
		$pdf->Cell(150,6, DIRECCIONFISCAL,'LRTB', '','C' );
		$pdf->Ln();
		$pdf->Ln();

		$pdf->Cell(100,6,'NOMBRE O RAZ'.utf8_decode('Ó').'N SOCIAL DEL SUJETO RETENIDO','LRBT','','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6,'R.I.F. DEL SUJETO RETENIDO','LRBT','','C');
		$pdf->Ln();

		$pdf->Cell(100,6, $oOrdenPago->proveedor,'LRB', '','C');
		$pdf->Cell(10,6,'',0,'','C');
		$pdf->Cell(90,6, $oOrdenPago->rif_proveedor,'LRB', '','C');

		$pdf->Ln();
		$pdf->Cell(188,6,'',0,'','C');
		$pdf->Cell(67,6,'COMPRAS INTERNAS O IMPORTACIONES','LRT','','C');

		$pdf->Ln(6);
		$pdf->Cell(255,0.1, '',1, '','C');

		$pdf->Ln();


		$pdf->SetFont('Courier','B',6);
		$cfact = 1;
		$pdf->Cell(5,5,'','LRT','','C');
		$pdf->Cell(15,5,'FECHA DE','LRT','','C');
		$pdf->Cell(18,5,'N'.utf8_decode('Ú').'MERO DE','LRT','','C');
		$pdf->Cell(20,5,'N'.utf8_decode('Ú').'MERO DE','LRT','','C');
		$pdf->Cell(15,5,'N'.utf8_decode('Ú').'MERO NOTA','LRT','','C');
		$pdf->Cell(15,5,'N'.utf8_decode('Ú').'MERO NOTA','LRT','','C');
		$pdf->Cell(19,5,'TIPO DE','LRT','','C');
		$pdf->Cell(21,5,utf8_decode('Nº FACTURA'),'LRT','','C');
		$pdf->Cell(25,5,'TOTAL COMPRAS','LRT','','C');
		$pdf->Cell(25,5,'COMPRAS SIN DERECHO','LRT','','C');
		$pdf->Cell(25,5,'BASE','LRT','','C');
		$pdf->Cell(12,5,'%','LRT','','C');
		$pdf->Cell(15,5,'IMPUESTO','LRT','','C');
		$pdf->Cell(10,5,'%','LRT','','C');
		$pdf->Cell(15,5,'IVA','LRT','','C');
		$pdf->Ln();
		$pdf->Cell(5,5,'','LRB','','C');
		$pdf->Cell(15,5,'FACTURA','LRB','','C');
		$pdf->Cell(18,5,'FACTURA','LRB','','C');
		$pdf->Cell(20,5,'CONTROL','LRB','','C');
		$pdf->Cell(15,5,'DE D'.utf8_decode('É').'BITO','LRB','','C');
		$pdf->Cell(15,5,'DE CR'.utf8_decode('É').'DITO','LRB','','C');
		$pdf->Cell(19,5,'TRANSACCI'.utf8_decode('Ó').'N','LRB','','C');
		$pdf->Cell(21,5,utf8_decode('AFECTADA'),'LRB','','C');
		$pdf->Cell(25,5,'INCLUYENO EL IVA','LRB','','C');
		$pdf->Cell(25,5,'A CR'.utf8_decode('É').'DITO IVA','LRB','','C');
		$pdf->Cell(25,5,'IMPONIBLE','LRB','','C');
		$pdf->Cell(12,5,'ALICUOTA','LRB','','C');
		$pdf->Cell(15,5,'IVA','LRB','','C');
		$pdf->Cell(10,5,'RETENIDO','LRT','','C');
		$pdf->Cell(15,5,'RETENIDO','LRB','','C');
		$pdf->Ln();

                $q2 = "SELECT f.*,ra.porcret  FROM finanzas.facturas f ";
                $q2.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.id_retencion = ra.id) ";
                $q2.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '1' AND f.id_proveedor=".$r->fields['id_proveedor'];                
                $f = $conn->Execute($q2);
                
		while(!$f->EOF){
			$pdf->SetFont('Courier','',6);
			$pdf->Cell(5,6,$cfact,1,'LRB','C');
			$pdf->Cell(15,6,muestraFecha($f->fields['fecha']),1,'LRB','C');
			$pdf->Cell(18,6,$f->fields['nrofactura'],1,'LRB','C');
			$pdf->Cell(20,6,$f->fields['nrocontrol'],1,'LRB','C');
			$pdf->Cell(15,6,'',1,'LRB','C');
			$pdf->Cell(15,6,'',1,'LRB','C');
			$pdf->Cell(19,6,'01-REG',1,'LRB','C');
			$pdf->Cell(21,6,'','LRB','','C');
                        //Total Compras Incluyendo IVA
			$pdf->Cell(25,6,muestraFloat($f->fields['monto']),1,'LRB','C');
                        //Compras sin derecho a Credito IVA
			$pdf->Cell(25,6,muestraFloat($f->fields['monto_excento']),1,'LRB','C');
                        //Base Imponible
			$pdf->Cell(25,6,muestraFloat($f->fields['base_imponible']),1,'LRB','C');
			$pdf->Cell(12,6,muestraFloat($f->fields['iva']),1,'LRB','C');
			$pdf->Cell(15,6,muestraFloat($f->fields['monto_iva']),1,'LRB','C');
			$pdf->Cell(10,6,muestraFloat($f->fields['porcret']),1,'LRB','C');
			$pdf->Cell(15,6,muestraFloat($f->fields['iva_retenido']),1,'LRB','C');
			$total_fact+= $f->fields['monto'];
			$total_bi+= $f->fields['base_imponible'];
			$total_iva+= $f->fields['monto_iva'];
			$total_ret+= $f->fields['iva_retenido'];
			$cfact++;
			$pdf->Ln();
			$f->movenext();
		}

		$pdf->Ln();
		$pdf->Cell(122,4, '',0,'','C');
		$pdf->Cell(25,4, muestraFloat($total_fact),0,'','R');
		$pdf->Cell(20,4, '',0,'','C');
		$pdf->Cell(30,4, muestraFloat($total_bi),0,'','R');
		$pdf->Cell(18,4, '',0,'','C');
		$pdf->Cell(15,4, muestraFloat($total_iva),0,'','R');
		$pdf->Cell(25,4, muestraFloat($total_ret),0,'','R');

		piepagina($pdf);
                $r->MoveNext();
       }
    }

}


function ret_mun_header($pdf,$row,$oOrdenPago){
        encabezado('COMPROBANTE DE RETENCI'.utf8_decode('Ó').'N DE '.$row->fields['descri'],$pdf, muestrafecha($oOrdenPago->fecha));
        $pdf->SetFont('Courier','',8);
        $pdf->SetLeftMargin(15);
        $pdf->SetFont('Courier','B',12);
        $pdf->Cell(0,0,'Nro.'.$row->fields['nrocorret'],0,0,'C');
        $pdf->Ln(10);
        $pdf->SetFont('Courier','B',12);
        $pdf->Cell(180,5,'Nombre o Raz'.utf8_decode('ó').'n Social',0,0,'L');
        $pdf->Cell(80,5,'RIF',0,0,'L');
        $pdf->Ln(5);
        $pdf->SetFont('Courier','',12);
        $pdf->Cell(180,5, $oOrdenPago->proveedor,0,0,'L');
        $pdf->Cell(80,5,$oOrdenPago->rif_proveedor,0,0,'L');
        $pdf->Ln(5);
        $pdf->SetFont('Courier','B',12);
        $pdf->Cell(200,5,'Direcci'.utf8_decode('ó').'n',0,0,'L');
        $pdf->Ln(5);
        $pdf->SetFont('Courier','',12);
        $pdf->Cell(200,5,$oOrdenPago->dir_proveedor,0,0,'L');
        $pdf->Ln(9);
        $pdf->SetFont('Courier','B',10);
        $pdf->SetDrawColor(255,255,255);
        $pdf->SetWidths(array(25,25,25,30,30,35,25,20,30));
        $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C'));
        $pdf->Row(array("Factura","Control","Fecha Fact.","Facturado","Exento","Base Imponible","Impuestos","% de Ret.","Imp. Retenido"),11);

        //$pdf->Ln();
}


function retencion_municipal($conn, $id, $escEnEje, $pdf) {
	
//	$sql = "SELECT nrocorret,B.descri FROM finanzas.relacion_retenciones_orden A ";
//	$sql.= "INNER JOIN finanzas.retenciones_adiciones B ON (A.codret = B.id) ";
//	$sql.= "WHERE A.nrodoc = '$id' AND B.es_iva = '3' ORDER BY b.comind";
//        
        $sql = "SELECT A.nrocorret,B.descri
                FROM finanzas.relacion_retenciones_orden A
                INNER JOIN finanzas.retenciones_adiciones B ON (A.codret = B.id)
                INNER JOIN  finanzas.facturas C ON A.nrofactura = C.nrofactura
                WHERE A.nrodoc = '$id' AND B.es_iva = '3' AND (C.id_proveedor = 0 or C.id_proveedor is null)
                ORDER BY b.comind";
	//die($sql);
	$row = $conn->Execute($sql);
	if ($row->RecordCount() < 1)
		return;
	else {
                
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
		$pdf->AddPage('L');
		ret_mun_header($pdf,$row,$oOrdenPago);
		$q = "SELECT f.*, fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura,ra.comind, ra.descri FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '3' AND (fa.id_proveedor = 0 or fa.id_proveedor is null)  ORDER BY ra.comind";
		//die($q);
		$r = $conn->Execute($q); //or die($q);
		$pdf->SetAligns(array('C','C','C','R','R','R','R','R','R'));

		while(!$r->EOF)
		{
                        if($r->fields['comind'] == 1 and $r->recordcount>1){ //La segunda comparacion es para identificar si hay mas retenciones municipales
                            $pdf->SetDrawColor (0,0,0);
                            $pdf->SetXY(15,70);
                            $pdf->Cell(250,8,"",1,0,'C');
                            $pdf->Ln(8);
                            $pdf->Cell(250,85,"",1,0,'C');
                            piepagina($pdf);
                            $pdf->AddPage('L');
                            ret_mun_header($pdf,$r,$oOrdenPago);
                        }
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nrofactura'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->movenext();
		}
		
		$pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(15,70);
		$pdf->Cell(250,8,"",1,0,'C');
		$pdf->Ln(8);
		$pdf->Cell(250,85,"",1,0,'C');
		
		
		//$pdf->Ln(95);
		piepagina($pdf);
	}
}

function retencion_municipal_tercero($conn, $id, $escEnEje, $pdf) {

	$q = "SELECT DISTINCT f.id_proveedor, f.nrocorret, ra.descri FROM finanzas.facturas f ";
	$q.= "INNER JOIN finanzas.relacion_retenciones_orden ro ON (ro.nrodoc = f.nrodoc) AND (ro.nrofactura = f.id_proveedor ||'-'|| f.nrofactura) ";
	$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (ra.id = ro.codret)  ";
        $q.= "INNER JOIN puser.proveedores p ON (p.id = f.id_proveedor) ";
	$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '3' AND f.iva_retenido > 0 ";
        //die($q);
	$rp = $conn->Execute($q); 

	if ($rp->RecordCount() < 1)
		return;
	else {
	while (!$rp->EOF) {
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
                $oProveedor = new proveedores();
                $oProveedor->get($conn,$rp->fields['id_proveedor']);
		
		$oOrdenPago->proveedor =  $oProveedor->nombre;
		$oOrdenPago->rif_proveedor= $oProveedor->rif;
		$oOrdenPago->dir_proveedor = $oProveedor->direccion;
		
		$pdf->AddPage('L');
		ret_mun_header($pdf,$rp,$oOrdenPago);
		$q = "SELECT f.*, fa.nrofactura as nfac,fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura,ra.comind, ra.descri FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.id_proveedor ||'-'|| fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '3' ORDER BY ra.comind";
                
		$r = $conn->Execute($q); 
		$pdf->SetAligns(array('C','C','C','R','R','R','R','R','R'));

		while(!$r->EOF)
		{
                        if($r->fields['comind'] == 1 and $r->RecordCount() >1){
                            $pdf->SetDrawColor (0,0,0);
                            $pdf->SetXY(18,60);
                            $pdf->Cell(250,8,"",1,0,'C');
                            $pdf->Ln(5);
                            $pdf->Cell(250,85,"",1,0,'C');
                            piepagina($pdf);
                            $pdf->AddPage('L');
                            ret_mun_header($pdf,$r,$oOrdenPago);

//                            $pdf->SetDrawColor (0,0,0);
//                            $pdf->SetXY(18,67);
//                            $pdf->Cell(330,5,"",1,0,'C');
//                            $pdf->Ln(5);
//                            $pdf->Cell(330,90,"",1,0,'C');
//                            piepagina($pdf);
//                            $pdf->AddPage('L');
//                            ret_mun_header($pdf,$r,$oOrdenPago);
                        }
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nfac'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->MoveNext();
		}

                $pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(18,70);
		$pdf->Cell(250,8,"",1,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(250,85,"",1,0,'C');

//		$pdf->SetDrawColor (0,0,0);
//		$pdf->SetXY(18,67);
//		$pdf->Cell(330,5,"",1,0,'C');
//		$pdf->Ln(5);
//		$pdf->Cell(330,90,"",1,0,'C');
		
		//$pdf->Ln(95);
		piepagina($pdf);
		$rp->MoveNext();
	}
	}
}

  function retencion_islr($conn, $id, $escEnEje, $pdf) {
	
	$sql = "SELECT A.nrocorret FROM finanzas.relacion_retenciones_orden A ";
	$sql.= "INNER JOIN finanzas.retenciones_adiciones B ON (A.codret = B.id) ";
        $sql.= "INNER JOIN finanzas.facturas F ON (F.nrofactura = A.nrofactura) ";
	$sql.= "WHERE A.nrodoc = '$id' AND B.es_iva = '2' ";
	//die($sql);
	$row = $conn->Execute($sql);
	if ($row->RecordCount() < 1)
		return;
	else {	
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
		$pdf->AddPage('L');
		encabezado('COMPROBANTE DE RETENCI'.utf8_decode('Ó').'N DE IMPUESTO SOBRE LA RENTA',$pdf, muestrafecha($oOrdenPago->fecha));
		$pdf->SetFont('Courier','',8);
		$pdf->SetLeftMargin(18);
		$pdf->SetFont('Courier','B',14);
		$pdf->Cell(0,0,'Nro.'.$row->fields['nrocorret'],0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Courier','B',12);
		$pdf->Cell(180,5,'Nombre o Raz'.utf8_decode('ó').'n Social',0,0,'L');
		$pdf->Cell(80,5,'RIF',0,0,'L');
		$pdf->Ln(5);
		$pdf->SetFont('Courier','',12);
		$pdf->Cell(180,5,	$oOrdenPago->proveedor,0,0,'L');
		$pdf->Cell(80,5,$oOrdenPago->rif_proveedor,0,0,'L');
		$pdf->Ln(5);
                $pdf->SetFont('Courier','B',12);
		$pdf->Cell(200,5,'Direcci'.utf8_decode('ó').'n',0,0,'L');
		$pdf->Ln(5);
                $pdf->SetFont('Courier','',12);
		$pdf->Cell(200,5,$oOrdenPago->dir_proveedor,0,0,'L');
		$pdf->Ln(9);
		$pdf->SetFont('Courier','B',10);
		$pdf->SetDrawColor (255,255,255);
		$pdf->SetWidths(array(25,30,25,25,30,35,25,20,30));
		$pdf->SetAligns(array('C','C','C','R','R','R','R','R','R'));
		$pdf->Row(array("Factura","Control","Fecha Fact.","Facturado","Exento","Base Imponible","Impuestos","% Ret.","Imp. Retenido"),11);
		
		
		$q = "SELECT f.*, fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '2' and (fa.id_proveedor = 0 or fa.id_proveedor is null)";
		//die($q);
		$r = $conn->Execute($q); //or die($q);
		
		while(!$r->EOF)
		{
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nrofactura'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->movenext();
		}
		$pdf->Ln();
		$pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(18,70);
		$pdf->Cell(250,8,"",1,0,'C');
		$pdf->Ln(8);
		$pdf->Cell(250,85,"",1,0,'C');
		
		
		//$pdf->Ln(95);
		piepagina($pdf);
	}
}

function retencion_islr_tercero($conn, $id, $escEnEje, $pdf) {
	
	$q = "SELECT DISTINCT f.id_proveedor, f.nrocorret FROM finanzas.facturas f ";
	$q.= "INNER JOIN finanzas.relacion_retenciones_orden ro ON (ro.nrodoc = f.nrodoc) AND (ro.nrofactura = f.id_proveedor ||'-'|| f.nrofactura) ";
	$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (ra.id = ro.codret)  ";
        $q.= "INNER JOIN puser.proveedores p ON (p.id = f.id_proveedor) ";
	$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '2' AND f.iva_retenido > 0 ";
	$rp = $conn->Execute($q); 
	
	if ($rp->RecordCount() < 1)
		return;
	else {	
	
	while (!$rp->EOF) {

		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
                $oProveedor = new proveedores();
                $oProveedor->get($conn,$rp->fields['id_proveedor']);

		$pdf->AddPage('L');
		encabezado('COMPROBANTE DE RETENCION DE IMPUESTO SOBRE LA RENTA',$pdf, muestrafecha($oOrdenPago->fecha));
		$pdf->SetFont('Courier','',8);
		$pdf->SetLeftMargin(18);
		$pdf->SetFont('Courier','B',14);
		$pdf->Cell(0,0,'Nro.'.$rp->fields['nrocorret'],0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Courier','',12);
		$pdf->Cell(250,5,'Nombre o Razon Social',0,0,'L');
		$pdf->Cell(80,5,'RIF',0,0,'L');
		$pdf->Ln(5);
		$pdf->SetFont('Courier','',12);
		$pdf->Cell(250,5,	$oProveedor->nombre,0,0,'L');
		$pdf->Cell(80,5,$oProveedor->rif,0,0,'L');
		$pdf->Ln(5);
		$pdf->Cell(330,5,'Direccion',0,0,'L');
		$pdf->Ln(5);
		$pdf->Cell(330,5,utf8_decode($oProveedor->direccion),0,0,'L');
		$pdf->Ln(10);
		$pdf->SetFont('Courier','B',10);
		$pdf->SetDrawColor (255,255,255);
		$pdf->SetWidths(array(35,35,35,35,35,35,35,35,50));			
		$pdf->SetAligns(array('C','C','C','C','C','C','C','C','C'));
		$pdf->Row(array("Factura","Control","Fecha Fact.","Facturado","Exento","Base Imponible","Impuestos","% de Retencion","Impuesto Retenido"),11);
		
		
		$q = "SELECT f.*, fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.id_proveedor ||'-'|| fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '2' ";
		//die($q);
		$r = $conn->Execute($q); //or die($q);
		
		while(!$r->EOF)
		{
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nrofactura'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->movenext();
		}

		$pdf->Ln();
		$pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(18,50);
		$pdf->Cell(250,5,"",1,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(250,90,"",1,0,'C');
		
		//$pdf->Ln(95);
		piepagina($pdf);		
		$rp->MoveNext();
		
	}
	}
}

function ret_nac_header($pdf,$row,$oOrdenPago){
    encabezado('COMPROBANTE DE RETENCI'.utf8_decode('Ó').'N DE '.$row->fields['descri'],$pdf, muestrafecha($oOrdenPago->fecha));
    $pdf->SetFont('Courier','',8);
    $pdf->SetLeftMargin(18);
    $pdf->SetFont('Courier','B',14);
    $pdf->Cell(0,0,'Nro.'.$row->fields['nrocorret'],0,0,'C');
    $pdf->Ln(10);
    $pdf->SetFont('Courier','B',12);
    $pdf->Cell(180,5,'Nombre o Raz'.utf8_decode('ó').'n Social',0,0,'L');
    $pdf->Cell(80,5,'RIF',0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Courier','',12);
    $pdf->Cell(180,5,	$oOrdenPago->proveedor,0,0,'L');
    $pdf->Cell(80,5,$oOrdenPago->rif_proveedor,0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Courier','B',12);
    $pdf->Cell(200,5,'Direcci'.utf8_decode('ó').'n',0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Courier','',12);
    $pdf->Cell(200,5,$oOrdenPago->dir_proveedor,0,0,'L');
    $pdf->Ln(9);
    $pdf->SetFont('Courier','B',10);
    $pdf->SetDrawColor (255,255,255);
    $pdf->SetWidths(array(25,25,25,30,30,35,25,20,30));
    $pdf->SetAligns(array('C','C','C','R','R','R','R','R','R'));
    $pdf->Row(array("Factura","Control","Fecha Fact.","Facturado","Exento","Base Imponible","Impuestos","% de Ret.","Imp. Retenido"),11);

    //$pdf->Ln();
}


function retencion_nacional($conn, $id, $escEnEje, $pdf) {
	
	$sql = "SELECT nrocorret,B.descri FROM finanzas.relacion_retenciones_orden A ";
	$sql.= "INNER JOIN finanzas.retenciones_adiciones B ON (A.codret = B.id) ";
	$sql.= "WHERE A.nrodoc = '$id' AND B.es_iva = '4'";
	//die($sql);
	$row = $conn->Execute($sql);
	if ($row->RecordCount() < 1)
		return;
	else {	
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
		$pdf->AddPage('L');
                ret_nac_header($pdf,$row,$oOrdenPago);
		
		$q = "SELECT f.*, fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura, ra.comind, ra.descri FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '4' ORDER BY ra.comind";
		//die($q);
		$r = $conn->Execute($q); //or die($q);
		
		while(!$r->EOF)
		{
                        if($r->fields['comind'] == 1){
                            $pdf->SetDrawColor (0,0,0);
                            $pdf->SetXY(18,60);
                            $pdf->Cell(250,8,"",1,0,'C');
                            $pdf->Ln(8);
                            $pdf->Cell(250,85,"",1,0,'C');
                            piepagina($pdf);
                            $pdf->AddPage('L');
                            ret_mun_header($pdf,$r,$oOrdenPago);
                        }
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nrofactura'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->movenext();
		}
		
		$pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(18,70);
		$pdf->Cell(250,8,"",1,0,'C');
		$pdf->Ln(8);
		$pdf->Cell(250,85,"",1,0,'C');
		
		
		//$pdf->Ln(95);
		piepagina($pdf);
	}
}

function retencion_nacional_tercero($conn, $id, $escEnEje, $pdf) {
	
	$q = "SELECT DISTINCT f.id_proveedor, f.nrocorret, ra.descri FROM finanzas.facturas f ";
	$q.= "INNER JOIN finanzas.relacion_retenciones_orden ro ON (ro.nrodoc = f.nrodoc) AND (ro.nrofactura = f.id_proveedor ||'-'|| f.nrofactura) ";
	$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (ra.id = ro.codret)  ";
        $q.= "INNER JOIN puser.proveedores p ON (p.id = f.id_proveedor) ";
	$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '4' AND f.iva_retenido > 0 ";
	$rp = $conn->Execute($q); 

	if ($rp->RecordCount() < 1)
		return;
	else {	
	
	while(!$rp->EOF) {
         	
		$oOrdenPago = new orden_pago();
		$oOrdenPago->get($conn, $id, $escEnEje);
                $oProveedor = new proveedores();
                $oProveedor->get($conn,$rp->fields['id_proveedor']);
		
		$oOrdenPago->proveedor =  $oProveedor->nombre;
		$oOrdenPago->rif_proveedor= $oProveedor->rif;
		$oOrdenPago->dir_proveedor = $oProveedor->direccion;
    		
		$pdf->AddPage('L');    
                ret_nac_header($pdf,$rp,$oOrdenPago);
		$q = "SELECT f.*, fa.nrocontrol, fa.fecha, fa.monto_excento, fa.monto AS mfactura, ra.comind, ra.descri FROM finanzas.relacion_retenciones_orden f ";
		$q.= "INNER JOIN finanzas.retenciones_adiciones ra ON (f.codret = ra.id) ";
		$q.= "INNER JOIN finanzas.facturas fa ON (f.nrofactura = fa.id_proveedor ||'-'|| fa.nrofactura AND f.nrodoc = fa.nrodoc) ";
		$q.= "WHERE f.nrodoc = '$id' AND ra.es_iva = '4' ORDER BY ra.comind";
		$r = $conn->Execute($q); 
		
		while(!$r->EOF)
		{
                        if($r->fields['comind'] == 1){
                            $pdf->SetDrawColor (0,0,0);
                            $pdf->SetXY(18,67);
                            $pdf->Cell(330,5,"",1,0,'C');
                            $pdf->Ln(5);
                            $pdf->Cell(330,90,"",1,0,'C');
                            piepagina($pdf);
                            $pdf->AddPage('L');
                            ret_mun_header($pdf,$r,$oOrdenPago);
                        }
			$pdf->SetFont('Courier','',10);
			$pdf->Row(array($r->fields['nrofactura'],$r->fields['nrocontrol'],muestraFecha($r->fields['fecha']),muestraFloat($r->fields['mfactura']),muestraFloat($r->fields['monto_excento']),muestraFloat($r->fields['mntbas']),muestraFloat($r->fields['monto_iva']),muestraFloat($r->fields['porcen']),muestraFloat($r->fields['mntret'])),11);
			$r->MoveNext();
		}
		
		$pdf->SetDrawColor (0,0,0);
		$pdf->SetXY(18,67);
		$pdf->Cell(330,5,"",1,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(330,90,"",1,0,'C');
	
		//$pdf->Ln(95);
		piepagina($pdf);
		$rp->MoveNext();
	  }
	}
}

?>
