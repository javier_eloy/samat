<? 
	require("comun/ini.php"); 
	require ("comun/header.php");
		
?>
<!--<script type="text/javascript" language="javascript">
	function carga_combo_fechas(){
		var url = 'json.php';
		var pars = 'op=cargaPeriodos&ms=' + new Date().getTime();
		var Request = new Ajax.Request(
			url,{
			method: 'get',
			parameters: pars,
			onLoading:function(request){}, 
			onComplete:function(request){
				
				var jsonData = eval('(' + request.responseText + ')');
					if (jsonData == undefined) { return }
					for(var j=0;j<jsonData.length;j++){
						var option = document.createElement('OPTION');
						option.value = jsonData[j]['id'];
						option.innerHTML = jsonData[j]['descripcion'];
						try 
						{$('lapso').add(option, null);}
						catch( e )
						{$('lapso').add(option);}
	
					}						
			}
		}
		);	
	}     	   
		
	carga_combo_fechas();
</script>-->
<span style="text-align:left" class="titulo_maestro">
	Generar Archivo TXT de retenciones de ISLR 
</span>
<center>
	<div align="center" id="formulario">
		<table>
			<tr>
				<td>Desde:
				</td>
				<td>
				</td>
				<td>Hasta:
				</td>
			</tr>
			<tr>
				<td>
					<input style="width:100px"  type="text" name="busca_fecha_desde" id="busca_fecha_desde"/>
				</td>
				<td>
					<div id="boton_busca_fecha_desde">
						<a href="#" onclick="return false;">
						<img border="0" alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
						</a>
					</div>
				</td>
				<td>
					<input style="width:100px" type="text" name="busca_fecha_hasta" id="busca_fecha_hasta"/>
				</td>
				<td>
					<div id="boton_busca_fecha_hasta">
					<a href="#" onclick="return false;">
						<img border="0"  alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
						</a>
					</div>
				</td>
			</tr>
			 <tr>
		  		<td colspan="4" align="center">
					<input name="boton" id="boton" type="button" onClick="imprimir()" value="Generar Reporte" />
				</td>
		  </tr>
		</table>				
	</div>
</center>
<br>
<br>
<br>
<div style="height:40px;padding-top:10px;">
	<p id="cargando" style="display:none;margin-top:0px;">
  		<img alt="Cargando" src="images/loading.gif" /> Cargando...
	</p>
</div>

<script type="text/javascript">
	new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_desde",
            button            : "boton_busca_fecha_desde",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });	
		
	new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_hasta",
            button            : "boton_busca_fecha_hasta",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });		
	var wxR;
	
	function imprimir()
	{
	
		var JsonAux;		
			if($('busca_fecha_desde').value=='' || $('busca_fecha_hasta').value==''){
				alert("Debe Seleccionar un Rango de Fechas Para Generar el Archivo");
			} 
			else{
				var url = 'json.php';
				var pars = 'op=generaTXTISLR&rangoIni=' + $('busca_fecha_desde').value +'&rangoFin=' + $('busca_fecha_hasta').value + '&ms='+ new Date().getTime();			
				var Request = new Ajax.Request(
				url,{
				method: 'get',
				parameters: pars,
				onLoading:function(request){}, 
				onComplete:function(request){
					var JsonRec = eval( '(' + request.responseText + ')');
					if(JsonRec){
						Popup= window.open("GenerarArchivosTxtA.php?archivo=ISLR.txt","winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
					}else{
						alert("No se pudo generar el archivo");
					}
				}
				});
			}
	}
  
  function desactivar(opcion)
  {
    if (opcion==1)
    {
      if ($F('id_cp') != '')
      {
        $('busca_cp').disabled = true;
      }
      else
      {
        $('busca_cp').disabled = false;
      }
    }
    else
    {
      if ($F('busca_cp') == 0)
      {
        $('id_cp').disabled = false;
      }
      else
      {
        $('id_cp').disabled = true;
      }
    }
  }
  
  function act_codigo(tipo)
  {
    if (tipo == 'pc')
    {
      $('txt_cod_cp').innerHTML = '<br /><br />C&oacute;digo';
      $('cod_cp').innerHTML = "<input style:\"width:80px;\" name=\"id_cp\" id=\"id_cp\" maxlength=\"10\" onkeyup=\"desactivar(1);\" />";
      $('txt_combo_cp').innerHTML = '<br /><br />Categor&iacute;a Program&aacute;tica';
      $('combo_cp').innerHTML = <?=helpers::superCombo($conn, "SELECT * FROM categorias_programaticas WHERE id_escenario=$escEnEje",0,'busca_cp','busca_cp', '', 'desactivar(0)', 'id', 'descripcion', '', '', '', 'Seleccione...', true)?>; 
      
    }
    else
    {
      $('txt_cod_cp').innerHTML = '';
      $('cod_cp').innerHTML = '';
      $('txt_combo_cp').innerHTML = '';
      $('combo_cp').innerHTML = '';
    }
  } 
  
</script>
<? 
	require ("comun/footer.php"); 
?>