<?
include('adodb/adodb-exceptions.inc.php');
require ("comun/ini.php");
$odepartamento = new departamento;
$accion = $_REQUEST['accion'];
$status=$_REQUEST['status'];

switch($accion ) {
    case 'Guardar' :
        if (!empty($_REQUEST['codigo']))
            $odepartamento->add($conn, $_REQUEST['codigo'], $_REQUEST['nombre'], $_REQUEST['division'], $_REQUEST['estatus'], $_REQUEST['orden'],$_REQUEST['unidad']);
        break;
    case 'Actualizar' :
        if (!empty($_REQUEST['codigo']))
            $odepartamento->set($conn, $_REQUEST['int_cod'], $_REQUEST['codigo'], $_REQUEST['nombre'], $_REQUEST['division'], $_REQUEST['estatus'], $_REQUEST['orden'],$_REQUEST['unidad']);
        break;
    case 'del':
        $odepartamento->del($conn, $_REQUEST['int_cod']);
        break;
}
$msg = $odepartamento->msg;

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";
?>
<br />
<span class="titulo_maestro">Maestro de Departamentos </span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />

    <table border="0" style="margin-left:10px" width="800">
        <tr align="center" >
            <td>
                <label style="margin-right:10px;">Buscar:</label>
                <select name="EstatusB" id="EstatusB">
                    <option value=0 <?=$_POST['EstatusB']==0 ? "selected" : ""?>>Activo</option>
                    <option value=1 <?=$_POST['EstatusB']==1 ? "selected" : ""?>>Inactivos</option>
                    <option value=2 <?=$_POST['EstatusB']==2 ? "selected" : ""?>>Todos</option>
                </select>
                <input style="margin-left:20px;" type="button" value="Buscar" id="buscar_estado">
            </td>
        </tr>
    </table>

<div id="busqueda"></div>

<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<script>
    busca(1,$F('EstatusB'));
    function busca(pagina,statusB)
    {
        var url  = 'updater_busca_departamento.php';
        var pars = 'pagina='+pagina+'&EstatusB='+statusB;
        var updater = new Ajax.Updater('busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        });
    }
      Event.observe('buscar_estado', "click", function () {
      busca(1,$F('EstatusB'));

    });
</script>
<?
$validator->create_message("error_codigo", "codigo", "Este campo no puede estar vacio");
$validator->create_message("error_desc", "nombre", "Este campo no puede estar vacio");
$validator->print_script();
require ("comun/footer.php"); ?>
