<? 
require("comun/ini.php"); 
require ("comun/header.php");
?>
<span style="text-align:left" class="titulo_maestro">
	Reportes de Ingresos
</span>
<center>
    <div align="left" id="formulario">
        <table width="424" border="0" align="center" style=" margin-left: auto; margin-right: auto; font-size:10px; ">
            <tr>
                <td width="42" nowrap>Reporte:</td>
                <td width="54">
                    <select name="tipo" id="tipo">
                        <option value="" selected>Seleccione...</option>
                        <option value="tra">Documentos de Ingresos</option>
                        <option value="adc">Arqueo de Caja</option>
                        <!--<option value="pc">Partidas por Categorias</option>-->
                    </select></td>
                <td width="84" align="left"><input name="button" type="button" onClick="imprimir()" value="Generar..." /></td>
            </tr>
        </table>
        <table width="454" border="0" align="center" style=" margin-left: auto; margin-right: auto; font-size:10px; ">
            <tr >
                 <td colspan="2" id="txt_status"> </td>
                 <td colspan="2" id="txt_tipo"></td>
            </tr>
            <tr>
                <td colspan="2" id="td_combo_status"> <div id="combo_status"></div> </td>
                <td colspan="2"><div id="combo_tipo"></div></td>
            </tr>
            <tr>
                <td colspan="2"> <div id="fecha"></div> </td>
                <td colspan="2"> <div id="fecha_hasta"></div> </td>
            </tr>
        </table>
    </div>
</center>
<br>
<br>
<br>
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<script type="text/javascript">

    var wxR;
    act_codigo($F('tipo'));
    
    function imprimir()
    {
        var JsonAux;
        var escEnEje = <?=$escEnEje?>;
        if($('tipo').value=='')
            alert("Debe Seleccionar un Tipo de Reporte a Generar");
		else if($('tipo').value == 'tra')
        {
            wxR = window.open("reporte_documentos_ing.pdf.php?fecha_desde=" + $('busca_fecha_desde').value + "&tipo=" + $('busca_tipo').value + "&status=" + $('busca_status').value, "winX", "width=500,height=500,scrollbars=yes,resizable=yes,status=yes,toolbar=no,menubar=no,location=no");
            wxR.focus()
        }else if($('tipo').value == 'adc')
        {
            wxR = window.open("reporte_arqueos_caja.pdf.php?fecha_desde=" + $('busca_fecha_desde').value +"&fecha_hasta=" + $('busca_fecha_hasta').value + "&tipo=" + $('busca_tipo').value + "&status=" + $('busca_status').value, "winX", "width=500,height=500,scrollbars=yes,resizable=yes,status=yes,toolbar=no,menubar=no,location=no");
            wxR.focus()
        }
    }

    function desactivar(opcion)
    {
        if (opcion==1)
        {
            if ($F('id_cp') != '')
            {
                $('busca_cp').disabled = true;
            }
            else
            {
                $('busca_cp').disabled = false;
            }
        }
        else
        {
            if ($F('busca_cp') == 0)
            {
                $('id_cp').disabled = false;
            }
            else
            {
                $('id_cp').disabled = true;
            }
        }
    }

    function act_codigo(tipo)
    {
        if (tipo == 'tra')
        {
            Element.show('td_combo_status');
            Element.show('txt_status');
            $('txt_status').innerHTML = '<br />Estado';
            $('txt_tipo').innerHTML = '<br />Tipo';
            $('combo_status').innerHTML = "<select name=\"busca_status\" id=\"busca_status\"><option value=\"-1\">Seleccione<option value=\"false\">Abierta</option><option value=\"true\">Cerrada</option></select>";
            $('combo_tipo').innerHTML = "<select name=\"busca_tipo\" id=\"busca_tipo\"><option value=\"-1\">Seleccione<option value=\"0\">Tributo</option><option value=\"1\">Servicios Municipales</option></select>";
            $('fecha').innerHTML = "<table><tr><td>Fecha:</td><td></td></tr><tr><td><input style=\"width:100px\"  type=\"text\" name=\"busca_fecha_desde\" id=\"busca_fecha_desde\"/></td><td><div id=\"boton_busca_fecha_desde\"><a href=\"#\" onclick=\"return false;\" onchange=\"validafecha(this);\"><img border=\"0\" alt=\"Seleccionar Fecha\" src=\"images/calendarA.png\" width=\"20\" height=\"20\" /></a></div></td></tr></table>";
            $('fecha_hasta').innerHTML ='';
            calendario_desde();
            
		}else if (tipo == 'adc')
        {
            Element.show('td_combo_status');
            Element.show('txt_status');
            $('txt_status').innerHTML = '<br />Estado';
            $('txt_tipo').innerHTML = '<br />Tipo';
            $('combo_status').innerHTML = "<select name=\"busca_status\" id=\"busca_status\"><option value=\"-1\">Seleccione<option value=\"false\">Abierta</option><option value=\"true\">Cerrada</option></select>";
            $('combo_tipo').innerHTML = "<select name=\"busca_tipo\" id=\"busca_tipo\"><option value=\"-1\">Seleccione<option value=\"0\">Tributo</option><option value=\"1\">Servicios Municipales</option></select>";
            $('fecha').innerHTML = "<table><tr><td>Fecha Desde:</td><td></td></tr><tr><td><input style=\"width:100px\"  type=\"text\" name=\"busca_fecha_desde\" id=\"busca_fecha_desde\"/></td><td><div id=\"boton_busca_fecha_desde\"><a href=\"#\" onclick=\"return false;\"><img border=\"0\" alt=\"Seleccionar Fecha\" src=\"images/calendarA.png\" width=\"20\" height=\"20\" /></a></div></td></tr></table>";
            $('fecha_hasta').innerHTML = "<table><tr><td>Fecha Hasta:</td><td></td></tr><tr><td><input style=\"width:100px\"  type=\"text\" name=\"busca_fecha_hasta\" id=\"busca_fecha_hasta\" onchange=\"validafecha(this);\"/></td><td><div id=\"boton_busca_fecha_hasta\"><a href=\"#\" onclick=\"return false;\"><img border=\"0\" alt=\"Seleccionar Fecha\" src=\"images/calendarA.png\" width=\"20\" height=\"20\" /></a></div></td></tr></table>";
            calendario_desde();
            calendario_hasta();
		}
        else{
            $('txt_status').innerHTML = '';
            $('txt_tipo').innerHTML = '';
            $('combo_status').innerHTML = "";
            $('combo_tipo').innerHTML = '';
            $('fecha').innerHTML = '';
            $('fecha_hasta').innerHTML ='';
        }
    }

    Event.observe('tipo', "change", function () {
        act_codigo($F('tipo'));
    });

    /*Event.observe('id_cp', "change", function () {
  alert ($('id_cp'));
     *if ($F('id_cp') != '')
  {
    $('busca_cp').selectedIndex = 0; 
          desactivar($('busca_cp'));
  }/ 
});*/

    /*Event.observe('busca_cp', "change", function () {
  alert ($('busca_cp'));
     *  if ($F('busca_cp') != '')
  {
    $F('id').value = '';
          desactivar($('id_cp'));
  }* 
});*/

    function calendario_desde(){
        new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_desde",
            button            : "boton_busca_fecha_desde",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });
    }

    function calendario_hasta(){
        new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_hasta",
            button            : "boton_busca_fecha_hasta",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });
    }

    
    function validafecha(fecha)
    {
        var upper = 31;
        if(/^(\d{2})\/(\d{2})\/(\d{4})$/.test(fecha.value))
        { // dd/mm/yyyy
            if(RegExp.$2 == '02')
                upper = 29;

            if((RegExp.$1 <= upper) && (RegExp.$2 <= 12))
            {
                fecha_desde = new Date();
                fecha_hasta = new Date();
                if ($('busca_fecha_desde').value!='' && $('busca_fecha_hasta').value!='')
                {
                    fecha_desde.setDate($('busca_fecha_desde').value.substr(0,2));
                    fecha_desde.setMonth(parseInt($('busca_fecha_desde').value.substr(3,2))-1);
                    fecha_desde.setFullYear($('busca_fecha_desde').value.substr(6,4));

                    fecha_hasta.setDate($('busca_fecha_hasta').value.substr(0,2));
                    fecha_hasta.setMonth(parseInt($('busca_fecha_hasta').value.substr(3,2))-1);
                    fecha_hasta.setFullYear($('busca_fecha_hasta').value.substr(6,4));

                    if (fecha_desde.getTime() > fecha_hasta.getTime())
                    {
                        alert("La fecha 'Desde' debe ser menor que la fecha 'Hasta'");
                        $(fecha).value = "";
                        return false;
                    }
                }
            }
            else
            {
                alert("Fecha incorrecta");
                $(fecha).value = "";
            }
        }
        else if(fecha.value != '')
        {
            alert("Fecha incorrecta");
            $(fecha).value = "";
        }
    }

</script>
<? 

require ("comun/footer.php"); 
?>	

