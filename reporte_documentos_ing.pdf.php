<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//die($_REQUEST['status'].':'.$_REQUEST['tipo']);

$tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo']:'-1'; 
$status = isset($_REQUEST['status'])? $_REQUEST['status']:'-1';
$fecha_desde =  isset($_REQUEST['fecha_desde'])?$_REQUEST['fecha_desde']: '';


function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $tipo;
  var $wcolumnas = array();
  var $fecha_caja;
  //Cabecera de página
	function Header()
	{
			parent::Header();
			$this->SetXY(160, 7);
                        $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');			
			$this->Ln(18);
			$this->SetFont('Courier','b',12);
			$this->Cell(180, 5, "RELACION DE DOCUMENTOS DE INGRESOS",0,1,'C');
                        $this->Cell(180, 5, "DEL DIA: ".$this->fecha_caja,0,1,'C');
			$this->SetFont('Courier','B',10);			
			if($this->tipo == 0){
	  			$this->Cell(180,5,'Tributos',1,1,'L');
                                $this->Cell(100,5,'Cuenta',1,0,'L');
                                $this->Cell(80,5,'Fecha',1,0,'C');
	  			$this->Ln(5);
			}else{
	  			$this->Cell(180,5,'Servicios Municipales',1,1,'L');
                                $this->Cell(100,5,'Cuenta',1,0,'C');
                                $this->Cell(80,5,'Fecha',1,1,'C');
	  			$this->Ln(5);
			}
	}

	function Footer()
	{
		$this->SetFont('Courier','I',10);
		//Número de página
		$this->Cell(180,10,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('P','mm');
$pdf->wcolumnas[1] = 20;
$pdf->wcolumnas[2] = 25;
$pdf->wcolumnas[3] = 25;

$total_general = 0;


$q = "select a.id as id_doc,a.fecha,a.id_cta_contable,d.descripcion as des_tipo,tipo_caja,b.*,c.descripcion,d.operacion from  ingresos.doc_ing_enc a
inner join ingresos.doc_ing_det b on a.id = b.id_documento
inner join contabilidad.plan_cuenta c on a.id_cta_contable = c.id
inner join ingresos.tipos_ingresos d on a.id_tipo = d.id
where 1=1 ";
if($status != '-1') $q .=" AND a.cerrado= $status "; //else $q.=" AND a.status = 0 ";
if($fecha_desde != '') $q .= " AND a.fecha = '".guardafecha($fecha_desde)."' ";
if($tipo != '-1') $q .= " AND a.tipo_caja = $tipo ";
//WHERE a.status = 0 AND a.tipo = 0 
$q .= " order by tipo_caja,id_documento,fecha_documento";
//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos
$pdf->fecha_caja = $fecha_desde;
if(!$rD->EOF and $rD->fields['tipo_caja'])
	$pdf->tipo = $rD->fields['tipo_caja'];
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);

$tipo_des_actual = 'Tributos';
$subtotal = 0;

$pdf->SetFont('Courier','',8);


$id_actual = '';
while(!$rD->EOF){
  $encabezado = 0;  
  if($id_actual == '') {
    $id_actual = $rD->fields['id_doc'];
    $encabezado = 1;
  }
  elseif($id_actual != $rD->fields['id_doc']){
         //Total Documento actual
         $pdf->SetFont('Courier','B',8);
         $pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso Bs.: "),0,0,'L');
         $pdf->Cell(25,5,muestrafloat(redondeado($total_doc_actual)),0,1,'R');
         if($rD->fields['tipo_caja'] == 1 and $pdf->tipo == 0){
            $pdf->tipo = 1;
            $subtotal = redondeado($total_documentos);
            $pdf->SetFont('Courier','B',8);
            $pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso ".$tipo_des_actual." Bs.: "),0,0,'L');
            $pdf->Cell(25,5,muestrafloat(redondeado($subtotal)),0,1,'R');
            $tipo_des_actual = 'Servicios Municipales';
            $pdf->addpage();
         }
         $id_actual = $rD->fields['id_doc'];
         $total_doc_actual = 0;
         $encabezado = 1;
         }
         
  if($encabezado == 1){
     $pdf->SetFont('Courier','B',8); 
     $pdf->Cell(130,5,$rD->fields['des_tipo'].' : '.$rD->fields['descripcion'],0,0,'L'); 
     $pdf->Cell(50,5,muestrafecha($rD->fields['fecha']),0,1,'R'); 
     $pdf->SetFont('Courier','',8);
  }     
  //if($rD->fields['status'] == 1) $pdf->SetFont('Courier','U',8); else	$pdf->SetFont('Courier','',8);
  $pdf->SetFont('Courier','',6);
  $pdf->Cell($pdf->wcolumnas[1],2,muestrafecha($rD->fields['fecha_documento']),0,0,'L');
  $pdf->Cell($pdf->wcolumnas[2],2,$rD->fields['num_doc'],0,0,'L');
  $pdf->Cell($pdf->wcolumnas[3],2,muestrafloat($rD->fields['monto']),0,1,'R');
  $pdf->SetFont('Courier','B',8);
  
//  for($li = 1;$li<count($descripcion);$li++){
//    $pdf->Cell($pdf->wcolumnas[1],5,'',0,0,'L');
//    $pdf->Cell($pdf->wcolumnas[2],5,'',0,0,'L');
//    $pdf->Cell($pdf->wcolumnas[3],5,$descripcion[$li],0,0,'L');
//
//  } 
  if($rD->fields['operacion'] == 1){
    $total_doc_actual += redondeado($rD->fields['monto']);
    $total_documentos += redondeado($rD->fields['monto']);
  }else {
    $total_doc_actual -= redondeado($rD->fields['monto']);
    $total_documentos -= redondeado($rD->fields['monto']);  
  }  
  $rD->movenext();
} //Fin loop departamentos

//Total Ultimo documento de la lista
$pdf->SetFont('Courier','B', 8);
$pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso  Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat(redondeado($total_doc_actual)),0,1,'R');

/*
 * Sub total
 */
$pdf->SetFont('Courier','B',10);
$pdf->Cell(100,5,utf8_decode("Total Documento de Ingreso ".$tipo_des_actual." Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat(redondeado($total_documentos-$subtotal)),0,1,'R');

/*
* TOTAL 
*/
$pdf->ln(3);
$pdf->SetFont('Courier','B',12);
$pdf->Cell(160,5,utf8_decode("Total Ingresos Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat($total_documentos),0,1,'R');


$pdf->Output();
?>
