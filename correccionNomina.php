<?php
require ("comun/ini.php");
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//Busco todas las nominas cerradas en el 2011, para corregir montos a afectar al presupuesto
$qnominas = "select * from rrhh.historial_nom where nom_fec_ini >= '2011-01-01' and nom_fec_fin <= '2011-06-15'";
$rqnominas = $conn->execute($qnominas);
echo "Correccion de partidas presupuestarias afectada por nomina"."<br>";
While(!$rqnominas->EOF){
    //Busco informacion en historico de nomina segun consulta utilizada para emitir reporte de nomina 
    //utilizado para emitir orden de pago con sistema viejo
    $qreporte = "select pp,sum(valor) as valor,sum(aporte) as aporte,max(conc_tipo) as conc_tipo from (
    SELECT sum(A.conc_val) AS valor,sum(A.conc_aporte) AS aporte,A.conc_cod,C.conc_nom,C.conc_tipo,max(d.par_cod) as pp
        FROM rrhh.hist_nom_tra_conc AS A 
        INNER JOIN rrhh.concepto AS C ON A.conc_cod=C.int_cod
        inner join rrhh.conc_part d on a.conc_cod = d.conc_cod
        WHERE A.hnom_cod=(select int_cod from rrhh.historial_nom where nrodoc='".$rqnominas->fields['nrodoc']."'  and substring(nom_fec_ini from 1 for 4)='2011') GROUP BY A.conc_cod,C.conc_nom,C.conc_tipo ORDER BY C.conc_tipo,A.conc_cod,C.conc_nom) as x
    where x.valor >0 or x.aporte >0
    group by pp
    order by pp";
    //die($qreporte);
    $rqreporte = $conn->execute($qreporte); 
    echo "Actualizando nomina:".$rqnominas->fields['cont_nom'].' de '.$rqnominas->fields['nom_fec_fin']."<br>" ;
    
    while(!$rqreporte->EOF){
        
        //Partidas presupuestaria a afectada segun cierre de nomina,
        $qhistorial = "select b.* from puser.movimientos_presupuestarios a
        inner join puser.relacion_movimientos b on a.nrodoc = b.nrodoc
        where  a.nrodoc = '".$rqnominas->fields['nrodoc']."' and b.id_partida_presupuestaria = '".$rqreporte->fields['pp']."'";
        $rqhistorial = $conn->execute($qhistorial);
        if(!$rqhistorial->EOF){
            $qupd ='';
            echo($rqreporte->fields['conc_tipo'].'='.$rqhistorial->fields['monto'].':'.$rqreporte->fields['valor']."<br>");
            if($rqreporte->fields['conc_tipo'] == 0){
                if($rqhistorial->fields['monto'] != $rqreporte->fields['valor']) //Modifico monto a afectar el presupuesto
                    $qupd = "UPDATE puser.relacion_movimientos SET monto=".$rqreporte->fields['valor']." WHERE id=".$rqhistorial->fields['id'];
            }else{
                if($rqhistorial->fields['monto'] != $rqreporte->fields['aporte'])
                    $qupd = "UPDATE puser.relacion_movimientos SET monto=".$rqreporte->fields['aporte']." WHERE id=".$rqhistorial->fields['id'];
            }
            if($qupd <> ''){
                echo "Modificando PP ".$rqreporte->fields['pp']." con valor de ".$rqreporte->fields['valor']."<br>";
                $rqupd = $conn->execute($qupd);
            }    
        }else{//Inserto la pp al documento de nomina
            $qins = "INSERT INTO puser.relacion_movimientos( nrodoc, id_categoria_programatica, id_partida_presupuestaria, monto, id_parcat)
                select b.nrodoc,b.id_categoria_programatica,".$rqreporte->fields['pp'].",".$rqreporte->fields['valor'].",(select id from puser.relacion_pp_cp where id_escenario = '1111' and id_categoria_programatica = '0101000051' and id_partida_presupuestaria = '".$rqreporte->fields['pp']."') from puser.movimientos_presupuestarios a
                inner join puser.relacion_movimientos b on a.nrodoc = b.nrodoc
                where  a.nrodoc ='".$rqnominas->fields['nrodoc']."' limit 1";
            $rqins = $conn->Execute($qins);
            //echo($qins);
            echo "Insertando PP ".$rqreporte->fields['pp']." con valor de ".$rqreporte->fields['valor']."<br>";
        }
        $rqreporte->MoveNext();
    }
    $rqnominas->MoveNext();
}


?>
