<? require ("comun/ini.php");
require ("comun/header.php"); ?>
<br />
<span class="titulo_maestro">Generar Reporte</span>
<div id="formulario">
    <table width="700" border="0" >
        <tr >
            <td width="100" >Tipo Reporte:</td>
            <td width="350">
                <SELECT name="Tipo" id="Tipo" onChange="ProcesosComboContrato()" >
                    <option value="0">Recibos de pago</option>
                    <option value="1">Historial de N&oacute;mina</option>
                    <option value="2">Carta al Banco</option>
                    <option value="3">Conceptos Acumulados Por Mes</option>
                    <option value="4">Conceptos Acumulados Por A&ntilde;o y Divisi&oacute;n</option>
                    <option value="5">Historial de N&oacute;mina por Depatamentos</option>
                    <option value="6">Pagos Con Cheque</option>
                    <option value="7">Retenciones</option>
                    <option value="8">Resumen Anual por Trabajador</option>
                    <option value="9">Fondos</option>
                    <option value="10">Pagos en Efectivo</option>
                    <option value="11">Relaci&oacute;n de Pagos por Banco (Depositos)</option>
                    <option value="12">Reporte de Fideicomiso</option>
                    <option value="13">Relaci&oacute;n Aportes de Empleados FAOV</option>
                    <option value="14">Comprobante de ISLR retenido</option>
                </SELECT>
            </td>
            <td width="250"></td>
        </tr>
        <tr id="trNomina" >
            <td width="100" >N&oacute;mina:</td>
            <td ><SELECT name="Nomina" id="Nomina" onChange="ProcesosComboContrato()" ></SELECT></td>
        </tr>
        <tr id="trTrabajador">
            <td >Trabajador:</td>
            <td ><SELECT name="Trabajador" id="Trabajador" ></SELECT></td>
        </tr>
        <tr id="trConcepto">
            <td >Concepto:</td>
            <td ><SELECT name="Concepto" id="Concepto" ></SELECT></td>
        </tr>
        <tr id="trCuentaBanco">
            <td >Cuenta - Banco:</td>
            <td ><?=helpers::combonominaIII($conn, '','' ,'','A.nro_cuenta,B.descripcion','Cuenta','id','nro_cuenta','descripcion','Cuenta','','SELECT A.id,A.nro_cuenta,B.descripcion FROM finanzas.cuentas_bancarias AS A INNER JOIN public.banco AS B ON A.id_banco=B.id','','true','true')?></td>
        </tr>
        <tr id="trBanco">
            <td >Banco:</td>
            <td ><?=helpers::combonomina($conn, '','' ,'','','Banco','id','descripcion','Banco','','SELECT A.id, A.descripcion FROM public.banco AS A','','true','Seleccione...')?></td>
        </tr>
        <tr id="trPeriodo">
            <td >Periodo:</td>
            <td ><input type="text" id="Periodo" name="Periodo"></td>
            <td width="200"> Formato de periodo: mm/YYYY. Ejm. 01/2010</td>
        </tr>
        <tr id="trDivision">
            <td >Divisi&oacute;n:</td>
            <td ><?=helpers::combonomina($conn, '','' ,'','div_nom','Division','int_cod','div_nom','Division','','SELECT int_cod,div_nom FROM rrhh.division WHERE emp_cod='.$_SESSION['EmpresaL'],'','true','Todas')?></td>
        </tr>
        <tr id="trFechaInicio">
            <td >Fecha Inicio:</td>
            <td ><input type="text" id="FecIni" name="FecIni"></td>
            <td width="200"> Formato de fecha: mm/YYYY. Ejm. 01/2010</td>
        </tr>
        <tr id="trFechaFin">
            <td >Fecha Fin:</td>
            <td ><input type="text" id="FecFin" name="FecFin"></td>
            <td width="200"> Formato de fecha: mm/YYYY. Ejm. 01/2010</td>
        </tr>
        <tr>
            <td align="right" colspan="2"><br /><input  type="button"  value="Generar Reporte" onClick="Imprimir()" ></td>
        </tr>
    </table>
</div>

<div style="height:40px;padding-top:10px;">
    <p id="Procesando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script language="javascript"  type="text/javascript"> 
    ComboNomina();
    function ComboNomina(){
        var JsonAux;
        Element.show('Procesando');
        $('Nomina').length=1;
		JsonAux={"Forma":1};
        var url = 'CargarCombo.php';
        var pars = 'JsonEnv=' + JsonAux.toJSONString();
        var Request = new Ajax.Request(
        url,
        {
            method: 'post',
            parameters: pars,
            //		asynchronous:true,
            onComplete:function(request){
                var JsonRec = eval( '(' + request.responseText + ')');
                if(JsonRec){
                    $('Nomina').options[0]= new Option("Seleccione",-1);
                    for(var i=1;i<=JsonRec.length;i++){
                        $('Nomina').options[i]= new Option(JsonRec[i-1]['D'],JsonRec[i-1]['CI']);
                    }
                }
            }
        }
    );
        ProcesosComboContrato()
        Element.hide('Procesando');
    }

    function ProcesosComboContrato(){
        Element.show('Procesando');
        var result=$('Tipo').options[$('Tipo').selectedIndex].value;
		Element.hide('trPeriodo');
        Element.hide('trFechaInicio');
        Element.hide('trFechaFin');
        Element.hide('trBanco');
        Element.hide('trDivision');
		Element.hide('trNomina');
        Element.hide('trConcepto');
        Element.hide('trCuentaBanco');
        Element.hide('trTrabajador');
        switch(result){
		    case '0':
            case '1':
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trTrabajador');
                break;
            case '2': // Carta al Banco
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trCuentaBanco');
                break;
			case '3': // Conceptos acumulados por mes
                Element.show('trPeriodo');
                break;
			case '4': // Conceptos acumulados por a�o y por Division
                Element.show('trPeriodo');
                Element.show('trDivision');
                break;		
			case '5': // Historial de Nomina por Departamentos
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trTrabajador');
                Element.show('trConcepto');
                break;	
			case '6': // Pagos con cheques
                Element.show('trNomina');
                Element.show('trCuentaBanco');
                break;		
			case '7': // Retenciones
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trTrabajador');
                break;
			case '8': // Resumen Anual por Trabajador
                Element.show('trNomina');
                Element.show('trFechaIni');
                Element.show('trTrabajador');
                Element.show('trFechaFin');
                break;
			case '9': // Fondos
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trTrabajador');
                break;			
			case '10': // Pagos en Efectivo
                Element.show('trNomina');
                Element.show('trCuentaBanco');
                break;
			case '11': // Relacion de Pagos por Banco (Depositos)
                Element.show('trNomina');
                Element.show('trBanco');
                break;					
			case '12': // Reporte de Fideicomiso
				Element.show('trNomina');
                Element.show('trPeriodo');
                break;
			case '13':
                Element.show('trPeriodo');
                break;					
			case '14': // Comprobante de ISLR retenido
                Element.show('trTrabajador');
                Element.show('trNomina');
                Element.show('trPeriodo');
                break;							
            default:
                Element.show('trNomina');
                Element.show('trConcepto');
                Element.show('trCuentaBanco');
                Element.show('trTrabajador');
                Element.show('trConcepto');
                Element.show('trPeriodo');
                Element.show('trFechaInicio');
                Element.show('trFechaFin');
                Element.show('trBanco');
                Element.show('trDivision');
                break
        }
        ComboTrabajador($('Nomina').options[$('Nomina').selectedIndex].value);
        ComboConcepto($('Nomina').options[$('Nomina').selectedIndex].value);
        Element.hide('Procesando');

    }
    function ComboTrabajador(Nomina){
        var JsonAux;
        $('Trabajador').length=1;
        if(Nomina!=-1){
            JsonAux={"Nomina":parseInt(Nomina),"Forma":2};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var Request = new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: pars,
                //		asynchronous:true,
                onComplete:function(request){
                    var JsonRec = eval( '(' + request.responseText + ')');
                    if(JsonRec){
                        //$('Trabajador').options[0]= new Option("Todos",-1);
                        $('Trabajador').options[0]= new Option("Seleccione",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Trabajador').options[i]= new Option(Cadena(JsonRec[i-1]['N'])+" "+Cadena(JsonRec[i-1]['A']),JsonRec[i-1]['CI']);
                        }
                    }
                }
            }
        );
        }
    }
    function ComboConcepto(Nomina){
        var JsonAux;
        $('Concepto').length=1;
        if(Nomina!=-1){
            JsonAux={"Nomina":parseInt(Nomina),"Forma":5};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var Request = new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: pars,
                //		asynchronous:true,
                onComplete:function(request){
                    var JsonRec = eval( '(' + request.responseText + ')');
                    if(JsonRec){
                        $('Concepto').options[0]= new Option("Todos",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Concepto').options[i]= new Option(JsonRec[i-1]['N'],JsonRec[i-1]['CI']);
                        }
                    }
                }
            }
        );
        }
    }
    var wxR;
    function Imprimir(){
        var JsonAux;
        if(($('Nomina').options[$('Nomina').selectedIndex].value==-1 || !($('Nomina').value)) && $('Tipo').options[$('Tipo').selectedIndex].value!=3 && $('Tipo').options[$('Tipo').selectedIndex].value!=4 && $('Tipo').options[$('Tipo').selectedIndex].value!=5 && $('Tipo').options[$('Tipo').selectedIndex].value!=8 && $('Tipo').options[$('Tipo').selectedIndex].value!=13){
            alert("Debe escojer una Nomina");
        }else if($('Tipo').options[$('Tipo').selectedIndex].value==2 && $('Cuenta').options[$('Cuenta').selectedIndex].value==-1){
            alert("Debe escojer un Banco");
        }else if($('Tipo').options[$('Tipo').selectedIndex].value==7 && $('Concepto').options[$('Concepto').selectedIndex].value==-1){
            alert("Debe escojer un Concepto");
        }else if($('Tipo').options[$('Tipo').selectedIndex].value==8 && ($('FecIni').value=='' || $('FecFin').value=='')){
            alert("Debe colocar un fecha de inicio y fin");
        }else if($('Tipo').options[$('Tipo').selectedIndex].value==9 && $('Concepto').options[$('Concepto').selectedIndex].value==-1){
            alert("Debe escojer un Concepto");
        }else{
            if($('Tipo').options[$('Tipo').selectedIndex].value==0){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hrecibopago.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==1){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hnomina.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==2){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hcartabanco.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Cuenta="+$('Cuenta').options[$('Cuenta').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==3){
                if (!wxR || wxR.closed) {
                    wxR = window.open("acumuladosMes.pdf.php?Periodo="+$('Periodo').value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==4){
                if (!wxR || wxR.closed) {
                    wxR = window.open("acumuladosAno.pdf.php?Periodo="+$('Periodo').value+"&Division="+$('Division').options[$('Division').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==5){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hnominaA.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==6){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hrelacion_pago_cheque.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Cuenta="+$('Cuenta').options[$('Cuenta').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==7){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hretenciones.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==8){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hResumenAnual.pdf.php?Nomina="+$('Nomina').options[$('Nomina').selectedIndex].value+"&FecIni="+$('FecIni').value+"&FecFin="+$('FecFin').value+"&Trabajador="+$('Trabajador').options[$('Trabajador').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==9){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hfondo.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Conc="+$('Concepto').options[$('Concepto').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==10){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hrelacion_pago_efectivo.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Cuenta="+$('Cuenta').options[$('Cuenta').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==11){
                if (!wxR || wxR.closed) {
                    wxR = window.open("hrelacionBanco.pdf.php?id="+$('Nomina').options[$('Nomina').selectedIndex].value+"&Banco="+$('Banco').options[$('Banco').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==12){
                if (!wxR || wxR.closed) {
                    wxR = window.open("acumuladosFideicomiso.pdf.php?Periodo="+$('Periodo').value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==13){
                if (!wxR || wxR.closed) {
                    wxR = window.open("reporte_faov.pdf.php?Periodo="+$('Periodo').value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
            if($('Tipo').options[$('Tipo').selectedIndex].value==14){
                if (!wxR || wxR.closed) {
                    wxR = window.open("reporte_nom_islr.pdf.php?Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value+"&Periodo="+$('Periodo').value+"&PeriodoID="+$('Nomina').options[$('Nomina').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }
            }
        }
    }
</script>
<? require ("comun/footer.php"); ?>