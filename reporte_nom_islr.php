<?
require ("comun/ini.php");
require ("comun/header.php");
?>
<br />
<span class="titulo_maestro">Comprobante de ISLR Retenido </span>

<div id="formulario">
    <table width="700" border="0" >
       <tr id="trNomina" >
            <td width="100" >N&oacute;mina:</td>
            <td ><SELECT name="Nomina" id="Nomina" onChange="RefrescaCombosDependiente();" ></SELECT></td>
        </tr>
        <tr id="trTrabajador">
            <td >Trabajador:</td>
            <td ><SELECT name="Trabajador" id="Trabajador" onChange="RefrescaCombosAnno();" ></SELECT></td>
        </tr>
        <tr id="trAno">
            <td >Año:</td>
            <td ><SELECT name="Anno" id="Anno"></SELECT></td>
        </tr>
        <tr id="trMes">
            <td >Mes:</td>
            <td ><SELECT name="Mes" id="Mes" >
                    <option value="0"> (Todos) </option>
                    <option value="1"> Enero </option>
                    <option value="2"> Febrero </option>
                    <option value="3"> Marzo </option>
                    <option value="4"> Abril </option>
                    <option value="5"> Mayo </option>
                    <option value="6"> Junio </option>
                    <option value="7"> Julio </option>
                    <option value="8"> Agosto </option>
                    <option value="9"> Septiembre </option>
                    <option value="10"> Octubre </option>
                    <option value="11"> Noviembre </option>
                    <option value="12"> Diciembre </option>
                </SELECT></td>
        </tr>
       <tr>
            <td align="right" colspan="2"><br /><input  type="button"  value="Generar Reporte" onClick="Imprimir()" ></td>
        </tr>
    </table>
</div>

<div style="height:40px;padding-top:10px;">
        <p id="cargando" style="display:none;margin-top:0px;">
            <img alt="Cargando" src="images/loading.gif" /> Cargando...
        </p>
</div>
<br>

<script language="javascript"  type="text/javascript">
    ComboNomina();

   function RefrescaCombosAnno() {
        ComboAnno($('Trabajador').options[$('Trabajador').selectedIndex].value);
   }
   function RefrescaCombosDependiente() {
        ComboTrabajador($('Nomina').options[$('Nomina').selectedIndex].value);       
    }
 
    // Funciones JSON para actualizar combos
    function ComboNomina(){
        var JsonAux;
        $('Nomina').length=1;
	JsonAux={"Forma":1};
        var url = 'CargarCombo.php';
        var pars = 'JsonEnv=' + JsonAux.toJSONString();
        var Request = new Ajax.Request(url,{
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(){Element.show('cargando')},
            onComplete:function(transport){
                var JsonRec = eval( '(' + transport.responseText + ')');
                if(JsonRec){
                    $('Nomina').options[0]= new Option("Seleccione",-1);
                    for(var i=1;i<=JsonRec.length;i++){
                        $('Nomina').options[i]= new Option(JsonRec[i-1]['D'],JsonRec[i-1]['CI']);
                    }
                }
                Element.hide('cargando');
            }
        }
    );
   }

    function ComboTrabajador(Nomina){
        var JsonAux;
        $('Trabajador').length=1;
        if(Nomina!=-1){
            JsonAux={"Nomina":parseInt(Nomina),"Forma":2};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var request = new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: pars,
                asynchronous:true,
                evalScripts:true,
                onLoading:function(transport){Element.show('cargando')},
                onComplete:function(transport){
                    var JsonRec = eval( '(' + transport.responseText + ')');
                    if(JsonRec){
                        //$('Trabajador').options[0]= new Option("Todos",-1);
                        $('Trabajador').options[0]= new Option("Seleccione",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Trabajador').options[i]= new Option(Cadena(JsonRec[i-1]['N'])+" "+Cadena(JsonRec[i-1]['A']),JsonRec[i-1]['CI']);
                        }
                    }
                    Element.hide('cargando');
                }
            }
        );
      }
    }

    function ComboAnno(Trabajador) {
       var JsonAux;
        $('Anno').length=1;
        if(Trabajador!=-1){
            JsonAux={"Trabajador":parseInt(Trabajador),"Forma":8};
            var url = 'CargarCombo.php';
            var pars = 'JsonEnv=' + JsonAux.toJSONString();
            var request = new Ajax.Request(
            url,
            {
                method: 'post',
                parameters: pars,
                asynchronous:true,
                evalScripts:true,
                onLoading:function(transport){Element.show('cargando')},
                onComplete:function(transport){
                    var JsonRec = eval( '(' + transport.responseText + ')');
                    if(JsonRec){
                        $('Anno').options[0]= new Option("Seleccione",-1);
                        for(var i=1;i<=JsonRec.length;i++){
                            $('Anno').options[i]= new Option(JsonRec[i-1]['ID'],JsonRec[i-1]['ID']);
                        }
                    }
                    Element.hide('cargando');
                }
            }
        );
      }
    }
    var wxR;
    function Imprimir(){
        if( $('Nomina').options[$('Nomina').selectedIndex].value==-1 ) {
            alert("Debe escojer una Nomina");
        } else if ($('Anno').options[$('Anno').selectedIndex].value==-1 ) {
            alert("Debe escojer un Año");
        } else
        {
               if (!wxR || wxR.closed) {
                    wxR = window.open("reporte_nom_islr.pdf.php?Tra="+$('Trabajador').options[$('Trabajador').selectedIndex].value + 
                                                               "&Anno="+$('Anno').options[$('Anno').selectedIndex].value+
                                                               "&Mes="+$('Mes').options[$('Mes').selectedIndex].value+
                                                               "&PeriodoID="+$('Nomina').options[$('Nomina').selectedIndex].value,"winX","width=500,height=500,scrollbars=yes,resizable=yes,status=yes");
                    wxR.focus()
                } else {
                    wxR.focus()
                }

        }
    }
</script>
<? require ("comun/footer.php"); ?>