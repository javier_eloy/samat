<?
require ("comun/ini.php");

$oArqueoCaja = new arqueo_caja($conn);

$accion         = $_REQUEST['accion'];
$id             = $_POST['id'];
$fecha          = $_POST['fecha'];
$descripcion    = $_POST['descripcion'];
$status         = $_POST['status'];
$monto_debe     = $_POST['monto_debe'];
$monto_haber    = $_POST['monto_haber'];
$tipo_caja      = $_POST['tipocaja'];
$json_det       = $_POST['json_det'];

switch($accion) {
    case 'Guardar':
        $oArqueoCaja->create(guardafecha($fecha),$descripcion,'1',$monto_debe,$monto_haber,$json_det,$tipo_caja);
        break;
    case 'Actualizar':
        $oArqueoCaja->set($id,guardafecha($fecha),$descripcion,$status,$monto_debe,$monto_haber,$json_det,$tipo_caja);
        break;
    case 'del':
        $oArqueoCaja->delete($_REQUEST['id']);
        break;
    case 'Cerrar':
        $oArqueoCaja->cerrar_caja($id,$fecha,$tipo_caja);
        break;
    case 'Anular':
        $oArqueoCaja->reversar_cierre_caja($id,$fecha);
        break;
}
$msg = $oArqueoCaja->msg;

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\" >".$msg."</div><br/>";

?>
<br />
<script type="text/javascript">var i=0;</script>
<span class="titulo_maestro">Arqueo de Caja</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<div>
    <fieldset id="buscador">
        <legend>Buscar:</legend>
        <table width="600" border="0">
            <tr>
                <td>Desde:</td>
                <td>Hasta:</td>
            <tr>
                <td style="width:150">
                    <input style="width:100px"  type="text" name="busca_fecha_desde" id="busca_fecha_desde" onchange="validafecha(this);"/>
                    <a href="#" id="boton_busca_fecha_desde" onclick="return false;">
                        <img border="0" alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup
                        ({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_desde",
                            button            : "boton_busca_fecha_desde",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>
                <td>
                    <input style="width:100px"  type="text" name="busca_fecha_hasta" id="busca_fecha_hasta" onchange="validafecha(this);"/>
                    <a href="#" id="boton_busca_fecha_hasta" onclick="return false;">
                        <img border="0" alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup
                        ({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_hasta",
                            button            : "boton_busca_fecha_hasta",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>
            </tr>
        </table>
    </fieldset>

</div>
<br />
<div style="margin-bottom:10px" id="busqueda"></div>
<br />
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">

    var t;
    var i = 0;
    var iEli = 0;
    var valor_tipo_caja = 0;





    function busca(fecha_desde, fecha_hasta, pagina)
    {
        //alert('cc: '+ cc +'  fecha_desde: '+fecha_desde+ ' fecha_hasta: '+fecha_hasta+' pagina: '+pagina);
        var url = 'updater_busca_ingresos.php';
        var pars = 'fecha_desde='+fecha_desde+'&fecha_hasta='+fecha_hasta+'&pagina='+pagina+'&ms='+new Date().getTime();
        var updater = new Ajax.Updater
        (
        'busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        }
    );
    }

    function buscador()
    {
        //alert($('busca_fecha_hasta').value);
        busca($('busca_fecha_desde').value, $('busca_fecha_hasta').value, 1);
    }

    function validafecha(fecha)
    {
        var upper = 31;
        if(/^(\d{2})\/(\d{2})\/(\d{4})$/.test(fecha.value))
        { // dd/mm/yyyy
            if(RegExp.$2 == '02')
                upper = 29;

            if((RegExp.$1 <= upper) && (RegExp.$2 <= 12))
            {
                fecha_desde = new Date();
                fecha_hasta = new Date();
                if ($('busca_fecha_desde').value!='' && $('busca_fecha_hasta').value!='')
                {
                    fecha_desde.setDate($('busca_fecha_desde').value.substr(0,2));
                    fecha_desde.setMonth(parseInt($('busca_fecha_desde').value.substr(3,2))-1);
                    fecha_desde.setFullYear($('busca_fecha_desde').value.substr(6,4));

                    fecha_hasta.setDate($('busca_fecha_hasta').value.substr(0,2));
                    fecha_hasta.setMonth(parseInt($('busca_fecha_hasta').value.substr(3,2))-1);
                    fecha_hasta.setFullYear($('busca_fecha_hasta').value.substr(6,4));

                    if (fecha_desde.getTime() > fecha_hasta.getTime())
                    {
                        alert("La fecha 'Desde' debe ser menor que la fecha 'Hasta'");
                        $(fecha).value = "";
                        return false;
                    }
                }

                busca($('busca_fecha_desde').value, $('busca_fecha_hasta').value, 1);
            }
            else
            {
                alert("Fecha incorrecta");
                $(fecha).value = "";
            }
        }
        else if(fecha.value != '')
        {
            alert("Fecha incorrecta");
            $(fecha).value = "";
        }
    }



    function sumaTotal(opcion){
        var total = 0;
        var valores = new Array();
        if(opcion == 3){
            filas = mygrid.getRowsNum();// + iEli;
            for(j=0;j<filas;j++){
                valor_accion=mygrid.cells(j,4).getValue();
                if(mygrid.getRowIndex(j)!=-1){
                    if(valor_accion==1){  //Debe
                        valores[j] = usaFloat(mygrid.cells(j,3).getValue());
                    }
                    else if(valor_accion==2){ //Haber
                        valores[j] = usaFloat(mygrid.cells(j,3).getValue())*(-1);
                    }
                }else{
                    valores[j] = 0;
                }
            }
        }else{
            filas = mygrid.getRowsNum();// + iEli;
            for(j=0;j<filas;j++){
                valor_accion=mygrid.cells(j,4).getValue();
                if(mygrid.getRowIndex(j)!=-1){                
                    if(valor_accion==opcion)
                        valores[j] = usaFloat(mygrid.cells(j,3).getValue());
                    else
                        valores[j] = 0;
                }else{
                    valores[j] = 0;
                }
            }
        }
        //Aqui creamos un json para realizar la suma en php, se va a pasar un arreglo con los valores a ser sumados
        var url = 'json.php';
        var pars = 'op=sumaValores&valores='+ valores;
        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){
                var suma = request.responseText;
                //alert('y dice: '+opcion);
                if(opcion==1){
                    $('monto_debe').value = muestraFloat(suma);
                }else {if(opcion==2)
                        $('monto_haber').value = muestraFloat(suma);
                    else
                        $('monto_diferencia').value = muestraFloat(suma);
                           
                }
                //Esta suma deberia realizarse por PHP
                //$('monto_diferencia').value = muestraFloat(usaFloat($('monto_debe').value)-usaFloat($('monto_haber').value));
            }
        }
    );

    }

    function GuardarDet()
    {

        var JsonAux = new Array;
        var lon = mygrid.getRowsNum();

        if (lon == 0)
            return false;
        //alert('entro');
        for(j=0; j<lon; j++)
        {
            rowId = mygrid.getRowId(j);
            //alert(mygrid.cells(rowId,0).getValue()+ ' ' +mygrid.cells(rowId,5).getValue())
            JsonAux.push({tipo:mygrid.cells(rowId,0).getValue(), nro_doc:mygrid.cells(rowId,1).getValue(), fecha:mygrid.cells(rowId,2).getValue(), monto:mygrid.cells(rowId,3).getValue(), origen:mygrid.cells(rowId,4).getValue()});
        }

        $('json_det').value = JsonAux.toJSONString();
        validate();
    }



    function Eliminar(){

        mygrid.deleteRow(mygrid.getSelectedId());
        iEli++;
        sumaTotal(1);
        sumaTotal(2);
        sumaTotal(3);

    }

    function fecha_convert_to_db(fch,separador){
        return fch.slice(6,10)+separador+fch.slice(3,5)+separador+fch.slice(0,2);
    }

    function fecha_convert_to_human(fch,separador){
        return fch.slice(8,10)+separador+fch.slice(5,7)+separador+fch.slice(0,4);
    }

    function buscaIngresos(fecha,id){
        //alert(fecha+':'+id);
        fch = new String(fecha);
        if(id==''){
            mygrid.clearSelection();
            mygrid.clearAll();
            tipo = valor_tipo_caja;

            var url = 'json.php';
            var pars = 'op=buscaIngresos&fecha_caja='+fecha_convert_to_db(fch,'-')+'&tipo='+tipo+'&ms='+new Date().getTime();
            var Request = new Ajax.Request(
            url,
            {
                method: 'get',
                parameters: pars,
                onLoading:function(request){},
                onComplete:function(request){
                    var ingresos = eval( '(' + request.responseText + ')');
                    //var Facturas = request.responseText;
                    if(ingresos != -1){
                        for(var j=0;j<ingresos.length;j++){
                            //alert(ingresos[j]['operacion']);
                            if(ingresos[j]['operacion'] == '1'){
                              mygrid.addRow(i,ingresos[j]['id_tipo']+";"+ingresos[j]['nrodoc']+";"+fecha_convert_to_human(ingresos[j]['fecha'],'/')+";"+muestraFloat(ingresos[j]['monto'])+";1");
                            }else{
                              mygrid.addRow(i,ingresos[j]['id_tipo']+";"+ingresos[j]['nrodoc']+";"+fecha_convert_to_human(ingresos[j]['fecha'],'/')+";"+muestraFloat(ingresos[j]['monto'])+";2");  
                            }  
                            mygrid.lockRow(i,true);
                            i++;
                        }
                        sumaTotal(1);
                        sumaTotal(2);
                        sumaTotal(3);
                    }else{
                        alert('Caja de fecha '+fch+' no tiene documentos de ingresos abiertos.');
                    }
                }
            });
        }
    }

    function traeTiposIngreso(cp){

        var url = 'buscar_ingresos.php';
        var pars = 'opcion=' + cp +'&ms='+new Date().getTime();
        
        var Request = new Ajax.Request(
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){},
            onComplete:function(request){

                Dialog.closeInfo();
                Dialog.alert(request.responseText, {windowParameters: {width:600, height:400,
                        showEffect:Element.show,hideEffect:Element.hide,
                        showEffectOptions: { duration: 1}, hideEffectOptions: { duration:1 }

                    }});

            }

        }
    );
    }

    function selPartidas(id, nombre){

        $('txttipo_ingreso').value = nombre;
        $('tipo_ingreso').value = id;
        Dialog.okCallback();

    }

    function AgregarCS(){
        //alert($('unidad_ejecutora').value);
        //alert(parseFloat($('montoip').value.replace(".","").replace(",","."))+':'+$('montoip').value.replace(".","").replace(",","."));
        if($('montoip').value=="" || parseFloat($('montoip').value.replace(".","").replace(",","."))<=0.00){

            alert("Primero debe colocar el monto de la cuenta.");
            return;
        }else if($('montoip').value=="" || parseFloat($('montoip').value.replace(".","").replace(",","."))<=0){

            alert("Primero debe colocar el monto de la Imputacion Presupuestaria.");
            return;

        }else{

            filas = mygrid.getRowsNum() + iEli;
            for(j=0;j<filas;j++){

                if (mygrid.getRowId(j)!=undefined){
                    if (mygrid.cells(mygrid.getRowId(j),'0').getValue() == $('tipo_ingreso').value){

                        alert('Esta cuenta ya ha sido seleccionada, por favor seleccione otra partida');
                        return false;

                    }

                }


            }

            mygrid.addRow(i,$('tipo_ingreso').value+";AC;;"+$('montoip').value+";2");
            i++;
            sumaTotal(2);
            sumaTotal(3);
            $('montoip').value='0,00';
        }
    }

    function actapr(valor) {
        if((parseFloat($('monto_debe').value.replace(".","","gi").replace(",",".")) != parseFloat($('monto_haber').value.replace(".","","gi").replace(",","."))) && $('accion').value == 'Cerrar' ){
            alert('No se puede cerrar la caja si los montos no coinciden');
            return false;
        }else{

            if(valor == 'Cerrar')
                $('accion').value = 'Cerrar';
            else
                $('accion').value = 'Anular';
            validate();
        }
    }


    function calcular_totales(stage,rowId,cellInd){

        //EN ESTE ESTADO CONVIERTO EL MONTO DE FORMATO VENEZOLANO AL FORMATO IMPERIALISTA//
        alert(stage);

        if (cellInd==4){

            var valor = usaFloat(mygridpp.cells(rowId,3).getValue());
            mygridpp.cells(rowId,3).setValue(valor);

        }

    }


</script>

<?
//$validator->create_message("error_escenario", "escenario", "*");
$validator->create_message("error_fecha", "fecha", "*");
$validator->create_message("error_descripcion", "descripcion", "*");
$validator->print_script();

require ("comun/footer.php");
?>