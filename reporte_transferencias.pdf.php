<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//die($_REQUEST['status'].':'.$_REQUEST['tipo']);

$tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo']:'-1'; 
$status = isset($_REQUEST['status'])? $_REQUEST['status']:'-1';
$fecha_desde =  isset($_REQUEST['fecha_desde'])?$_REQUEST['fecha_desde']: '';
$fecha_hasta =  isset($_REQUEST['fecha_hasta'])?$_REQUEST['fecha_hasta']: '';
$ctacedente = isset($_REQUEST['lstcedente']) ? $_REQUEST['lstcedente'] : '';
$ctareceptora = isset($_REQUEST['lstreceptora']) ? $_REQUEST['lstreceptora'] : '';


function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $tipo;
  var $wcolumnas = array();
  //Cabecera de página
	function Header()
	{
			parent::Header();
			$this->SetXY(200, 7);
            $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');			
			$this->Ln(15);
			$this->SetFont('Courier','b',12);
			$this->Cell(260, 10, "RELACION DE TRANSFERENCIAS",0,1,'C');
			$this->SetFont('Courier','B',10);			
			if($this->tipo == 0){
	  			$this->Cell(260,5,'Transferencias Internas',1,1,'L');
      			$this->Cell($this->wcolumnas[1],5,'Fecha',1,0,'L');
      			$this->Cell($this->wcolumnas[2],5,'Documento',1,0,'C');
      			$this->Cell($this->wcolumnas[3],5,utf8_decode('Descripción'),1,0,'C');
      			$this->Cell($this->wcolumnas[5],5,'Cuenta B. Cedente',1,0,'C');
      			$this->Cell($this->wcolumnas[6],5,'Cuenta B. Receptora',1,0,'C');
      			$this->Cell($this->wcolumnas[7],5,'Monto',1,0,'C');
	  			$this->Ln(5);
			}else{
	  			$this->Cell(260,5,'Transferencias Externas',1,1,'L');
      			$this->Cell($this->wcolumnas[1],5,'Fecha',1,0,'C');
      			$this->Cell($this->wcolumnas[2],5,'Documento',1,0,'C');
      			$this->Cell($this->wcolumnas[3],5,utf8_decode('Descripción'),1,0,'C');
     			$this->Cell($this->wcolumnas[4],5,utf8_decode('Beneficiario'),1,0,'C');
      			$this->Cell($this->wcolumnas[5],5,'C.C. Cedente',1,0,'C');
      			$this->Cell($this->wcolumnas[6],5,'C.C. Receptora',1,0,'C');
      			$this->Cell($this->wcolumnas[7],5,'Monto',1,1,'C');
	  			$this->Ln(1);
			}
	}

	function Footer()
	{
		$this->SetFont('Courier','I',12);
		//Número de página
		$this->Cell(260,10,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('L','mm');
$pdf->wcolumnas[1] = 20;
$pdf->wcolumnas[2] = 25;
$pdf->wcolumnas[3] = 85;
$pdf->wcolumnas[4] = 25;
$pdf->wcolumnas[5] = 50;
$pdf->wcolumnas[6] = 50;
$pdf->wcolumnas[7] = 30;

$total_general = 0;

/*
*
*SECCION TRANSFERENDCIAS INTERNAS
*/
if($tipo == 0 OR $tipo == -1){
$q = "SELECT a.status,a.fecha,a.nrodoc,a.descripcion,a.beneficiario,b.nro_cuenta||'('||d.nombre_corto::char(20)||')' AS cedente,c.nro_cuenta||'('||e.nombre_corto::char(20)||')' AS receptora,case when a.status = 1 then a.monto*-1 else a.monto end
FROM finanzas.transferencias a
INNER JOIN finanzas.cuentas_bancarias b ON a.id_cuenta_cedente = b.id
INNER JOIN finanzas.cuentas_bancarias c ON a.id_cuenta_receptora = c.id
INNER JOIN public.banco d ON b.id_banco = d.id
INNER JOIN public.banco e ON c.id_banco = e.id
WHERE a.tipo = 0 ";
if($status != '-1') $q .=" AND a.status= $status "; //else $q.=" AND a.status = 0 ";
if($fecha_desde != '') $q .= " AND a.fecha >= '".guardafecha($fecha_desde)."' ";
if($fecha_hasta != '') $q .= " AND a.fecha <= '".guardafecha($fecha_hasta)."' ";
if($ctacedente != '' and $ctareceptora == '0') $q .= " AND (a.id_cuenta_cedente = $ctacedente) ";
if($ctareceptora != '' and $ctacedente == '0') $q .= " AND (a.id_cuenta_receptora = $ctareceptora) ";
if($ctacedente != '0' and $ctareceptora != '0') $q .= " AND (a.id_cuenta_cedente = $ctacedente  and a.id_cuenta_receptora = $ctareceptora) ";
//WHERE a.status = 0 AND a.tipo = 0 
$q .= "ORDER BY a.fecha";
//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos
$pdf->tipo = 0;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);

$total_transferencia = 0;

$pdf->SetFont('Courier','',8);

while(!$rD->EOF){
  $descripcion = dividirStr(utf8_decode($rD->fields['descripcion']),$pdf->wcolumnas[3]*0.60); //Utilizo el 60% del ancho de la columna para cortar la frase  
  if($rD->fields['status'] == 1) $pdf->SetFont('Courier','U',8); else	$pdf->SetFont('Courier','',8);
  $pdf->Cell($pdf->wcolumnas[1],5,muestrafecha($rD->fields['fecha']),0,0,'L');
  $pdf->Cell($pdf->wcolumnas[2],5,$rD->fields['nrodoc'],0,0,'L');
  $pdf->Cell($pdf->wcolumnas[3],5,$descripcion[0],0,0,'L');
  $pdf->Cell($pdf->wcolumnas[5],5,substr($rD->fields['cedente'],0,$pdf->wcolumnas[5]*0.55),0,0,'R');
  $pdf->Cell($pdf->wcolumnas[6],5,substr($rD->fields['receptora'],0,$pdf->wcolumnas[6]*0.55),0,0,'R');    
  $pdf->Cell($pdf->wcolumnas[7],5,muestrafloat(redondeado($rD->fields['monto'])),0,1,'R');
	
  for($li = 1;$li<count($descripcion);$li++){
    $pdf->Cell($pdf->wcolumnas[1],5,'',0,0,'L');
    $pdf->Cell($pdf->wcolumnas[2],5,'',0,0,'L');
    $pdf->Cell($pdf->wcolumnas[3],5,$descripcion[$li],0,0,'L');
    $pdf->Cell($pdf->wcolumnas[5],5,'',0,0,'R');
    $pdf->Cell($pdf->wcolumnas[6],5,'',0,0,'R');
    $pdf->Cell($pdf->wcolumnas[7],5,'',0,1,'R');
  } 
  
  $total_transferencia += redondeado($rD->fields['monto']);
  $rD->movenext();
} //Fin loop departamentos

//$total_emp = $total_emp;
$pdf->SetFont('Courier','B',8);
$pdf->Cell(170,5,utf8_decode("Total transferencia entre cuentas Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat(redondeado($total_transferencia)),0,1,'R');
$total_general = redondeado($total_transferencia);
}

/*
*
*SECCION TRANSFERENCIAS EXTERNAS
*/

if($tipo == 1 OR $tipo == -1){

$pdf->wcolumnas[4] = 25;
$pdf->wcolumnas[5] = 40;
$pdf->wcolumnas[6] = 40;
$pdf->wcolumnas[7] = 25;


$q = "SELECT a.status,a.fecha,a.nrodoc,a.descripcion,a.beneficiario,b.codcta||'('||b.descripcion::char(20)||')' AS cedente,c.codcta||'('||c.descripcion::char(20)||')' AS receptora,case when a.status = 1 then a.monto*-1 else a.monto end
FROM finanzas.transferencias a
INNER JOIN contabilidad.plan_cuenta b ON a.id_cuenta_cedente = b.id
INNER JOIN contabilidad.plan_cuenta c ON a.id_cuenta_receptora = c.id
WHERE a.tipo = 1 ";
if($status != '-1') $q .=" AND a.status= $status "; //else $q.=" AND a.status = 0 ";
if($fecha_desde != '') $q .= " AND a.fecha >= '".guardafecha($fecha_desde)."' ";
if($fecha_hasta != '') $q .= " AND a.fecha <= '".guardafecha($fecha_hasta)."' ";
if($ctacedente != '' and $ctareceptora == '0') $q .= " AND (a.id_cuenta_cedente = $ctacedente) ";
if($ctareceptora != '' and $ctacedente == '0') $q .= " AND (a.id_cuenta_receptora = $ctareceptora) ";
if($ctacedente != '0' and $ctareceptora != '0') $q .= " AND (a.id_cuenta_cedente = $ctacedente  and a.id_cuenta_receptora = $ctareceptora) ";

//WHERE a.status = 0 AND a.tipo = 1
$q .= " ORDER BY a.fecha";

//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos
$pdf->tipo = 1;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);

$total_transferencia = 0;
$pdf->SetFont('Courier','',7);
while(!$rD->EOF){
  if($rD->fields['status'] == 1) $pdf->SetFont('Courier','U',8); else	$pdf->SetFont('Courier','',8);
  $descripcion = dividirStr(utf8_decode($rD->fields['descripcion']),$pdf->wcolumnas[3]*0.60); //Utilizo el 60% del ancho de la columna para cortar la frase
  
  $pdf->Cell($pdf->wcolumnas[1],5,muestrafecha($rD->fields['fecha']),0,0,'L');
  $pdf->Cell($pdf->wcolumnas[2],5,$rD->fields['nrodoc'],0,0,'L');
  $pdf->Cell($pdf->wcolumnas[3],5,$descripcion[0],0,0,'L');
  $pdf->Cell($pdf->wcolumnas[4],5,substr($rD->fields['beneficiario'],0,$pdf->wcolumnas[4]*0.60),0,0,'C');
  $pdf->Cell($pdf->wcolumnas[5],5,substr($rD->fields['cedente'],0,$pdf->wcolumnas[5]*0.6),0,0,'R');
  $pdf->Cell($pdf->wcolumnas[6],5,substr($rD->fields['receptora'],0,$pdf->wcolumnas[6]*0.6),0,0,'R');
  $pdf->Cell($pdf->wcolumnas[7],5,muestrafloat(redondeado($rD->fields['monto'])),0,1,'R');
  
  for($li = 1;$li<count($descripcion);$li++){
  	$pdf->Cell($pdf->wcolumnas[1],5,'',0,0,'L');
    $pdf->Cell($pdf->wcolumnas[2],5,'',0,0,'L');
    $pdf->Cell($pdf->wcolumnas[3],5,$descripcion[$li],0,0,'L');
    $pdf->Cell($pdf->wcolumnas[5],5,'',0,0,'R');
    $pdf->Cell($pdf->wcolumnas[6],5,'',0,0,'R');
    $pdf->Cell($pdf->wcolumnas[7],5,'',0,1,'R');
  } 

  $total_transferencia += redondeado($rD->fields['monto']);

	$rD->movenext();
} //Fin loop departamentos

$pdf->SetFont('Courier','B',8);
$pdf->Cell(170,5,utf8_decode("Total Transferencias Externas Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat(redondeado($total_transferencia)),0,1,'R');
$total_general += redondeado($total_transferencia);

} //Fin bloque de transferencias externas
/*
* TOTAL TRANSFERENCIAS
*/
$pdf->SetFont('Courier','B',10);
$pdf->Cell(170,5,utf8_decode("Total Transferencias Bs.: "),0,0,'L');
$pdf->Cell(25,5,muestrafloat($total_general),0,1,'R');


$pdf->Output();
?>
