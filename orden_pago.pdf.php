<?php
include("comun/ini.php");
include("Constantes.php");
set_time_limit(0);

$_SESSION['conex'] = $conn;

$oOrdenPago = new orden_pago();
$oOrdenPago->get($conn, $_GET['id'], $escEnEje);
//var_dump($oOrdenPago->nrodoc);
if(empty($oOrdenPago->nrodoc))
    header ("location: orden_pago.php");

$objOP = new orden_pago();
$_SESSION['objOP'] = $objOP;

//Variable para corte de pagina manual
$YFinRollover = 160;
//Variable para controlar lineas de pp o cuentas contables a mostrar 
$YFinFooter = 10;

function dividirStr($str, $max) {
    $strArray = array();
    do {
        if (strlen($str) > $max)
            $posF = strrpos( substr($str, 0, $max), ' ' );
        else
            $posF = -1;
        if ($posF===false || $posF==-1) {
            $strArray[] = substr($str, 0);
            $str = substr($str, 0);
            $posF = -1;
        }
        else {
            $strArray[] = substr($str, 0, $posF);
            $str = substr($str, $posF+1 );
        }
    }
    while ($posF != -1);
    return ($strArray);
}


function mascara_cc($codcc) {
    $pri_val = substr($codcc,0,1);
    $seg_val = substr($codcc,1,2);
    //$x= substr($codcc,4,2);
    $ter_val = substr($codcc,3,2);
    $cua_val = substr($codcc,5,2);
    $qui_val = substr($codcc,7,2);
    $sex_val = substr($codcc,9);
    return $pri_val.'.'.$seg_val.'.'.$ter_val.'.'.$cua_val.'.'.$qui_val.'.'.$sex_val;
}


// Crea un array donde cada posicion es un string de tama�o 'max' caracteres,
// teniendo en cuenta de no cortar una palabra, busca el espacio en blanco  
// mas cerca del tama�o 'max' y ahi corta el string
function ver_anexo($presupuesto, $contabilidad, $pdf) {
    $pdf->AddPage();
    $sec = 5;
    $pro = 5;
    $act = 5;
    $gen = 5;
    $par = 7;
    $gene = 5;
    $espe= 5;
    $sesp1 = 5;
    $sesp2 = 8;
    $desc_part = 80;
    $pdf->Cell(175,4,'IMPUTACION PRESUPUESTARIA',0,'','C');
    $pdf->Ln();
    //$pdf->Ln();

    foreach($presupuesto as $prep) {
        $desc_partida = dividirStr($prep->partida_presupuestaria, intval($desc_part/$pdf->GetStringWidth('M')));
        $sec_val = substr($prep->id_categoria_programatica,0,2);
        $pro_val = substr($prep->id_categoria_programatica,2,2);
        $act_val = substr($prep->id_categoria_programatica,6,2);
        $gen_val = substr($prep->id_categoria_programatica,8,2);
        $par_val = substr($prep->id_partida_presupuestaria,0,3);
        $gene_val = substr($prep->id_partida_presupuestaria,3,2);
        $espe_val = substr($prep->id_partida_presupuestaria,5,2);
        $sesp1_val = substr($prep->id_partida_presupuestaria,7,2);
        $sesp2_val = substr($prep->id_partida_presupuestaria,9,4);
        /*$pdf->Ln();
			$pdf->Cell(50,3,$relaciones->id_categoria_programatica.' - '.$relaciones->id_partida_presupuestaria,0,'','C');*/
        $pdf->Ln();

        $pdf->Cell($sec,3,$sec_val,0,'','C');
        $pdf->Cell($pro,3,$pro_val,0,'','C');
        $pdf->Cell($act,3,$act_val,0,'','C');
        $pdf->Cell($gen,3,$gen_val,0,'','C');
        $pdf->Cell($par,3,$par_val,0,'','C');
        $pdf->Cell($gene,3,$gene_val,0,'','C');
        $pdf->Cell($espe,3,$espe_val,0,'','C');
        $pdf->Cell($sesp1,3,$sesp1_val,0,'','C');
        $pdf->Cell($sesp2,3,$sesp2_val,0,'','C');
        $pdf->Cell(2,3,'',0,'','C');
        $pdf->Cell($desc_part,3,utf8_decode($desc_partida[0]),0,'','L');
        $pdf->Cell(40,3,muestraFloat($prep->monto),0,'','R');
        $total+= $prep->monto;
    }
    $ancho = $sec + $pro + $act + $gen + $par + $gene + $espe + $sesp1 + $sesp2 + 2 + $desc_part;
    $pdf->Ln();
    $pdf->Cell($ancho,4,'TOTAL:',0,'','R');
    $pdf->Cell(40,4,muestraFloat($total),0,'','R');
    $pdf->Ln(12);
    $pdf->Cell(175,4,'IMPUTACION CONTABLE',0,'','C');
    $pdf->Ln();

    while(!$contabilidad->EOF) {
        //die($contabilidad->fields['descripcion']);
        $desc_contabilidad = dividirStr($contabilidad->fields['descripcion'], intval(75/$pdf->GetStringWidth('M')));
        $sumaDebe+=$contabilidad->fields['debe'];
        $sumaHaber+=$contabilidad->fields['haber'];
        $pdf->Cell(5,4, utf8_decode(substr($contabilidad->fields['codcta'],1,1)),0, '','C');
        $pdf->Cell(8,4, utf8_decode(substr($contabilidad->fields['codcta'],5,2)),0, '','C');
        $pdf->Cell(8,4, utf8_decode(substr($contabilidad->fields['codcta'],7,2)),0, '','C');
        $pdf->Cell(8,4, utf8_decode(substr($contabilidad->fields['codcta'],9,2)),0, '','C');
        $pdf->Cell(8,4, utf8_decode(substr($contabilidad->fields['codcta'],11,2)),0, '','C');
        $pdf->Cell(8,4, utf8_decode(substr($contabilidad->fields['codcta'],13,3)),0, '','C');
        $pdf->Cell(75,4, utf8_decode($desc_contabilidad[0]),0, '','C');
        $pdf->Cell(40,4, utf8_decode(muestraFloat($contabilidad->fields['debe'])),0, '','C');
        $pdf->Cell(40,4, utf8_decode(muestraFloat($contabilidad->fields['haber'])),0, '','C');
        $pdf->Ln();
        $contabilidad->movenext();
    }
    $pdf->Ln();
    $pdf->Cell(120,4,'',0,'','C');
    $pdf->Cell(40,4,muestraFloat($sumaDebe),0,'','C');
    $pdf->Cell(40,4,muestraFloat($sumaHaber),0,'','C');
}


class PDF extends FPDF {
    
    var $objOrdenPago;
    var $escEnEje;
    var $id;
    var $paginaDetalle;
    var $YFinFooter;
    
    //Cabecera de p�gina
    function Header() {
    	
    	parent::Header();
        $conn = $_SESSION['conex'];
        $q = $conn->Execute("SELECT * FROM finanzas.orden_pago WHERE nrodoc='".$_GET['id']."'");
        //var_dump($q);

        $this->SetXY(150, 15);
        $this->SetFont('Courier','b',12);
        $this->Text(150, 10, "Orden de Pago");
        $this->Text(150, 15, "Nro.:".$q->fields['nrodoc']."\n");
        $textoFecha = "Fecha: ".muestrafecha($q->fields['fecha'])."\n";
        $this->Text(150, 20, $textoFecha);
        $textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}\n";
        $this->Text(150, 25, $textoPag);
		
        $this->Ln(15);
        $this->SetFont('Courier','',10);
        $this->SetDrawColor (255,255,255);
        $this->SetWidths(array(165));
        $this->SetAligns(array('L'));
        $this->Row(array("He recibido del ".ORGANISMO_NOMBRE." - ".ORGANISMO_SIGLAS." la cantidad de:	**********".muestraFloat($q->fields['montodoc'])." son: ".utf8_decode(num2letras(floatval(substr($q->fields['montodoc'],0,-1))))),11);
        $this->SetFont('Arial','',10);
        $this->Ln(10);

    }

    //Pie de p�gina
    function Footer() {
        
        $conn = $_SESSION['conex'];
        $objOP = $_SESSION['objOP'];
        $this->SetXY(5,191);
        $this->Cell(90,5,"ASIENTO CONTABLE DEL PAGO",1,0,'C');
        $this->SetX(115);
        $this->Cell(90,5,"PRESUPUESTO",1,0,'C');
        $this->Ln(5);
        $this->SetX(5);
        $this->Cell(90,50,"",1,0,'C');
        $this->SetX(115);
        $this->Cell(90,50,"",1,0,'C');
            
        $espacio = 10;
        //$desc_part = 90;*/
        if($this->objOrdenPago->id_tipo_solicitud_si==0) {
            //$oRelacion = orden_pago::getRelacionPartidas($conn,$_GET['id'],$escEnEje);
            $oRelacion = $objOP->getRelacionPartidas($conn,$this->id,$this->escEnEje);
            $this->SetFont('Arial','',6);
            $cant_lineas = 0;
            foreach($oRelacion as $relaciones) {
                //$desc_partida = dividirStr($relaciones->partida_presupuestaria, intval($desc_part/$this->GetStringWidth('M')));
                $desc_partida = $relaciones->partida_presupuestaria;
                $cant_lineas++;
            }
            $this->SetXY(116,195);
            $cant_pp = 0;
            foreach($oRelacion as $relaciones) {
                //$desc_partida = dividirStr($relaciones->partida_presupuestaria, intval($desc_part/$this->GetStringWidth('M')));
                /*$this->Ln();
                                $this->Cell(50,3,$relaciones->id_categoria_programatica.' - '.$relaciones->id_partida_presupuestaria,0,'','C');*/
                $this->SetDrawColor (255,255,255);
                $this->SetWidths(array(8,55,15));
                $this->SetAligns(array('L','L','R'));
                if(($cant_pp < $this->YFinFooter and $this->PageNo() == 1) or $cant_lineas < $this->YFinFooter){
                    $this->Row(array($anoCurso,masc_presupuesto($relaciones->id_categoria_programatica,$relaciones->id_partida_presupuestaria), muestraFloat($relaciones->monto)),11);
                }else{
                    if($cant_pp >= $this->YFinFooter and $this->PageNo() > 1)
                        $this->Row(array($anoCurso,masc_presupuesto($relaciones->id_categoria_programatica,$relaciones->id_partida_presupuestaria), muestraFloat($relaciones->monto)),11);
                }    
                
                $cant_pp++;
                $this->SetX(116);
            }
        } 
        else {
            $this->SetXY(135,222);
            $this->Cell(30,4,'ORDEN DE PAGO SIN IMPUTACION',0,'','L');
            //$this->SetXY(15,135);
        }

        //PARTE CONTABLE C.E.P.V
        //$q="SELECT C.codcta,C.descripcion,B.debe,B.haber,A.numcom FROM contabilidad.com_enc AS A INNER JOIN contabilidad.com_det AS B ON A.id=B.id_com INNER JOIN contabilidad.plan_cuenta AS C ON B.id_cta=C.id  WHERE A.origen='OP' AND A.num_doc='".$_GET['id']."' AND A.id_escenario = '".$escEnEje."'";
        //die($q);
        //echo $cant_lineas."<br>";
        //
        if(!$EsNomina) {
            //Primera parte cancel deuda proveedor
            $q = "SELECT codcta FROM contabilidad.plan_cuenta pc INNER JOIN puser.proveedores p ON (pc.id = p.cta_contable) WHERE p.id = ".$this->objOrdenPago->id_proveedor;
            $rProv = $conn->Execute($q);

            //Obtenemos total de retenciones
            $qRet = "SELECT SUM(r.mntret) AS monto, r.codret, pc.codcta FROM finanzas.relacion_retenciones_orden r ";
            $qRet.= "INNER JOIN finanzas.retenciones_adiciones ra ON (ra.id = r.codret) ";
            $qRet.= "INNER JOIN contabilidad.plan_cuenta pc ON (pc.id = ra.id_cta) ";
            $qRet.= "WHERE r.nrodoc = '".$this->objOrdenPago->nrodoc."' ";
            $qRet.= "GROUP BY 2,3 ORDER BY 2 ";

            $rRet = $conn->Execute($qRet);

            //Obtenemos total de Iva
            $qIva = "SELECT SUM(r.iva_retenido) AS monto, r.id_retencion, pc.codcta FROM finanzas.facturas r ";
            $qIva.= "INNER JOIN finanzas.retenciones_adiciones ra ON (ra.id = r.id_retencion) ";
            $qIva.= "INNER JOIN contabilidad.plan_cuenta pc ON (pc.id = ra.id_cta) ";
            $qIva.= "WHERE r.nrodoc = '".$this->objOrdenPago->nrodoc."' ";
            $qIva.= "GROUP BY 2,3";

            $rIva = $conn->Execute($qIva);

            //Obtenemos Retenciones de anticipo
            $qAnt = "SELECT COALESCE(A.monto_anticipo,0) AS monto, A.cuenta_contable_anticipo, B.codcta ";
            $qAnt.= "FROM finanzas.orden_pago A ";
            $qAnt.= "INNER JOIN contabilidad.plan_cuenta B ON (A.cuenta_contable_anticipo = B.id) ";
            $qAnt.= "WHERE A.nrodoc = '".$this->objOrdenPago->nrodoc."' ";

            $rAnt= $conn->Execute($qAnt);

            //Obtenemos el monto del banco
            //SQL cmbiado para incluir el anticipo tengo mis dudas al respecto originalmente estaba asi (A.montodoc - A.montoret)
            $qBco = "SELECT (A.montodoc - A.montoret - A.monto_anticipo) AS monto, A.id_cuenta, B.codcta ";
            $qBco.= "FROM finanzas.orden_pago A ";
            $qBco.= "INNER JOIN finanzas.cuentas_bancarias C ON (C.id = A.id_cuenta) ";
            $qBco.= "INNER JOIN contabilidad.plan_cuenta B ON (C.id_plan_cuenta = B.id) ";
            $qBco.= "WHERE A.nrodoc = '".$this->objOrdenPago->nrodoc."'  ORDER BY B.codcta";

            $rBco = $conn->Execute($qBco);
            //$cant_lineas = $r->RecordCount();
            //die(var_dump($r));
            $sumaDebe=0;
            $sumaHaber=0;
            $this->Ln(12);
            //echo 'aqui '.$cant_lineas;
            //if($cant_lineas <= 5){
            $this->SetXY(11,195);

            $this->SetFont('Arial','',6);
            $this->SetDrawColor (255,255,255);
            //die(var_dump($rProv));
            $this->SetWidths(array(50,15,15));
            $this->SetAligns(array('L','R','R'));
            $this->Row(array(mascara_cc($rProv->fields['codcta']), utf8_decode(muestraFloat($this->objOrdenPago->montodoc)),muestraFloat(0)),8);
            $sumaDebe += $this->objOrdenPago->montodoc;
            $this->SetX(11);
            if($rRet) {
                while(!$rRet->EOF) {
                    $this->Row(array(mascara_cc($rRet->fields['codcta']), muestraFloat(0),utf8_decode(muestraFloat($rRet->fields['monto']))),8);
                    $sumaHaber += $rRet->fields['monto'];
                    $this->SetX(11);
                    $rRet->movenext();
                }
            }
            if($rIva) {
                while(!$rIva->EOF) {
                    if($rIva->fields['monto'] > 0) {
                        $this->Row(array(mascara_cc($rIva->fields['codcta']), muestraFloat(0),utf8_decode(muestraFloat($rIva->fields['monto']))),11);
                        $sumaHaber += $rIva->fields['monto'];
                        $this->SetX(11);
                    }
                    $rIva->movenext();
                }
            }
            //var_dump($rAnt);
            if($rAnt->fields['codcta'] and $rAnt->fields['monto']>0) {
                $this->Row(array(mascara_cc($rAnt->fields['codcta']), utf8_decode(muestraFloat(0)),muestraFloat($rAnt->fields['monto'])),11);
                $sumaHaber += $rAnt->fields['monto'];
                $this->SetX(11);
            }


        }else {
            $qDetCon = "SELECT A.*,C.descripcion,E.id_cuenta_contable FROM finanzas.relacion_orden_pago A
                            INNER JOIN rrhh.conc_part B ON B.par_cod = A.id_partida_presupuestaria
                            INNER JOIN rrhh.concepto D ON B.conc_cod = D.int_cod AND D.conc_tipo = 1
                            INNER JOIN puser.partidas_presupuestarias C ON  C.id = A.id_partida_presupuestaria
                            INNER JOIN contabilidad.relacion_cc_pp E ON A.id_partida_presupuestaria = E.id_partida_presupuestaria
                            WHERE id_orden_pago = '".$this->objOrdenPago->nroref."'
                            GROUP BY 1,2,3,4,5,6,7,8";
            $rDetCon = $conn->Execute($qDetCon);
            //die($qDetCon);
            if(!$rDetCon->EOF) {
                //Obtenemos el monto del banco
                //Modificaco por ricardo, a la fecha 23-06-11 para incoorporar los anticipos
                //Modificacion de SQL agregando + A.montopagado para garantizar el monto a pagar en la cuenta de banco
                $qBco = "SELECT (A.montodoc - A.montoret - A.monto_anticipo + A.montopagado) AS monto, A.id_cuenta, B.codcta ";
                $qBco.= "FROM finanzas.orden_pago A ";
                $qBco.= "INNER JOIN finanzas.cuentas_bancarias C ON (C.id = A.id_cuenta) ";
                $qBco.= "INNER JOIN contabilidad.plan_cuenta B ON (C.id_plan_cuenta = B.id) ";
                $qBco.= "WHERE A.nrodoc = '".$this->objOrdenPago->nrodoc."' ";
                $rBco = $conn->Execute($qBco);
                while(!$rDetCon->EOF) {
                    $qDatosCC = "select codcta from contabilidad.plan_cuenta where id = ". $rDetCon->Fields['codcta'];
                    $rDatosCC = $conn->Execute($qDatosCC);
                    if(!$rDatosCC->EOF) {
                        $this->Row(array(mascara_cc($rDatosCC->fields['codcta']), utf8_decode(muestraFloat(0)),muestraFloat($rDetCon->fields['monto'])),11);
                    }
                }
            }
        }
        if($rBco) {
            $this->Row(array(mascara_cc($rBco->fields['codcta']), utf8_decode(muestraFloat(0)),muestraFloat($rBco->fields['monto'])),11);
            $sumaHaber += $rBco->fields['monto'];
        }
        
        $this->SetDrawColor(150,150,150);
        $this->SetFont('Arial','',8);
        $this->SetXY(5,250);
        $this->Cell(42.5,5,"ADMINISTRADOR",1,0,'C');
        $this->SetX(57.5);
        $this->Cell(42.5,5,"INTENDENTE",1,0,'C');
        $this->SetX(110);
        $this->Cell(42.5,5,"PRESUPUESTO",1,0,'C');
        $this->SetX(162.5);
        $this->Cell(42.5,5,"BENEFICIARIO",1,0,'C');
        $this->Ln(5);
        $this->SetX(5);
        $this->Cell(42.5,20,"",1,0,'C');
        $this->SetX(57.5);
        $this->Cell(42.5,20,"",1,0,'C');
        $this->SetX(110);
        $this->Cell(42.5,20,"",1,0,'C');
        $this->SetX(162.5);
        $this->Cell(42.5,20,"",1,0,'C');
        $this->SetFont('Arial','',6);
        $this->Ln(15);
        $this->SetX(5);
        $this->Cell(42.5,5,GTEADMINISTRACION,0,0,'C');
        $this->SetX(57.5);
        $this->Cell(42.5,5,INTENDENTE,0,0,'C');
        $this->SetX(110);
        $this->Cell(42.5,5,CONTROLINTERNO,0,0,'');
    }
} //Fin class


/*
 * 
 * CUERPO DEL REPORTE
 * 
 * 
 */
$pdf=new PDF();
$pdf->objOrdenPago = $oOrdenPago;
$pdf->escEnEje  = $escEnEje;
$pdf->id = $_GET['id'];
$pdf->YFinFooter = $YFinFooter;
$pdf->paginaDetalle = false;
//$pdf->SetAutoPageBreak(true, 40);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->SetLeftMargin(5);

#NOMBREE  DEL PROVEEDOR#
$oProveedor = new proveedores;
$oProveedor->get($conn, $oOrdenPago->id_proveedor);
//var_dump($oProveedor);
$oMP = new movimientos_presupuestarios;
$oMP->get($conn, $oOrdenPago->nrorefcomp, '1');

$pdf->SetFont('Arial','',8);

$pdf->Ln(6);
$pdf->SetDrawColor (150,150,150);
$pdf->SetXY(5, 52);
$pdf->Cell(140,12,utf8_decode($oOrdenPago->proveedor),'L'.'T'.'B',0,'L');
$pdf->SetXY(145, 52);
$pdf->Cell(30,12, $oProveedor->rif,'T'.'B',0,'C');
$pdf->SetXY(175, 52);
$pdf->Cell(30,12, $oProveedor->nit,'R'.'T'.'B',0,'C');
$pdf->SetXY(7, 49); 
$pdf->SetFillColor (255,255,255);
$pdf->SetDrawColor (255,255,255);
$pdf->Cell(20,4,"A favor de:",1,0,'L',1);
$pdf->Ln(20);
$pdf->SetDrawColor (150,150,150);
$pdf->Cell(200,5,"CONCEPTO",1,0,'C');
$pdf->Ln(5);
$pdf->SetDrawColor (255,255,255);
$pdf->SetWidths(array(25,20,20,75,20,20,20));			
$pdf->SetAligns(array('C','C','C','L','C','C','C'));
$pdf->Row(array("Documento", "Soporte", "Fecha", "Descripci".utf8_decode('ó')."n", "Monto", "Retenciones", "Monto"),11);
$pdf->Ln(3);
$pdf->SetAligns(array('C','C','C','L','R','R','R'));
$Total=0;
//Seccion para Nomina
$EsNomina = False;
$qEsNomina =  "select A.montodoc,A.descripcion,A.fecha as fecodp,B.nroref as doccom from finanzas.orden_pago A inner join finanzas.solicitud_pago B on A.nroref = B.nrodoc 
               WHERE substring(B.nroref from 1 for 3) = '010' and A.nrodoc = '$oOrdenPago->nrodoc'";
$rqEsNomina = $conn->Execute($qEsNomina);
//die($rqEsNomina->fields['doccom']);

if(!$rqEsNomina->EOF)
    if(substr($rqEsNomina->fields['doccom'],0,3) == '010') {
        $EsNomina = True;
        $pdf->Row(array("NM","",muestrafecha($rqEsNomina->fields['fecodc']), utf8_decode($rqEsNomina->fields['descripcion']), muestrafloat($rqEsNomina->fields['montodoc']), "", ""),11);
        $Total = $rqEsNomina->fields['montodoc'];
        $qPartNom = "SELECT A.*,C.descripcion FROM finanzas.relacion_orden_pago A
                    INNER JOIN rrhh.conc_part B ON B.par_cod = A.id_partida_presupuestaria
                    INNER JOIN rrhh.concepto D ON B.conc_cod = D.int_cod AND D.conc_tipo = 1
                    INNER JOIN puser.partidas_presupuestarias C ON  C.id = A.id_partida_presupuestaria WHERE id_orden_pago = '$oOrdenPago->nrodoc'
                    GROUP BY 1,2,3,4,5,6,7";
        //die($qPartNom);
        $rqPartNom = $conn->Execute($qPartNom);
        if($rqPartNom->RecordCount()>0)
            while(!$rqPartNom->EOF) {
                $pdf->Row(array("","","", utf8_decode($rqPartNom->fields['descripcion']), "", muestrafloat($rqPartNom->fields['monto']), ""),11);
                $Total-=$rqPartNom->fields['monto'];
                $rqPartNom->MoveNext();
            }
        //Obtengo el numero de documeto de compromiso de nomina
        $qNroSP = "SELECT A.nroref, B.nroref  FROM finanzas.orden_pago A
                   INNER JOIN finanzas.solicitud_pago B ON A.nroref = B.nrodoc
                   WHERE A.nrodoc = '$oOrdenPago->nrodoc'";
        //die($qNroSP);
        $rqNroSP = $conn->Execute($qNroSP) or die('1');
        //Obtengo id de la nomina a evaluar
        $qidNom = "SELECT int_cod, nom_fec_ini, nom_fec_fin, cont_nom, nrodoc
                   FROM rrhh.historial_nom
                   WHERE nrodoc = '".$rqNroSP->fields['nroref']."'";
        //die($qidNom);
        $rqidNom = $conn->Execute($qidNom);
        //Obtengo las deduccion de la nomina a procesar
        $qDeduccion = "SELECT conc_cod,conc_desc, sum(conc_val) as valor
                        FROM rrhh.hist_nom_tra_conc
                        WHERE hnom_cod='".$rqidNom->fields['int_cod']."' AND conc_val<>0 AND conc_tipo = 1
                        GROUP BY conc_cod,conc_desc";
        //die($qDeduccion);
        $rqDeduccion = $conn->Execute($qDeduccion);
        while(!$rqDeduccion->EOF) {
            $pdf->Row(array("","","", utf8_decode($rqDeduccion->fields['conc_desc']), "", muestrafloat($rqDeduccion->fields['valor']), ""),11);
            $Total -= $rqDeduccion->fields['valor'];
            $rqDeduccion->MoveNext();
            
        }
    }

//ODP con imputacion presupuestaria
if(!$EsNomina)
    if($oOrdenPago->id_tipo_solicitud_si==0) {

        $orden_c="SELECT COALESCE(A.id,0) As id_factura, B.nrodoc,
				A.fecha,
				B.descripcion,
				A.monto-A.descuento as monto,
				A.nrofactura,
				A.iva_retenido,
                                P.rif,
                                P.nombre
			FROM   finanzas.orden_pago AS B
				INNER Join finanzas.facturas AS A ON B.nrodoc = A.nrodoc
                                LEFT JOIN  puser.proveedores AS P ON P.id = A.id_proveedor
			WHERE B.nrodoc = '".$oOrdenPago->nrodoc."' ORDER BY A.id_proveedor";
        $r_orden_c=$conn->Execute($orden_c);
        //die($orden_c);
        while(!$r_orden_c->EOF) {       // MUESTRA EL DOCUMENTO ------
            if ($r_orden_c->fields['rif']) {
               $pdf->SetDrawColor (150,150,150);
               $pdf->ln();
               $pdf->Cell(200,4,"FACTURA DE TERCEROS: [".$r_orden_c->fields['rif']."] - ".$r_orden_c->fields['nombre'],"BT",1,"C");
               $pdf->ln();
               $pdf->SetDrawColor (255,255,255);
            }
            $desc_op = $r_orden_c->fields['descripcion'];
            $pdf->Row(array($r_orden_c->fields['nrofactura'], $oOrdenPago->nrorefcomp, muestrafecha($r_orden_c->fields['fecha']), $desc_op,  muestraFloat($r_orden_c->fields['monto']), "", ""),11);

            // MUESTRA EL IVA -----------
            if($r_orden_c->fields['iva_retenido']>0)
                $pdf->Row(array("","","", "IVA", "", muestraFloat($r_orden_c->fields['iva_retenido']), ""),11);

            // OTRAS RETENCIONES ------
            $Total+=$r_orden_c->fields['monto']-$r_orden_c->fields['iva_retenido'];
            /* Comentado por Javier Hernandez  05/07/2010
             * $q_retencion="SELECT A.id, B.mntret,A.descri,B.codret,
			               A.es_iva
                                FROM    finanzas.facturas F
					finanzas.retenciones_adiciones AS A
					INNER JOIN finanzas.relacion_retenciones_orden AS B ON B.codret = A.id
				WHERE
                                         B.nrofactura =  '".$r_orden_c->fields['nrofactura']."' AND
					 B.nrodoc = '".$oOrdenPago->nrodoc."'"; */
            
            $q_retencion="SELECT F.id, A.id, RRO.mntret, A.descri, RRO.codret, A.es_iva
                            FROM finanzas.facturas F
                            INNER JOIN finanzas.relacion_retenciones_orden RRO ON RRO.nrodoc = F.nrodoc
                               AND  (CASE WHEN F.id_proveedor > 0 THEN  F.id_proveedor || '-' || F.nrofactura ELSE F.nrofactura END)  = RRO.nrofactura
                            INNER JOIN finanzas.retenciones_adiciones AS A ON RRO.codret = A.id
                            WHERE F.nrodoc = '".$oOrdenPago->nrodoc."' AND F.id =".$r_orden_c->fields['id_factura'];
            $r_retencion=$conn->Execute($q_retencion);
            while(!$r_retencion->EOF) {
                //echo $r_retencion->fields['es_iva'].'<\br>';
                switch($r_retencion->fields['es_iva']) {
                    case '1': $nomRet = 'IVA';
                        break;
                    case '2': $nomRet = 'ISLR';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                    case '3': $nomRet = 'Impuesto Municipal';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                    case '4':
                        $nomRet = 'Impuesto Nacional';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                }
                //$r_retencion->fields['es_iva']==1?'IVA':$r_retencion->fields['es_iva']==2?'ISLR':$r_retencion->fields['es_iva']==3?'Retencion Municipal':$r_retencion->fields['es_iva']==4?'Retencion Nacional':''
                $pdf->Row(array("","", "", $nomRet, "", muestraFloat($r_retencion->fields['mntret']), ""),11);
                $Total-=$r_retencion->fields['mntret'];
                $r_retencion->movenext();
            }
            $r_orden_c->movenext();
            //Metodo para corte de pagina manual pero no el optimo
            if($pdf->GetY() >= $YFinRollover){
                //Cierre de body
                //$pdf->SetFont('Arial','B',8);
                $pdf->ln();
                $pdf->SetAligns(array('C','C','C','R','R','R','R'));
                //$pdf->Row(array("","", "", "Total a Pagar","" , "",  muestraFloat($Total)),T,11);
                $pdf->SetDrawColor (150,150,150);
                $pdf->SetXY(5, 74); //74
                $pdf->Cell(200,5,"",1,0,'C');
                $pdf->Ln(5);
                $pdf->Cell(200,110,"",1,0,'C');
                $pdf->Ln(95);
                $pdf->SetDrawColor (150,150,150);
                //$pdf->Cell(200,5,"BANCO",1,0,'C');
                //$oCB = new cuentas_bancarias;
                //$oCB->get($conn, $oOrdenPago->id_nro_cuenta);
                //die(var_dump($oCB->banco));
                $pdf->Ln(17);
                //$pdf->SetDrawColor (150,150,150);
                //$pdf->Cell(90,10,utf8_decode($oCB->banco->descripcion),'LBT',0,'L');
                //$pdf->Cell(80,10,utf8_decode($oCB->nro_cuenta),'BT',0,'C');
                //$pdf->Cell(30,10,muestraFloat($Total),'BTR',0,'R');

                //Nueva pagina
                $pdf->AddPage();
                //$pdf->SetY(30);
                $pdf->SetFont('Arial','',10);
                $pdf->SetLeftMargin(5);
                $pdf->SetFont('Arial','',8);
                $pdf->Ln(6);
                $pdf->SetDrawColor (150,150,150);
                $pdf->SetXY(5, 52);
                $pdf->Cell(140,12,utf8_decode($oOrdenPago->proveedor),'L'.'T'.'B',0,'L');
                $pdf->SetXY(145, 52);
                $pdf->Cell(30,12, $oProveedor->rif,'T'.'B',0,'C');
                $pdf->SetXY(175, 52);
                $pdf->Cell(30,12, $oProveedor->nit,'R'.'T'.'B',0,'C');
                $pdf->SetXY(7, 49); 
                $pdf->SetFillColor (255,255,255);
                $pdf->SetDrawColor (255,255,255);
                $pdf->Cell(20,4,"A favor de:",1,0,'L',1);
                $pdf->Ln(20);
                $pdf->SetDrawColor (150,150,150);
                $pdf->Cell(200,5,"CONCEPTO",1,0,'C');
                $pdf->Ln(5);
                $pdf->SetDrawColor (255,255,255);
                $pdf->SetWidths(array(25,20,20,75,20,20,20));			
                $pdf->SetAligns(array('C','C','C','L','C','C','C'));
                $pdf->Row(array("Documento", "Soporte", "Fecha", "Descripci".utf8_decode('ó')."n", "Monto", "Retenciones", "Monto"),11);
                $pdf->Ln(3);
                $pdf->SetAligns(array('C','C','C','L','R','R','R'));
            }
        }

    }else {
        //Asi estaba antes del 20/01/2010
        //echo 'entro else<\br>';
        //$pdf->Row(array('', '', muestrafecha($oOrdenPago->fecha), $oOrdenPago->descripcion,  muestraFloat($oOrdenPago->montodoc), "", ""),11);
        //$Total+=$oOrdenPago->montodoc-$oOrdenPago->montoret;
        //Modificacion del 20/01/2010
        $orden_c="SELECT  COALESCE(A.id,0) As id_factura,
                          B.nrodoc,
                          B.fecha,
                          B.descripcion,
                          B.monto_si as monto,
                          B.nrodoc_anticipo,
                          0 as iva_retenido
                  FROM   finanzas.orden_pago AS B
                  LEFT JOIN finanzas.facturas AS A ON B.nrodoc = A.nrodoc
                  WHERE  B.nrodoc = '".$oOrdenPago->nrodoc."'";
        //die($orden_c);
        $r_orden_c=$conn->Execute($orden_c);

        while(!$r_orden_c->EOF) {
            // MUESTRA EL MONTO DE LA ORDEN DE PAGO
            $pdf->Row(array("-", $r_orden_c->fields['nrodoc_anticipo'], muestrafecha($r_orden_c->fields['fecha']), $r_orden_c->fields['descripcion'],  muestraFloat($r_orden_c->fields['monto']), "", ""),11);
            // ANEXA LAS RETENCIONES DE LA FACTURA
            $pdf->Row(array("","","", "IVA", "", muestraFloat($r_orden_c->fields['iva_retenido']), ""),11);

            $Total+=$r_orden_c->fields['monto']-$r_orden_c->fields['iva_retenido'];

        /*  Comentado por Javier Hernandez  05/07/2010
         * $q_retencion="	SELECT B.mntret, A.descri,
				       B.codret, A.es_iva
				FROM
                                        finanzas.retenciones_adiciones AS A
                                INNER JOIN finanzas.relacion_retenciones_orden AS B ON B.codret = A.id
				WHERE B.nrofactura =  '".$r_orden_c->fields['nrofactura']."' AND
                                      B.nrodoc = '".$oOrdenPago->nrodoc."'";
            */
            $q_retencion="SELECT F.id, A.id, RRO.mntret, A.descri,
                                RRO.codret, A.es_iva
                            FROM finanzas.facturas F
                            INNER JOIN finanzas.relacion_retenciones_orden RRO ON RRO.nrodoc = F.nrodoc
                               AND  (CASE WHEN F.id_proveedor > 0 THEN  F.id_proveedor || '-' || F.nrofactura ELSE F.nrofactura END)  = RRO.nrofactura
                            INNER JOIN finanzas.retenciones_adiciones AS A ON RRO.codret = A.id
                            WHERE F.nrodoc = '".$oOrdenPago->nrodoc."' AND F.id = ".$r_orden_c->fields['id_factura'];
            
            $r_retencion=$conn->Execute($q_retencion);
            while(!$r_retencion->EOF) {
                switch($r_retencion->fields['es_iva']) {
                    case '1': $nomRet = 'IVA';
                        break;
                    case '2': $nomRet = 'ISLR';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                    case '3':
                        $nomRet = 'Impuesto Municipal';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                    case '4':
                        $nomRet = 'Impuesto Nacional';
                        $nomRet = $r_retencion->fields['descri'];
                        break;
                }
                //$r_retencion->fields['es_iva']==1?'IVA':$r_retencion->fields['es_iva']==2?'ISLR':$r_retencion->fields['es_iva']==3?'Retencion Municipal':$r_retencion->fields['es_iva']==4?'Retencion Nacional':''
                $pdf->Row(array("","", "", $nomRet, "", muestraFloat($r_retencion->fields['mntret']), ""),11);
                $Total-=$r_retencion->fields['mntret'];
                $r_retencion->movenext();
            }
            $r_orden_c->movenext();
        }


    }
if($oOrdenPago->montoanticipo > 0 ) {
    $pdf->Row(array("","", "", utf8_decode("Amortización Anticipo"), "", muestraFloat($oOrdenPago->montoanticipo), ""),11);
    $Total-= $oOrdenPago->montoanticipo;
}
//echo 'salio<\br>';
//$pdf->Line();
$pdf->SetFont('Arial','B',8);
$pdf->SetAligns(array('C','C','C','R','R','R','R'));
$pdf->Row(array("","", "", "Total a Pagar","" , "",  muestraFloat($Total)),T,11);
$pdf->SetDrawColor (150,150,150);
$pdf->SetXY(5, 74);
$pdf->Cell(200,5,"",1,0,'C');
$pdf->Ln(5);
$pdf->Cell(200,90,"",1,0,'C');
$pdf->Ln(95);
$pdf->SetDrawColor (150,150,150);
$pdf->Cell(200,5,"BANCO",1,0,'C');
$oCB = new cuentas_bancarias;
$oCB->get($conn, $oOrdenPago->id_nro_cuenta);
//die(var_dump($oCB->banco));
$pdf->Ln(5);
$pdf->SetDrawColor (150,150,150);
$pdf->Cell(90,10,utf8_decode($oCB->banco->descripcion),'LBT',0,'L');
$pdf->Cell(80,10,utf8_decode($oCB->nro_cuenta),'BT',0,'C');
$pdf->Cell(30,10,muestraFloat($Total),'BTR',0,'R');

$pdf->Ln(12);

//Verifico si las pp entran en el cuadro de texto del formato
$oRelacion = orden_pago::getRelacionPartidas($conn,$_GET['id'],$escEnEje);
$cpp =0;
foreach($oRelacion as $relacion){
    $cpp++;
}
if($cpp > $YFinFooter and !$pdf->paginaDetalle){
    //echo('aqui');
    $pdf->paginaDetalle = true;
    $pdf->AddPage();
}
$pdf->Output();

?>
