<?
include("comun/ini.php");
include ("Constantes.php");

$tipo   = $_GET['tipo'];
$anio   = $_GET['anio'];
$mes    = $_GET['mes'];
$detallado = $_GET['detallado'];

$sql = "select d.id, c.tdebe,c.thaber,d.numcom,d.descrip,d.fecha,d.origen,d.status,d.transferido,d.num_doc from (
        select id_com,sum(debe) as tdebe,sum(haber) as thaber from contabilidad.com_det a
        inner join contabilidad.com_enc b on a.id_com = b.id and b.ano = $anio and b.mes = $mes and b.id_escenario = 1111
        group by id_com) c
        inner join contabilidad.com_enc d on c.id_com = d.id
        where c.tdebe <> c.thaber";
//die($sql);
$r = $conn->Execute($sql);

function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
  }

class PDF extends FPDF
{
	var $anio;
	var $mes;
	var $tipo;
	
	// Definen el ancho de las celdas en el balance de comprobacion
	var $fecDW      = 20;
	var $numDW      = 25;
	var $desDW      = 50;
	var $tipDW      = 15;
	var $refDW      = 20;
        var $debDW      = 25;
	var $habDW      = 25;

	//Cabecera de pÃ¡gina
	function Header()
	{
			$this->SetFillColor(240);
			$this->SetLeftMargin(15);
			$this->SetFont('Courier','',6);
			$this->Ln(1);
			$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda 
			$this->SetXY(42, 6);
			$textoCabecera = PAIS."\n";
			$textoCabecera.= ENTE."\n";
                        $textoCabecera.= UBICACION."\n";
			$this->MultiCell(50,2, $textoCabecera, 0, 'L');

			$this->SetXY(163, 6);
			$textoDerecha = "Fecha: ".date('d/m/Y')."\n";
			$textoDerecha.= "Pag: ".$this->PageNo()." de {nb}\n";
			$this->MultiCell(50,2, $textoDerecha, 0, 'L');
			
			$this->Ln(12);
			$this->SetFont('Courier','b',12);
			if ($this->tipo == 'CNB')
				$titulo = "Comprobantes No Balanceados del mes ".sprintf("%02d", $this->mes)." del ".$this->anio;
			
			$this->MultiCell(0, 2, utf8_decode($titulo), 0, 'C');
			$this->SetLeftMargin(10);
                        $this->Ln(5);
                        $this->setFont('Courier', 'B', 7);
                        $this->Cell($this->fecDW, 4, utf8_decode('Fecha'), 0, '', 'L', 1);
                        $this->Cell($this->numDW, 4, utf8_decode('# Comprobante'), 0, '', 'L', 1);
                        $this->Cell($this->desDW, 4, utf8_decode('Descripción'), 0, '', 'L', 1);
                        $this->Cell($this->tipDW, 4, utf8_decode('Tipo'), 0, '', 'C', 1);
                        $this->Cell($this->refDW, 4, utf8_decode('Referencia'), 0, '', 'C', 1);
                        $this->Cell($this->debDW, 4, utf8_decode('Total Debe'), 0, '', 'C', 1);
                        $this->Cell($this->habDW, 4, utf8_decode('Total Haber'), 0, '', 'C', 1);
                        $this->Ln();
                        $this->Line(10, $this->GetY(), 198, $this->GetY());
                        $this->Ln();
	}

	//Pie de pÃ¡gina
	function Footer()
	{
		$this->SetFont('Arial','I', 7);
	}


}

//CreaciÃ³n del objeto de la clase heredada
$pdf=new PDF();
$pdf->anio  = $anio;
$pdf->mes   = $mes;
$pdf->tipo  = $tipo;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','', 7);
$pdf->SetLeftMargin(10);
$pdf->SetFillColor(240);
$TotalDebe  = 0;
$TotalHaber = 0;
$fill       = 0;
$imprime    = false;

	while (!$r->EOF)
	{
		$descCta = dividirStr(utf8_decode($r->fields['descrip']), intval($pdf->desDW/$pdf->GetStringWidth('M')));
                $hayCta  = next($descCta);
                if($fill == 0)
		   $fill = 1;
                else
                   $fill = 0;
                //Proceso de impresion de reporte
                $imprime = true;
                $pdf->SetX(10);
                $pdf->Cell($pdf->fecDW, 4, muestrafecha($r->fields['fecha']), 0, '', 'L', $fill);
                $pdf->Cell($pdf->numDW, 4, $r->fields['numcom'], 0, '', 'C', $fill);
                $pdf->Cell($pdf->desDW, 4, $descCta[0], 0, '', 'L', $fill);
                $pdf->Cell($pdf->tipDW, 4, $r->fields['origen'], 0, '', 'C', $fill);
                $pdf->Cell($pdf->refDW, 4, $r->fields['num_doc'], 0, '', 'C', $fill);
                $pdf->Cell($pdf->debDW, 4, $r->fields['tdebe'], 0, '', 'R', $fill);
                $pdf->Cell($pdf->habDW, 4, $r->fields['thaber'], 0, '', 'R', $fill);
                $TotalDebe  += $r->fields['tdebe'];
                $TotalHaber += $r->fields['thaber'];

		for ($i=1; $hayCta!=false; $i++)
		{
			$pdf->Ln();
			$pdf->Cell($pdf->fecDW, 4, '', 0, '', '', $fill);
                        $pdf->Cell($pdf->numDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->desDW, 4, $descCta[$i], 0, '', '', $fill);
			$pdf->Cell($pdf->tipDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->refDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->debDW, 4, '', 0, '', '', $fill);
                        $pdf->Cell($pdf->habDW, 4, '', 0, '', '', $fill);
			$hayCta = next($descCta);
		}

                $pdf->ln(4);
		$r->movenext();

	}

$pdf->ln();
$pdf->Line(10, $pdf->GetY(), 198, $pdf->GetY());
$pdf->ln();
//Total
if($imprime)
   $pdf->Cell(180, 4,'Totales: '. number_format($TotalDebe,3,'.',',').'         '.number_format($TotalHaber,3,'.',','), 0, 'B', 'R', $fill);
else
   $pdf->Cell(180, 4,'NO HAY COMPROBANTES DESBALANCEADOS', 0, 'B', 'C', $fill);
$pdf->ln();
$pdf->Line(10, $pdf->GetY(), 198, $pdf->GetY());

$pdf->Output();
?>
