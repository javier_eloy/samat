
CREATE OR REPLACE FUNCTION public.asiento_cheque(nrodoc character varying, opcion smallint, escenario bigint)
  RETURNS smallint AS
$BODY$
DECLARE
	encabezado RECORD;
	retenciones RECORD;
	retencionesnomina RECORD;
	aportesnomina RECORD;
	retencion_anticipo RECORD;
	retenciones_iva RECORD;
	totalret numeric;
	totaliva numeric;
	id_comenc numeric;
	id_cta_prov numeric;
	num_com numeric;
	var_anio int;
	var_mes int;
	fec_anu date;


BEGIN

	-- Selecciona el ultimo correlativo del numero de comprobante (numero del asiento)
	SELECT INTO encabezado COALESCE(substring(numcom from 9 for 5 )::numeric,0)::int as numcom
	FROM contabilidad.com_enc WHERE id = (SELECT MAX(id) FROM contabilidad.com_enc);

 	IF (NOT FOUND) THEN
		num_com := 1;
	ELSE
		num_com := encabezado.numcom + 1;
	END IF;

-- Cheque Eliminando retenciones ================================================================================
IF (opcion = 9) THEN

	-- Obtiene los datos del cheque
	SELECT INTO encabezado cheques.fecha as fec_emi, cheques.id_escenario, cheques.status, cheques.nro_cuenta, cheques.nrodoc, cheques.id_proveedor, cheques.nro_cheque FROM finanzas.cheques WHERE cheques.nrodoc = nrodoc;

        -- Obtiene el id del proveedor --- JAVIER HERNANDEZ
        SELECT proveedores.cta_contable INTO id_cta_prov  FROM puser.proveedores WHERE proveedores.id = encabezado.id_proveedor;

	var_mes := to_char(encabezado.fec_emi,'mm')::int2;
	var_anio := to_char(encabezado.fec_emi,'yyyy')::int2;

	-- Cheque Anulado que no conserva retenciones ------------------------------------------------------------------------
	IF (encabezado.status = 1) THEN

		-- Guarda el encabezado del asiento
	   	SELECT INTO fec_anu TO_CHAR(now(),'dd/mm/yyyy');
                -- Modificacion necesaria para transcripcion de data vieja. Comentar esta linea para trabajo actualizado
                fec_anu=encabezado.fec_emi;

		-- Se agrega cabecera del comprobante contable
		INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, status, transferido,num_doc,num_doc2)
		VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
		'Asiento de Cheque Anulado con Retenciones', fec_anu, 'CHQ', 'A', 'S', encabezado.nrodoc, encabezado.nro_cheque);

		-- Valido si la cabecera fue agrega en la tabla correctamente
		IF FOUND THEN

			-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
			SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc;-- WHERE origen='CHQ' And transferido='S'; --Cambio de origen=OP a CHQ

			-- Disminuye la cuenta contable del proveedor principal (detalle del asiento) --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			SELECT id_comenc, id_cta_prov, 0, COALESCE(SUM(monto)::float,0)
				FROM
				(SELECT sum(ch.monto) As monto  FROM finanzas.relacion_cheque ch WHERE ch.nrodoc = nrodoc::varchar(8)
				UNION
				SELECT SUM(f.iva_retenido) AS monto FROM finanzas.facturas as f
				INNER JOIN finanzas.relacion_cheque as rc ON  rc.nroref = f.nrodoc
				WHERE COALESCE(id_proveedor,0) = 0 AND rc.nrodoc= nrodoc::varchar(8)
				UNION
				SELECT SUM(rro.mntret) AS monto
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND fc.nrofactura = rro.nrofactura
				INNER JOIN finanzas.relacion_cheque rc ON rc.nroref = fc.nrodoc
				WHERE rc.nrodoc =  nrodoc::varchar(8)) As total;

			-- Disminuye la cuenta contable del proveedor con facturas de terceros  --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			SELECT id_comenc, total_terceros.cta_contable, 0, COALESCE(SUM(total_terceros.montoret)::float,0)
				FROM
				(SELECT p.cta_contable,SUM(f.iva_retenido) As montoret FROM finanzas.facturas as f
				INNER JOIN puser.proveedores p ON p.id = f.id_proveedor
				WHERE f.nrodoc IN (SELECT DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc= nrodoc::varchar(8))
				AND COALESCE(id_proveedor,0) <> 0
				GROUP BY p.cta_contable
				UNION
				SELECT p.cta_contable,SUM(mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND  fc.id_proveedor || '-'|| fc.nrofactura = rro.nrofactura
				INNER JOIN puser.proveedores p ON p.id = fc.id_proveedor
				WHERE rro.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc::varchar(8))
				GROUP BY p.cta_contable) As total_terceros
				GROUP BY total_terceros.cta_contable;

			-- Aumenta el saldo  de las cuentas contables de las retenciones en los detalles del asiento
			<<retencioness>>
			FOR retenciones IN SELECT a.codret, a.mntret as monto FROM finanzas.relacion_retenciones_orden as a WHERE a.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque AS rc WHERE rc.nrodoc =  nrodoc) LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen='CHQ' And transferido='S'),   --Cambio de origen=OP a CHQ
                 		(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones.codret), retenciones.monto, 0);
			END LOOP retencioness;

			<<retencioness_iva>>
			FOR retenciones_iva IN	SELECT f.id_retencion, f.iva_retenido as monto FROM finanzas.facturas as f WHERE f.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc) LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES (id_comenc,  --Cambio de origen=OP a CHQ
                 		(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones_iva.id_retencion), retenciones_iva.monto, 0);
			END LOOP retencioness_iva;

			-- Disminuye la cuenta bancaria (detalle del asiento)
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			VALUES (id_comenc,
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta),
				(SELECT sum(relacion_cheque.monto) as monto FROM finanzas.relacion_cheque WHERE relacion_cheque.nrodoc = nrodoc),0);

		ELSE
			--Sino se inserta la cabecera del comprobante se genera una excepcion
			RAISE EXCEPTION 'No se puede generar asiento contable(1).';
			RETURN 0;
		END IF; --FIN DE FOUND

	--Emito cheque con retenciones
	ELSIF  (encabezado.status = 0) THEN

		-- Guarda el encabezado del asiento
	   	SELECT INTO fec_anu TO_CHAR(now(),'dd/mm/yyyy');
                fec_anu=encabezado.fec_emi;


		-- Se agrega cabecera del comprobante contable
		INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, status, transferido,num_doc,num_doc2)
		VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
		'Asiento de Cheque Emitido con Retenciones', fec_anu, 'CHQ', 'R', 'S', encabezado.nrodoc, encabezado.nro_cheque);

		-- Valido si la cabecera fue agrega en la tabla correctamente
		IF FOUND THEN

			-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
			SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc;-- WHERE origen='CHQ' And transferido='S'; --Cambio de origen=OP a CHQ

			-- Disminuye la cuenta contable del proveedor principal (detalle del asiento) --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			SELECT id_comenc, id_cta_prov, COALESCE(SUM(monto)::float,0),0
				FROM
				(SELECT sum(ch.monto) As monto  FROM finanzas.relacion_cheque ch WHERE ch.nrodoc = nrodoc::varchar(8)
				UNION
				SELECT SUM(f.iva_retenido) AS monto FROM finanzas.facturas as f
				INNER JOIN finanzas.relacion_cheque as rc ON  rc.nroref = f.nrodoc
				WHERE COALESCE(id_proveedor,0) = 0 AND rc.nrodoc= nrodoc::varchar(8)
				UNION
				SELECT SUM(rro.mntret) AS monto
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND fc.nrofactura = rro.nrofactura
				INNER JOIN finanzas.relacion_cheque rc ON rc.nroref = fc.nrodoc
				WHERE rc.nrodoc =  nrodoc::varchar(8)) As total;

			-- Disminuye la cuenta contable del proveedor con facturas de terceros  --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			SELECT id_comenc, total_terceros.cta_contable, COALESCE(SUM(total_terceros.montoret)::float,0), 0
				FROM
				(SELECT p.cta_contable,SUM(f.iva_retenido) As montoret FROM finanzas.facturas as f
				INNER JOIN puser.proveedores p ON p.id = f.id_proveedor
				WHERE f.nrodoc IN (SELECT DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc= nrodoc::varchar(8))
				AND COALESCE(id_proveedor,0) <> 0
				GROUP BY p.cta_contable
				UNION
				SELECT p.cta_contable,SUM(mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND  fc.id_proveedor || '-'|| fc.nrofactura = rro.nrofactura
				INNER JOIN puser.proveedores p ON p.id = fc.id_proveedor
				WHERE rro.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc::varchar(8))
				GROUP BY p.cta_contable) As total_terceros
				GROUP BY total_terceros.cta_contable;

			-- Aumenta el saldo  de las cuentas contables de las retenciones en los detalles del asiento
			<<retencioness>>
			FOR retenciones IN SELECT a.codret, a.mntret as monto FROM finanzas.relacion_retenciones_orden as a WHERE a.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque AS rc WHERE rc.nrodoc =  nrodoc) LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen='CHQ' And transferido='S'),   --Cambio de origen=OP a CHQ
                 		(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones.codret), 0, retenciones.monto);
			END LOOP retencioness;

			<<retencioness_iva>>
			FOR retenciones_iva IN	SELECT f.id_retencion, f.iva_retenido as monto FROM finanzas.facturas as f WHERE f.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc) LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES (id_comenc,  --Cambio de origen=OP a CHQ
                 		(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones_iva.id_retencion), 0, retenciones_iva.monto);
			END LOOP retencioness_iva;

			-- Disminuye la cuenta bancaria (detalle del asiento)
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
			VALUES (id_comenc,
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta),
				0,(SELECT sum(relacion_cheque.monto) as monto FROM finanzas.relacion_cheque WHERE relacion_cheque.nrodoc = nrodoc));
		ELSE
			--Sino se inserta la cabecera del comprobante se genera una excepcion
			RAISE EXCEPTION 'No se puede generar asiento contable(2).';
			RETURN 0;
		END IF;
        END IF;
--
--
-- Cheque Emision sin Eliminar retenciones ===========================================================================================
ELSIF  (opcion = 0) THEN

		-- Obtiene los datos del cheque
		SELECT INTO encabezado cheques.fecha as fec_emi, cheques.id_escenario, cheques.status, cheques.nro_cuenta, cheques.nrodoc, cheques.id_proveedor, cheques.nro_cheque FROM finanzas.cheques WHERE cheques.nrodoc = nrodoc;

		-- Obtiene el id del proveedor --- JAVIER HERNANDEZ
		SELECT proveedores.cta_contable INTO id_cta_prov  FROM puser.proveedores WHERE proveedores.id = encabezado.id_proveedor;


		var_mes := to_char(encabezado.fec_emi,'mm')::int2;
		var_anio := to_char(encabezado.fec_emi,'yyyy')::int2;

		-- Cheque Emitido ----------------------------------------------------------------------
		IF (encabezado.status = 0) THEN
			-- Guarda el encabezado del asiento
			INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, transferido, num_doc,num_doc2)
			VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
			'Asiento de Cheque Emitido', encabezado.fec_emi, 'CHQ', 'S', encabezado.nrodoc, encabezado.nro_cheque);

			IF FOUND THEN
				-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
				SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc WHERE origen='CHQ' And transferido='S'; --Cambio de origen=OP a CHQ

				-- Disminuye la cuenta contable del proveedor principal (detalle del asiento) --- JAVIER HERNANDEZ
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				SELECT id_comenc, id_cta_prov, COALESCE(SUM(monto)::float,0), 0
				FROM
				(SELECT sum(ch.monto) As monto  FROM finanzas.relacion_cheque ch WHERE ch.nrodoc = nrodoc::varchar(8)
				/*UNION
				SELECT SUM(f.iva_retenido) AS montoret FROM finanzas.facturas as f
				INNER JOIN finanzas.relacion_cheque as rc ON  rc.nroref = f.nrodoc
				WHERE COALESCE(id_proveedor,0) = 0 AND rc.nrodoc= nrodoc::varchar(8)
				UNION
				SELECT 0 as monto,SUM(rro.mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND fc.nrofactura = rro.nrofactura
				INNER JOIN finanzas.relacion_cheque rc ON rc.nroref = fc.nrodoc
				WHERE rc.nrodoc =  nrodoc::varchar(8)*/
				) As total;

				-- Disminuye la cuenta contable del proveedor con facturas de terceros  --- JAVIER HERNANDEZ
				--INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				--SELECT id_comenc, total_terceros.cta_contable, COALESCE(SUM(total_terceros.montoret)::float,0),0
				--FROM
				--(SELECT p.cta_contable,SUM(f.iva_retenido) As montoret FROM finanzas.facturas as f
				--INNER JOIN puser.proveedores p ON p.id = f.id_proveedor
				--WHERE f.nrodoc IN (SELECT DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc= nrodoc::varchar(8))
				--AND COALESCE(id_proveedor,0) <> 0
				--GROUP BY p.cta_contable
				--UNION
				--SELECT p.cta_contable,SUM(mntret) AS montoret
				--FROM finanzas.relacion_retenciones_orden rro
				--INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND  fc.id_proveedor || '-'|| fc.nrofactura = rro.nrofactura
				--INNER JOIN puser.proveedores p ON p.id = fc.id_proveedor
				--WHERE rro.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc::varchar(8))
				--GROUP BY p.cta_contable) As total_terceros
				/*GROUP BY total_terceros.cta_contable;

				-- Aumenta el saldo  de las cuentas contables de las retenciones en los detalles del asiento
				<<retencioness>>
				FOR retenciones IN SELECT a.codret, a.mntret as monto FROM finanzas.relacion_retenciones_orden as a WHERE a.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque AS rc WHERE rc.nrodoc =  nrodoc)
				LOOP
					INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
					VALUES (id_comenc, (SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones.codret), 0, retenciones.monto);
				END LOOP retencioness;

				<<retencioness_iva>>
				FOR retenciones_iva IN	SELECT f.id_retencion, f.iva_retenido as monto FROM finanzas.facturas as f WHERE f.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_cheque as rc WHERE rc.nrodoc =  nrodoc)
				LOOP
					INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
					VALUES (id_comenc, (SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones_iva.id_retencion),             retenciones_iva.monto);
				END LOOP retencioness_iva;
*/
				-- Disminuye la cuenta bancaria (detalle del asiento)
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES (id_comenc,(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta),0,
					(SELECT sum(relacion_cheque.monto) as monto FROM finanzas.relacion_cheque WHERE relacion_cheque.nrodoc = nrodoc));

			ELSE
				--Sino se inserta la cabecera del comprobante se genera una excepcion
				RAISE EXCEPTION 'No se puede generar asiento contable(3).';
				RETURN 0;
			END IF;

		-- Cheque Anulado que conserva retenciones ----------------------------------------------
		ELSIF (encabezado.status = 1) THEN
			-- Guarda el encabezado del asiento
			SELECT INTO fec_anu TO_CHAR(now(),'dd/mm/yyyy');
                        fec_anu = encabezado.fec_emi;

			INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, status, transferido,num_doc,num_doc2)
			VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
			'Asiento de Cheque Anulado', fec_anu, 'CHQ', 'A', 'S', encabezado.nrodoc, encabezado.nro_cheque);

			IF FOUND THEN
				-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
				SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc WHERE origen='CHQ' And transferido='S'; --Cambio de origen=OP a CHQ

				-- Aumenta la cuenta bancaria (detalle del asiento)
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES (id_comenc,
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta),
				(SELECT sum(relacion_cheque.monto) as monto FROM finanzas.relacion_cheque WHERE relacion_cheque.nrodoc = nrodoc), 0);

				-- Aumenta la cuenta contable del proveedor (detalle del asiento)
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES (id_comenc,
				(SELECT proveedores.cta_contable FROM puser.proveedores WHERE proveedores.id = encabezado.id_proveedor), 0,
				(SELECT sum(relacion_cheque.monto) as monto FROM finanzas.relacion_cheque WHERE relacion_cheque.nrodoc = nrodoc));
			ELSE
				--Sino se inserta la cabecera del comprobante se genera una excepcion
				RAISE EXCEPTION 'No se puede generar asiento contable(4).';
				RETURN 0;
			END IF;
		END IF;

	-- Otros Pagos ================================================================================================
	ELSIF (opcion = 1) THEN
		-- Obtiene los datos del pago
		SELECT INTO encabezado otros_pagos.fecha as fec_emi, otros_pagos.id_escenario, otros_pagos.status, otros_pagos.nro_cuenta, otros_pagos.nrodoc, otros_pagos.id_proveedor, otros_pagos.nro_control FROM finanzas.otros_pagos
		WHERE otros_pagos.nrodoc = nrodoc;

		-- Obtiene el id del proveedor --- JAVIER HERNANDEZ
		SELECT proveedores.cta_contable INTO id_cta_prov  FROM puser.proveedores WHERE proveedores.id = encabezado.id_proveedor;

		var_mes := to_char(encabezado.fec_emi,'mm')::int2;
		var_anio := to_char(encabezado.fec_emi,'yyyy')::int2;

		-- Otro Pago Emitido --------------------------------------------------------------------------------------
		IF (encabezado.status = 0) THEN
			-- Guarda el encabezado del asiento
			INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, transferido, num_doc,num_doc2)
			VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
			'Asiento de Otro Pago Emitido', encabezado.fec_emi, 'TRA', 'S', encabezado.nrodoc,encabezado.nro_control);

 		 	-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
			SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc;-- WHERE origen='TRA' And transferido='S';

		    IF FOUND THEN
			-- Aumenta la cuenta contable del proveedor principal (detalle del asiento) --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				SELECT id_comenc, id_cta_prov, COALESCE(SUM(monto)::float,0), 0
				FROM
				(SELECT sum(ch.monto) As monto FROM finanzas.relacion_otros_pagos ch WHERE ch.nrodoc = encabezado.nrodoc::varchar(8)
				UNION
				SELECT SUM(f.iva_retenido) AS monto FROM finanzas.facturas as f
				INNER JOIN finanzas.relacion_otros_pagos as rc ON  rc.nroref = f.nrodoc
				WHERE COALESCE(id_proveedor,0) = 0 AND rc.nrodoc= nrodoc::varchar(8)
				UNION
				SELECT SUM(rro.mntret) AS monto
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND fc.nrofactura = rro.nrofactura
				INNER JOIN finanzas.relacion_otros_pagos rc ON rc.nroref = fc.nrodoc
				WHERE rc.nrodoc =  nrodoc::varchar(8)) As total;

			-- Disminuye la cuenta bancaria (detalle del asiento)
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES (id_comenc,
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta), 0,
				(SELECT sum(relacion_otros_pagos.monto)::float as monto FROM finanzas.relacion_otros_pagos WHERE finanzas.relacion_otros_pagos.nrodoc = encabezado.nrodoc));

			-- Disminuye la cuenta contable del proveedor con facturas de terceros  --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				SELECT id_comenc, total_terceros.cta_contable, COALESCE(SUM(total_terceros.montoret)::float,0), 0
				FROM
				(SELECT p.cta_contable,SUM(f.iva_retenido) As montoret FROM finanzas.facturas as f
				INNER JOIN puser.proveedores p ON p.id = f.id_proveedor
				WHERE f.nrodoc IN (SELECT DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc= encabezado.nrodoc::varchar(8))
				AND COALESCE(id_proveedor,0) <> 0
				GROUP BY p.cta_contable
				UNION
				SELECT p.cta_contable,SUM(mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND  fc.id_proveedor || '-'|| fc.nrofactura = rro.nrofactura
				INNER JOIN puser.proveedores p ON p.id = fc.id_proveedor
				WHERE rro.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc =  encabezado.nrodoc::varchar(8))
				GROUP BY p.cta_contable) As total_terceros
				GROUP BY total_terceros.cta_contable;

			-- Aumenta el saldo  de las cuentas contables de las retenciones en los detalles del asiento
			<<retencioness>>
			FOR retenciones IN SELECT a.codret, a.mntret as monto FROM finanzas.relacion_retenciones_orden as a WHERE a.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos AS rc WHERE rc.nrodoc =  encabezado.nrodoc)
			LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES (id_comenc,
				       (SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones.codret),
					0, retenciones.monto);
			END LOOP retencioness;

			<<retencioness_iva>>
			FOR retenciones_iva IN	SELECT f.id_retencion, f.iva_retenido as monto FROM finanzas.facturas as f WHERE f.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc =  encabezado.nrodoc)
			LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
				VALUES (id_comenc,  --Cambio de origen=OP a CHQ
			           (SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones_iva.id_retencion),
				   0, retenciones_iva.monto);
			END LOOP retencioness_iva;
		   ELSE
			--Sino se inserta la cabecera del comprobante se genera una excepcion
			RAISE EXCEPTION 'No se puede generar asiento contable(5).';
			RETURN 0;
		   END IF;


		-- Otro Pago Anulado -------------------------------------------------------------------------------------
		ELSIF (encabezado.status = 1) THEN
			-- Guarda el encabezado del asiento
			SELECT INTO fec_anu TO_CHAR(now(),'dd/mm/yyyy');
                        fec_anu = encabezado.fec_emi;

			INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, status, transferido,num_doc,num_doc2)
			VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
			'Asiento de Otro Pago Anulado', fec_anu, 'TRA', 'A', 'S', encabezado.nrodoc,encabezado.nro_control);

		    IF FOUND THEN
			-- Obtiene el ID del encabezado para posterior uso -- JAVIER HERNANDEZ
			SELECT MAX(id) INTO id_comenc FROM contabilidad.com_enc; -- WHERE origen='TRA' And transferido='S';

			-- Aumenta la cuenta bancaria (detalle del asiento)
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen='TRA' And transferido='S'),
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.nro_cuenta),
				(SELECT sum(relacion_otros_pagos.monto) as monto FROM finanzas.relacion_otros_pagos WHERE relacion_otros_pagos.nrodoc = nrodoc), 0);


			-- Disminuye la cuenta contable del proveedor con facturas de terceros  --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				SELECT id_comenc, total_terceros.cta_contable, 0, COALESCE(SUM(total_terceros.montoret)::float,0)
				FROM
				(SELECT p.cta_contable,SUM(f.iva_retenido) As montoret FROM finanzas.facturas as f
				INNER JOIN puser.proveedores p ON p.id = f.id_proveedor
				WHERE f.nrodoc IN (SELECT DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc= nrodoc::varchar(8))
				AND COALESCE(id_proveedor,0) <> 0
				GROUP BY p.cta_contable
				UNION
				SELECT p.cta_contable,SUM(mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND  fc.id_proveedor || '-'|| fc.nrofactura = rro.nrofactura
				INNER JOIN puser.proveedores p ON p.id = fc.id_proveedor
				WHERE rro.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc =  nrodoc::varchar(8))
				GROUP BY p.cta_contable) As total_terceros
				GROUP BY total_terceros.cta_contable;

			-- Aumenta el saldo  de las cuentas contables de las retenciones en los detalles del asiento
			<<retencioness>>
			FOR retenciones IN SELECT a.codret, a.mntret as monto FROM finanzas.relacion_retenciones_orden as a WHERE a.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos AS rc WHERE rc.nrodoc =  nrodoc)
			LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
					VALUES (id_comenc,--(SELECT MAX(id) FROM contabilidad.com_enc WHERE origen='TRA' And transferido='S'),   --Cambio de origen=OP a CHQ
					(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones.codret), retenciones.monto, 0);
			END LOOP retencioness;

			<<retencioness_iva>>
			FOR retenciones_iva IN	SELECT f.id_retencion, f.iva_retenido as monto FROM finanzas.facturas as f WHERE f.nrodoc IN (SELECT  DISTINCT rc.nroref FROM finanzas.relacion_otros_pagos as rc WHERE rc.nrodoc =  nrodoc)
			LOOP
				INSERT INTO contabilidad.com_det(id_com, id_cta, debe, haber)
					VALUES (id_comenc,  --Cambio de origen=OP a CHQ
					(SELECT a.id_cta FROM finanzas.retenciones_adiciones as a WHERE a.id = retenciones_iva.id_retencion), retenciones_iva.monto, 0);
			END LOOP retencioness_iva;

			-- Disminuye la cuenta contable del proveedor principal (detalle del asiento) --- JAVIER HERNANDEZ
			INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				SELECT id_comenc, id_cta_prov, 0, COALESCE(SUM(monto)::float,0)
				FROM
				(SELECT sum(ch.monto) As monto  FROM finanzas.relacion_otros_pagos ch WHERE ch.nrodoc = nrodoc::varchar(8)
				UNION
				SELECT SUM(f.iva_retenido) AS montoret FROM finanzas.facturas as f
				INNER JOIN finanzas.relacion_otros_pagos as rc ON  rc.nroref = f.nrodoc
				WHERE COALESCE(id_proveedor,0) = 0 AND rc.nrodoc= nrodoc::varchar(8)
				UNION
				SELECT SUM(rro.mntret) AS montoret
				FROM finanzas.relacion_retenciones_orden rro
				INNER JOIN finanzas.facturas fc ON fc.nrodoc = rro.nrodoc AND fc.nrofactura = rro.nrofactura
				INNER JOIN finanzas.relacion_otros_pagos rc ON rc.nroref = fc.nrodoc
				WHERE rc.nrodoc =  nrodoc::varchar(8)) As total;
		   ELSE
			--Sino se inserta la cabecera del comprobante se genera una excepcion
			RAISE EXCEPTION 'No se puede generar asiento contable(5).';
			RETURN 0;
		   END IF;
		END IF;
	--Transferencias bancarias ========================================================================================
	ELSIF(opcion=2) THEN
			-- Obtiene los datos de la transferencia
			SELECT INTO encabezado transferencias.fecha as fec_emi, transferencias.id_escenario, transferencias.origen, transferencias.status, transferencias.nrodoc, transferencias.descripcion, transferencias.id_cuenta_cedente, transferencias.id_cuenta_receptora, transferencias.monto FROM finanzas.transferencias
			WHERE transferencias.nrodoc = nrodoc;

			var_mes := to_char(encabezado.fec_emi,'mm')::int2;
			var_anio := to_char(encabezado.fec_emi,'yyyy')::int2;

			-- Transferencia Emitida ------------------------------------------------------------------------------------
			IF (encabezado.status = 0) THEN
				-- Guarda el encabezado del asiento
				INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, transferido, num_doc, num_doc2)
				VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
				'Asiento de Transferencia Bancaria Emitido', encabezado.fec_emi, encabezado.origen, 'S', encabezado.nrodoc, encabezado.nrodoc);

				-- Disminuye la cuenta cedente por el haber
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber,descrip)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen= encabezado.origen And transferido='S'),
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.id_cuenta_cedente),
				0, encabezado.monto, encabezado.descripcion);

				-- Aumenta la cuenta receptora por el debe
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber,descrip)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen= encabezado.origen And transferido='S'),
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.id_cuenta_receptora),
				encabezado.monto, 0, encabezado.descripcion);

			-- Transferencia Anulada ------------------------------------------------------------------------------------
			ELSIF (encabezado.status = 1) THEN
			-- Guarda el encabezado del asiento
				SELECT INTO fec_anu TO_CHAR(now(),'dd/mm/yyyy');
                                fec_anu = encabezado.fec_emi;
				INSERT INTO contabilidad.com_enc (id_escenario, mes, ano, numcom, descrip, fecha, origen, status, transferido, num_doc, num_doc2)
				VALUES (escenario, var_mes, var_anio, (var_anio || '-' || substring(to_char(var_mes, '00'),2) || '-' || substring(to_char(num_com, '00000'),2)),
				'Asiento de Transferencia Bancaria Anulado', fec_anu, encabezado.origen, 'A', 'S', encabezado.nrodoc, encabezado.nrodoc);

				-- Aumenta la cuenta cedente por el debe
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen= encabezado.origen And transferido='S'),
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.id_cuenta_cedente),
				encabezado.monto, 0);

				-- Disminuye la cuenta receptora por el haber
				INSERT INTO contabilidad.com_det (id_com, id_cta, debe, haber)
				VALUES ((SELECT MAX(id) FROM contabilidad.com_enc WHERE origen= encabezado.origen And transferido='S'),
				(SELECT c.id_plan_cuenta FROM finanzas.cuentas_bancarias as c WHERE c.id=encabezado.id_cuenta_receptora),
				0, encabezado.monto);

			END IF;
	END IF;

	RETURN 1;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
ALTER FUNCTION public.asiento_cheque(nrodoc character varying, opcion smallint, escenario bigint) OWNER TO puser;
