CREATE OR REPLACE FUNCTION contabilidad.fix_pc()
  RETURNS bigint AS
$BODY$declare
cc_sa record;
cc_bad  integer;


begin
--Correccion de cc de 14 digitos marcadas como de no movimeinto
update contabilidad.plan_cuenta set movim = 'S' where length(codcta) = 14 and movim = 'N';

--Correccion de cc con datos en NULL
update contabilidad.plan_cuenta set movim = 'N',nominal= 'N', id_escenario = '1111' where id_escenario is null;

--Correccion de cc contables sin id_acumuladora de 14 digitos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 14 and movim = 'S' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 9) as c2 ON substring(c1.codcta from 1 for 9)  = c2.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Correccion de cc contables sin id_acumuladora de 9 digitos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 9 and movim = 'S' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7) as c2 ON substring(c1.codcta from 1 for 7)  = c2.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Correccion de cc contables sin id_acumuladora de 9 digitos y sin movimiento
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 9 and movim = 'N' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7) as c2 ON substring(c1.codcta from 1 for 7)  = c2.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Sigo trabajando con las cc de 9 digitos pero ahora debo evaluar los casos marcados sin movimientos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 9 and movim = 'N' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 14) as c2 ON substring(c2.codcta from 1 for 9)  = c1.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Correccion de cc contables sin id_acumuladora de 7 digitos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7 and movim = 'S' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 5) as c2 ON substring(c1.codcta from 1 for 5)  = c2.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Sigo trabajando con las cc de 7 digitos pero ahora debo evaluar los casos marcados sin movimientos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7 and movim = 'N' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 9) as c2 ON substring(c2.codcta from 1 for 7)  = c1.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Correccion de cc contables sin id_acumuladora de 5 digitos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 5 and movim = 'S' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 3) as c2 ON substring(c1.codcta from 1 for 3)  = c2.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Sigo trabajando con las cc de 5 digitos pero ahora debo evaluar los casos marcados sin movimientos
for cc_sa in select c1.id,c2.id as id_acumulador from (
	select codcta,id from contabilidad.plan_cuenta where length(codcta) = 5 and movim = 'N' and id_acumuladora is null) as c1
inner join
	(select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7) as c2 ON substring(c2.codcta from 1 for 5)  = c1.codcta
loop
update contabilidad.plan_cuenta set id_acumuladora = cc_sa.id_acumulador where id = cc_sa.id;
end loop;

--Evaluacion de las cuentas de 7 digitos
for cc_sa in select b.*
from contabilidad.plan_cuenta as a
inner join (select codcta,id from contabilidad.plan_cuenta where length(codcta) = 7 and movim = 'S') as b
	ON substring(a.codcta from 1 for 7) = b.codcta
where length(a.codcta) = 9
loop
update contabilidad.plan_cuenta set movim = 'N' where id = cc_sa.id;
end loop;

cc_bad=0;
--Valido si existen cc sin id_acumulador
select into cc_bad count(*) from contabilidad.plan_cuenta where id_acumuladora is null and length(codcta) > 1;

return cc_bad;
end;$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
ALTER FUNCTION contabilidad.fix_pc() OWNER TO puser;
