CREATE OR REPLACE FUNCTION contabilidad.actualizar(smallint, smallint, bigint)
  RETURNS smallint AS
$BODY$declare
cc_mov record;
racum record;
facum record;
total_sm numeric;
sal_ant numeric;
cc bigint;

begin
--Primero se borran todos los registro pertenecientes al periodo a actualizar que no esten con estatus cerrado
DELETE FROM contabilidad.consolidado WHERE ano = $1 AND mes = $2 AND (status = 'A' OR status='R');

--Copio todas las cuentas contables en la tabla consolidado, con saldo_ant = saldo_inicial para cc, para el mes y ano en estudio
insert into contabilidad.consolidado (cod_cta,id_acum,saldo_ant,debe,haber,saldo_act,fecha,ano,mes,status)
select codcta,id_acumuladora,saldo_inicial,0,0,0,current_date,$1,$2,'A' from contabilidad.plan_cuenta where id_escenario = $3;

--Actualizo solo aquellas cuentas con movimiento en el periodo estudiado
for cc_mov in select id_cta,sum(a.debe) as debe,sum(a.haber) as haber from contabilidad.com_det a
inner join contabilidad.com_enc b on b.id = a.id_com and b.ano = $1 and b.mes = $2
where a.id_cta is not null
group by id_cta

loop
select into cc codcta from contabilidad.plan_cuenta where id = cc_mov.id_cta;
update contabilidad.consolidado set debe = cc_mov.debe,haber = cc_mov.haber where cod_cta = cc;
if(substring(cc::character from 1 for 1) = '2' or substring(cc::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = cc;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = cc;
end if;
end loop;

--Actualizo cc de movimiento sin movimiento en el periodo
update contabilidad.consolidado set saldo_act = saldo_ant
where length(cod_cta) = 14 and ano = $1 and mes = $2 and debe = 0 and haber = 0;

--Nivel 5
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 14 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;

--Nivel 4
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 9 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;

--Nivel 3
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 7 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;

--Nivel 2
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 5 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;

--Nivel 1
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 3 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;

--Nivel 0
--Cursor de las cuentas de movimiento con su repectivo cuenta acumuladora
for racum in select distinct id_acum,b.codcta from contabilidad.consolidado a
inner join contabilidad.plan_cuenta b on a.id_acum = b.id
where length(a.cod_cta) = 1 and a.ano = $1 and a.mes = $2 order by b.codcta
loop
--Suma del debe y haber para subgrupo
select into facum sum(debe) as debe,sum(haber) as haber from contabilidad.consolidado
where id_acum = racum.id_acum and ano=$1 and mes = $2;
--Recalculo de Saldo Anterior a traves de las cc hijas
select into sal_ant sum(saldo_ant) from contabilidad.consolidado where id_acum = racum.id_acum and ano=$1 and mes = $2;
update contabilidad.consolidado set saldo_ant=sal_ant,debe=facum.debe,haber=facum.haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
--Actualizo saldo_act
if(substring(racum.codcta::character from 1 for 1) = '2' or substring(racum.codcta::character from 1 for 1) = '3') then
  update contabilidad.consolidado set saldo_act = saldo_ant - debe + haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
else
  update contabilidad.consolidado set saldo_act = saldo_ant + debe - haber where cod_cta = racum.codcta and ano=$1 and mes = $2;
end if;
end loop;


return 0;
end$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
ALTER FUNCTION contabilidad.actualizar(smallint, smallint, bigint) OWNER TO puser;