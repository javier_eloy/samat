<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//set_time_limit(0);
if(isset($_REQUEST['Periodo'])){
  $mes  = substr($_REQUEST['Periodo'],0,2);
  $ano  = substr($_REQUEST['Periodo'],-4); 
  $qp   = "select min(nom_fec_ini),max(nom_fec_fin) from rrhh.historial_nom where substring(nom_fec_fin from 6 for 2) = '$mes' and substring(nom_fec_fin from 1 for 4) = '$ano' and substring(nom_fec_ini from 6 for 2) = '$mes'";
  $rqp  = $conn->Execute($qp);
  //die($qp);
  if(!$rqp->EOF){
    $fecha_inicio = $rqp->fields['min'];
    $fecha_final  = $rqp->fields['max'];
    //die('Fecha'.$rqp->fields[1]);
  }
     
}else{ 
  return false;
}

function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $periodo;
//Cabecera de página
	function Header()
	{
			parent::Header();
			
			$this->SetXY(200, 7);
                        $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');
			
			$this->Ln(20);
			$this->SetFont('Courier','b',12);
			$this->Cell(260, 10, "RELACION DE APORTES DE EMPLEADOS DE REGIMEN PRESTACIONAL DE VIVIENDA Y HABITAT",0,1,'C');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(120, 4, "Nombre Empresa",0,0,'L');
                        $this->Cell(40, 4, "Nro. de Contrato",0,0,'L');
                        $this->Cell(30, 4, "R.I.F.",0,0,'L');
                        $this->Cell(30, 4, "Responsable",0,1,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(120, 4, ORGANISMO_NOMBRE,0,0,'L');
                        $this->Cell(40, 4, FAOVCONTRATO,0,0,'C');
                        $this->Cell(30, 4, RIF,0,0,'C');
                        $this->Cell(30, 4, JEFERRHH,0,1,'L');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(180, 4, "Direci".utf8_decode('ó')."n",0,1,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(180, 4, DIRECCIONFISCAL,0,1,'L');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(30, 4, "Tel".utf8_decode('é')."fonos",0,0,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(170, 4, TELFENTE,0,0,'L');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(20, 4, "Mes y A".utf8_decode('ñ')."o",0,0,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(20, 4, $this->periodo,0,1,'R');
			$this->SetFont('Courier','B',12);
			//$this->Cell(190, 0, "Periodo: ".muestrafecha($rN->fields['nom_fec_ini'])." a ".muestrafecha($rN->fields['nom_fec_fin']),0,0,'R');
			$this->Line(15, 62, 260, 62);
			//Titulos
			$this->Ln(5);
      $this->SetFont('Courier','B',10);
      $this->Cell(15,5,'Nro',0,0,'C');
      $this->Cell(20,5,'C.I.',0,0,'C');
      $this->Cell(40,5,'Apellidos',0,0,'C');
      $this->Cell(40,5,'Nombres',0,0,'C');
      $this->Cell(15,5,'Sexo',0,0,'C');
      $this->Cell(20,5,'Fecha/Nac.',0,0,'C');
      $this->Cell(25,5,'S. Integral',0,0,'C');
      $this->Cell(25,5,'1%',0,0,'C');
      $this->Cell(25,5,'2%',0,0,'C');
      $this->Cell(25,5,'Total',0,1,'C');
      $this->Line(15, 68, 260, 68);
			$this->Ln(5);
	}

	function Footer()
	{
		$this->SetFont('Courier','I',12);
		//Número de página
		$this->Cell(260,10,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('L','mm');
$pdf->periodo = $_REQUEST['Periodo'];
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);

$q = "select b.int_cod,b.tra_ced,b.tra_nom,b.tra_ape,b.tra_fec_nac,b.tra_sex, 
	CASE WHEN tra_sex=1 THEN 'F'
            WHEN tra_sex=0 THEN 'M'
	END as sexo,
	sum(si.sueldo) as sueldo,
	sum(a.conc_val) as deduccion,sum(a.conc_aporte) as aporte,sum(a.conc_val)+sum(a.conc_aporte) as total
from rrhh.trabajador as b 
inner join rrhh.hist_nom_tra_conc as a on a.tra_cod = b.int_cod
inner join rrhh.historial_nom as c on c.int_cod = a.hnom_cod
inner join (
	select sum(conc_val) as sueldo,hnom_cod,tra_cod
	from rrhh.hist_nom_tra_conc as x
	where x.conc_cod in(86,152,155,156,162,73,148,168,139,178,169,140,149)
	group by x.hnom_cod,tra_cod
	) as si on si.hnom_cod = c.int_cod and si.tra_cod = a.tra_cod
where c.nom_fec_ini >= '$fecha_inicio' and c.nom_fec_fin <= '$fecha_final' and a.conc_cod = 12 and b.tra_ced <> '' 
group by b.int_cod,b.tra_ced,b.tra_nom,b.tra_ape,b.tra_fec_nac,b.tra_sex
order by CAST(b.tra_ced AS numeric)";
//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos


$total_deduccion = 0;
$total_aporte = 0;
$total_emp = 1;

$pdf->SetFont('Courier','',8);
while(!$rD->EOF){
  $pdf->Cell(15,5,$total_emp++,0,0,'R');
  $pdf->Cell(20,5,$rD->fields['tra_ced'],0,0,'R');
  $pdf->Cell(40,5,utf8_decode($rD->fields['tra_ape']),0,0,'L');
  $pdf->Cell(40,5,utf8_decode($rD->fields['tra_nom']),0,0,'L');
  $pdf->Cell(10,5,$rD->fields['sexo'],0,0,'C');
  $pdf->Cell(20,5,muestrafecha($rD->fields['tra_fec_nac']),0,0,'C');
  $pdf->Cell(25,5,muestrafloat(redondeado($rD->fields['sueldo'])),0,0,'R');
  $pdf->Cell(25,5,muestrafloat(redondeado($rD->fields['deduccion'])),0,0,'R');
  $pdf->Cell(25,5,muestrafloat(redondeado($rD->fields['aporte'])),0,0,'R');
  $pdf->Cell(25,5,muestrafloat(redondeado($rD->fields['total'])),0,1,'R');
  $total_deduccion += redondeado($rD->fields['deduccion']);
  $total_aporte += redondeado($rD->fields['aporte']);
	$rD->movenext();
} //Fin loop departamentos

//$total_emp = $total_emp;
$pdf->SetFont('Courier','B',8);
$pdf->Cell(170,5,utf8_decode("Total Relación con $total_emp empleados"),0,0,'L');
$pdf->Cell(25,5,muestrafloat(redondeado($total_deduccion)),0,0,'R');
$pdf->Cell(25,5,muestrafloat(redondeado($total_aporte)),0,0,'R');
$pdf->Cell(25,5,muestrafloat(redondeado($total_deduccion)+redondeado($total_aporte)),0,1,'R');

$pdf->Output();
?>
