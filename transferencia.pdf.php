<?php

include("comun/ini.php");
include("Constantes.php");
set_time_limit(0);

$_SESSION['conex'] = $conn;

$oUsuario = new usuarios();

class PDF extends FPDF
{

        var $usuario_activo;
        var $fecha_doc;
	//Cabecera de p�gina
	function Header()
	{
	
                parent::Header();
//		$this->SetLeftMargin(5);
//		$this->SetFont('Arial','',10);
//		$this->Ln(1);
//		$this->Image ("images/logoa.jpg",5,4,66,20);//logo a la izquierda
		$this->SetFont('Arial','b',12);
		//$separaFecha= explode('-',date('Y-m-d'));
		$separaFecha= explode('-',$this->fecha_doc);
		$dia = $separaFecha[2];
		$mes = $separaFecha[1];
		$ano = $separaFecha[0];
		$this->Ln(20);	
		$this->Cell(190,5, 'MARACAIBO, '.$dia.' DE '.strtoupper(obtieneMes($mes)).' DE '.$ano,0,0, 'R','L');
		$this->Ln(10);
	}
	//Pie de p�gina
	function Footer()
	{
		//$conn = $_SESSION['conex'];
		$this->SetFont('Arial','',8);
		$this->Ln(10);
		$this->Cell(50,5,"Elaborado por",1,0,'C');
		$this->SetX(75);
		$this->Cell(50,5,"Firma 1",1,0,'C');
		$this->SetX(145);
		$this->Cell(50,5,"Firma 2",1,0,'C');
		$this->Ln(5);
		$this->Cell(50,30,"",1,0,'C');
		$this->SetX(75);
		$this->Cell(50,30,"",1,0,'C');
		$this->SetX(145);
		$this->Cell(50,30,"",1,0,'C');
		$this->Ln(25);
		//$this->Cell(50,5,JEFETESORO,0,0,'C');
                $this->Cell(50,5,$this->usuario_activo,0,0,'C');
	}
}




$conn = $_SESSION['conex'];
$q = "	SELECT
			C.descripcion,
			B.nro_cuenta,
			A.monto,
                        A.id_usuario
		FROM
			finanzas.transferencias AS A
			INNER JOIN finanzas.cuentas_bancarias AS B ON B.id = A.id_cuenta_cedente
			INNER JOIN public.banco AS C ON B.id_banco = C.id
		WHERE
			A.nrodoc = '".$_GET['id']."'";
$rN = $conn->Execute($q);
$q_r = "SELECT
			A.fecha,
			A.descripcion as concepto,
			C.descripcion,
			B.nro_cuenta,
                        A.id_usuario
		FROM
			finanzas.transferencias AS A
			INNER JOIN finanzas.cuentas_bancarias AS B ON B.id = A.id_cuenta_receptora
			INNER JOIN public.banco AS C ON B.id_banco = C.id
		WHERE
			A.nrodoc = '".$_GET['id']."'";
//die($q_r);
$rR = $conn->Execute($q_r);
$oUsuario->get($conn,$rR->fields['id_usuario']);
$pdf=new PDF();
$pdf->usuario_activo = "$oUsuario->nombre $oUsuario->apellido";
$pdf->fecha_doc = $rR->fields['fecha'];
//die('AJA'.$oUsuario->nombre.$oUsuario->apellido);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','b',12);
//$montoLetras = num2letras(muestraFloat($rN->fields['monto']),false,true);
$montoLetras = num2letras($rN->fields['monto']);
$pdf->Cell(190,5, 'Sres.: '.utf8_decode($rN->fields['descripcion']) ,0, 0, 'L','L');
$pdf->SetFont('Arial','',10);
$pdf->ln(15);
$pdf->SetX(15);
$pdf->MultiCell(180,5,'Sirva la presente para autorizar nos debiten de la cuenta '.$rN->fields['nro_cuenta'].' la cantidad de '.utf8_decode($montoLetras).' (Bs.'. muestraFloat($rN->fields['monto']).')', 0, 'L');
$pdf->ln(5);
$pdf->SetX(15);
$pdf->MultiCell(180,5,utf8_decode('Por el concepto que se define a continuación'), 0, 'L');

$pdf->ln(10);
$pdf->Cell(190,5, 'CONCEPTO',1, 0, 'C');
$pdf->ln(5);
$pdf->SetDrawColor (0,0,0);
$pdf->Cell(190,10, $rR->fields['concepto'],'RL', 2, 'L');
$pdf->Cell(190,100, "",'RLB', 0, 'L');
$pdf->ln(55);
$pdf->SetX(15);
$pdf->Cell(180,100,'Para acreditar en la cuenta '.$rR->fields['nro_cuenta'].' del '.$rR->fields['descripcion'].'.',0, 0, 'L');
$pdf->Ln(70);
$separaFecha= explode('-',$rR->fields['fecha']);
$dia = $separaFecha[2];
$mes = $separaFecha[1];
$ano = $separaFecha[0];
$pdf->Cell(190,5,"Nro. de Transferencia: ".$ano.'-'.$mes.'-'.$_GET['id'],0,0,'R');
$pdf->Output();

?>
