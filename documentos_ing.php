<?
require ("comun/ini.php");

$oDocIng    = new documentos_ing($conn);

$accion         = $_REQUEST['accion'];
$id             = $_POST['id'];
$nrodoc         = ($_POST['nrodoc'])?$_POST['nrodoc']:'000';
$fecha          = $_POST['fecha'];
$tipo           = $_POST['id_tipo'];
$monto          = $_POST['monto'];
$tipocaja       = $_POST['tipo_caja'];
$fecha_actual   = $_POST['fecha'];//date("Y-m-d");
$json_det       = $_POST['json_det'];
//echo(date("d/m/Y"));
switch($accion) {
    case 'Guardar' :
        $oDocIng->create($nrodoc, $tipo, guardafecha($fecha), $monto, guardafecha($fecha_actual), $json_det, $tipocaja);
        break;
    case 'Actualizar':
        $oDocIng->set($id, $nrodoc, $tipo,  $monto, guardafecha($fecha), $json_det, $tipocaja);
        break;
    case 'del':
        $oDocIng->delete($_REQUEST['id']);
        break;
}
$msg = $oDocIng->msg;

require ("comun/header.php");
if(!empty($msg))   echo "<div id=\"msj\">".$msg."</div><br/>";

?>
<br />
<script type="text/javascript">var mygrid,i=0;</script>
<span class="titulo_maestro">Documentos de Ingresos</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<div id="divbuscador">
    <fieldset id="buscador">
        <legend>Buscar:</legend>
        <table width="600" border="0">
            <tr>
                <td colspan="2">Tipo de Documento de Ingreso</td>
            </tr>
            <tr>
                <td colspan="2">
                    <?=helpers::superComboSQL($conn,
                            '',
                            0,
                            'busca_cc',
                            'busca_cc',
                            '',
                            'buscador()',
                            'id',
                            'descripcion',
                            false,
                            '',
                            "SELECT id, descripcion FROM ingresos.tipos_ingresos",
                            '80')?>
                </td>
            </tr>
            <tr>
                <td>Desde:</td>
                <td>Hasta:</td>
            <tr>
                <td style="width:150">
                    <input style="width:100px"  type="text" name="busca_fecha_desde" id="busca_fecha_desde" onchange="validafecha(this);"/>
                    <a href="#" id="boton_busca_fecha_desde" onclick="return false;">
                        <img border="0" alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup
                        ({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_desde",
                            button            : "boton_busca_fecha_desde",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>
                <td>
                    <input style="width:100px"  type="text" name="busca_fecha_hasta" id="busca_fecha_hasta" onchange="validafecha(this);"/>
                    <a href="#" id="boton_busca_fecha_hasta" onclick="return false;">
                        <img border="0" alt="Seleccionar Fecha" src="images/calendarA.png" width="20" height="20" />
                    </a>
                    <script type="text/javascript">
                        new Zapatec.Calendar.setup
                        ({
                            firstDay          : 1,
                            weekNumbers       : true,
                            showOthers        : false,
                            showsTime         : false,
                            timeFormat        : "24",
                            step              : 2,
                            range             : [1900.01, 2999.12],
                            electric          : false,
                            singleClick       : true,
                            inputField        : "busca_fecha_hasta",
                            button            : "boton_busca_fecha_hasta",
                            ifFormat          : "%d/%m/%Y",
                            daFormat          : "%Y/%m/%d",
                            align             : "Br"
                        });
                    </script>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
</div>
<div style="margin-bottom:10px" id="busqueda"></div>
<br />
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>

<script type="text/javascript">

    var t;
    var i = 0;
    var iEli = 0;



    function busca(cc, fecha_desde, fecha_hasta, pagina)
    {
        //alert('cc: '+ cc +'  fecha_desde: '+fecha_desde+ ' fecha_hasta: '+fecha_hasta+' pagina: '+pagina);
        var url = 'updater_busca_documentos_ing.php';
        var pars = 'cc=' + cc + '&fecha_desde='+fecha_desde+'&fecha_hasta='+fecha_hasta+'&pagina='+pagina+'&ms='+new Date().getTime();
        var updater = new Ajax.Updater
        (
        'busqueda',
        url,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){Element.show('cargando')},
            onComplete:function(request){Element.hide('cargando')}
        }
    );
    }

    function buscador()
    {
        //alert($('busca_fecha_hasta').value);
        busca($('busca_cc').value, $('busca_fecha_desde').value, $('busca_fecha_hasta').value, 1);
    }

    function validafecha(fecha)
    {
        var upper = 31;
        if(/^(\d{2})\/(\d{2})\/(\d{4})$/.test(fecha.value))
        { // dd/mm/yyyy
            if(RegExp.$2 == '02')
                upper = 29;

            if((RegExp.$1 <= upper) && (RegExp.$2 <= 12))
            {
                fecha_desde = new Date();
                fecha_hasta = new Date();
                if ($('busca_fecha_desde').value!='' && $('busca_fecha_hasta').value!='')
                {
                    fecha_desde.setDate($('busca_fecha_desde').value.substr(0,2));
                    fecha_desde.setMonth(parseInt($('busca_fecha_desde').value.substr(3,2))-1);
                    fecha_desde.setFullYear($('busca_fecha_desde').value.substr(6,4));

                    fecha_hasta.setDate($('busca_fecha_hasta').value.substr(0,2));
                    fecha_hasta.setMonth(parseInt($('busca_fecha_hasta').value.substr(3,2))-1);
                    fecha_hasta.setFullYear($('busca_fecha_hasta').value.substr(6,4));

                    if (fecha_desde.getTime() > fecha_hasta.getTime())
                    {
                        alert("La fecha 'Desde' debe ser menor que la fecha 'Hasta'");
                        $(fecha).value = "";
                        return false;
                    }
                }

                busca($('busca_cc').value, $('busca_fecha_desde').value, $('busca_fecha_hasta').value, 1);
            }
            else
            {
                alert("Fecha incorrecta");
                $(fecha).value = "";
            }
        }
        else if(fecha.value != '')
        {
            alert("Fecha incorrecta");
            $(fecha).value = "";
        }
    }

    //Verifica si hay celdas vacias en el grid, si las consigue las borra.
    function celdasVacias(){
        for(er=0;er<=mygrid.getRowsNum();er++){
            if(mygrid.doesRowExist(mygrid.getRowId(er)))
               if(mygrid.cells(er,0).getValue() == '' && mygrid.cells(er,1).getValue() == '' && mygrid.cells(er,2).getValue() == '') {
                    mygrid.deleteRow(mygrid.getRowId(er)); 
                    iEli++;    
               }     
        }
    }

    function doOnEnter(rowId,cellInd)
    {
        //alert(stage+";"+rowId+":"+ cellInd);
        // Valida que no se repitan los documentos y las opciones en el grid
        if(cellInd == 1 )
        {
            filas = mygrid.getRowsNum() + iEli;
            //alert('filas: '+filas);
            for(j=0;j<filas;j++){
                if(mygrid.getRowIndex(j)!=-1 && rowId!=j){
                    //alert(mygrid.cells(j,1).getValue() + ' : ' + mygrid.cells(rowId,1).getValue())
                    //if( (mygrid.cells(j,0).getValue() == mygrid.cells(rowId,0).getValue()) && (mygrid.cells(j,1).getValue() == mygrid.cells(rowId,1).getValue()) && (mygrid.cells(j,2).getValue() == mygrid.cells(rowId,2).getValue()) ){
                    if(mygrid.cells(j,1).getValue() == mygrid.cells(rowId,1).getValue()){
                        alert('Número de documento registrado, en este Documento de Ingreso');
                        mygrid.cells(rowId,1).setValue("");
                        iEli++;
                        return false;
                    }
                }
            }
            //Valido en otros documentos
            //alert('aja');
            var url = 'json.php';
            var pars = 'op=validaDocIngRep&tipo_ingreso='+ $('id_tipo').value+'&documento='+mygrid.cells(rowId,1).getValue()+'&ms='+new Date().getTime();
            var Request = new Ajax.Request
            (
            url,
            {
                method: 'get',
                parameters: pars,
                onLoading:function(request){Element.show('Validando');},
                onComplete:function(request)
                {
                    var resp = eval( '(' + request.responseText + ')');
                    //alert(resp);
                    if(resp != false){
                        alert('Número de documento repetido en otro Documento de Ingreso. Detalle: '+resp);
                        return false;
                    }
                    Element.hide('Validando');
                }
            }
            );
            mygrid.selectCell(rowId, 2, false, false, true, true); 

        } else if(cellInd == 2){
            var puntos = mygrid.cells(rowId,2).getValue().replace(".","","gi").replace(",",".");
            var aux = parseFloat(puntos);//parseFloat(mygrid.cells(rowId,2).getValue());
            if(!isNaN(aux)){
                mygrid.cells(rowId,2).setValue(muestraFloat(aux));
                sumaTotal();
                agregar();
            }else{
                mygrid.cells(rowId,2).setValue('');
            }    
        } else if (cellInd == 0){
            mygrid.selectCell(rowId, 1, false, false, true, true);   
        }
        
        //return true;
    }


    function sumaTotal(){
        //alert('aidi');
        celdasVacias();
        filas = mygrid.getRowsNum() + iEli;
        var total = 0.0;
        for(j=0;j<filas;j++){
            if(mygrid.getRowIndex(j)!=-1){
                total += parseFloat(usaFloat(mygrid.cells(j,2).getValue()));
            }
        }
        $('monto').value = muestraFloat(total);
    }

    function GuardarDet()
    {

        var JsonAux = new Array;
        var lon = mygrid.getRowsNum();

        if (lon == 0)
            return false;

        
  
        for(j=0; j<lon; j++)
        {
            rowId = mygrid.getRowId(j);
            //JsonAux.push({tipo:mygrid.cells(rowId,0).getValue(), nro_doc:mygrid.cells(rowId,1).getValue(), id_banco:mygrid.cells(rowId,2).getValue(), monto:mygrid.cells(rowId,2).getValue()});
            //Validacion de valor vacio en monto
            //alert(rowId);
            var montoF = 0;
            if(mygrid.cells(rowId,2).getValue() != '')
                montoF = mygrid.cells(rowId,2).getValue();
            JsonAux.push({fecha:mygrid.cells(rowId,0).getValue(), nrodoc:mygrid.cells(rowId,1).getValue(), monto:montoF});
        }

        $('json_det').value = JsonAux.toJSONString();
        validate();
    }

    //Agrega una linea en blanco al grid.
    function agregar() {
        celdasVacias();
        if(i==0){
            mygrid.addRow(i,";;");
            var row = mygrid.doesRowExist(i);
            if(row){
                mygrid.selectCell(mygrid.getRowIndex(i),0,false,true,true);
                //mygrid.selectCell(i,0,false,true,true);
                i++;
            }
        }else{
            //alert(i+':'+iEli);
            //if(mygrid.cells(i-1+iEli,0).getValue() != '' && mygrid.cells(i-1+iEli,1).getValue() != '' && mygrid.cells(i-1+iEli,2).getValue() != ''){
            //if(mygrid.cells(i,0).getValue() != '' && mygrid.cells(i,1).getValue() != '' && mygrid.cells(i,2).getValue() != ''){
               mygrid.addRow(i-iEli,";;");
               mygrid.selectCell(mygrid.getRowIndex(i-iEli),0,false,true,true);
               i++;
            //}
        }
        
    } //Fin agregar()

    //Elimina una linea del grid. Automaticamente selecciona la que esta activa.
    function Eliminar(){
        var selId = mygrid.getSelectedId();
        alert('Valor de i al borrar: '+i);
        if(selId != null){
            i--;
            mygrid.deleteRow(selId);
            iEli++;
            sumaTotal();
        }
    } //Fin eliminar()

    //Funcion que detecta cual de los radio boton esta checkeado y devuelve el valor del mismo
    //Un fume de internet utilizando prototype
    function $RF(el) {
        return Form.getInputs($(el).form, 'radio', $(el).name).find(
            function (radioElement) {
                return radioElement.checked;
            }).value;
    } 


    function ValidaFechaArqueoCaja(fecha,tipo_caja){
        if(fecha == '' || tipo_caja == '') return;
        //alert(fecha+'por aqui'+tipo_caja);
        var url = 'json.php';
        var pars = 'op=validaFechaAC&fecha='+ fecha+'&tipo='+tipo_caja+'&ms='+new Date().getTime();
        var Request = new Ajax.Updater
        (
        'fecha',
        url,
        {
            method: 'get',
            parameters: pars,
            onLoading:function(request){Element.show('Validando');},
            onComplete:function(request)
            {
                var resp = eval( '(' + request.responseText + ')');
                //alert(resp);
                if(resp == 'false'){
                    alert('Fecha y tipo de caja con Arqueo de Caja cerrado, modifique los parametros.');
                    $('fecha').value = '';
                }
                Element.hide('Validando');
            }
        }
    );
    } //Fin ValidaFechaArqueoCaja(this.value)

</script>

<?
$validator->create_message("error_fecha", "fecha", "*");
$validator->create_message("error_id_tipo", "id_tipo", "*");
$validator->print_script();
require ("comun/footer.php");
?>