<?
include("comun/ini.php");
include("Constantes.php");

$_SESSION['conex'] = $conn;
//die($_REQUEST['PeriodoID']);
//Calculo el ultimo mes calculo de nomina
$qup =  "SELECT date_part('month'::text, nom_fec_fin) as mes FROM rrhh.historial_nom WHERE int_cod = ".$_REQUEST['PeriodoID'];
//die($qup);
$rqup = $conn->Execute($qup);
$maxMesPeriodo = '01';
if(!$rqup->EOF)
    $maxMesPeriodo = $rqup->Fields['mes'];
//die($maxMesPeriodo);


$todos = false;
if(empty($_REQUEST['Periodo']))
{
    $_REQUEST['Periodo'] = date('m-d-Y');
    $todos = true;
} 

if(isset($_REQUEST['Tra']) and isset($_REQUEST['Periodo'])){
  $mes  = substr($_REQUEST['Periodo'],0,2);
  $ano  = substr($_REQUEST['Periodo'],-4);
  $fecha_inicio = "$ano-01-01";
  $fecha_final  = "$ano-$mes-".date('t',mktime(0, 0, 0, $mes, 1, $ano));
  if(!$todos)
      $fecha_inicio = substr_replace($fecha_final,'01',8,2);
  $trabajador  = $_REQUEST['Tra'];
}else{ 
  return false;
}

function dividirStr($str, $max){
	$strArray = array();
    do{
    	if (strlen($str) > $max)
        	$posF = strrpos( substr($str, 0, $max), ' ' );
		else
        	$posF = -1;
		if ($posF===false || $posF==-1){
	    	$strArray[] = substr($str, 0);
        	$str = substr($str, 0);
        	$posF = -1;
      	}else{
        	$strArray[] = substr($str, 0, $posF);
        	$str = substr($str, $posF+1 );
      	}
    }while ($posF != -1);
    return ($strArray);
}


class PDF extends FPDF
{
  var $trabajador_id;
  var $periodo;
//Cabecera de página
	function Header()
	{
			parent::Header();
			
			$this->SetXY(140, 7);
                        $this->MultiCell(50,2, "Fecha: ".date('d/m/Y'), 0, 'L');

                        $this->SetFont('Courier','b',12);
			$this->Cell(365, 0, "AR-I",0,1,'C');

			$this->Ln(20);
			$this->SetFont('Courier','b',12);
			$this->Cell(190, 10, "COMPROBANTE DE RETENCIONES DEL ISLR",0,1,'C');
                        $this->Cell(190,5,"DATOS DEL AGENTE DE RETENCI".utf8_decode('Ó')."N",1,1,'C');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(120, 4, "Nombre Empresa",0,0,'L');
                        $this->Cell(30, 4, "R.I.F.",0,1,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(120, 4, ORGANISMO_NOMBRE,0,0,'L');
                        $this->Cell(30, 4, RIF,0,1,'C');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(180, 4, "Direci".utf8_decode('ó')."n",0,1,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(180, 4, DIRECCIONFISCAL,0,1,'L');
                        $this->SetFont('Courier','B',10);
                        $this->Cell(30, 4, "Tel".utf8_decode('é')."fonos",0,1,'L');
                        $this->SetFont('Courier','',10);
                        $this->Cell(170, 4, TELFENTE,0,1,'L');
                        $this->SetFont('Courier','b',12);
                        $this->Cell(190,5,'DATOS DEL FUNCIONARIO',1,1,'C');
                        
                        $qt = "select tra_nom,tra_ape,tra_ced from rrhh.trabajador where int_cod = ".$this->trabajador_id;
                        //die($qt);
                        $rqt = $_SESSION['conex']->Execute($qt);
                        if(!$rqt->EOF){
                            $this->SetFont('Courier','B',10);
                            $this->Cell(30, 4, "Nombre",0,1,'L');
                            $this->SetFont('Courier','',10);
                            $this->Cell(120, 4,utf8_decode($rqt->fields['tra_ape'].', '.$rqt->fields['tra_nom']) ,0,1,'L');
                            $this->SetFont('Courier','B',10);
                            $this->Cell(30, 4, "C".utf8_decode('é')."dula de Identidad",0,1,'L');
                            $this->SetFont('Courier','',10);
                            $this->Cell(170, 4,$rqt->fields['tra_ced'],0,1,'L');
                            $this->SetFont('Courier','B',10);
                            $this->Cell(30, 4, "Per".utf8_decode('í')."odo",0,1,'L');
                            $this->SetFont('Courier','',10);
                            $this->Cell(170, 4,$this->periodo,0,1,'L');
                        }
			$this->SetFont('Courier','B',12);
			//$this->Cell(190, 0, "Periodo: ".muestrafecha($rN->fields['nom_fec_ini'])." a ".muestrafecha($rN->fields['nom_fec_fin']),0,0,'R');
                        $this->Cell(190,10,"INFORMACI".utf8_decode('Ó')."N DEL IMPUESTO RETENIDO Y ENTERADO",1,1,'C');
			//Titulos
                        $this->SetFont('Courier','B',10);
                        $this->Cell(50,5,'Mes',1,0,'L');
                        $this->Cell(30,5,'Remuneraci'.utf8_decode('ó').'n',1,0,'C');
                        $this->Cell(25,5,'%',1,0,'C');
                        $this->Cell(25,5,'Retenido',1,0,'C');
                        $this->Cell(30,5,'Rem. Acumlada',1,0,'C');
                        $this->Cell(30,5,'Ret. Acumulada',1,0,'C');
			$this->Ln(5);
	}

	function Footer()
	{
		$this->SetFont('Courier','I',12);
		//Número de página
                $this->SetXY(70, 220);
                $this->Cell(80,35,'',1,1,'C');
                $this->SetXY(70, 220);
                $this->Cell(80,5,"AGENTE DE RETENCI".utf8_decode('Ó')."N",1,1,'C');
                $this->SetXY(70, 250);
                $this->Cell(70,4,"Sello y Firma",0,1,'C');
                $this->Ln(3);
		$this->Cell(190,10,'P'.utf8_decode('á').'gina '.$this->PageNo().'/{nb}',0,0,'C');
	}
} 
//Creación del objeto de la clase heredada
$pdf=new PDF('P','mm');
$pdf->trabajador_id = $_REQUEST['Tra'];
$pdf->periodo = "DESDE ".muestrafecha($fecha_inicio)." HASTA ".muestrafecha($fecha_final);
//die($pdf->periodo);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','',10);
if(empty($mes) or $todos)
    $q = "SELECT * FROM RRHH.vis_retenciones_islr where tra_cod = $trabajador  and ano=(SELECT date_part('year'::text, nom_fec_fin) as ano FROM rrhh.historial_nom WHERE int_cod = ".$_REQUEST['PeriodoID'].") order by indice";
else
    $q = "SELECT * FROM RRHH.vis_retenciones_islr where tra_cod = $trabajador and indice = $mes::int and ano=(SELECT date_part('year'::text, nom_fec_fin) as ano FROM rrhh.historial_nom WHERE int_cod = ".$_REQUEST['PeriodoID'].") order by indice";
//die($q);
$rD = $conn->Execute($q);
//Loop de Departamentos


$acum_sueldo = 0;
$acum_retenido = 0;

$pdf->SetFont('Courier','',8);
while(!$rD->EOF){
  if($ano == '2010')
    $acum_sueldo += $rD->fields['sueldo'];
  else
    $acum_sueldo += $rD->fields['sueldo_x'];  
  $acum_retenido += $rD->fields['retenido'];
  $pdf->Cell(50,5,$rD->fields['mes'],0,0,'L');
  if($ano == '2010')
    $pdf->Cell(30,5,muestrafloat($rD->fields['sueldo']),0,0,'R');
  else
     $pdf->Cell(30,5,muestrafloat($rD->fields['sueldo_x']),0,0,'R'); 
  $pdf->Cell(25,5,muestrafloat($rD->fields['porcentaje']),0,0,'R');
  $pdf->Cell(25,5,muestrafloat($rD->fields['retenido']),0,0,'R');
  $pdf->Cell(30,5,muestrafloat($acum_sueldo),0,0,'R');
  $pdf->Cell(30,5,muestrafloat($acum_retenido),0,1,'R');
  $rD->movenext();
} //Fin loop departamentos

$pdf->Output();
?>
