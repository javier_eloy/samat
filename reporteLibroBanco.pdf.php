<?
include("comun/ini.php");

$id_cta_banc = $_GET['id_cta'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];

//Ingresos
$q = "select 'AC de Ingresos del: '||a.fecha as descripIngreso,a.id_cta_contable,c.descripcion,b.fecha_documento,b.num_doc,b.monto,e.origen,d.id as id_cuenta_ban,a.fecha from ingresos.doc_ing_enc a 
inner join ingresos.doc_ing_det b on a.id = b.id_documento
inner join contabilidad.plan_cuenta c on a.id_cta_contable = c.id
inner join finanzas.cuentas_bancarias d on d.id_plan_cuenta = c.id
inner join ingresos.tipos_ingresos e on e.id = a.id_tipo 
where substring(b.fecha_documento,6, 2)::int = '$mes' and substring(b.fecha_documento,1, 4)::int = '$anio' and a.cerrado = true and d.id = $id_cta_banc 
order by b.fecha_documento";
//die($q);
//$r = $conn->Execute($q);

//Egresos
$qt = "select * from (
select 'AC de Ingresos del: '||a.fecha as descripIngreso,a.id_cta_contable,c.descripcion,b.fecha_documento,b.num_doc,b.monto,e.origen,d.id as id_cuenta_ban,a.fecha 
from ingresos.doc_ing_enc a 
inner join ingresos.doc_ing_det b on a.id = b.id_documento
inner join contabilidad.plan_cuenta c on a.id_cta_contable = c.id
inner join finanzas.cuentas_bancarias d on d.id_plan_cuenta = c.id
inner join ingresos.tipos_ingresos e on e.id = a.id_tipo 
where substring(b.fecha_documento,6, 2)::int = $mes and substring(b.fecha_documento,1, 4) = $anio and a.cerrado = true and d.id = $id_cta_banc
union
select 'EG: '|| a.descrip as descripIngreso,c.id as id_cta_contable,c.descripcion,a.fecha as fecha_documento,a.num_doc,
case b.haber 
 when 0 then b.debe
 else
  b.haber*-1
end as monto,a.origen,d.id as id_cuenta_ban,a.fecha  
from contabilidad.com_enc a
inner join contabilidad.com_det b on a.id = b.id_com
inner join contabilidad.plan_cuenta c on b.id_cta = c.id and substring(c.codcta,1,7) = '1010101'
inner join finanzas.cuentas_bancarias d on d.id_plan_cuenta = c.id
where a.id_escenario = 1111 and a.ano = $anio and a.mes = $mes and substring(a.num_doc,1,1) != 'T' and substring(a.num_doc,1,1) != 'S' and d.id = $id_cta_banc 
) as x
order by fecha_documento";
//die($qt);
$r = $conn->Execute($qt);

$cuenta_banc = new cuentas_bancarias; 
$cuenta_banc->get($conn, $id_cta_banc);
//die($cuenta_banc->desc_cuenta ."( $id_cta_banc)");
//var_dump($cuenta_banc);

// Crea un array donde cada posicion es un string de tamaño 'max' caracteres,
// teniendo en cuenta de no cortar una palabra, busca el espacio en blanco  
// mas cerca del tamaño 'max' y ahi corta el string
function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
}

class PDF extends FPDF
{
	var $anio;
	var $mes;
	var $id_cta_banc;
	var $subTotalTrans;
	var $subTotalBs;
	
	// Definen el ancho de las celdas 
	var $wDocumento 	= 35;
	var $wFecha 		= 25;
        var $wFechaReg 		= 25;
	var $wBeneficiario 	= 40;
	var $wConcepto 		= 75;
	var $wReferencia 	= 30;	
	var $wMonto 		= 20;
	var $descripCuenta  = '';
	
	//Cabecera de página
	function Header()
	{
		$this->SetFont('Courier','',12);
		$this->Ln(1);
		$this->Image ("images/logo-unico.jpg",15,4,50);//logo a la izquierda 
		$this->SetXY(42, 20); 

		$this->SetXY(215, 20); 
		$textoDerecha  = "Fecha: ".date('d/m/Y')."\n\n";
                $textoDerecha .= "Hora: ".date("h:m A")."\n\n";
		$textoDerecha .= "Pag: ".$this->PageNo()." de {nb}\n";
		$this->MultiCell(50,2, $textoDerecha, 0, 'L');
		
		$this->Ln(8);
		$this->SetFont('Courier','b',12);
		$titulo = "Libro de Banco del mes ".$this->mes." a".utf8_decode('ñ')."o ".$this->anio . "\n\n";
                $titulo .= "Cuenta: ".$this->descripCuenta;
		$this->MultiCell(0, 2, $titulo, 0, 'C');
		
		//Encabezados
		$this->Ln(5);
		$this->SetFont('Courier', 'B', 8);
		$this->Cell($this->wReferencia, 4, "# Doc. Origen", TLRB, '', 'C');
		$this->Cell($this->wDocumento, 4, utf8_decode("Tipo Operación"), TRB, '', 'C');
		$this->Cell($this->wFecha, 4, "Fecha Comp. C", TRB, '', 'C');
                $this->Cell($this->wFechaReg, 4, "Fecha Doc.", TRB, '', 'C');
		$this->Cell($this->wBeneficiario, 4, "Beneficiario", TRB, '', 'C');
		$this->Cell($this->wConcepto, 4, "Concepto", TRB, '', 'C');
		$this->Cell($this->wMonto, 4, "Monto", TRB, '', 'C');
		$this->Ln(5);
		
	}

	//Pie de página
	function Footer()
	{
		$this->line(10,$this->GetY(),260,$this->GetY());
		$this->SetFont('Arial','B', 10);
		$this->cell(250,5,'Total Transacciones: '.$this->subTotalTrans,0,1,'R');
		$this->cell(250,5,'Total Bs.: '.muestrafloat($this->subTotalBs),0,1,'R');
		$this->SetFont('Arial','I', 7);
	}
}

//Creación del objeto de la clase heredada
$pdf = new PDF('L');
$pdf->anio = $anio;
$pdf->mes = $mes;
$pdf->descripCuenta = $cuenta_banc->desc_cuenta; 
$pdf->subTotalBs = 0;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetLeftMargin(10);
$pdf->SetFillColor(240);

while (!$r->EOF)
{
	$tipo = $r->fields['origen'];
	if ($tipo == "CHQ")
		$origen_desc = "Cheque";
	else if ($tipo == "DEP")
		$origen_desc = utf8_decode("Depósito");
	else if ($tipo == "TRA" || $tipo == "TRM")
		$origen_desc = "Transferencia";
	else if ($tipo == "OP")
		$origen_desc = utf8_decode("Orden de Pago");
	else if ($tipo == "ND")
		$origen_desc = utf8_decode("Nota de Débito");
	else if ($tipo == "NC")
		$origen_desc = utf8_decode("Nota de Crédito");
	
	$pdf->SetFont('Courier', 	'', 8);
        $pdf->Cell($pdf->wReferencia, 4, $r->fields['num_doc'], 0,'','C');
	

	$sum = 0;
	
	
        $desdet = '';
	
        //Caso transferencias Internas o Externas
	if($r->fields['origen'] == 'TRM' or $r->fields['origen'] == 'TRA'){
            if(substr($r->fields['num_doc'],0,3) != '017'){
                $qdet = "select a.nrodoc,a.origen,a.descripcion,a.fecha,a.monto,a.beneficiario,a.tipo
                        from finanzas.transferencias a
                        where a.nrodoc = '".$r->fields['num_doc']."'";
            }else{
                $qdet = "select a.nrodoc,a.descripcion,a.fecha,a.beneficiario,1 as tipo,b.nroref
                        from finanzas.otros_pagos a
                        inner join finanzas.relacion_otros_pagos b on a.nrodoc = b.nrodoc
                        where a.nrodoc =  '".$r->fields['num_doc']."'";
            }
		$rqdet = $conn->Execute($qdet);
                $desbeneficiario = dividirStr(utf8_decode($rqdet->fields['beneficiario']),intval($pdf->wBeneficiario/$pdf->GetStringWidth('M')));
                $pdf->Cell($pdf->wDocumento, 4, $tipo.': '.$origen_desc);
                $pdf->Cell($pdf->wFecha, 4, muestraFecha($r->fields['fecha']), 0);
                $pdf->Cell($pdf->wFechaReg, 4, muestraFecha($rqdet->fields['fecha']), 0); //OK
                if($rqdet->fields['tipo']==1)
                    $pdf->Cell($pdf->wBeneficiario, 4, $desbeneficiario[0], 0, '', 'L');
                else
                    $pdf->Cell($pdf->wBeneficiario, 4, 'SAMAT', 0, '', 'L');
		$desdet = ' Detalle: '.$rqdet->fields['descripcion'];	
        //Caso para ingresos        
	}elseif($r->fields['origen'] == 'DEP' or $r->fields['origen'] == 'NC' or $r->fields['origen'] == 'ND'){
                $desdet = ' Detalle: '.$r->fields['descripcion'];	
                $pdf->Cell($pdf->wDocumento, 4, $tipo.': '.$origen_desc);
                $pdf->Cell($pdf->wFecha, 4, muestraFecha($r->fields['fecha']), 0);
                $pdf->Cell($pdf->wFechaReg, 4, muestraFecha($r->fields['fecha_documento']), 0); //OK
		$pdf->Cell($pdf->wBeneficiario, 4, 'SAMAT', 0, '', 'L');
        //Caso para Cheques, Otros Pagos y Retenciones        
	}else{
            $qp = "select a.nrodoc,b.nroref,a.fecha as fechapago,a.nro_cheque,a.concepto,c.rif,c.nombre, d.monto_anticipo
                from finanzas.cheques a
                inner join finanzas.relacion_cheque b on a.nrodoc = b.nrodoc
                inner join puser.proveedores c on a.id_proveedor = c.id 
                inner join finanzas.orden_pago d on b.nroref = d.nrodoc
                where a.nrodoc = '".$r->fields['num_doc']."'";
            $rqp = $conn->execute($qp);
            $desbeneficiario = dividirStr(utf8_decode($rqp->fields['nombre']."(".$rqp->fields['rif'].")"),intval($pdf->wBeneficiario/$pdf->GetStringWidth('M')));
            $pdf->Cell($pdf->wDocumento, 4, $tipo.': '.$rqp->fields['nro_cheque']);
            $pdf->Cell($pdf->wFecha, 4, muestraFecha($r->fields['fecha']), 0);
            $pdf->Cell($pdf->wFechaReg, 4, muestraFecha($rqp->fields['fechapago']), 0); //OK
            $pdf->Cell($pdf->wBeneficiario, 4,$desbeneficiario[0] , 0, '', 'L');
            $desdet = " Detalle: ".$rqp->fields['nroref']." => ".$rqp->fields['concepto'];
        }	
        $descCom = dividirStr(utf8_decode($r->fields['descripingreso'].$desdet), intval($pdf->wConcepto/$pdf->GetStringWidth('M')));
	$pdf->Cell($pdf->wConcepto, 4, $descCom[0], 0,'','L');
	$pdf->Cell($pdf->wMonto, 4, muestraFloat($r->fields['monto']), 0,0,'R');
	
	$pdf->Ln();
	next($descCom);
        next($desbeneficiario);
	while(!current($descCom)===false or !current($desbeneficiario)===false)
	{
                    $pdf->Cell($pdf->wReferencia, 4, '', 0);
                    $pdf->Cell($pdf->wDocumento, 4, '');
                    $pdf->Cell($pdf->wFecha, 4, '', 0);
                    $pdf->Cell($pdf->wFechaReg, 4, '', 0);
                    $pdf->Cell($pdf->wBeneficiario, 4, current($desbeneficiario), 0, '', 'L');
                    $pdf->Cell($pdf->wConcepto, 4, current($descCom), 0,'','L');
                    $pdf->Cell($pdf->wMonto, 4, '', 0,1);
                    next($descCom);
                    next($desbeneficiario);
	}	
	$pdf->subTotalTrans++;
	//if(substr($r->fields['origen'],1,2) != 'T-' and substr($r->fields['origen'],1,2) != 'S-')
		$pdf->subTotalBs += $r->fields['monto'];
	//else 
	//	$pdf->subTotalBs -= $r->fields['monto'];
	$r->movenext();
}



$pdf->Output();
?>
