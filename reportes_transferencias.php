<? 
require("comun/ini.php"); 
require ("comun/header.php");
?>
<span style="text-align:left" class="titulo_maestro">
	Reportes de Transferencias Internas/Externas
</span>
<center>
    <div align="left" id="formulario">
        <table width="454" border="0" align="center" style="margin-left: auto; margin-right: auto; font-size:10px;">
            <tr>
                <td width="100%" align="center">Reporte:
                    <select name="tipo" id="tipo">
                        <option value="" selected>Seleccione...</option>
                        <option value="tra">General de Transferencias</option>
                    </select>
                </td>    
            </tr>
        </table>
        <table width="454" border="0" align="center" style=" margin-left: auto; margin-right: auto; font-size:10px; ">
            <tr>
                <td id="txt_cedente"> </td>
                <td id="txt_receptora"></td>
            </tr>
            <tr>
                <td id="td_cuenta"> 
                    <div id="div_cedente">
                        
				
                        
                    </div> </td>
                    <td><div id="div_receptora">
                            
                            
                        </div></td>
            </tr>
            <tr >
                
                <td id="txt_status"> </td>
                <td id="txt_tipo"></td>
            </tr>
            <tr>
                <td id="td_combo_status"> <div id="combo_status"></div> </td><td><div id="combo_tipo"></div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td widht="55"  > <div id="fecha_desde"></div></td>
                <td widht="55" > <div id="fecha_hasta"></div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center"><div id="boton"></div></td>
            </tr>
        </table>
    </div>
</center>
<br>
<br>
<br>
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<script type="text/javascript">

    var wxR;
    act_codigo($F('tipo'));
    
    
    function imprimir()
    {
        var JsonAux;
        var escEnEje = <?=$escEnEje?>;
        if($('tipo').value=='')
            alert("Debe Seleccionar un Tipo de Reporte a Generar");
		else if($('tipo').value == 'tra')
        {
            wxR = window.open("reporte_transferencias.pdf.php?fecha_desde=" + $('busca_fecha_desde').value + "&fecha_hasta=" + $('busca_fecha_hasta').value + "&tipo=" + $('busca_tipo').value + "&status=" + $('busca_status').value + "&lstcedente=" + $('lst_cedente').value + "&lstreceptora=" + $('lst_receptora').value, "winX", "width=500,height=500,scrollbars=yes,resizable=yes,status=yes,toolbar=no,menubar=no,location=no");
            wxR.focus()
        }
    }

    function desactivar(opcion)
    {
        if (opcion==1)
        {
            if ($F('id_cp') != '')
            {
                $('busca_cp').disabled = true;
            }
            else
            {
                $('busca_cp').disabled = false;
            }
        }
        else
        {
            if ($F('busca_cp') == 0)
            {
                $('id_cp').disabled = false;
            }
            else
            {
                $('id_cp').disabled = true;
            }
        }
    }

    function act_codigo(tipo)
    {
        if (tipo == 'tra')
        {
            Element.show('txt_cedente');
            $('txt_cedente').innerHTML = '<br />Cuenta Cedente';
            $('div_cedente').innerHTML = <?=helpers::superComboSQL($conn,
                                                                        '',
                                                                        '',
                                                                        'lst_cedente',
                                                                        'lst_cedente',
                                                                        '',
                                                                        '',
                                                                        'id',
                                                                        'descripcion',
                                                                        false,
                                                                        'descripcion',
                                                                        "SELECT  b.id,a.nombre_corto ||' (' ||b.nro_cuenta ||')' as descripcion FROM public.banco a inner join finanzas.cuentas_bancarias b on a.id = b.id_banco order by a.nombre_corto,b.nro_cuenta")?>;
            Element.show('txt_receptora');                                                                                                            
            $('txt_receptora').innerHTML = '<br />Cuenta Receptora';
            $('div_receptora').innerHTML = <?=helpers::superComboSQL($conn,
                                                                        '',
                                                                        '',
                                                                        'lst_receptora',
                                                                        'lst_receptora',
                                                                        '',
                                                                        '',
                                                                        'id',
                                                                        'descripcion',
                                                                        false,
                                                                        'descripcion',
                                                                        "SELECT  b.id,a.nombre_corto ||' (' ||b.nro_cuenta ||')' as descripcion FROM public.banco a inner join finanzas.cuentas_bancarias b on a.id = b.id_banco order by a.nombre_corto,b.nro_cuenta")?>;
            
            Element.show('td_combo_status');
            Element.show('txt_status');
            $('txt_status').innerHTML = '<br />Estado';
            $('txt_tipo').innerHTML = '<br />Tipo';
            $('combo_status').innerHTML = "<select name=\"busca_status\" id=\"busca_status\"><option value=\"-1\">Seleccione<option value=\"0\">Aprobada</option><option value=\"1\">Anulada</option></select>";
            $('combo_tipo').innerHTML = "<select name=\"busca_tipo\" id=\"busca_tipo\"><option value=\"-1\">Seleccione<option value=\"0\">Internas</option><option value=\"1\">Externas</option></select>";
            $('fecha_desde').innerHTML = "<table width=\"120\" border=\"0\"><tr><td>Desde</td><td>&nbsp;</td></tr><tr><td><input style=\"width:100px\"  type=\"text\" name=\"busca_fecha_desde\" id=\"busca_fecha_desde\"/></td><td><div id=\"boton_busca_fecha_desde\"><a href=\"#\" onclick=\"return false;\"><img border=\"0\" alt=\"Seleccionar Fecha\" src=\"images/calendarA.png\" width=\"20\" height=\"20\" /></a></div></td></tr></table>";
            $('fecha_hasta').innerHTML = "<table width=\"120\" border=\"0\"><tr><td>Hasta</td><td>&nbsp;</td></tr><tr><td><input style=\"width:100px\" type=\"text\" name=\"busca_fecha_hasta\" id=\"busca_fecha_hasta\"/></td><td><div id=\"boton_busca_fecha_hasta\"><a href=\"#\" onclick=\"return false;\"><img border=\"0\"  alt=\"Seleccionar Fecha\" src=\"images/calendarA.png\" width=\"20\" height=\"20\" /></a></div></td></tr></table>";
            $('boton').innerHTML = "<td colspan=\"2\" width=\"84\" align=\"center\"><input name=\"button\" type=\"button\" onClick=\"imprimir()\" value=\"Generar...\" /></td>";
            calendario_desde();
            calendario_hasta();
		}
        else{
            $('txt_cedente').innerHTML = '';
            $('div_cedente').innerHTML = '';
            $('txt_receptora').innerHTML = '';
            $('div_receptora').innerHTML = '';
            $('txt_status').innerHTML = '';
            $('txt_tipo').innerHTML = '';
            $('combo_status').innerHTML = "";
            $('combo_tipo').innerHTML = '';
            $('fecha_desde').innerHTML = '';
            $('fecha_hasta').innerHTML = '';
            $('boton').innerHTML = '';
        }
    }

    Event.observe('tipo', "change", function () {
        act_codigo($F('tipo'));
    });

    /*Event.observe('id_cp', "change", function () {
  alert ($('id_cp'));
     *if ($F('id_cp') != '')
  {
    $('busca_cp').selectedIndex = 0; 
          desactivar($('busca_cp'));
  }/ 
});*/

    /*Event.observe('busca_cp', "change", function () {
  alert ($('busca_cp'));
     *  if ($F('busca_cp') != '')
  {
    $F('id').value = '';
          desactivar($('id_cp'));
  }* 
});*/

    function calendario_desde(){
        new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_desde",
            button            : "boton_busca_fecha_desde",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });
    }

    function calendario_hasta(){
        new Zapatec.Calendar.setup({
            firstDay          : 1,
            weekNumbers       : true,
            showOthers        : false,
            showsTime         : false,
            timeFormat        : "24",
            step              : 2,
            range             : [1900.01, 2999.12],
            electric          : false,
            singleClick       : true,
            inputField        : "busca_fecha_hasta",
            button            : "boton_busca_fecha_hasta",
            ifFormat          : "%d/%m/%Y",
            daFormat          : "%Y/%m/%d",
            align             : "Br"
        });
    }

</script>
<? 

require ("comun/footer.php"); 
?>	

