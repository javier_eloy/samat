<?
include ("comun/ini.php");
$pagina = $_REQUEST['pagina'];
$tamano_pagina = 20;

if (!$pagina) 
{
	$inicio = 10;
	$pagina=1;
}
else 
{
	$inicio = ($pagina - 1) * $tamano_pagina;
} 
$nombre = $_GET['nombre'];
$desde = $_GET['desde'];
$hasta = $_GET['hasta'];

$cResponsable = responsable::buscar($conn, $nombre, $desde, $hasta, $inicio);
//die(var_dump($cCuentaBanc));
$total = responsable::total_registro_busqueda($conn, $nombre, $desde, $hasta);

if(count($cResponsable) > 0)
{
?>
<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
	<tr class="cabecera"> 
		<td width="60%">Nombre</td>
		<td width="10%">Desde</td>
                <td width="10%">Hasta</td>
                <td width="10%">Tipossss</td>
		<td width="5%">&nbsp;</td>
		<td width="5%">&nbsp;</td>
	</tr>
<? 
	foreach($cResponsable as $cb) 
	{ 
?> 
	<tr class="filas"> 
		<td><?=$cb->nro_cuenta?></td>
		<td><?=$cb->banco->descripcion?></td>
		<td align="center">
			<a href="#" onClick="if (confirm('Si presiona Aceptar será eliminada esta información')){ return true;} else{return false;}"  title="Eliminar Registro"><img src="images/eliminar.gif" border="0" ></a>
		</td>
		<td align="center">
			<a href="#" onClick="updater(<?=$plan_cuenta->codcta?>); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a>
		</td>
	</tr>
<?
	}

	$total_paginas = ceil($total / $tamano_pagina);
?>
	<tr class="pietabla">
		<td colspan="7" align="center">
	<?
	for ($j=1;$j<=$total_paginas;$j++)
	{
		if ($j==1)
		{
			if ($j==$pagina)
				echo '<span class="actual">$j</span>';
			else
				echo '<span style="cursor:pointer" onclick="busca($(\'busca_nro_cta\').value,$(\'busca_bancos\').value , \'$j\');">$j</span>';
		}
		else
		{
			if ($j==$pagina)
				echo "<span>- $j</span>";
			else
				echo '<span style="cursor:pointer" onclick="busca($(\'busca_nro_cta\').value,$(\'busca_bancps\').value , \'$j\');">- $j</span>';
		}
	}
	?>
		</td>
	</tr>
	<tr class="pietabla">
		<td colspan="7" align="center"> Pagina <strong><?=$_REQUEST['pagina']?></strong> de <strong><?=$total_paginas?></strong></td>
	</tr>
</table>
<?
}
else 
{
	echo "No hay registros en la bd";
}
?>
