<?
require ("comun/ini.php");
// Creando el objeto entidades
$oEntidades = new entidades;
$accion = $_REQUEST['accion'];

switch($accion) {
    case 'Guardar':
        $oEntidades->add($conn, $_POST['id_nuevo'], $_POST['descripcion']);
        break;
    case 'Actualizar':
        $oEntidades->set($conn, $_POST['id_nuevo'], $_POST['id'], $_POST['descripcion']);
        break;
    case 'del':
        $oEntidades->del($conn, $_POST['id']));
        break;
}
$msg = $oEntidades->msg;

$cEntidades=$oEntidades->get_all($conn);

require ("comun/header.php");
if(!empty($msg)) echo "<div id=\"msj\">".$msg."</div><br/>";
?>

<br />
<span class="titulo_maestro">Maestro de entidades</span>
<div id="formulario">
    <a href="#" onclick="updater(0); return false;">Agregar Nuevo Registro</a>
</div>
<br />
<? if(is_array($cEntidades)) { ?>
<table class="sortable" id="grid" cellpadding="0" cellspacing="1">
    <tr class="cabecera">
        <td>C&oacute;digo</td>
        <td>Descripción</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <?
        $i = 0;
        foreach($cEntidades as $entidades) {
            ?>
    <tr class="filas">
        <td><?=$entidades->id?></td>
        <td><?=$entidades->descripcion?></td>
        <td><a href="?accion=del&id=<?=$entidades->id?>" onclick="if (confirm('Si presiona Aceptar será eliminada esta información')){ return true;} else{return false;}"  title="Eliminar Registro"><img src="images/eliminar.gif" border="0" ></a></td>
        <td align="center">
            <a href="#" onclick="updater('<?=$entidades->id?>'); return false;" title="Modificar ó Actualizar Registro" ><img src="images/actualizar.gif" width="16" height="10" border="0"></a></td>
    </tr>
            <? $i++;
        }
        ?>
</table>
    <? }else {
    echo "No hay registros en la bd";
} ?>
<br />
<div style="height:40px;padding-top:10px;">
    <p id="cargando" style="display:none;margin-top:0px;">
        <img alt="Cargando" src="images/loading.gif" /> Cargando...
    </p>
</div>
<!-- <a href="#" onclick="alert($('formulario').innerHTML)">AAAA</a> -->
<?
$validator->create_message("error_cod", "id_nuevo", "*");
$validator->create_message("error_desc", "descripcion", "*");
$validator->print_script();
require ("comun/footer.php");
?>
