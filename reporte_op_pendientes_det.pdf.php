<?
include("comun/ini.php");
$fecha_desde = $_REQUEST['fecha_desde'];
$fecha_hasta = $_REQUEST['fecha_hasta'];
$id_proveedor = $_REQUEST['id_proveedor'];
$id_ue = $_REQUEST['id_ue'];
$tipoCom = $_REQUEST['tipocom'];
$tipoProv = $_REQUEST['tipoprov'];
$id_status = 2;


function dividirStr($str, $max) {
    $strArray = array();
    do {
        if (strlen($str) > $max)
            $posF = strrpos( substr($str, 0, $max), ' ' );
        else
            $posF = -1;

        if ($posF===false || $posF==-1) {
            $strArray[] = substr($str, 0);
            $str = substr($str, 0);
            $posF = -1;
        }
        else {
            $strArray[] = substr($str, 0, $posF);
            $str = substr($str, $posF+1 );
        }
    }while ($posF != -1);

    return ($strArray);
}

class PDF extends FPDF {
    function Header() {
         parent::header();
        //$this->SetLeftMargin(18);
        //$this->SetFont('Courier','',10);
        //$this->Ln(1);
        //$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda
        //$this->SetXY(42, 15);
        //$textoCabecera = "INTENDENCIA MUNICIPAL\n";
        //$this->MultiCell(50,2, $textoCabecera, 0, 'L');

        //$ovehiculo = unserialize($_SESSION['pdf']);
        //$tipo = $ovehiculo->id_tipo_documento;

        $this->SetXY(200, 15);
        $textoFecha = "Fecha: ".date('d/m/Y')."\n\n";
        $this->Text(200, 10, $textoFecha);
        //$textoDerecha.= "Nro.:".$nro_recibo."\n";
        //$textoPag = "Fecha Generac.:".muestrafecha($fecha)."\n\n";
        
        //$textoDerecha.= "Fecha Aprob.:".muestrafecha($oOrden->fecha_aprobacion)."\n";
        $textoPag = "P".utf8_decode('á')."g: ".$this->PageNo()." de {nb}\n\n";
        $this->Text(200, 15, $textoPag);
        //$this->MultiCell(100,2, $textoDerecha, 0, 'L');

        $this->Ln();

        $this->SetFont('Courier','b',12);
        //if($tipo == '002')
        //	$tipoOrden = "Orden de Servicio";
        //elseif($tipo == '009')
        $tipoOrden = "DETALLADO DE ORDENES DE PAGO POR PAGAR";

        $this->Text(100, 20, $tipoOrden);
        //$this->Line(18, 41, 393, 41);
        $this->Ln(20);
        $this->Cell(70,5,'Beneficiario',1,0,'C');
        $this->Cell(30,5,'# Documento',1,0,'C');
        $this->Cell(25,5,'Fecha',1,0,'C');
        $this->Cell(30,5,'Monto',1,0,'C');
        $this->Cell(10,5,'TD',1,0,'C');
        $this->Cell(30,5,'Compromiso',1,0,'C');
        $this->Cell(25,5,'Fecha',1,0,'C');
        //$pdf->Cell(30,5,'Status',1,0,'C');
        $this->Cell(40,5,'Retenciones',1,0,'C');
        $this->Ln();
    }

    function Footer() {

        //$this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //N�mero de p�gina
        //$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
//Creaci�n del objeto de la clase heredada
$pdf=new PDF('l');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','B',12);
$pdf->SetLeftMargin(15);

//ORDENES DE PAGO SIN PAGOS RELACIONADOS
$q = "SELECT DISTINCT op.nrodoc as nrodoc, op.id_proveedor, op.id_unidad_ejecutora, op.fecha, ue.descripcion AS unidad_ejecutora, p.nombre AS proveedor,  op.montodoc AS montodoc, ";
$q.= " mp.nrodoc AS nrodoccom, mp.fechadoc AS fechacom, op.status, td.abreviacion ";
$q.= "FROM finanzas.orden_pago op ";
$q.= "INNER JOIN puser.unidades_ejecutoras ue ON (op.id_unidad_ejecutora = ue.id AND ue.id_escenario = '$escEnEje') ";
$q.= "INNER JOIN puser.proveedores p ON (op.id_proveedor = p.id) ";
$q.= "LEFT JOIN finanzas.solicitud_pago sp ON (sp.nrodoc = op.nroref) ";
$q.= "LEFT JOIN puser.movimientos_presupuestarios mp ON (mp.nrodoc = sp.nroref) ";
$q.= "LEFT JOIN puser.tipos_documentos td ON (substr(sp.nroref,1,3) = td.id )";
$q.= "WHERE op.nrodoc NOT IN ( SELECT COALESCE(nroref::char(13)) FROM finanzas.relacion_cheque) ";
$q.= "AND op.nrodoc NOT IN ( SELECT COALESCE(nroref::char(13)) FROM finanzas.relacion_otros_pagos) ";
$q.= !empty($id_status) ? "AND op.status = '$id_status' " : ""; 
$q.= !empty($fecha_desde) ? "AND op.fecha >= '".guardaFecha($fecha_desde)."' " : "";
$q.= !empty($fecha_hasta) ? "AND op.fecha <= '".guardaFecha($fecha_hasta)."' " : "";
$q.= !empty($id_proveedor) ? "AND op.id_proveedor = '$id_proveedor' " : "";
$q.= !empty($id_ue) ? "AND op.id_unidad_ejecutora = '$id_ue' " : "";
$q.= !empty($tipoCom) ? "AND COALESCE(mp.tipdoc,'0') = '$tipoCom' " : "";
$q.= !empty($tipoProv) ? "AND p.provee_contrat = '$tipoProv' " : "";
//$q.= "ORDER BY op.nrodoc ";
$q.= "ORDER BY op.id_proveedor,op.fecha ";


$r = $conn->Execute($q);
$pdf->SetFont('Courier','B',10);

//$pdf->Cell(115,5,'Beneficiario',1,0,'C');
//$pdf->Cell(30,5,'# Documento',1,0,'C');
//$pdf->Cell(25,5,'Fecha',1,0,'C');
//$pdf->Cell(40,5,'Monto',1,0,'C');
//$pdf->Cell(22,5,'Tipo Doc.',1,0,'C');
//$pdf->Cell(40,5,'Compromiso',1,0,'C');
//$pdf->Cell(35,5,'Fecha',1,0,'C');
//$pdf->Cell(30,5,'Status',1,0,'C');
//$pdf->Cell(68,5,'Retenciones',1,0,'C');
//$pdf->Ln();
$pdf->SetFont('Courier','',10);
$total_op_p = 0;
$pro_act = 0;
$pro_act_des = '';

$TotalRetenciones = 0;

while(!$r->EOF) {
    //Sub total por porveedor
    if($pro_act == 0) {
        $pro_act = $r->fields['id_proveedor'];
        $pro_act_des = $r->fields['proveedor'];
    }
    if($pro_act != $r->fields['id_proveedor']) {
        $pdf->Line(19, $pdf->GetY(), 265, $pdf->GetY());
        $pdf->SetXY(19,$pdf->GetY()+1);
        $pdf->SetFont('Courier','B');
        $total_op_p = $total_op_p  + calculafloat($sub_total_pro,2);
        $pdf->Cell(100,5,'SUB-TOTAL '.$pro_act_des.' BsF. '.muestraFloat($sub_total_pro-$TotalRetenido),0,1,'L');
        $pdf->SetFont('Courier','');
        $pdf->Line(19, $pdf->GetY(), 265, $pdf->GetY());
        $pdf->Ln(3);
        $sub_total_pro = $r->fields['montodoc'];
        $pro_act_des = $r->fields['proveedor'];
        $pro_act = $r->fields['id_proveedor'];
    }else {
        $sub_total_pro = $sub_total_pro + $r->fields['montodoc'];
    }
    //$desc_unidad = dividirStr($r->fields['unidad_ejecutora'], intval(65/$pdf->GetStringWidth('M')));
    //$pdf->Cell(90,5,utf8_decode($desc_unidad[0]),0,0,'L');
    $desc_proveedor = dividirStr($r->fields['proveedor'], intval(70/$pdf->GetStringWidth('M')));
    $pdf->Cell(70,5,utf8_decode($desc_proveedor[0]),0,0,'L');
    $pdf->Cell(30,5,$r->fields['nrodoc'],0,0,'C');
    $pdf->Cell(25,5,muestraFecha($r->fields['fecha']),0,0,'C');
    $pdf->Cell(30,5,muestraFloat($r->fields['montodoc']),0,0,'R');
    $pdf->Cell(10,5,$r->fields['abreviacion'],0,0,'C');
    $nrodoccom = (!empty($r->fields['nrodoccom'])) ? $r->fields['nrodoccom'] : "OP";
    $pdf->Cell(30,5,$nrodoccom,0,0,'C');
    $fechacom = (!empty($r->fields['fechacom'])) ? muestraFecha($r->fields['fechacom']) : "SIN/IMP";
    $pdf->Cell(25,5,$fechacom,0,0,'C');

    //Segundas lineas de descripciones
    $hay_pv = next($desc_proveedor);
    for ($i=1; $hay_pv!==false; $i++) {
        $pdf->Ln();
        $pdf->Cell(65,4, $desc_proveedor[$i],0, '','L');
        $pdf->Cell(25,4,'',0,0,'C');
        $pdf->Cell(30,4, '',0, '','L');
        $pdf->Cell(25,4, '',0, '','L');
        $pdf->Cell(20,4, '',0, '','C');
        $hay_pv = next($desc_proveedor);
    }
    /// Retenciones -------------------------------
    $d="SELECT rr.mntret,ra.descri AS descripcion ";
    $d.="FROM finanzas.relacion_retenciones_orden rr ";
    $d.="INNER JOIN finanzas.retenciones_adiciones ra ON (rr.codret = ra.id) ";
    $d.="WHERE nrodoc = '".$r->fields['nrodoc']."'";
    $d.=" UNION ";
    $d.="SELECT pr.monto,'Ret. Nomina' As Descripcion FROM rrhh.presupuesto_retenciones pr ";
    $d.="WHERE nro_doc_causado = '".$r->fields['nrodoc']."'";
    $d.=" UNION ";
    $d.="SELECT f.iva_retenido,'Ret. IVA' As Descripcion FROM finanzas.facturas f ";
    $d.="WHERE nrodoc = '".$r->fields['nrodoc']."'";
    $pdf->Ln();
    //$pdf->Cell(255,10,$d,2,0,'R');
    //die($d);
    $rr = $conn->Execute($d);
    $ii=1;
    //$rr->MoveFirst;
    $TotalRetenido = 0;
    while(!$rr->EOF) {
        $pdf->Cell(200,5,"---------->",0,0,'R');
        $pdf->Cell(25,5,$rr->fields['descripcion'],0,0,'L');
        $pdf->Cell(15,5,muestraFloat($rr->fields['mntret']),0,1,'R');
        $TotalRetenido += $rr->fields['mntret'];
        $rr->MoveNext();
        //$pdf->Ln();
    }
    if($TotalRetenido > 0){
      $pdf->SetX(225);
      $pdf->SetFont('Courier','B',11);
      $pdf->Cell(30,5,'Total Retenido: '.muestraFloat($TotalRetenido),0,1,'R');
      $TotalRetenciones += $TotalRetenido;
    }
    /// Siguiente Registro -----------------------
    $r->MoveNext();
    $pdf->Ln();

}

if($sub_total_pro >= 0) {
    $pdf->Line(19, $pdf->GetY(), 265, $pdf->GetY());
    $pdf->SetXY(19,$pdf->GetY()+1);
    $pdf->SetFont('Courier','B');
    $total_op_p = $total_op_p  + calculafloat($sub_total_pro,2);
    $pdf->Cell(100,5,'SUB-TOTAL '.$pro_act_des.' BsF. '.muestraFloat($sub_total_pro-$TotalRetenido),0,1,'L');
    $pdf->SetFont('Courier','');
    $pdf->Line(19, $pdf->GetY(), 265, $pdf->GetY());
    $pdf->Ln(3);
    $sub_total_pro = 0.0;
    $pro_act_des = '';
    $pro_act = 0.0;
}

$pdf->Ln();
$pdf->SetFont('Courier','B',11);
$pdf->SetX(20);
$pdf->Cell(19,4,'TOTAL RETENCIONES',0,0,'L');
$pdf->Cell(55,4,'BsF. '.muestraFloat($TotalRetenciones),0,0,'R');
$pdf->SetX(130);
$pdf->Cell(19,4,'TOTAL GENERAL',0,0,'L');
$pdf->Cell(55,4,'BsF. '.muestraFloat($total_op_p-$TotalRetenciones),0,0,'R');
if($anoCurso <= 2007) {
    $pdf->Ln();
    $pdf->Cell(74,4,'Bs.- '.muestraFloat($total_op_p /1000),0,0,'R');
}


$pdf->SetFont('Courier','',8);
$pdf->Output();
?>