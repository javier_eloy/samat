<?
include("comun/ini.php");
include ("Constantes.php");

$tipo = $_GET['tipo'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];
$detallado = $_GET['detallado'];

//Estado de Ganancias y Perdidas
if ($tipo == 'GP')
{
        $sql = "SELECT consolidado.*, plan_cuenta.codcta,plan_cuenta.descripcion, plan_cuenta.movim FROM contabilidad.consolidado ";
        $sql.= "INNER JOIN contabilidad.plan_cuenta ON (plan_cuenta.codcta = consolidado.cod_cta) ";
        $sql.= "WHERE consolidado.ano = $anio AND consolidado.mes = $mes AND (substring(codcta,1,1) = '3' or substring(codcta,1,1) = '4') ";
        $sql.= "ORDER BY plan_cuenta.codcta::char(1), plan_cuenta.codcta::char(2), plan_cuenta.codcta::char(3), plan_cuenta.codcta::char(4), plan_cuenta.codcta::char(5), plan_cuenta.codcta::char(6), plan_cuenta.codcta::char(7), plan_cuenta.codcta::char(8), plan_cuenta.codcta::char(9), plan_cuenta.codcta::char(14) ";
        //$sql.= "ORDER BY substring(codcta, 1, 1)::char, substring(codcta, 2, 1)::char, substring(codcta, 3, 3)::char ";
}

//Balance de Comprobacion, Libro Legal y Diario Legal
if ($tipo == 'D' or $tipo == 'L' or $tipo == 'DL')
{
	if ($detallado == 'N')
	{
		$sql = "SELECT consolidado.*, plan_cuenta.codcta,plan_cuenta.descripcion, plan_cuenta.movim FROM contabilidad.consolidado ";
		$sql.= "INNER JOIN contabilidad.plan_cuenta ON (plan_cuenta.codcta = consolidado.cod_cta and consolidado.debe + consolidado.haber + consolidado.saldo_act <> 0) ";
                $sql.= "WHERE consolidado.ano = $anio AND consolidado.mes = $mes order by plan_cuenta.codcta::char(1),plan_cuenta.codcta::char(3),plan_cuenta.codcta::char(5),plan_cuenta.codcta::char(7),plan_cuenta.codcta::char(9),plan_cuenta.codcta::char(14)";
		//$sql.= "WHERE plan_cuenta.movim = 'N' ";
	}
	else
	{
		$sql = "SELECT consolidado.*, plan_cuenta.codcta, plan_cuenta.descripcion, plan_cuenta.movim FROM contabilidad.consolidado ";
		$sql.= "INNER JOIN contabilidad.plan_cuenta ON (consolidado.cod_cta = plan_cuenta.codcta) ";
		$sql.= "WHERE consolidado.ano = $anio AND consolidado.mes = $mes  and consolidado.saldo_act <>0 ";
		$sql.= "ORDER BY plan_cuenta.codcta::char(1), plan_cuenta.codcta::char(3), plan_cuenta.codcta::char(5), plan_cuenta.codcta::char(7), plan_cuenta.codcta::char(9), plan_cuenta.codcta::char(14) ";
        //$sql.= "ORDER BY cod_cta::char(14)";
	}
		
	
}elseif($tipo == 'G')
{
	$sql = "SELECT plan_cuenta.descripcion, plan_cuenta.codcta, COALESCE(consolidado.saldo_act, 0) AS saldo_act FROM contabilidad.plan_cuenta ";
	$sql.= "LEFT JOIN contabilidad.consolidado ON (consolidado.cod_cta = plan_cuenta.codcta) ";
	$sql.= "WHERE saldo_act <> 0 and LENGTH(codcta) = 5 AND (consolidado.ano = $anio OR consolidado.ano IS NULL) AND (consolidado.mes = $mes OR consolidado.mes IS NULL) ";
	$sql.= "ORDER BY plan_cuenta.codcta::char(1), plan_cuenta.codcta::char(3), plan_cuenta.codcta::char(5) ";
}elseif($tipo == 'BG')
{
	$sql = "SELECT consolidado.*, plan_cuenta.codcta,plan_cuenta.descripcion, plan_cuenta.movim FROM contabilidad.consolidado ";
        $sql.= "INNER JOIN contabilidad.plan_cuenta ON (plan_cuenta.codcta = consolidado.cod_cta) ";
        $sql.= "WHERE consolidado.ano = $anio AND consolidado.mes = $mes AND (substring(codcta,1,1) = '1' or substring(codcta,1,1) = '2' or substring(codcta,1,1) = '3') AND length(codcta)<=9 ";
        //$sql.= "ORDER BY substring(plan_cuenta.codcta, 1, 1)::char, substring(plan_cuenta.codcta, 2, 2)::char, substring(plan_cuenta.codcta, 4, 2)::char ";
        $sql.= "ORDER BY plan_cuenta.codcta::char(1), plan_cuenta.codcta::char(2), plan_cuenta.codcta::char(3), plan_cuenta.codcta::char(4), plan_cuenta.codcta::char(5), plan_cuenta.codcta::char(6), plan_cuenta.codcta::char(7), plan_cuenta.codcta::char(8), plan_cuenta.codcta::char(9), plan_cuenta.codcta::char(14) ";
}
//die($sql);

$r = $conn->Execute($sql);
/*while(!$r->EOF)
{
	var_dump($r->fields);
	echo "<br /><br />";
	$r->movenext();
}*/
// Crea un array donde cada posicion es un string de tamaÃ±o 'max' caracteres,
// teniendo en cuenta de no cortar una palabra, busca el espacio en blanco  
// mas cerca del tamaÃ±o 'max' y ahi corta el string

function dividirStr($str, $max)
  {
    $strArray = array();
    do
    {
      if (strlen($str) > $max)
        $posF = strrpos( substr($str, 0, $max), ' ' );
      else
        $posF = -1;
      
      if ($posF===false || $posF==-1)
      {
        $strArray[] = substr($str, 0);
        $str = substr($str, 0);
        $posF = -1;
      }
      else
      {
        $strArray[] = substr($str, 0, $posF);
        $str = substr($str, $posF+1 );
      }
    }while ($posF != -1);
    
    return ($strArray);
  }

class PDF extends FPDF
{
	var $anio;
	var $mes;
	var $tipo;
	
	// Definen el ancho de las celdas en el balance de comprobacion
	var $codCtaDW   = 26;
	var $descCtaDW  = 50;
	var $saldoAntDW = 23;
	var $debeDW     = 23;
	var $haberDW    = 23;
        var $saldoMes   = 23;
	var $saldoActDW = 23;
	
	// Definen el ancho de las celdas en el balance general
	var $codCtaGW   = 7;
	var $descCtaGW  = 57;
	var $saldoGW    = 28;

	//Cabecera de pÃ¡gina
	function Header()
	{
                        parent::Header();
//			$this->SetFillColor(240);
//			$this->SetLeftMargin(15);
//			$this->SetFont('Courier','',6);
//			$this->Ln(1);
//			$this->Image ("images/logoa.jpg",15,4,26);//logo a la izquierda
//			$this->SetXY(42, 6);
//			$textoCabecera = PAIS."\n";
//			$textoCabecera.= ENTE."\n";
//                        $textoCabecera.= UBICACION."\n";
//			$this->MultiCell(50,2, $textoCabecera, 0, 'L');

                        $this->SetFillColor(240);
                        $this->SetFont('Courier','',6);
			$this->SetXY(163, 6);
			$textoFecha = "Fecha: ".date('d/m/Y')."\n";
                        $this->Text(163, 10, $textoFecha);
			$textoPag= "Pag: ".$this->PageNo()." de {nb}\n";
                        $this->Text(163, 15, $textoPag);
			//$this->MultiCell(50,2, $textoDerecha, 0, 'L');
			
			//$this->Ln(12);
                        $this->SetXY(15, 30);
			$this->SetFont('Courier','b',12);
			if ($this->tipo == 'G')
				$titulo = "Balance General de la Hacienda Pública Municipal\n\n Al mes ".sprintf("%02d", $this->mes)." del ".$this->anio;
			if ($this->tipo == 'D')
				$titulo = "Balance de Comprobación al Mes ".sprintf("%02d", $this->mes)." del Año ".$this->anio;
                        if ($this->tipo == 'L')
				$titulo = "Libro Mayor del Mes ".sprintf("%02d", $this->mes)." del Año ".$this->anio;
                        if ($this->tipo == 'DL')
				$titulo = "Diario Legal del Mes ".sprintf("%02d", $this->mes)." del Año ".$this->anio;
                        if ($this->tipo == 'GP')
				$titulo = "Estado de Ganancias y Pérdidas al Mes ".sprintf("%02d", $this->mes)." del Año ".$this->anio;
                        if ($this->tipo == 'BG')
				$titulo = "Balance General al Mes ".sprintf("%02d", $this->mes)." del Año ".$this->anio;
			
			$this->MultiCell(0, 2, utf8_decode($titulo), 0, 'C');
			$this->SetLeftMargin(10);
			if ($this->tipo == 'D' or $this->tipo == 'L')
			{
				$this->Ln(5);
				$this->setFont('Courier', 'B', 7);
				$this->Cell($this->codCtaDW, 4, utf8_decode('Código de Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->descCtaDW, 4, utf8_decode('Descripción de la Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->saldoAntDW, 4, utf8_decode('Saldo Anterior'), 0, '', 'C', 1);
				$this->Cell($this->debeDW, 4, utf8_decode('Monto Debe'), 0, '', 'C', 1);
				$this->Cell($this->haberDW, 4, utf8_decode('Monto Haber'), 0, '', 'C', 1);
                                if($this->tipo == 'D')
                                    $this->Cell($this->saldoMes, 4, utf8_decode('Saldo del Mes'), 0, '', 'C', 1);
				$this->Cell($this->saldoActDW, 4, utf8_decode('Saldo Actual'), 0, '', 'C', 1);
				$this->Ln(4.8);
			}
                        elseif($this->tipo == 'DL'){
                            $this->Ln(5);
				$this->setFont('Courier', 'B', 7);
				$this->Cell($this->codCtaDW, 4, utf8_decode('Código de Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->descCtaDW+$this->saldoAntDW, 4, utf8_decode('Descripción de la Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->debeDW+$this->saldoMes, 4, utf8_decode('Monto Debe'), 0, '', 'C', 1);
				$this->Cell($this->haberDW+$this->saldoActDW, 4, utf8_decode('Monto Haber'), 0, '', 'C', 1);
				$this->Ln(4.8);
                        }
                        elseif ($this->tipo == 'GP')
			{
				$this->Ln(5);
				$this->setFont('Courier', 'B', 7);
				$this->Cell($this->codCtaDW, 4, utf8_decode('Código de Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->descCtaDW+$this->saldoAntDW, 4, utf8_decode('Descripción de la Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->debeDW, 4, utf8_decode('Monto Debe'), 0, '', 'C', 1);
				$this->Cell($this->haberDW, 4, utf8_decode('Monto Haber'), 0, '', 'C', 1);
				$this->Cell($this->saldoActDW, 4, utf8_decode('Saldo'), 0, '', 'C', 1);
				$this->Ln(4.8);
			}elseif ($this->tipo == 'BG')
			{
				$this->Ln(5);
				$this->setFont('Courier', 'B', 7);
				$this->Cell($this->codCtaDW, 4, utf8_decode('Código de Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->descCtaDW+$this->saldoAntDW+$this->debeDW, 4, utf8_decode('Descripción de la Cuenta'), 0, '', 'L', 1);
				$this->Cell($this->saldoActDW+$this->haberDW, 4, utf8_decode('Saldo'), 0, '', 'C', 1);
				$this->Ln(4.8);
			}
			else
			{
				$this->Ln();
				$this->Line(10, $this->GetY(), 198, $this->GetY());
				$this->Ln();
			}
	}

	//Pie de pÃ¡gina
	function Footer()
	{
		//Arial italic 8
		$this->SetFont('Arial','I', 7);
		//NÃºmero de pÃ¡gina
		//$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}

        //Imprime subtotal de cuentas contables
        function imp_total($cc,$descripcion,$acumulador){
        	return true;
            if($acumulador[4] != 0 and $this->tipo == 'L'){
                $this->SetFont('Courier', 'B', 7);
                $this->Line(85, $this->GetY(), 200, $this->GetY());
                $this->SetX(25);
                $this->Cell($this->descCtaDW,4, 'Total '.$descripcion.' ('.$cc.'): ',0,'','L',false);
                $this->SetX(85);
                $this->Cell($this->saldoAntDW,4,number_format($acumulador[0],2,',','.'),0,'','R',false);
                $this->Cell($this->debeDW,4,number_format($acumulador[1],2,',','.'),0,'','R',false);
                $this->Cell($this->haberDW,4,number_format($acumulador[2],2,',','.'),0,'','R',false);
                if($this->tipo == 'D')
                  $this->Cell($this->saldoMes,4,number_format($acumulador[3],2,',','.'),0,'','R',false);
                $this->Cell($this->saldoActDW,4,number_format($acumulador[4],2,',','.'),0,'1','R',false);
                $this->Ln(2);
                $this->SetFont('Courier','', 7);
            }elseif($this->tipo == 'D'){
                $this->SetFont('Courier', 'B', 7);
                $this->Line(85, $this->GetY(), 200, $this->GetY());
                $this->SetX(25);
                $this->Cell($this->descCtaDW,4, 'Total '.$descripcion.' ('.$cc.'): ',0,'','L',false);
                $this->SetX(85);
                $this->Cell($this->saldoAntDW,4,number_format($acumulador[0],2,',','.'),0,'','R',false);
                $this->Cell($this->debeDW,4,number_format($acumulador[1],2,',','.'),0,'','R',false);
                $this->Cell($this->haberDW,4,number_format($acumulador[2],2,',','.'),0,'','R',false);
                $this->Cell($this->saldoMes,4,number_format($acumulador[3],2,',','.'),0,'','R',false);
                $this->Cell($this->saldoActDW,4,number_format($acumulador[4],2,',','.'),0,'1','R',false);
                $this->Ln(2);
                $this->SetFont('Courier','', 7);
            }elseif($this->tipo == 'DL' and $acumulador[1] + $acumulador[2] != 0){
                $this->SetFont('Courier', 'B', 7);
                $this->Line(105, $this->GetY(), 200, $this->GetY());
                $this->SetX(25);
                $this->Cell($this->descCtaDW+$this->saldoAntDW,4, 'Total '.$descripcion.' ('.$cc.'): ',0,'','L',false);
                $this->SetX(95);
                $this->Cell($this->debeDW+$this->saldoMes,4,number_format($acumulador[1],2,',','.'),0,'','R',false);
                $this->Cell($this->haberDW+$this->saldoActDW,4,number_format($acumulador[2],2,',','.'),0,'','R',false);
                $this->Ln(4);
                $this->SetFont('Courier','', 7);
            }elseif($this->tipo == 'GP' and $acumulador[4] != 0){
                $this->SetFont('Courier', 'B', 7);
                $this->Line(85, $this->GetY(), 200, $this->GetY());
                $this->SetX(25);
                $this->Cell($this->descCtaDW+$this->saldoAntDW,4, 'Total '.$descripcion.' ('.$cc.'): ',0,'','L',false);
                $this->SetX(95);
                $this->Cell($this->debeDW,4,number_format($acumulador[1],2,',','.'),0,'','R',false);
                $this->Cell($this->haberDW,4,number_format($acumulador[2],2,',','.'),0,'','R',false);
                $this->Cell($this->saldoActDW,4,number_format($acumulador[4],2,',','.'),0,'1','R',false);
                $this->Ln(2);
                $this->SetFont('Courier','', 7);
            }elseif($this->tipo == 'BG' and $acumulador[4] != 0){
                $this->SetFont('Courier', 'B', 7);
//                $this->Line(150, $this->GetY(), 200, $this->GetY());
                $this->SetX(25);
                $this->Cell($this->descCtaDW+$this->saldoAntDW+$this->debeDW,4, 'Total '.$descripcion.' ('.$cc.'): ',0,'','L',false);
                $this->SetX(125);
                $this->Cell($this->saldoActDW+$this->haberDW,4,number_format($acumulador[4],2,',','.'),0,'1','R',false);
                $this->Ln(2);
                $this->SetFont('Courier','', 7);
            }
        }
}


//CreaciÃ³n del objeto de la clase heredada
$pdf=new PDF();
$pdf->anio = $anio;
$pdf->mes = $mes;
$pdf->tipo = $tipo;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Courier','', 7);
$pdf->SetLeftMargin(10);
$pdf->SetFillColor(240);

if ($tipo == 'G')
{
	$pdf->SetFont('Courier', '', 10);
	$pdf->Cell(94, 4, "A C T I V O", 0, '', 'C');
	$pdf->Cell(94, 4, "P A S I V O", 0, '', 'C');
	$pdf->Ln(2);

	/*Inicio de Cuentas del Tesoro*/
	$pdf->SetFont('Courier', 'BU', 10);
	$pdf->Cell(188, 4, "CUENTAS DEL TESORO", 0, '', 'C');
	$pdf->SetFont('Courier', '', 7);
	$pdf->Ln(6);
	
	$posxAct = 10;
	$posyAct = $pdf->GetY();
	$posxPas = 106;
	$posyPas = $pdf->GetY();
	
	$totalAct = 0;
	$totalPas = 0;
	
	$tipoCta = substr($r->fields['codcta'], 0, 1);
	//Mientras el primer digito de la cuenta comience en 
	while ($tipoCta == substr($r->fields['codcta'], 0, 1) && !$r->EOF)
	{
		// Es una cuenta de activo
		if (substr($r->fields['codcta'], 0, 1) == '1')
		{
			$pdf->SetXY($posxAct, $posyAct);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalAct += $r->fields['saldo_act'];
			$posyAct += 4;
		}
		// Es una cuenta de pasivo
		else
		{
			$pdf->SetXY($posxPas, $posyPas);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalPas += $r->fields['saldo_act'];
			$posyPas += 4;
		}
		
		$pdf->Ln();
		$r->movenext();

	}
	
	if ($posyAct > $posyPas)
		$pdf->SetXY($posxAct, $posyAct);
	else
		$pdf->SetXY($posxAct, $posyPas);

	$pdf->Ln(1);
	$pdf->SetXY($posxAct, $pdf->GetY());
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "TOTAL Cuentas del Tesoro", T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalAct), T, '', 'R', 1);
	$pdf->Cell(4, 4, '');
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalPas), T, '', 'R', 1);
	
	if($anoCurso == 2007){
		$pdf->Ln();
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "Bs.F.:", T, '', 'R', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalAct/1000), T, '', 'R', 1);
		$pdf->Cell(4, 4, '');
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalPas/1000), T, '', 'R', 1);
	}
	
	$pdf->Ln(8);
	/*Fin de Cuentas del Tesoro*/
	
	/*Inicio de Cuentas de la Hacienda*/
	$pdf->SetFont('Courier', 'BU', 10);
	$pdf->Cell(188, 4, "CUENTAS DE LA HACIENDA", 0, '', 'C');
	$pdf->SetFont('Courier', '', 7);
	$pdf->Ln(6);
	
	$posyAct = $pdf->GetY();
	$posyPas = $pdf->GetY();
	
	$totalAct = 0;
	$totalPas = 0;
	
	$tipoCta = substr($r->fields['codcta'], 0, 1);
	while ($tipoCta == substr($r->fields['codcta'], 0, 1) && !$r->EOF)
	{
		// Es una cuenta de activo
		if (substr($r->fields['codcta'], 1, 1) == '1')
		{
			$pdf->SetXY($posxAct, $posyAct);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalAct += $r->fields['saldo_act'];
			$posyAct += 4;
		}
		// Es una cuenta de pasivo
		else
		{
			$pdf->SetXY($posxPas, $posyPas);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalPas += $r->fields['saldo_act'];
			$posyPas += 4;
		}

		$pdf->Ln();		
		$r->movenext();
	}

	if ($posyAct > $posyPas)
		$pdf->SetXY($posxAct, $posyAct);
	else
		$pdf->SetXY($posxAct, $posyPas);

	$pdf->Ln(1);
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "SUBTOTAL Cuentas de la Hacienda", T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalAct), T, '', 'R', 1);
	$pdf->Cell(4, 4, '');
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalPas), T, '', 'R', 1);
	
	if($anoCurso == 2007){
		$pdf->Ln();
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "Bs.F.:", T, '', 'R', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalAct/1000), T, '', 'R', 1);
		$pdf->Cell(4, 4, '');
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalPas/1000), T, '', 'R', 1);
	}
	
	$pdf->Ln(8);
	/*Fin de Cuentas de la Hacienda*/

	/*Inicio de Cuentas del Presupuesto*/
	$pdf->SetFont('Courier', 'BU', 10);
	$pdf->Cell(188, 4, "CUENTAS DEL PRESUPUESTO", 0, '', 'C');
	$pdf->SetFont('Courier', '', 7);
	$pdf->Ln(6);
	
	$posyAct = $pdf->GetY();
	$posyPas = $pdf->GetY();
	
	$totalActHac = $totalAct;
	$totalPasHac = $totalPas;
	$totalAct = 0;
	$totalPas = 0;
	
	$tipoCta = substr($r->fields['codcta'], 0, 1);
	while ($tipoCta == substr($r->fields['codcta'], 0, 1) && !$r->EOF)
	{
		// Es una cuenta de activo
		if (substr($r->fields['codcta'], 1, 1) == '4')
		{
			$pdf->SetXY($posxAct, $posyAct);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalAct += $r->fields['saldo_act'];
			$posyAct += 4;
		}
		// Es una cuenta de pasivo
		else
		{
			$pdf->SetXY($posxPas, $posyPas);
			$pdf->Cell($pdf->codCtaGW, 4, substr($r->fields['codcta'], 2, 3));
			$pdf->Cell($pdf->descCtaGW, 4, utf8_decode($r->fields['descripcion']));
			$pdf->Cell($pdf->saldoGW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R');
			
			$totalPas += $r->fields['saldo_act'];
			$posyPas += 4;
		}
		
		$pdf->Ln();
		$r->movenext();
	}
	
	if ($posyAct > $posyPas)
		$pdf->SetXY($posxAct, $posyAct);
	else
		$pdf->SetXY($posxAct, $posyPas);

	$pdf->Ln(1);
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "TOTAL Cuentas de la Hacienda", T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalActHac+$totalAct), T, '', 'R', 1);
	$pdf->Cell(4, 4, '');
	$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
	$pdf->Cell($pdf->saldoGW, 4, muestraFloat($totalPasHac+$totalPas), T, '', 'R', 1);
	
	if($anoCurso == 2007){
		$pdf->Ln();
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, "Bs.F.:", T, '', 'R', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat(($totalActHac+$totalAct)/1000), T, '', 'R', 1);
		$pdf->Cell(4, 4, '');
		$pdf->Cell($pdf->codCtaGW+$pdf->descCtaGW, 4, '', T, '', 'L', 1);
		$pdf->Cell($pdf->saldoGW, 4, muestraFloat(($totalPasHac+$totalPas)/1000), T, '', 'R', 1);
	}
	
	/*Fin de Cuentas del Presupuesto*/
}//Fin de balance de hacienda
/*
 *
 *
 *
 *
 *  Balance de Comprobacion
 *
 *
 *
 */

if ($tipo == 'D' or $tipo == 'L')
{
        $totalN1 = array();
        $desN1 = '';
        $N1Act = 0;
        $totalN2 = array();
        $desN2 = '';
        $N2Act = '';
        $totalN3 = array();
        $descN3 = '';
        $N3Act = '';
        $totalN4 = array();
        $descN4 = '';
        $N4Act = '';
        $totalN5 = array();
        $descN5 = '';
        $N5Act = '';
        $totalN6 = array();

        $total = '';
        $totalActual = 0;
		$totalDebe = 0;
		$totalHaber = 0;
        $totalAnterior = 0;
        $totalMes = 0;
/*	for ($j=0; $j<10; $j++)
	{*/
        
	while (!$r->EOF)
	{      
		$descCta = dividirStr(utf8_decode($r->fields['descripcion']), intval($pdf->descCtaDW/$pdf->GetStringWidth('M')));
	
		if ($r->fields['movim'] == 'N')
			$fill = 1;
		else
			$fill = 0;

                //Obtengo nivel 1
                if($N1Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 1){
                    //Cambio de 5 a 4
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        //$pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        //$pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de 4  3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        //$pdf->imp_total($N3Act,$desN3,$totalN3);
                        ini_total($totalN3);
                        $N3Act = 0;
                        $desN3 = '';
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       //$pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }
                    //
                    if($totalN1[0]+$totalN1[1] >= 0 and $N1Act != ''){
                        //$pdf->imp_total($N1Act,$desN1,$totalN1);
                    }
                    $N1Act = $r->fields['codcta'];
                    $desN1 = $descCta[0];
                    $totalAnterior  += $totalN1[0];
                    //$totalDebe      += $totalN1[1];
                    //$totalHaber     += $totalN1[2];
                    $totalMes       += $totalN1[3];
                    $totalActual    += $totalN1[4];
                    ini_total($totalN1);
                }
                
                if(strlen($r->fields['codcta']) == 14){
                    $totalDebe += $r->fields['debe'];
                    $totalHaber += $r->fields['haber'];
                }

                //Obtengo nivel 2
                if($N2Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 3){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        //$pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        //$pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       //$pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }
                    $N2Act = $r->fields['codcta'];
                    $desN2 = $descCta[0];
                    ini_total($totalN2);
                }
                    
                //Obtengo nivel 3
                if($N3Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 5){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        //$pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        //$pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    $N3Act = $r->fields['codcta'];
                    $desN3 = $descCta[0];
                    ini_total($totalN3);
                }
                //Obtengo nivel 4
                if($N4Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 7){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        //$pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        //$pdf->imp_total($N4Act,$desN4,$totalN4);
                    }
                    $N4Act = $r->fields['codcta'];
                    $desN4 = $descCta[0];
                    ini_total($totalN4);
                }
                //Obtengo nivel 5
                if($N5Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 9){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        //$pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                    }
                    $N5Act = $r->fields['codcta'];
                    $desN5 = $descCta[0];
                    ini_total($totalN5);
                }

                //Cuentas de movimientos para reporte detallado
                if(strlen($r->fields['codcta']) == 14){
                         $totalN6[0] += $r->fields['saldo_ant'];
                         $totalN6[1] += $r->fields['debe'];
                         $totalN6[2] += $r->fields['haber'];
                         $totalN6[3] += $r->fields['debe']-$r->fields['haber'];
                         $totalN6[4] += $r->fields['saldo_act'];
                }

                //Proceso de impresion de reporte
                $imprime = false;
                if($detallado == 'S'){
                    if($r->fields['movim'] == 'S'){
                        //if($r->fields['saldo_act'] != 0 and $tipo == 'L'){
                            $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', false);
                            $pdf->Cell($pdf->descCtaDW, 4, $descCta[0], 0, '', 'L', false);
                            $pdf->SetX(85);
                            $pdf->Cell($pdf->saldoAntDW, 4, muestraFloat($r->fields['saldo_ant']), 0, '', 'R', false);
                            $pdf->Cell($pdf->debeDW, 4, muestraFloat($r->fields['debe']), 0, '', 'R', false);
                            $pdf->Cell($pdf->haberDW, 4, muestraFloat($r->fields['haber']), 0, '', 'R', false);
                            //if($tipo == 'D')
                            $pdf->Cell($pdf->saldoMes, 4, muestraFloat($r->fields['debe']-$r->fields['haber']), 0, '', 'R', false);
                            $pdf->Cell($pdf->saldoActDW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R', false);
                            $imprime = true;
                        //}elseif($tipo == 'D'){
                    }else{
                            $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', true);
                            $pdf->Cell($pdf->descCtaDW, 4, $descCta[0], 0, '', 'L', true);
                            $pdf->SetX(85);
                            $pdf->Cell($pdf->saldoAntDW, 4, muestraFloat($r->fields['saldo_ant']), 0, '', 'R', true);
                            $pdf->Cell($pdf->debeDW, 4, '', 0, '', 'R', true);
                            $pdf->Cell($pdf->haberDW, 4, '', 0, '', 'R', true);
                            $pdf->Cell($pdf->saldoMes, 4, muestraFloat($r->fields['debe']-$r->fields['haber']), 0, '', 'R', true);
                            $pdf->Cell($pdf->saldoActDW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R', true);
                            $imprime = true;
                     }
                    //}
                }else{
                    if(strlen($r->fields['codcta']) != 14){
                        $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', false);
                        $pdf->Cell($pdf->descCtaDW, 4, $descCta[0], 0, '', 'L', false);
                        $pdf->SetX(85);
                        $pdf->Cell($pdf->saldoAntDW, 4, muestraFloat($r->fields['saldo_ant']), 0, '', 'R', false);
                        if(strlen($r->fields['codcta']) == 9 ){
                            $pdf->Cell($pdf->debeDW, 4, muestraFloat($r->fields['debe']), 0, '', 'R', false);
                            $pdf->Cell($pdf->haberDW, 4, muestraFloat($r->fields['haber']), 0, '', 'R', false);
                        }else{
                            $pdf->Cell($pdf->debeDW, 4, "", 0, '', 'R', false);
                            $pdf->Cell($pdf->haberDW, 4, "", 0, '', 'R', false);
                        }
                        //if($tipo == 'D')
                        $pdf->Cell($pdf->saldoMes, 4, muestraFloat($r->fields['debe']-$r->fields['haber']), 0, '', 'R', false);
                        $pdf->Cell($pdf->saldoActDW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R', false);
                        $imprime = true;
                        $fill = false;
                    }
                }

                
		
		$hayCta = next($descCta);
		for ($i=1; $hayCta!=false and $imprime==true; $i++)
		{
			$pdf->Ln();
			$pdf->Cell($pdf->codCtaDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->descCtaDW, 4, $descCta[$i], 0, '', 'L', $fill);
			$pdf->Cell($pdf->saldoAntDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->debeDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->haberDW, 4, '', 0, '', '', $fill);
                        $pdf->Cell($pdf->saldomes, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->saldoActDW, 4, '', 0, '', '', $fill);
			$hayCta = next($descCta);
		}

                if($imprime){
		  $pdf->Ln();
		  if ($fill == 1)
		     $pdf->Ln(0.7);
                }
		$r->movenext();
                
	}
}


/*
 *
 *
 *
 *
 *  Balance General
 *
 *
 *
 */

if ($tipo == 'BG')
{
        $totalN1 = array();
        $desN1 = '';
        $N1Act = 0;
        $totalN2 = array();
        $desN2 = '';
        $N2Act = '';
        $totalN3 = array();
        $descN3 = '';
        $N3Act = '';
        $totalN4 = array();
        $descN4 = '';
        $N4Act = '';
        $totalN5 = array();
        $descN5 = '';
        $N5Act = '';
        $totalN6 = array();

        $total = '';
        $totalActual = 0;
		$totalDebe = 0;
		$totalHaber = 0;
        $totalAnterior = 0;
        $totalMes = 0;
/*	for ($j=0; $j<10; $j++)
	{*/

	while (!$r->EOF)
	{
		$descCta = dividirStr(utf8_decode($r->fields['descripcion']), intval(($pdf->descCtaDW+$pdf->saldoAntDW+$pdf->debeDW)/$pdf->GetStringWidth('M')));

		if ($r->fields['movim'] == 'N')
			$fill = 1;
		else
			$fill = 0;

                //Obtengo nivel 1
                if($N1Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 1){
                    //Cambio de 5 a 4
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de 4  3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                        ini_total($totalN3);
                        $N3Act = 0;
                        $desN3 = '';
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }

                    if($totalN1[0]+$totalN1[1] >= 0 and $N1Act != ''){
                        $pdf->imp_total($N1Act,$desN1,$totalN1);
                    }
                    $N1Act = $r->fields['codcta'];
                    $desN1 = $descCta[0];
                    $totalAnterior  += $totalN1[0];
                    $totalDebe      += $totalN1[1];
                    $totalHaber     += $totalN1[2];
                    $totalMes       += $totalN1[3];
                    $totalActual    += $totalN1[4];
                    ini_total($totalN1);
                }
                //Obtengo nivel 2
                if($N2Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 3){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }
                    $N2Act = $r->fields['codcta'];
                    $desN2 = $descCta[0];
                    ini_total($totalN2);
                }

                //Obtengo nivel 3
                if($N3Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 5){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    $N3Act = $r->fields['codcta'];
                    $desN3 = $descCta[0];
                    ini_total($totalN3);
                }
                //Obtengo nivel 4
                if($N4Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 7){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                    }
                    $N4Act = $r->fields['codcta'];

                    $desN4 = $descCta[0];
                    ini_total($totalN4);
                }
                //Obtengo nivel 5
                if($N5Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 9){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                    }
                    $N5Act = $r->fields['codcta'];
                    $desN5 = $descCta[0];
                    ini_total($totalN5);
                }

                //Cuentas de movimientos para reporte detallado
                if(strlen($r->fields['codcta']) == 14){
                         $totalN6[0] += $r->fields['saldo_ant'];
                         $totalN6[1] += $r->fields['debe'];
                         $totalN6[2] += $r->fields['haber'];
                         $totalN6[3] += $r->fields['debe']-$r->fields['haber'];
                         $totalN6[4] += $r->fields['saldo_act'];
                }

                //Proceso de impresion de reporte
                $imprime = false;
                    //if($r->fields['movim'] == 'N'){
                        if($r->fields['saldo_act'] != 0){
                            $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', 0);
                            $pdf->Cell($pdf->descCtaDW+$pdf->saldoAntDW+$pdf->debeDW, 4, $descCta[0], 0, '', 'L', 0);
                            $pdf->SetX(125);
                            $pdf->Cell($pdf->saldoActDW+$pdf->haberDW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R', 0);
                            $imprime = true;
                        }
                    //}

		$hayCta = next($descCta);
		for ($i=1; $hayCta!=false and $imprime==true; $i++)
		{
			$pdf->Ln();
			$pdf->Cell($pdf->codCtaDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->descCtaDW+$pdf->saldoAntDW+$pdf->debeDW, 4, $descCta[$i], 0, '', '', 0);
			$pdf->Cell($pdf->saldoActDW+$pdf->haberDW, 4, '', 0, '', '', 0);
			$hayCta = next($descCta);
		}

                if($imprime){
		  $pdf->Ln();
		  if ($fill == 1)
		     $pdf->Ln(0.7);
                }
		$r->movenext();

	}
} //Fin Balance General

/*
 *
 *
 *
 *  Estado de Ganancias y Perdidas
 *
 *
 *
 */

if ($tipo == 'GP')
{
        $totalN1 = array();
        $desN1 = '';
        $N1Act = 0;
        $totalN2 = array();
        $desN2 = '';
        $N2Act = '';
        $totalN3 = array();
        $descN3 = '';
        $N3Act = '';
        $totalN4 = array();
        $descN4 = '';
        $N4Act = '';
        $totalN5 = array();
        $descN5 = '';
        $N5Act = '';
        $totalN6 = array();

        $total = '';
        $totalActual = 0;
		$totalDebe = 0;
		$totalHaber = 0;
        $totalAnterior = 0;
        $totalMes = 0;
        $GanPer = 0;
/*	for ($j=0; $j<10; $j++)
	{*/

	while (!$r->EOF)
	{
		$descCta = dividirStr(utf8_decode($r->fields['descripcion']), intval($pdf->descCtaDW/$pdf->GetStringWidth('M')));

		if ($r->fields['movim'] == 'N')
			$fill = 1;
		else
			$fill = 0;

                //Obtengo nivel 1
                if($N1Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 1){
                    //Cambio de 5 a 4
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de 4  3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                        ini_total($totalN3);
                        $N3Act = 0;
                        $desN3 = '';
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }

                    if($totalN1[0]+$totalN1[1] >= 0 and $N1Act != ''){
                        $pdf->imp_total($N1Act,$desN1,$totalN1);
                    }
                    $N1Act = $r->fields['codcta'];
                    $desN1 = $descCta[0];
                    $totalAnterior  += $totalN1[0];
                    $totalDebe      += $totalN1[1];
                    $totalHaber     += $totalN1[2];
                    $totalMes       += $totalN1[3];
                    $totalActual    += $totalN1[4];
                    ini_total($totalN1);
                }
                //Obtengo nivel 2
                if($N2Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 3){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }
                    $N2Act = $r->fields['codcta'];
                    $desN2 = $descCta[0];
                    ini_total($totalN2);
                }

                //Obtengo nivel 3
                if($N3Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 5){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    $N3Act = $r->fields['codcta'];
                    $desN3 = $descCta[0];
                    ini_total($totalN3);
                }
                //Obtengo nivel 4
                if($N4Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 7){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                    }
                    $N4Act = $r->fields['codcta'];
                    $desN4 = $descCta[0];
                    ini_total($totalN4);
                }
                //Obtengo nivel 5
                if($N5Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 9){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                    }
                    $N5Act = $r->fields['codcta'];
                    $desN5 = $descCta[0];
                    ini_total($totalN5);
                }

                //Cuentas de movimientos para reporte detallado
                if(strlen($r->fields['codcta']) == 14){
                         $totalN6[0] += $r->fields['saldo_ant'];
                         $totalN6[1] += $r->fields['debe'];
                         $totalN6[2] += $r->fields['haber'];
                         $totalN6[3] += $r->fields['debe']-$r->fields['haber'];
                         $totalN6[4] += $r->fields['saldo_act'];
                }

                //Proceso de impresion de reporte
                $imprime = false;
                    if($r->fields['movim'] == 'N'){
                        if($r->fields['saldo_act'] != 0){
                            $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', 0);
                            $pdf->Cell($pdf->descCtaDW+$pdf->saldoAntDW, 4, $descCta[0], 0, '', 'L', 0);
                            $pdf->SetX(105);
                            $pdf->Cell($pdf->debeDW, 4, muestraFloat($r->fields['debe']), 0, '', 'R', 0);
                            $pdf->Cell($pdf->haberDW, 4, muestraFloat($r->fields['haber']), 0, '', 'R', 0);
                            $pdf->Cell($pdf->saldoActDW, 4, muestraFloat($r->fields['saldo_act']), 0, '', 'R', 0);
                            $imprime = true;
                            $GanPer += $r->fields['saldo_act'];
                        }
                    }


		$hayCta = next($descCta);
		for ($i=1; $hayCta!=false and $imprime==true; $i++)
		{
			$pdf->Ln();
			$pdf->Cell($pdf->codCtaDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->descCtaDW, 4, $descCta[$i], 0, '', '', $fill);
			$pdf->Cell($pdf->debeDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->haberDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->saldoActDW, 4, '', 0, '', '', $fill);
			$hayCta = next($descCta);
		}

                if($imprime){
		  $pdf->Ln();
		  if ($fill == 1)
		     $pdf->Ln(0.7);
                }
		$r->movenext();

	}
} //Fin de Ganancias y Perdidas



        /*
         *
         * Diario Legal
         *
         */

        if ($tipo == 'DL')
        {
        $totalN1 = array();
        $desN1 = '';
        $N1Act = 0;
        $totalN2 = array();
        $desN2 = '';
        $N2Act = '';
        $totalN3 = array();
        $descN3 = '';
        $N3Act = '';
        $totalN4 = array();
        $descN4 = '';
        $N4Act = '';
        $totalN5 = array();
        $descN5 = '';
        $N5Act = '';
        $totalN6 = array();

        $total = '';
        $totalActual = 0;
	$totalDebe = 0;
	$totalHaber = 0;
        $totalAnterior = 0;
        $totalMes = 0;
/*	for ($j=0; $j<10; $j++)
	{*/

	while (!$r->EOF)
	{
		$descCta = dividirStr(utf8_decode($r->fields['descripcion']), intval(($pdf->descCtaDW+$pdf->saldoAntDW)/$pdf->GetStringWidth('M')));

		if ($r->fields['movim'] == 'N')
			$fill = 1;
		else
			$fill = 0;

                //Obtengo nivel 1
                if($N1Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 1){
                    //Cambio de 5 a 4
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de 4  3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                        ini_total($totalN3);
                        $N3Act = 0;
                        $desN3 = '';
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }

                    if($totalN1[0]+$totalN1[1] >= 0 and $N1Act != ''){
                        $pdf->imp_total($N1Act,$desN1,$totalN1);
                    }
                    $N1Act = $r->fields['codcta'];
                    $desN1 = $descCta[0];
                    $totalAnterior  += $totalN1[0];
                    $totalDebe      += $totalN1[1];
                    $totalHaber     += $totalN1[2];
                    $totalMes       += $totalN1[3];

                    $totalActual    += $totalN1[4];
                    ini_total($totalN1);
                }
                //Obtengo nivel 2
                if($N2Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 3){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    //Cambio de nivel de 2 a 1
                    if(sum_cc($totalN2) >= 0 and $N2Act != ''){
                       sum_total($totalN1,$totalN2);
                       $pdf->imp_total($N2Act,$desN2,$totalN2);
                       ini_total($totalN2);
                       $N2Act = 0;
                       $desN2 = '';
                    }
                    $N2Act = $r->fields['codcta'];
                    $desN2 = $descCta[0];
                    ini_total($totalN2);
                }

                //Obtengo nivel 3
                if($N3Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 5){
                    //Cambio de nivel 4 a 2
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                        ini_total($totalN4);
                        $N4Act = 0;
                        $desN4 = '';
                    }
                    //Cambio de nivel 4 a 3
                    if(sum_cc($totalN3) >= 0 and $N3Act != ''){
                        sum_total($totalN2,$totalN3);
                        $pdf->imp_total($N3Act,$desN3,$totalN3);
                    }
                    $N3Act = $r->fields['codcta'];
                    $desN3 = $descCta[0];
                    ini_total($totalN3);
                }
                //Obtengo nivel 4
                if($N4Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 7){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                        $N5Act = 0;
                        $desN5 = '';
                    }
                    if(sum_cc($totalN4) >= 0 and $N4Act != ''){
                        sum_total($totalN3,$totalN4);
                        $pdf->imp_total($N4Act,$desN4,$totalN4);
                    }
                    $N4Act = $r->fields['codcta'];
                    $desN4 = $descCta[0];
                    ini_total($totalN4);
                }
                //Obtengo nivel 5
                if($N5Act != $r->fields['codcta'] and strlen($r->fields['codcta']) == 9){
                    if(sum_cc($totalN6) >= 0 and $N5Act != ''){
                        sum_total($totalN5,$totalN6);
                        sum_total($totalN4,$totalN5);
                        $pdf->imp_total($N5Act,$desN5,$totalN5);
                        ini_total($totalN6);
                    }
                    $N5Act = $r->fields['codcta'];
                    $desN5 = $descCta[0];
                    ini_total($totalN5);
                }

                //Cuentas de movimientos para reporte detallado
                if(strlen($r->fields['codcta']) == 14){
                         $totalN6[0] += $r->fields['saldo_ant'];
                         $totalN6[1] += $r->fields['debe'];
                         $totalN6[2] += $r->fields['haber'];
                         $totalN6[3] += $r->fields['debe']-$r->fields['haber'];
                         $totalN6[4] += $r->fields['saldo_act'];
                }

                //Proceso de impresion de reporte
                $imprime = false;
                if($detallado == 'N'){
                        if($r->fields['debe'] + $r->fields['haber'] != 0){
                            $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', true);
                            $pdf->Cell($pdf->descCtaDW+$pdf->saldoAntDW, 4, $descCta[0], 0, '', 'L', true);
                            $pdf->SetX(95);
                            $pdf->Cell($pdf->debeDW+$pdf->saldoMes, 4, muestraFloat($r->fields['debe']), 0, '', 'R', false);
                            $pdf->Cell($pdf->haberDW+$pdf->saldoActDW, 4, muestraFloat($r->fields['haber']), 0, '', 'R', false);
                            $imprime = true;
                        }
                }else{
                    $pdf->Cell($pdf->codCtaDW, 4, $r->fields['codcta'], 0, '', 'L', false);
                    $pdf->Cell($pdf->descCtaDW+$pdf->saldoAntDW, 4, $descCta[0], 0, '', 'L', false);
                    $pdf->SetX(95);
                    $pdf->Cell($pdf->debeDW+$pdf->saldoMes, 4, muestraFloat($r->fields['debe']), 0, '', 'R', false);
                    $pdf->Cell($pdf->haberDW+$pdf->saldoActDW, 4, muestraFloat($r->fields['haber']), 0, '', 'R', false);
                    $imprime = true;
                }



		$hayCta = next($descCta);
		for ($i=1; $hayCta!=false and $imprime==true; $i++)
		{
			$pdf->Ln();
			$pdf->Cell($pdf->codCtaDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->descCtaDW, 4, $descCta[$i], 0, '', '', $fill);
			$pdf->Cell($pdf->debeDW, 4, '', 0, '', '', $fill);
			$pdf->Cell($pdf->haberDW, 4, '', 0, '', '', $fill);
			$hayCta = next($descCta);
		}

                if($imprime){
		  $pdf->Ln();
		  if ($fill == 1)
		     $pdf->Ln(0.7);
                }
		$r->movenext();

	}

    }   //Fin de Diario Legal

	/*
         *
         * Seccion de Totales
         *
         */
	
	$pdf->Ln(1);
	$pdf->SetFont('Courier', 'B', 7);
        if($tipo == 'D')
            $pdf->Cell($pdf->codCtaDW+$pdf->descCtaDW, 4, "TOTAL BALANCE DE COMPROBACION", T, '', 'C', 1);
        elseif($tipo == 'DL')
            $pdf->Cell($pdf->codCtaDW+$pdf->descCtaDW*2, 4, "TOTAL DIARIO LEGAL", T, '', 'C', 0);
        elseif($tipo == 'GP')
            $pdf->Cell($pdf->codCtaDW+$pdf->descCtaDW*2, 4, "UTILIDAD DEL EJERCICIO", T, '', 'C', 0);
        else
            $pdf->Cell($pdf->codCtaDW+$pdf->descCtaDW, 4, "TOTAL ", T, '', 'C', 1);

        if($tipo != 'DL' and $tipo != 'GP' and $tipo != 'BG'){
            $pdf->Cell($pdf->saldoAntDW, 4, muestraFloat($totalAnterior), T, '', 'R', 0);
            $pdf->Cell($pdf->debeDW, 4, muestraFloat($totalDebe), T, '', 'R', 0);
            $pdf->Cell($pdf->haberDW, 4, muestraFloat($totalHaber), T, '', 'R', 0);
            if($tipo == 'D')
                $pdf->Cell($pdf->saldoMes, 4, muestraFloat($totalMes), T, '', 'R', 0);
            $pdf->Cell($pdf->saldoActDW, 4, muestraFloat($totalActual), T, '', 'R', 0);
        }elseif($tipo == 'GP'){
            $pdf->SetX(105);
            $pdf->Cell($pdf->saldoActDW+$pdf->debeDW+$pdf->haberDW, 4, muestraFloat($GanPer), T, '', 'R', 0);
        }elseif($tipo == 'BG'){
            $pdf->SetX(85);
            $pdf->Cell($pdf->saldoActDW+$pdf->debeDW+$pdf->haberDW+10, 4, muestraFloat($totalActual), T, '', 'R', 0);
        }else{
            $pdf->SetX(95);
            $pdf->Cell($pdf->debeDW+$pdf->saldoMes, 4, muestraFloat($totalDebe), T, '', 'R', 0);
            $pdf->Cell($pdf->haberDW+$pdf->saldoActDW, 4, muestraFloat($totalHaber), T, '', 'R', 0);
        }

	
	if ($totalDebe != $totalHaber and $tipo == 'D')
	{
		$pdf->Ln(6);
		$pdf->Cell(188, 4, "BALANCE DESCUADRADO", 0, '', 'C');
	}
        




function ini_total(&$var_total){
    for($i=0;$i<=4;$i++)
      $var_total[$i]=0;
}

//Suma valores del arreglo de linea
function sum_cc($cc_act){
    $total_linea = 0;
    for($i=0;$i<=4;$i++)
     $total_linea += $cc_act[$i];
    return  $total_linea;
}

//Total_sum es la variable donde acumulo o sumo valor de total
function sum_total(&$total_sum,$total){
    for($i=0;$i<=4;$i++)
       $total_sum[$i] += $total[$i];
}

$pdf->Output();
?>
